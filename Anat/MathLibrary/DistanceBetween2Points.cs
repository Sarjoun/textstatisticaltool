﻿using System;
using System.Linq;

namespace MathLibrary
{
    public static class DistanceBetween2Points
    {
        public static float Calculate(float[] pointA, float[] pointB)
        {
            var total = pointA
                        .Select(
                            (t, i) => pointB[i] - t)
                        .Sum(diff => diff * diff);

            return (float)Math.Sqrt(total);


            //var total = pointA.Select((t, i) => pointB[i] - t).Sum(diff => diff * diff);
            //return (float)Math.Sqrt(total);

        }

    }
}
