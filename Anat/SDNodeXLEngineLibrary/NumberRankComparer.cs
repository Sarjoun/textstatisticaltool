﻿using System;
using System.Collections.Generic;

namespace SDNodeXLEngineLibrary
{
    //http://stackoverflow.com/questions/1606679/remove-duplicates-in-the-list-using-linq
    //var distinctItems = items.Distinct(new DistinctItemComparer());
    class NumberRankComparer : IEqualityComparer<NumberRank>
    {

        public bool Equals(NumberRank x, NumberRank y)
        {
            return Math.Abs(x.Number - y.Number) < 0.00000000000000000001 &&
                x.Rank == y.Rank;
        }

        public int GetHashCode(NumberRank obj)
        {
            return obj.Number.GetHashCode() ^
                obj.Rank.GetHashCode();
        }
    }
}
