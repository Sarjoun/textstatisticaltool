﻿using System;
using System.Collections.Generic;
using Smrf.NodeXL.Algorithms;
using Smrf.NodeXL.Core;

namespace SDNodeXLEngineLibrary
{
    interface ISDNodeXLEngine
    {
        /* Initialization functions */
        void CreateAndPopulateGraph(bool directedGraph);
        void RandomizeCreateAndPopulateGraph(bool directedGraph);
        bool CheckIfVerticesHaveEdge(int vertex1Id, int vertex2Id);
        IVertex GetRandomVertexInRange(int minId, int maxId);
        void RunAllNetworkMetricsCalculations();
        void RunAllNetworkMetricsCalculations(int runId);
        bool WriteOutAllNodesStatisticsToOutputFile();
        bool WriteOutAllNodesStatisticsToOutputFileWithEmptyVocabValues();//For Network reduction and Barabasi metrics

        //bool WriteOutGeneralGraphStatisticsToOutputFile(ClusterAlgorithm clusterAlgorithm);
        bool WriteOutGeneralGraphStatisticsToOutputFile(string clusterAlgorithmName);


        /* Graph properties */
        Graph Graph { get; set; }
        string GraphName { get; set; }
        string SourceFileUri { get; set; }
        string NodeOutputResultFileUri { get; set; }
        string GraphOutputResultFileUri { get; set; }
        DateTime GraphDate { get; set; }
        int DisconnectedEdges { get; set; }
        int ConnectedEdges { get; set; }
        int NumberOfVertices { get; set; }
        string[][] Matrix { get; set; }    //Represents the matrix as a double array of string.
        double GraphDegree { get; set; }

        /* File Operations */
        bool LoadMatrixFile();//Short efficient code but can run into memory problems.
        bool LoadMatrixFileLargeFiles(); //Longer code that can handle large files.
        bool LoadMatrixFile(string fileUri, bool hasHeaders, bool negativeToZero, bool roundToZeroOrOne);

        /* Network statistics */
        List<double> CalculateVertexDegree();
        List<double> CalculateBetweennessCentrality();
        List<double> CalculateClosenessCentrality();
        List<double> CalculateClusteringCoefficient();
        double CalculateGraphModularity(ClusterAlgorithm clusterAlgorithm);

        /* Network operations */
        bool DeleteVertex(int vertexId);
        bool DeleteVertex(IVertex vertex);
        bool DeleteVerticesWithDegree(int vertexId);
        bool SetUpBarabasi { get; set; }//to control extra metrics and ignore ImportDictionary (and still add the 
        //empty columns for Frequency and Marginal Probability) so not to mess up the ordering in the Excel generating
        //program (and avoid lengthy adjustments)
    }
}
