﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smrf.NodeXL.Algorithms;
using Smrf.NodeXL.Core;

namespace SDNodeXLEngineLibrary
{
    public class Modularity
    {
        public ClusterAlgorithm ClusteringAlgorithm { get; set; }
        public int CommunitiesCount { get; set; }
        public ICollection<Community> Communities { get; set; }
        public double ModularityValue { get; set; }
        public List<IVertex> Vertices { get; set; }
        public IVertexCollection VerticesCollection { get; set; }
        public IEdgeCollection EdgesCollection { get; set; }

        private const string SnapCalculatorLocation = @"..\..\..\SDNodeXLEngineLibrary\CalculatorFolder\SnapGraphMetricCalculator.exe";
        public string[][] Matrix { get; set; }

        private int NumberOfEdges { get; set; }

        public Modularity(ClusterAlgorithm clusterAlgorithm)
        {
            ClusteringAlgorithm = clusterAlgorithm;
            Count = 0;
        }

        private int Count { get; set; }

        public Modularity(ClusterAlgorithm clusterAlgorithm, Graph graph, int numberOfNumberOfEdges, int startingValueForVertexID)
        {
            ClusteringAlgorithm = clusterAlgorithm;
            NumberOfEdges = numberOfNumberOfEdges;
            Count = startingValueForVertexID;
            TryCalculateModularity(graph);
        }

        public bool TryCalculateModularity(Graph graph)
        {
            try
            {
                ClusterCalculator oClusterCalculator = new ClusterCalculator { Algorithm = ClusteringAlgorithm };
                GraphMetricCalculatorBase.SetSnapGraphMetricCalculatorPath(SnapCalculatorLocation);
                Communities = oClusterCalculator.CalculateGraphMetrics(graph);
                CommunitiesCount = Communities.Count;
                VerticesCollection = graph.Vertices;
                Vertices = VerticesCollection.ToList();

                EdgesCollection = graph.Edges;

                ModularityValue = CalculateQ();
                return true;

            }
            catch (Exception c)
            {
                return false;
            }
        }


        /// <summary>
        /// This is the function that calculates the modularity value based on Newman's paper.
        /// Modularity is represented by Q.
        /// </summary>
        /// <returns></returns>
        private double CalculateQ()
        {
            double m = GetWeightOfNetwork();
            double part1 = 1 / (m * 2);

            var total = Vertices.Count + Count;
            double modularity = 0;

            //for (int i = Count + 1; i <= total; i++)
            //{
            //    for (int j = Count + 2; j <= total; j++)
            for (int i = Count; i <= total-1; i++)
            {
                for (int j = Count + 1; j <= total-1; j++)

                {
                    if (Delta(i, j) > 0)
                    {
                        var a_i_j = GetAdjacencyOf2Vertices(i, j);
                        var k_i = GetDegreeOfVertex(i);
                        var k_j = GetDegreeOfVertex(j);
                        var K = (k_i * k_j) / (2 * m);
                        modularity += a_i_j - K;
                    }
                }
            }
            modularity = part1 * modularity;

            return modularity;
        }

        public double GetWeightOfNetwork()
        {
            var connections = EdgesCollection.ToList();
            double val = 0;
            foreach (var connect in connections)
            {
                val += (float)connect.GetValue(ReservedMetadataKeys.PerEdgeWidth);
            }
            return val;
        }

        private double GetDegreeOfVertex(int id)
        {
            IVertex v1 = GetVertex(id);

            var connections = v1.IncidentEdges.ToList();
            double val = 0;
            foreach (var connect in connections)
            {
                val += (float)connect.GetValue(ReservedMetadataKeys.PerEdgeWidth);
            }
            return val;
        }

        private double GetAdjacencyOf2Vertices(int id1, int id2)
        {
            // Get community 

            IVertex v1 = GetVertex(id1);
            IVertex v2 = GetVertex(id2);

            IEdge edge;
            var connections = EdgesCollection.GetConnectingEdges(v1, v2).ToList();
            double val = 0;
            foreach (var connect in connections)
            {
                val += (float)connect.GetValue(ReservedMetadataKeys.PerEdgeWidth);
            }

            //var adjacency =
            //    v1.IncidentEdges.FirstOrDefault(v => v.Vertex1.ID == id1 && v.Vertex2.ID == id2)
            //        .GetValue(ReservedMetadataKeys.EdgeWeight);

            return val;
            //vertex.GetValue(ReservedMetadataKeys.PerVertexLabel)
        }
                  

        private IVertex GetVertex(int vertexId)
        {
            //return Vertices.FirstOrDefault(v => v.ID == vertexId);
            IVertex vertex;
            if (VerticesCollection.Find(vertexId, out vertex))
                return vertex;
            else
            {
                return null;
            }
        }

        /// <summary>
        /// This is the delta(ci, cj) functions that returns the value 1 when ci and cj are in the same
        /// community/cluster/group, otherwise returns 0.
        /// </summary>
        /// <param name="ci">First vertex/node/word</param>
        /// <param name="cj">Second vertex/node/word</param>
        /// <returns></returns>
        private int Delta(int ci, int cj)
        {
            IVertex vertex1 = GetVertex(ci);
            IVertex vertex2 = GetVertex(cj);
            if ((vertex1 == null) || (vertex2 == null))
                return 0;
            return GetCommunityIdOfVertex(vertex1) == GetCommunityIdOfVertex(vertex2) ? 1 : 0;
        }


        /// <summary>
        /// This function gets the Id of the community (or cluster) that a vertex (or node)
        /// belongs to. This is used by the delta(ci, cj) function to determine the value of 1 or 0.
        /// </summary>
        /// <param name="vertex"></param>
        /// <returns></returns>
        private int GetCommunityIdOfVertex(IVertex vertex)
        {
            var community = Communities.FirstOrDefault(c => c.Vertices.Contains(vertex));
            if (community != null)
                return community.ID;
            return -1;
        }
    }
}



//            for(var i = 0; i<oCommunities.Count; i++)
//            {
//                var communityId = i + 1;
//var communityNodeNames = "";

//var s = oCommunities.ToArray();

////List<int, CommunityPair> memberNodes = ((Community)oCommunities[i]).CommunityPairs;//not working

//var memberVertices = s[i].Vertices.ToList();

////textBox_NodeXL.Text += @"Community " + communityId + @" [" + memberVertices.Count + @"v] : ";
//textBox_NodeXL.Text += @"Community " + s[i].ID.ToString() + @" [" + memberVertices.Count + @"v] : ";

//                foreach (var member in memberVertices)
//                {
//                    communityNodeNames += member.ID + ", ";
//                }
//                communityNodeNames = communityNodeNames.TrimEnd('?', '.', ',', ' ');
//                //Community community = oCommunities.FirstOrDefault(x => x.ID == i);                
//                //foreach(var member in community.CommunityPairs)
//                //{
//                //    communityNodeNames += member.Key.ToString() + " ";
//                //}

//                //textBox_NodeXL.Text += @"vertices[" + memberVertices.Count + @"] "+ communityNodeNames + ".\r\n";
//                textBox_NodeXL.Text += communityNodeNames + ".\r\n";
//            }

//Find out what is MaximumDeltaQ?

//float QValue = 0;
//foreach (var community in oCommunities)
//{
//    QValue += community.MaximumDeltaQ;
//}
//tw.WriteLine("Total Graph modularity = " + QValue);

//tw.WriteLine("Average Graph modularity = " + QValue/oCommunities.Count);
