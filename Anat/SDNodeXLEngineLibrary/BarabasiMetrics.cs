﻿using System;
using System.Collections.Generic;

namespace SDNodeXLEngineLibrary
{

    public class BarabasiSingleRunResults
    {
        public int RunId { get; set; }
        public int NumberOfAllComponents { get; set; }
        public int SizeOfCurrentLargestComponent { get; set; }
        public int SizeOfPreviousLargestComponent { get; set; }
        public int SizeOfOriginalLargestComponent { get; set; }
        public int SumOfAllSizesOfComponents { get; set; }

        public int SumOfAllSizesOfComponentsMinusLargestComponent { get; set; }
        public double MeanSizeOfAllComponentsMinusLargestComponent { get; set; }
        public double RelativeSizeOfLargestComponent { get; set; }

        //public List<int> ListOfAllComponents { get; set; }                       

        //Generic ctor
        public BarabasiSingleRunResults()
        {}

        //public BarabasiSingleRunResults(int runId, int numberOfAllComponents, int sizeOfCurrentLargestComponent,
        //    int sumOfAllSizesOfComponents)
        //{
        //    RunId = runId;
        //    NumberOfAllComponents = numberOfAllComponents;
        //    SizeOfCurrentLargestComponent = sizeOfCurrentLargestComponent;
        //    SumOfAllSizesOfComponents = sumOfAllSizesOfComponents;
        //}

        public BarabasiSingleRunResults(int runId, int numberOfAllComponents, int sizeOfCurrentLargestComponent,
            int sumOfAllSizesOfComponents, int sizeOfOriginalLargestComponent, int sizeOfPreviousLargestComponent)
        {
            RunId = runId;
            NumberOfAllComponents = numberOfAllComponents;
            SizeOfCurrentLargestComponent = sizeOfCurrentLargestComponent;
            SumOfAllSizesOfComponents = sumOfAllSizesOfComponents;
            SizeOfOriginalLargestComponent = sizeOfOriginalLargestComponent;
            SizeOfPreviousLargestComponent = sizeOfPreviousLargestComponent;
            CalculateMeanSizeOfAllComponentsMinusLargest();
            CalculateRelativeSizeOfLargestComponentToOriginal();
            //CalculateRelativeSizeOfLargestComponentToThePreviousLargestComponent();
        }

        public BarabasiSingleRunResults(int runId, int numberOfAllComponents, int sizeOfCurrentLargestComponent,
    int sumOfAllSizesOfComponents, int sizeOfOriginalLargestComponent)
        {
            RunId = runId;
            NumberOfAllComponents = numberOfAllComponents;
            SizeOfCurrentLargestComponent = sizeOfCurrentLargestComponent;
            SumOfAllSizesOfComponents = sumOfAllSizesOfComponents;
            SizeOfOriginalLargestComponent = sizeOfOriginalLargestComponent;
            CalculateMeanSizeOfAllComponentsMinusLargest();
            CalculateRelativeSizeOfLargestComponentToOriginal();
            //CalculateRelativeSizeOfLargestComponentToThePreviousLargestComponent();
        }


        public double CalculateMeanSizeOfAllComponentsMinusLargest()
        {
            SumOfAllSizesOfComponentsMinusLargestComponent = SumOfAllSizesOfComponents - SizeOfCurrentLargestComponent;
            if (SumOfAllSizesOfComponentsMinusLargestComponent == 0)
                return 0;

            MeanSizeOfAllComponentsMinusLargestComponent = Convert.ToDouble(SumOfAllSizesOfComponentsMinusLargestComponent) /
                                                           Convert.ToDouble(NumberOfAllComponents - 1);
            return MeanSizeOfAllComponentsMinusLargestComponent;
        }

        public double CalculateRelativeSizeOfLargestComponentToOriginal()
        {
            //Check if it is the first run first
            //if (SizeOfOriginalLargestComponent == -1)
            //    return 1;
            if (RunId == 1)
            {
                SizeOfOriginalLargestComponent = SizeOfCurrentLargestComponent;
                return RunId;
            }

            return
                RelativeSizeOfLargestComponent =
                    Convert.ToDouble(SizeOfCurrentLargestComponent) / Convert.ToDouble(SizeOfOriginalLargestComponent);
        }

        public double CalculateRelativeSizeOfLargestComponentToThePreviousLargestComponent()
        {
            //Check if it is the first run first
            if (SizeOfPreviousLargestComponent == -1)
                return 1;

            //SizeOfPreviousLargestComponent = SizeOfCurrentLargestComponent;
            return
                RelativeSizeOfLargestComponent =
                    Convert.ToDouble(SizeOfCurrentLargestComponent) / Convert.ToDouble(SizeOfPreviousLargestComponent);
        }
    }

    public class BarabasiMetrics
    {

        //Values that are written to the file
        public double CalculatedRelativeSizeOfLargestComponent { get; set; }
        public double CalculatedMeanSizeOfAllComponentsMinusLargest { get; set; }
        public int CurrentSizeOfCurrentLargestComponent { get; set; }
        public int OriginalSizeOfLargestComponent { get; set; }
        public int NumberOfAllComponents { get; set; }

        /// <summary>
        /// This property would return the size of the current largest component so that it can be used
        /// in the calculation of the ssubsequent RelativeSizeOfLargestComponent of the next network reduction iterations.
        /// It is defaulted at -1 and then the value is updated.
        /// </summary>

        public int NumberOfRuns { get; set; }

        //public bool BeingUsed { get; set; }

        public List<BarabasiSingleRunResults> ListOfRuns { get; set; }


        public BarabasiMetrics()
        {
            
            //BeingUsed = false;//default value
            OriginalSizeOfLargestComponent = -1;
            //CurrentSizeOfCurrentLargestComponent = -1;
            ListOfRuns = new List<BarabasiSingleRunResults>();
        }


        public void Reset()
        {
            ListOfRuns = new List<BarabasiSingleRunResults>();
        }

        public void AddNewEntry(BarabasiSingleRunResults barabasiSingleRunResults)
        {
            barabasiSingleRunResults.SizeOfPreviousLargestComponent = CurrentSizeOfCurrentLargestComponent;
            
            #region Two ways to calculate the relative size of the largest component
            //CalculatedRelativeSizeOfLargestComponent =  barabasiSingleRunResults.CalculateRelativeSizeOfLargestComponentToThePreviousLargestComponent();
            CalculatedRelativeSizeOfLargestComponent = barabasiSingleRunResults.CalculateRelativeSizeOfLargestComponentToOriginal();
            #endregion

            CalculatedMeanSizeOfAllComponentsMinusLargest = barabasiSingleRunResults.CalculateMeanSizeOfAllComponentsMinusLargest();

            NumberOfAllComponents = barabasiSingleRunResults.NumberOfAllComponents;
            CurrentSizeOfCurrentLargestComponent = barabasiSingleRunResults.SizeOfCurrentLargestComponent;
            OriginalSizeOfLargestComponent = barabasiSingleRunResults.SizeOfOriginalLargestComponent;

            ListOfRuns.Add(barabasiSingleRunResults);
            CurrentSizeOfCurrentLargestComponent = barabasiSingleRunResults.SizeOfCurrentLargestComponent;
        }
    }
}
