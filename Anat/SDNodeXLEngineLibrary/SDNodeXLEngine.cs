﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Smrf.NodeXL.Algorithms;
using Smrf.NodeXL.Core;
using Smrf.NodeXL.Visualization.Wpf;

/* Because NodeXL does not recycle the IDs of the Vertices. (Even creating a new object does not reset it,
 it is attached to the duration of the whole program, until I figure out more about it).
 It means that everytime I create a new network, the nodes IDs start counting from there  */


namespace SDNodeXLEngineLibrary
{
    public class SDNodeXLEngine : ISDNodeXLEngine
    {
        private readonly NodeXLControl _nodeXLControl;
        private const string Precision = "{0:0.00000000}"; //"{0:0.00000}";
        //private const string SnapCalculatorLocation = @"..\..\CalculatorFolder\SnapGraphMetricCalculator.exe";

        private const string SnapCalculatorLocation = @"..\..\..\SDNodeXLEngineLibrary\CalculatorFolder\SnapGraphMetricCalculator.exe";
        //private const string SnapCalculatorLocation = @"C:\SnapGraphMetricCalculator.exe";
        private IVertexCollection _oVertices; // = _nodeXLControl.Graph.Vertices;
        private IEdgeCollection _oEdges; //The edges

        readonly Random _randomizer = new Random();

        public SDNodeXLEngine()
        {
            SourceFileUri = "";
            NodeOutputResultFileUri = "";
            try
            {
                _nodeXLControl = new NodeXLControl();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            Graph = (Graph)_nodeXLControl.Graph;
            GraphDate = DateTime.Now;
            DisconnectedEdges = 0;
            ConnectedEdges = 0;
            //PreviousVertexStartID = 0;
            VertexStartId = 0;
            SetUpBarabasi = false;

        }

        public SDNodeXLEngine(bool setUpBarabasiTrue)
        {
            SourceFileUri = "";
            NodeOutputResultFileUri = "";
            _nodeXLControl = new NodeXLControl();
            Graph = (Graph)_nodeXLControl.Graph;
            GraphDate = DateTime.Now;
            DisconnectedEdges = 0;
            ConnectedEdges = 0;
            //PreviousVertexStartID = 0;
            VertexStartId = 0;
            SetUpBarabasi = setUpBarabasiTrue;
            if (setUpBarabasiTrue)
            {
                BarabasiMetrics = new BarabasiMetrics();
                BarabasiSingleRunResults = new BarabasiSingleRunResults();
            }
        }


        public SDNodeXLEngine(string sourceFileUri, string resultFileUri, int previousVertexStartId,
            bool setUpBarabasiTrue)
        {
            SourceFileUri = sourceFileUri;
            NodeOutputResultFileUri = resultFileUri;
            _nodeXLControl = new NodeXLControl();
            Graph = (Graph)_nodeXLControl.Graph;
            GraphDate = DateTime.Now;
            DisconnectedEdges = 0;
            ConnectedEdges = 0;
            SetUpBarabasi = setUpBarabasiTrue;
            if (setUpBarabasiTrue)
                BarabasiMetrics = new BarabasiMetrics();
        }
        public Graph Graph { get; set; }

        public string GraphName { get; set; }

        public DateTime GraphDate { get; set; }

        public int DisconnectedEdges { get; set; }
        public int ConnectedEdges { get; set; }

        public int NumberOfVertices { get; set; }
        //public int EdgesCount { get; set; }
        public BarabasiSingleRunResults BarabasiSingleRunResults { get; set; }
        public BarabasiMetrics BarabasiMetrics { get; set; }
        public double CalculatedBarabasiRelativeSizeOfOriginalLargestComponent { get; set; }
        //public double CalculatedBarabasiRelativeSizeOfPreviousLargestComponent { get; set; }
        public double CalculatedBarabasiMeanSizeOfAllComponentsMinusLargest { get; set; }
        public int NumberOfAllComponents { get; set; }
        public int CurrentSizeOfCurrentLargestComponent { get; set; }
        public int OriginalSizeOfLargestComponent { get; set; }


        /// <summary>
        /// The Source file is the folder path that contains the files that contain
        /// the probability matrices on which the system is going to run its analysis.
        /// 
        /// It is usually in the form of:
        /// 
        /// folder\SourceName\corpus
        /// folder\SourceName\network
        /// folder\SourceName\statistics_SourceName
        /// 
        /// and in the folder\SourceName\statistics_SourceName is where the 
        /// probability matrices reside which are mainly:
        /// 1. ConditionalProbabilityGephi.csv
        /// 2. CorrelationCoefficientGephi.csv
        /// 3. JointFrequencyGephi.csv
        /// 4. JointProbabilityMatrixGephi.csv
        /// 5. LogOddsRatioGephi.csv
        /// 6. PointWiseMutualInformationGephi.csv
        /// 
        /// And then for each of these values, the system loads up the matrix into NodeXL and generates
        /// the corresponding network metrics such as degree, centralities, etc.. 
        /// </summary>
        public string SourceFileUri { get; set; }

        public string NodeOutputResultFileUri { get; set; }

        public string GraphOutputResultFileUri { get; set; }

        public bool SetUpBarabasi { get; set; }

        public List<Tuple<string, string, string>> ImportDictionary { get; set; }

        //To Do: replace List with array for faster performance
        private List<double> Degree { get; set; }
        private List<double> EigenvectorCentrality { get; set; }
        private List<double> BetweennessCentrality { get; set; }
        private List<double> ClosenessCentrality { get; set; }
        private List<double> ClusteringCoefficient { get; set; }

        /// <summary>
        /// Average vertex/node degree
        /// </summary>
        public double GraphDegree { get; set; }

        public int HighestVertexDegree { get; set; }
        public double PercentageOfNetworkDeleted { get; set; }

        public int OriginalSizeOfGraph { get; set; }
        public int NumberOfVerticesRemoved { get; set; }

        public int PercentageOfGraphRemoved { get; set; }

        /// <summary>
        /// Average vertex/node clustering coefficient
        /// </summary>
        public double GraphClusteringCoefficient { get; set; }

        //Represents the matrix as a double array of string.
        public string[][] Matrix { get; set; }

        public bool LoadMatrixFileLargeFiles()
        {
            GraphDate = DateTime.Now;
            DisconnectedEdges = 0;
            ConnectedEdges = 0;

            try
            {
                if (string.IsNullOrEmpty(SourceFileUri))
                    return false;
                string[] lines = File.ReadAllLines(SourceFileUri);
                Matrix = new string[lines.Length][]; //(Length-1) To get rid of the first title line
                for (int i = 1; i < lines.Length; i++) //(i=1) To get rid of the first title line
                {
                    var line = lines[i].Split(';');
                    var line2 = line.Skip(1).ToArray();
                    Matrix[i - 1] = line2.Take(line2.Count() - 1).ToArray();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }


        public bool LoadMatrixFile()
        {
            try
            {
                if (string.IsNullOrEmpty(SourceFileUri))
                    return false;

                var matrixRaw =
                    File.ReadAllLines(SourceFileUri)
                        .Select(l => l.Split(';').Skip(1).Reverse().Skip(1).Reverse().ToArray());

                var enumerable = matrixRaw as string[][] ?? matrixRaw.ToArray();
                Matrix = enumerable.Take(enumerable.Length).ToArray();
            }
            catch (Exception)
            {
                return false;
            }

            //original code before resharper refactoring was:
            //var count = list.Count();
            //var list2 = list.Take(count).ToArray();  

            return true;
        }

        public bool LoadMatrixFile(string fileURI, bool hasHeaders, bool negativeToZero, bool roundToZeroOrOne)
        {
            try
            {
                SourceFileUri = fileURI;
                var matrixRaw =
                    File.ReadAllLines(fileURI).Select(l => l.Split(';').Skip(1).Reverse().Skip(1).Reverse().ToArray());

                var enumerable = matrixRaw as string[][] ?? matrixRaw.ToArray();
                Matrix = enumerable.Take(enumerable.Length).ToArray();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

            //original code before resharper refactoring was:
            //var count = list.Count();
            //var list2 = list.Take(count).ToArray();  
        }

        //private IVertexCollection oVertices;

        public int VertexStartId { get; set; }
        public int PreviousVertexStartId { get; set; }
        private int MaxIdOfVertices { get; set; }

        public int OriginalSizeOfNetwork { get; set; }
        public int CurrentSizeOfNetwork { get; set; }//after removal of an edge or degree

        public void CreateAndPopulateGraph(bool directedGraph)
        {
            /*
              Remember to use as a counter, the last ID used by a previous graph (if present).
              Simply let nodexl let you know what ID to use.
            */


            //if (_nodeXLControl == null)
            //_nodeXLControl = new NodeXLControl();
            //elementHost1.Child = nodeXLControl1;//not needed since this function does not draw the graph

            string[][] nodeValues = Matrix;
            //if (nodeValues == null) 
            //    throw new ArgumentNullException(nameof(nodeValues));
            if (nodeValues == null)
                throw new ArgumentNullException("nodeValues");


            //Needed to reset the graph, especially for a first run for a single source.
            //In the first run, the nodes are setup and the edges calculated based on the values in the matrix.
            //In consequent runs (for the same source), there is no need to setup the nodes because they are the same,
            //   only the edges need to be setup. BUT BUT BUT .. the problem is that when you clearGraph, nodeXL assigns
            // a new "ID" to the graph, and with that screws up the node IDs, meaning that the previous node IDs are 
            // preserved for the 'previous' graph and now you have to count from the last vertex id of the previous graph,
            // which makes accessing nodes and edges a logistic nightmare and overall makes the program unscalable and 
            // prone to memory-leaks and crashes!
            //_nodeXLControl.ClearGraph();
            // I have to find a way to reset the graph's nodes without getting a new ID.
            //_nodeXLControl.Graph.Vertices.Clear();//Clear did remove all the vertices but when I added a new vertex, the Id was not reset.

            //Trying to manually remove all the vertices to see if that resets the Id counter.
            //Answer: No it did not reset the counter. This is a bug from MS (althouh they claim it to be a feature so that no 2 vertices could have same ID)
            //var totalVerticesCount = _nodeXLControl.Graph.Vertices.Count;
            //for (var i = 1; i <= totalVerticesCount; i++)
            //    _nodeXLControl.Graph.Vertices.Remove(i);

            //_nodeXLControl.Graph.Vertices.Clear();
            //_nodeXLControl.Graph.Edges.Clear();

            //Graph oGraph = new Graph(GraphDirectedness.Undirected);
            //IVertexCollection oVertices = oGraph.Vertices;          

            _oVertices = _nodeXLControl.Graph.Vertices;

            NumberOfVertices = nodeValues.GetLength(0) - 1; //Get the Length of One Dimension of an Array

            /* Get the names of the vertices from the first row */
            /* source: http://peterkellner.net/2009/10/14/linq-convert-array-to-list/ */
            var headerValues = nodeValues[0].Select(data => data).ToList();


            //No need to use (for (var i = startingCounter; i <= MaxIDOfVertices; i++)) here because I'm simply building the vertices
            for (var i = 1; i <= NumberOfVertices; i++)
            {
                IVertex oVertex = _oVertices.Add();
                //oVertex.SetValue(ReservedMetadataKeys.PerVertexShape, VertexShape.Label);
                oVertex.SetValue(ReservedMetadataKeys.PerVertexLabel, headerValues[i - 1]);
                /*needed for name of vertex*/
                //oVertex.SetValue(ReservedMetadataKeys.Visibility, VisibilityKeyValue.Hidden);
            }

            /* Remove a row from 2d array
               http://stackoverflow.com/questions/7992728/how-to-delete-a-row-from-a-2d-array-in-c  */

            // Remove the first row which comprises the actual word (row at index 0)
            int rowToRemove = 0;
            nodeValues = Matrix
                .Where((arr, index) => index != rowToRemove)
                .ToArray();

            //var startingCounter = 1;
            //VertexStartID = startingCounter;
            ////PreviousVertexStartID = VertexStartID;
            //MaxIDOfVertices = NumberOfVertices;

            int startingCounter = _nodeXLControl.Graph.Vertices.FirstOrDefault().ID;
            VertexStartId = startingCounter;
            MaxIdOfVertices = startingCounter + NumberOfVertices - 1;

            OriginalSizeOfGraph = NumberOfVertices;

            #region old implementation (re-use same idea but with different logic and implementation)

            ////If _nodeXLControl.Graph.ID == 1, then the counting is normal,
            ////otherwise I have to get the right ID.
            //if (_nodeXLControl.Graph.ID != 1)
            //{
            //    startingCounter = _nodeXLControl.Graph.Vertices.FirstOrDefault().ID;
            //    VertexStartID = startingCounter;
            //    MaxIDOfVertices = startingCounter + NumberOfVertices - 1;
            //    //startingCounter = NumberOfVertices + 1;
            //    //MaxIDOfVertices = _nodeXLControl.Graph.ID*NumberOfVertices;
            //}

            #endregion

            //Connect the vertices with edges:
            //IEdgeCollection oEdges = oGraph.Edges;
            //IEdgeCollection oEdges = _nodeXLControl.Graph.Edges;
            _oEdges = _nodeXLControl.Graph.Edges;

            //var startingCounter = 1;
            //for (var i = startingCounter; i <= NumberOfVertices; i++)
            for (var i = startingCounter; i <= MaxIdOfVertices; i++)
            {
                IVertex vertex1;
                _oVertices.Find(i, out vertex1);

                //for (var j = startingCounter; j <= MaxIDOfVertices; j++)
                //for (var j = i; j <= NumberOfVertices; j++)
                for (var j = i; j <= MaxIdOfVertices; j++)
                {
                    //var edgeValue = float.Parse(nodeValues[i - 1][j - 1], CultureInfo.InvariantCulture.NumberFormat);
                    var edgeValue = float.Parse(nodeValues[i - startingCounter][j - startingCounter],
                        CultureInfo.InvariantCulture.NumberFormat);
                    if (edgeValue > 0)
                    {
                        IVertex vertex2;
                        _oVertices.Find(j, out vertex2);
                        IEdge edge = _oEdges.Add(vertex1, vertex2, directedGraph);
                        edge.SetValue(ReservedMetadataKeys.PerEdgeWidth, edgeValue); //edgeValue or 1f (depends)
                        ConnectedEdges++;
                    }
                    else
                    {
                        DisconnectedEdges++;
                    }
                }
                //startingCounter++;
            }

            //check if oEdges == ConnectedEdges
            //EdgesCount = oEdges.Count; //should be equal to ConnectedEdges

            Graph = (Graph)_nodeXLControl.Graph;
        }


        public bool CheckIfVerticesHaveEdge(int vertex1Id, int vertex2Id)
        {
            IVertex vertex1;
            _oVertices.Find(vertex1Id, out vertex1);

            IVertex vertex2;
            _oVertices.Find(vertex2Id, out vertex2);

            var result = _oEdges.GetConnectingEdges(vertex1, vertex2);

            return result.Count > 0;
        }

        public IVertex GetRandomVertexInRange(int minId, int maxId)
        {
            int rInt = _randomizer.Next(minId, maxId); //for ints
            IVertex vertex;
            _oVertices.Find(rInt, out vertex);
            return vertex;
        }

        //Reassings the edges to random vertices
        public void RandomizeCreateAndPopulateGraph(bool directedGraph)
        {
            /*
              Remember to use as a counter, the last ID used by a previous graph (if present).
              Simply let nodexl let you know what ID to use.
            */


            //if (_nodeXLControl == null)
            //_nodeXLControl = new NodeXLControl();
            //elementHost1.Child = nodeXLControl1;//not needed since this function does not draw the graph

            string[][] nodeValues = Matrix;
            //if (nodeValues == null) 
            //    throw new ArgumentNullException(nameof(nodeValues));
            if (nodeValues == null)
                throw new ArgumentNullException("nodeValues");


            //Needed to reset the graph, especially for a first run for a single source.
            //In the first run, the nodes are setup and the edges calculated based on the values in the matrix.
            //In consequent runs (for the same source), there is no need to setup the nodes because they are the same,
            //   only the edges need to be setup. BUT BUT BUT .. the problem is that when you clearGraph, nodeXL assigns
            // a new "ID" to the graph, and with that screws up the node IDs, meaning that the previous node IDs are 
            // preserved for the 'previous' graph and now you have to count from the last vertex id of the previous graph,
            // which makes accessing nodes and edges a logistic nightmare and overall makes the program unscalable and 
            // prone to memory-leaks and crashes!
            //_nodeXLControl.ClearGraph();
            // I have to find a way to reset the graph's nodes without getting a new ID.
            //_nodeXLControl.Graph.Vertices.Clear();//Clear did remove all the vertices but when I added a new vertex, the Id was not reset.

            //Trying to manually remove all the vertices to see if that resets the Id counter.
            //Answer: No it did not reset the counter. This is a bug from MS (althouh they claim it to be a feature so that no 2 vertices could have same ID)
            //var totalVerticesCount = _nodeXLControl.Graph.Vertices.Count;
            //for (var i = 1; i <= totalVerticesCount; i++)
            //    _nodeXLControl.Graph.Vertices.Remove(i);

            //_nodeXLControl.Graph.Vertices.Clear();
            //_nodeXLControl.Graph.Edges.Clear();

            //Graph oGraph = new Graph(GraphDirectedness.Undirected);
            //IVertexCollection oVertices = oGraph.Vertices;          

            _oVertices = _nodeXLControl.Graph.Vertices;

            NumberOfVertices = nodeValues.GetLength(0) - 1; //Get the Length of One Dimension of an Array

            /* Get the names of the vertices from the first row */
            /* source: http://peterkellner.net/2009/10/14/linq-convert-array-to-list/ */
            var headerValues = nodeValues[0].Select(data => data).ToList();


            //No need to use (for (var i = startingCounter; i <= MaxIDOfVertices; i++)) here because I'm simply building the vertices
            for (var i = 1; i <= NumberOfVertices; i++)
            {
                IVertex oVertex = _oVertices.Add();
                //oVertex.SetValue(ReservedMetadataKeys.PerVertexShape, VertexShape.Label);
                oVertex.SetValue(ReservedMetadataKeys.PerVertexLabel, headerValues[i - 1]);
                /*needed for name of vertex*/
                //oVertex.SetValue(ReservedMetadataKeys.Visibility, VisibilityKeyValue.Hidden);
            }

            /* Remove a row from 2d array
               http://stackoverflow.com/questions/7992728/how-to-delete-a-row-from-a-2d-array-in-c  */

            // Remove the first row which comprises the actual word (row at index 0)
            int rowToRemove = 0;
            nodeValues = Matrix
                .Where((arr, index) => index != rowToRemove)
                .ToArray();

            //var startingCounter = 1;
            //VertexStartID = startingCounter;
            ////PreviousVertexStartID = VertexStartID;
            //MaxIDOfVertices = NumberOfVertices;

            int startingCounter = _nodeXLControl.Graph.Vertices.FirstOrDefault().ID;
            VertexStartId = startingCounter;
            MaxIdOfVertices = startingCounter + NumberOfVertices - 1;

            #region old implementation (re-use same idea but with different logic and implementation)

            ////If _nodeXLControl.Graph.ID == 1, then the counting is normal,
            ////otherwise I have to get the right ID.
            //if (_nodeXLControl.Graph.ID != 1)
            //{
            //    startingCounter = _nodeXLControl.Graph.Vertices.FirstOrDefault().ID;
            //    VertexStartID = startingCounter;
            //    MaxIDOfVertices = startingCounter + NumberOfVertices - 1;
            //    //startingCounter = NumberOfVertices + 1;
            //    //MaxIDOfVertices = _nodeXLControl.Graph.ID*NumberOfVertices;
            //}

            #endregion

            //Connect the vertices with edges:
            //IEdgeCollection oEdges = oGraph.Edges;
            //IEdgeCollection oEdges = _nodeXLControl.Graph.Edges;
            _oEdges = _nodeXLControl.Graph.Edges;

            //var startingCounter = 1;
            //for (var i = startingCounter; i <= NumberOfVertices; i++)
            for (var i = startingCounter; i <= MaxIdOfVertices; i++)
            {
                //IVertex vertex1;
                //oVertices.Find(i, out vertex1);

                //for (var j = startingCounter; j <= MaxIDOfVertices; j++)
                //for (var j = i; j <= NumberOfVertices; j++)
                for (var j = i; j <= MaxIdOfVertices; j++)
                {
                    //var edgeValue = float.Parse(nodeValues[i - 1][j - 1], CultureInfo.InvariantCulture.NumberFormat);
                    var edgeValue = float.Parse(nodeValues[i - startingCounter][j - startingCounter],
                        CultureInfo.InvariantCulture.NumberFormat);
                    if (edgeValue > 0)
                    {
                        bool realEdgeNotAdded = false;
                        while (realEdgeNotAdded != true)
                        {
                            int rInt1 = _randomizer.Next(startingCounter, MaxIdOfVertices);
                            int rInt2 = _randomizer.Next(startingCounter, MaxIdOfVertices);
                            if (!CheckIfVerticesHaveEdge(rInt1, rInt2))
                            {
                                IVertex vertex1;
                                _oVertices.Find(rInt1, out vertex1);
                                IVertex vertex2;
                                _oVertices.Find(rInt2, out vertex2);
                                IEdge edge = _oEdges.Add(vertex1, vertex2, directedGraph);
                                edge.SetValue(ReservedMetadataKeys.PerEdgeWidth, edgeValue); //edgeValue or 1f (depends)
                                ConnectedEdges++;
                                realEdgeNotAdded = true;
                            }
                        }
                    }
                    else
                    {
                        DisconnectedEdges++;
                    }
                }
                //startingCounter++;
            }

            //check if oEdges == ConnectedEdges
            //EdgesCount = oEdges.Count; //should be equal to ConnectedEdges

            Graph = (Graph)_nodeXLControl.Graph;
        }

        /// <summary>
        /// Because NodeXL does not recycle the IDs of the Vertices. (Even creating a new object does not reset it,
        /// it is attached to the duration of the whole program, until I figure out more about it).
        /// So in order to run multiple runs, the Vertices IDs must match for the for loops, so for consequent
        /// runs, I only re-assign the edges, since this is the only thing changing anyways.
        /// It also more efficient and time saving.
        /// </summary>
        /// <param name="directedGraph"></param>
        public void ReassignTheEdgesValues(bool directedGraph)
        {
            Graph.Edges.Clear();
            ConnectedEdges = 0;
            DisconnectedEdges = 0;
            GraphDate = DateTime.Now;

            //if (_nodeXLControl == null)
            //    _nodeXLControl = new NodeXLControl();
            //elementHost1.Child = nodeXLControl1;//not needed since this function does not draw the graph

            string[][] nodeValues = Matrix;
            //if (nodeValues == null) 
            //    throw new ArgumentNullException(nameof(nodeValues));
            if (nodeValues == null)
                throw new ArgumentNullException(@"nodeValues");


            ////Graph oGraph = new Graph(GraphDirectedness.Undirected);
            ////IVertexCollection oVertices = oGraph.Vertices;          
            //IVertexCollection oVertices = _nodeXLControl.Graph.Vertices;

            //NumberOfVertices = nodeValues.GetLength(0) - 1;//Get the Length of One Dimension of an Array

            ///* Get the names of the vertices from the first row */
            ///* source: http://peterkellner.net/2009/10/14/linq-convert-array-to-list/ */
            //var headerValues = nodeValues[0].Select(data => data).ToList();

            //for (var i = 1; i <= NumberOfVertices; i++)
            //{
            //    IVertex oVertex = oVertices.Add();
            //    oVertex.SetValue(ReservedMetadataKeys.PerVertexShape, VertexShape.Label);
            //    oVertex.SetValue(ReservedMetadataKeys.PerVertexLabel, headerValues[i - 1]);
            //    oVertex.SetValue(ReservedMetadataKeys.Visibility, VisibilityKeyValue.Hidden);
            //}

            /* Remove a row from 2d array
               http://stackoverflow.com/questions/7992728/how-to-delete-a-row-from-a-2d-array-in-c  */

            // to remove the first row of word names i.e. index 0
            int rowToRemove = 0;
            nodeValues = Matrix
                .Where((arr, index) => index != rowToRemove)
                .ToArray();

            //Connect the vertices with edges:
            //IEdgeCollection oEdges = oGraph.Edges;
            IEdgeCollection oEdges = _nodeXLControl.Graph.Edges;
            //var startingCounter = 1;

            //var startingCounter = 1;
            //MaxIDOfVertices = NumberOfVertices;

            #region old implementation (re-use same idea but with different logic and implementation)

            ////If _nodeXLControl.Graph.ID == 1, then the counting is normal,
            ////otherwise I have to get the right ID.
            //if (_nodeXLControl.Graph.ID != 1)
            //{
            //    startingCounter = _nodeXLControl.Graph.Vertices.FirstOrDefault().ID;
            //    VertexStartID = startingCounter;
            //    MaxIDOfVertices = startingCounter + NumberOfVertices - 1;
            //    //startingCounter = NumberOfVertices + 1;
            //    //MaxIDOfVertices = _nodeXLControl.Graph.ID*NumberOfVertices;
            //}

            #endregion


            int startingCounter = _nodeXLControl.Graph.Vertices.FirstOrDefault().ID;


            //VertexStartID = startingCounter;
            MaxIdOfVertices = startingCounter + NumberOfVertices - 1;


            for (var i = startingCounter; i <= MaxIdOfVertices; i++)
            //for (var i = 1; i <= NumberOfVertices; i++)
            {
                IVertex vertex1;
                _oVertices.Find(i, out vertex1);

                //for (var j = startingCounter; j <= NumberOfVertices; j++)
                for (var j = i; j <= MaxIdOfVertices; j++)
                {
                    //var edgeValue = float.Parse(nodeValues[i - 1][j - 1], CultureInfo.InvariantCulture.NumberFormat);
                    var edgeValue = float.Parse(nodeValues[i - startingCounter][j - startingCounter],
                        CultureInfo.InvariantCulture.NumberFormat);
                    if (edgeValue > 0)
                    {
                        IVertex vertex2;
                        _oVertices.Find(j, out vertex2);
                        IEdge edge = oEdges.Add(vertex1, vertex2, directedGraph);
                        edge.SetValue(ReservedMetadataKeys.PerEdgeWidth, edgeValue); //edgeValue or 1f (depends)
                        ConnectedEdges++;
                    }
                    else
                    {
                        DisconnectedEdges++;
                    }
                }
                //startingCounter++;
            }

            Graph = (Graph)_nodeXLControl.Graph;
        }

        //private IVertexCollection oVertices;// = _nodeXLControl.Graph.Vertices;
        /// <summary>
        /// Returns a List of all the vertices and their degrees. Also the function calculates
        /// and updates the average GraphDegree and the HighestVertexDegree of the graph.
        /// </summary>
        /// <returns></returns>
        /// I don't know why I resorted to VertexDegreeCalculator since I can calculate directly from the
        /// graph itself.
        public List<double> OldCalculateVertexDegree()
        {
            //Calculate some graph metrics; vertex degree, for example.  
            //Note that there is an entire family of "GraphMetricCalculator" classes in 
            //the Smrf.NodeXL.Algorithms namespace.

            List<double> returnedResults = new List<double>();

            VertexDegreeCalculator oVertexDegreeCalculator = new VertexDegreeCalculator();

            Dictionary<Int32, VertexDegrees> oVertexDegreeDictionary =
                oVertexDegreeCalculator.CalculateGraphMetrics(Graph);


            _oVertices = _nodeXLControl.Graph.Vertices;

            MaxIdOfVertices = _oVertices.Count;

            //for (var i = 1; i <= NumberOfVertices; i++)
            for (var i = VertexStartId; i <= MaxIdOfVertices; i++)
            {
                IVertex vertex;
                try
                {
                    if (_oVertices.Find(i, out vertex))
                        returnedResults.Add(oVertexDegreeDictionary[vertex.ID].Degree);
                    else
                        returnedResults.Add(-1);

                    //output += vertex.GetValue(ReservedMetadataKeys.PerVertexLabel) +
                    //          " Degree = " + oVertexDegreeDictionary[vertex.ID].Degree + eol;

                }
                catch (Exception)
                {
                    //throw;
                }
            }

            double sumVertex = oVertexDegreeDictionary.Sum(ix => ix.Value.Degree);
            GraphDegree = sumVertex / oVertexDegreeDictionary.Count;
            HighestVertexDegree = oVertexDegreeDictionary.Max(ix => ix.Value.Degree);
            var tempHighestVertexDegree = _oVertices.Max(ix => ix.Degree);
            if (HighestVertexDegree != tempHighestVertexDegree)
            {
                var x = "Houston we have a problem";
            }

            return returnedResults;
            //output += "\r\n" + @"Graph Degree = " + averageVertex + eol;
            //return output;
        }

        /// <summary>
        /// Calculates all vertices degrees and other values as well.
        /// </summary>
        /// <returns></returns>
        public List<double> CalculateVertexDegree()
        {
            //Calculate some graph metrics; vertex degree, for example.  
            //Note that there is an entire family of "GraphMetricCalculator" classes in 
            //the Smrf.NodeXL.Algorithms namespace.

            List<double> returnedResults = new List<double>();
            _oVertices = _nodeXLControl.Graph.Vertices;

            MaxIdOfVertices = _oVertices.Count;

            var inputValue = Convert.ToDouble(OriginalSizeOfGraph - MaxIdOfVertices) / Convert.ToDouble(OriginalSizeOfGraph) * 100;
            PercentageOfNetworkDeleted = Math.Round(inputValue, 2, MidpointRounding.AwayFromZero);

            //for (var i = 1; i <= NumberOfVertices; i++)
            for (var i = VertexStartId; i <= MaxIdOfVertices; i++)
            {
                IVertex vertex;
                try
                {
                    if (_oVertices.Find(i, out vertex))
                        returnedResults.Add(vertex.Degree);
                    else
                        returnedResults.Add(-1);
                }
                catch (Exception)
                {
                    //throw;
                }
            }

            double sumVertex = _oVertices.Sum(ix => ix.Degree);
            GraphDegree = sumVertex / _oVertices.Count;
            HighestVertexDegree = _oVertices.Max(ix => ix.Degree);

            var tempHighestVertexDegree = _oVertices.Max(ix => ix.Degree);
            if (HighestVertexDegree != tempHighestVertexDegree)
            {
                var x = "Houston we have a problem";
            }

            return returnedResults;
        }

        public List<double> CalculateEigenvectorCentrality()
        {
            
            //EigenvectorCentralityCalculator.cs
            //Calculate some graph metrics; vertex degree, for example.  
            //Note that there is an entire family of "GraphMetricCalculator" classes in 
            //the Smrf.NodeXL.Algorithms namespace.


            //var output = "";
            List<double> returnedResults = new List<double>();
            //IVertexCollection oVertices = _nodeXLControl.Graph.Vertices;

            var test = Directory.GetCurrentDirectory();

            /* I placed it in a location that can always be found, because NodeXL was not
             able to find the executable file. */
            GraphMetricCalculatorBase.SetSnapGraphMetricCalculatorPath(SnapCalculatorLocation);

            EigenvectorCentralityCalculator eEigenvectorCentralityCalculator =
                new EigenvectorCentralityCalculator();

            Dictionary<Int32, double>
                eEigenvectorCentralityDictionary =
                    eEigenvectorCentralityCalculator.CalculateGraphMetrics(Graph);

            //for (var i = 1; i <= NumberOfVertices; i++)
            for (var i = VertexStartId; i <= MaxIdOfVertices; i++)
            {
                try
                {
                    IVertex vertex;
                    if (_oVertices.Find(i, out vertex))
                        returnedResults.Add(eEigenvectorCentralityDictionary[i]);
                    else
                        returnedResults.Add(-1);
                }
                catch (Exception)
                {
                    //throw;
                }
            }

            return returnedResults;
        }

        public List<double> CalculateBetweennessCentrality()
        {
            //Calculate some graph metrics; vertex degree, for example.  
            //Note that there is an entire family of "GraphMetricCalculator" classes in 
            //the Smrf.NodeXL.Algorithms namespace.

            //var output = "";
            List<double> returnedResults = new List<double>();
            //IVertexCollection oVertices = _nodeXLControl.Graph.Vertices;


            //var names = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceNames().ToList();
            //var allresources = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceNames();


            /* I placed it in a location that can always be found, because NodeXL was not
             able to find the executable file. */
            //GraphMetricCalculatorBase.SetSnapGraphMetricCalculatorPath(
            //@"..\..\..\NodeXLClassLibraries_1.0.1.234\SnapGraphMetricCalculator.exe");

            //var startupPath = Environment.CurrentDirectory;

            GraphMetricCalculatorBase.SetSnapGraphMetricCalculatorPath(SnapCalculatorLocation);


            //GraphMetricCalculatorBase.SetSnapGraphMetricCalculatorPath(
            //Properties.Resources.SnapGraphMetricCalculator);

            //Properties.Resources.RedIcon;

            //Trying to pull the file's location from the resources. (for emebedding)


            BrandesFastCentralityCalculator oBrandesFastCentralityCalculator =
                new BrandesFastCentralityCalculator();

            Dictionary<Int32, BrandesVertexCentralities>
                oBrandesVertexCentralityDictionary =
                    oBrandesFastCentralityCalculator.CalculateGraphMetrics(Graph);

            for (var i = VertexStartId; i <= MaxIdOfVertices; i++)
            //for (var i = 1; i <= NumberOfVertices; i++)
            {
                IVertex vertex;

                try
                {
                    if (_oVertices.Find(i, out vertex))
                    {
                        //output += string.Format("{0} Betweenness centrality = {1} \r\n", vertex.GetValue(ReservedMetadataKeys.PerVertexLabel),
                        //  oBrandesVertexCentralityDictionary[i].BetweennessCentrality);
                        returnedResults.Add(oBrandesVertexCentralityDictionary[i].BetweennessCentrality);
                    }
                    else
                        returnedResults.Add(-1);

                }
                catch (Exception)
                {

                    //throw;
                }
            }

            return returnedResults;
        }

        public List<double> CalculateClosenessCentrality()
        {
            //Calculate some graph metrics; vertex degree, for example.  
            //Note that there is an entire family of "GraphMetricCalculator" classes in 
            //the Smrf.NodeXL.Algorithms namespace.

            List<double> returnedResults = new List<double>();
            //IVertexCollection oVertices = _nodeXLControl.Graph.Vertices;
            GraphMetricCalculatorBase.SetSnapGraphMetricCalculatorPath(SnapCalculatorLocation);
            //// I placed it in a location that can always be found, because NodeXL was not 
            //// able to find the executable file.


            //Calculator


            BrandesFastCentralityCalculator oBrandesFastCentralityCalculator =
                new BrandesFastCentralityCalculator();

            Dictionary<Int32, BrandesVertexCentralities>
                oBrandesVertexCentralityDictionary =
                    oBrandesFastCentralityCalculator.CalculateGraphMetrics(Graph);

            for (var i = VertexStartId; i <= MaxIdOfVertices; i++)
            //for (var i = 1; i <= NumberOfVertices; i++)
            {
                IVertex vertex;

                try
                {
                    if (_oVertices.Find(i, out vertex))
                    {
                        //output += string.Format("{0} Closeness centrality = {1} \r\n", vertex.GetValue(ReservedMetadataKeys.PerVertexLabel),
                        //    oBrandesVertexCentralityDictionary[i].ClosenessCentrality);
                        returnedResults.Add(oBrandesVertexCentralityDictionary[i].ClosenessCentrality);
                    }
                    else
                        returnedResults.Add(-1);

                }
                catch (Exception)
                {
                    //throw;
                }
            }

            return returnedResults;
        }

        public List<double> CalculateClusteringCoefficient()
        {
            ClusteringCoefficientCalculator clusteringCoefficientCalculator = new ClusteringCoefficientCalculator();
            Dictionary<int, double> clusteringCoefficientsDictionary =
                clusteringCoefficientCalculator.CalculateGraphMetrics(Graph);

            //IVertexCollection oVertices = _nodeXLControl.Graph.Vertices;
            List<double> returnedResults =
                _oVertices.Select(oVertex => clusteringCoefficientsDictionary[oVertex.ID]).ToList();

            int dictionaryCount = clusteringCoefficientsDictionary.Keys.Count;
            GraphClusteringCoefficient = clusteringCoefficientsDictionary.Sum(x => x.Value) / dictionaryCount;

            return returnedResults;
        }

        public double CalculateGraphModularity(ClusterAlgorithm clusterAlgorithm)
        {
            ClusterCalculator oClusterCalculator = new ClusterCalculator();

            //4. Select a cluster algorithm:
            //oClusterCalculator.Algorithm = ClusterAlgorithm.Clique;
            oClusterCalculator.Algorithm = clusterAlgorithm;

            //oClusterCalculator.Algorithm = ClusterAlgorithm.WakitaTsurumi;   

            GraphMetricCalculatorBase.SetSnapGraphMetricCalculatorPath(SnapCalculatorLocation);

            //5. Tell the ClusterCalculator to calculate clusters:
            ICollection<Community> oCommunities = (ICollection<Community>)
                oClusterCalculator.CalculateGraphMetrics(_nodeXLControl.Graph);

            return 0;
        }

        public void RunAllNetworkMetricsCalculations()
        {
            Degree = CalculateVertexDegree();
            EigenvectorCentrality = CalculateEigenvectorCentrality();
            BetweennessCentrality = CalculateBetweennessCentrality();
            ClosenessCentrality = CalculateClosenessCentrality();
            ClusteringCoefficient = CalculateClusteringCoefficient();
        }


        public void RunAllNetworkMetricsCalculations(int runId)
        {
            Degree = CalculateVertexDegree();
            EigenvectorCentrality = CalculateEigenvectorCentrality();
            BetweennessCentrality = CalculateBetweennessCentrality();
            ClosenessCentrality = CalculateClosenessCentrality();
            ClusteringCoefficient = CalculateClusteringCoefficient();
            DetermineTheConnectedComponents(runId);
        }

        public bool WriteOutAllNodesStatisticsToOutputFile()
        {
            if (string.IsNullOrEmpty(NodeOutputResultFileUri))
                return false;

            // create a header file to represent the statistics calculated for all the available vertices (words).

            TextWriter tw = new StreamWriter(NodeOutputResultFileUri);
            try
            {
                #region commented out header

                //tw.WriteLine("{0,-15} \t {1,-8} \t {2,-25} \t {3,-25} \t {4,-25} \t {5,-25}",
                //    "Word", 
                //    "Degree", 
                //    "Eigenvector Centrality", 
                //    "Betweenness Centrality", 
                //    "Closeness Centrality",
                //    "Clustering Coefficient");

                #endregion

                //IVertexCollection oVertices = _nodeXLControl.Graph.Vertices;

                if (ImportDictionary == null)
                {
                    for (var i = VertexStartId; i <= MaxIdOfVertices; i++)
                    //for (var i = 1; i <= NumberOfVertices; i++)
                    {
                        IVertex vertex;
                        _oVertices.Find(i, out vertex);
                        tw.WriteLine("{0,-15} \t {1,-8} \t {2,-25} \t {3,-25} \t {4,-25} \t {5,-25}",
                            vertex.GetValue(ReservedMetadataKeys.PerVertexLabel),
                            Degree[i - VertexStartId],
                            EigenvectorCentrality[i - VertexStartId],
                            BetweennessCentrality[i - VertexStartId],
                            ClosenessCentrality[i - VertexStartId],
                            ClusteringCoefficient[i - VertexStartId]);
                    }
                    tw.Close();
                }
                else
                {
                    for (var i = VertexStartId; i <= MaxIdOfVertices; i++)
                    //for (var i = 1; i <= NumberOfVertices; i++)
                    {
                        IVertex vertex;
                        _oVertices.Find(i, out vertex);
                        tw.WriteLine(
                            "{0,-15} \t {1,-8} \t {2,-25} \t {3,-25} \t {4,-25} \t {5,-25} \t {6,-25} \t {7,-25}",
                            vertex.GetValue(ReservedMetadataKeys.PerVertexLabel),
                            ImportDictionary[i - VertexStartId].Item2,
                            ImportDictionary[i - VertexStartId].Item3,
                            Degree[i - VertexStartId],
                            EigenvectorCentrality[i - VertexStartId],
                            BetweennessCentrality[i - VertexStartId],
                            ClosenessCentrality[i - VertexStartId],
                            ClusteringCoefficient[i - VertexStartId]);
                    }
                    tw.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(@"Error in SDNodeXLEngine. Check: MaxIDOfVertices(" + MaxIdOfVertices +
                                @") - VertexStartID(" + VertexStartId + @") should be equal to ImportDictionary(" +
                                ImportDictionary.Count + @"). Error message = " + e.Message);
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }
            return true;
        }

        //This function is a duplicate of WriteOutAllNodesStatisticsToOutputFile with the difference
        //that it still writes values for the "2. Frequency." and "3. Marginal Probability." even if the ImportDictionary is null.
        //The need for it to be able to produce files that can be consumed by the Excel generator and since it is called
        //after the data calculations have been completed. Could theoretically go into the files, and load the Vocabulary file
        //and lots of calculation to re-create the ImportDictionary but it is not worth it for now, so I added this function
        //so that it can produce files that can be consumed by the Excel generator (since it was getting confused on the column Id)
        public bool WriteOutAllNodesStatisticsToOutputFileWithEmptyVocabValues()
        {
            if (string.IsNullOrEmpty(NodeOutputResultFileUri))
                return false;

            int tempID = 1;
            //update oVertices
            TextWriter tw = new StreamWriter(NodeOutputResultFileUri);
            try
            {
                for (var i = 0; i <= MaxIdOfVertices; i++)
                //for (var i = 1; i <= NumberOfVertices; i++)
                {
                    IVertex vertex;

                    try
                    {
                        if (_oVertices.Find(i, out vertex))
                        {
                            tw.WriteLine(
                                "{0,-15} \t {1,-8} \t {2,-25} \t {3,-25} \t {4,-25} \t {5,-25} \t {6,-25} \t {7,-25}",
                                vertex.GetValue(ReservedMetadataKeys.PerVertexLabel),
                                0,
                                0,
                                Degree[i - 1],
                                EigenvectorCentrality[i - 1],
                                BetweennessCentrality[i - 1],
                                ClosenessCentrality[i - 1],
                                ClusteringCoefficient[i - 1]);
                        }
                    }
                    catch (Exception)
                    {
                        //throw;
                    }
                }
                tw.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(@"Error in SDNodeXLEngine. Check: MaxIDOfVertices(" + MaxIdOfVertices +
                                @") - tempID(" + tempID + @") should be equal to ImportDictionary(" +
                                ImportDictionary.Count + @"). Error message = " + e.Message);
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }
            return true;
        }

        public bool WriteOutGeneralGraphStatisticsToOutputFile(string clusterAlgorithmName)
        {
            if (string.IsNullOrEmpty(GraphOutputResultFileUri))
                return false;

            //Update the edges (and vertices) count
            _oEdges = _nodeXLControl.Graph.Edges;
            ConnectedEdges = _oEdges.Count;

            ClusterAlgorithm clusterAlgorithm;
            //Get enum (algorithm) from name (string)
            if (Enum.TryParse(clusterAlgorithmName, out clusterAlgorithm))
            {
                //int value = (int) clusterAlgorithm;
            }
            else
            {
                /* error: the string was not an enum member */
                return false;
            }

            TextWriter tw = new StreamWriter(GraphOutputResultFileUri);

            int counter = 1;
            try
            {
                tw.WriteLine("The column header order of the Nodes result file is: ");
                tw.WriteLine("____________________________________________________");

                string addInfo = @"";
                if ((SetUpBarabasi) || (ImportDictionary != null))
                {
                    if (SetUpBarabasi)
                        addInfo = @"(disabled)";
                    tw.WriteLine("{0} \r\n{1} \r\n{2} \r\n{3} \r\n{4} \r\n{5} \r\n{6} \r\n{7} \r\n",
                        "1. Word.",
                        "2. Frequency." + addInfo,
                        "3. Marginal Probability." + addInfo,
                        "4. Degree.",
                        "5. Eigenvector Centrality.",
                        "6. Betweenness Centrality.",
                        "7. Closeness Centrality.",
                        "8. Clustering Coefficient.");
                }
                else
                {
                    tw.WriteLine("{0} \r\n{1} \r\n{2} \r\n{3} \r\n{4} \r\n{5} \r\n",
                        "1. Word.",
                        "2. Degree.",
                        "3. Eigenvector Centrality.",
                        "4. Betweenness Centrality.",
                        "5. Closeness Centrality.",
                        "6. Clustering Coefficient.");
                }
                tw.WriteLine("Graph Statistics:");
                tw.WriteLine("_________________");

                var timeStamp = @"Time: " + DateTime.Now + "\r\n";
                var sourceFile = @"Source file: " + SourceFileUri + "\r\n";
                var detailInfo = timeStamp;

                DialogResult result = DialogResult.Yes;

                if (NumberOfVertices < 0)
                {
                    result = MessageBox.Show(@"The number of vertices is less than 0!" + detailInfo,
                        @"Error in results. Error collection has stopped. Press Yes to continue collecting and ignore this error or No to stop.",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                }

                if (result == DialogResult.No)
                {
                    if (Application.MessageLoop)
                    {
                        // WinForms app
                        Application.Exit();
                    }
                    else
                    {
                        // Console app
                        Environment.Exit(1);
                    }
                }

                tw.WriteLine("{0}) Vertices = {1}.", counter, NumberOfVertices);
                counter++;


                if (ConnectedEdges < 0)
                {
                    result = MessageBox.Show(@"The number of edges is less than 0!" + detailInfo,
                        @"Error in results. Error collection has stopped. 
                        Press Yes to continue collecting and ignore this error or No to stop.", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Error);
                }

                if (result == DialogResult.No)
                {
                    if (Application.MessageLoop)
                    {
                        // WinForms app
                        Application.Exit();
                    }
                    else
                    {
                        // Console app
                        Environment.Exit(1);
                    }
                }

                //ConnectedEdges has the same value as _nodeXLControl.Graph.Edges.Count
                //The issue of having wrong number edges was that  
                tw.WriteLine("{0}) Edges/Links = {1}.", counter, ConnectedEdges);
                //tw.WriteLine("{0}) Edges/Links = {1}.", counter, _nodeXLControl.Graph.Edges.Count);
                counter++;


                GraphMetricCalculatorBase.SetSnapGraphMetricCalculatorPath(SnapCalculatorLocation); //could be removed
                OverallMetricCalculator overallMetricCalculator = new OverallMetricCalculator();
                //GraphMetricCalculatorBase.SetSnapGraphMetricCalculatorPath(SnapCalculatorLocation);

                //overallMetricCalculator.CalculateGraphMetrics(Graph).AverageGeodesicDistance;

                //overallMetricCalculator
                //GraphMetricCalculatorBase.SetSnapGraphMetricCalculatorPath() method.

                if (overallMetricCalculator.CalculateGraphMetrics(Graph).Modularity == null)
                {
                    try
                    {
                        Modularity modularity = new Modularity(clusterAlgorithm, Graph, ConnectedEdges, VertexStartId);
                        tw.WriteLine("{0}) Clustering algorithm = {1}.", counter, modularity.ClusteringAlgorithm);
                        counter++;
                        tw.WriteLine("{0}) Graph communities = {1}.", counter, modularity.CommunitiesCount);
                        counter++;
                        tw.WriteLine("{0}) Modularity = {1}.", counter, modularity.ModularityValue);
                        counter++;
                    }
                    catch (Exception)
                    {
                        tw.WriteLine("{0}) Clustering algorithm = {1}.", counter, @"NodeXL could not determine.");
                        counter++;
                        tw.WriteLine("{0}) Graph communities = {1}.", counter, @"NodeXL could not determine.");
                        counter++;
                        tw.WriteLine("{0}) Modularity = {1}.", counter, @"NodeXL could not determine.");
                        counter++;
                    }
                }
                else
                {
                    tw.WriteLine("{0}) Modularity = {1}.", counter,
                        overallMetricCalculator.CalculateGraphMetrics(Graph).Modularity);
                    counter++;
                }


                //overallMetricCalculator.CalculateGraphMetrics(Graph).Vertice

                tw.WriteLine("{0}) Density = {1}.", counter,
                    overallMetricCalculator.CalculateGraphMetrics(Graph).GraphDensity);
                counter++;

                //The length of the maximum geodesic in a given graph is called the graph diameter, 
                //and the length of the minimum geodesic is called the graph radius
                tw.WriteLine("{0}) Diameter = {1}.", counter,
                    overallMetricCalculator.CalculateGraphMetrics(Graph).MaximumGeodesicDistance);
                counter++;
                tw.WriteLine("{0}) Mean-shortest-path (MSP) = {1}.", counter,
                    overallMetricCalculator.CalculateGraphMetrics(Graph).AverageGeodesicDistance);
                counter++;
                tw.WriteLine("{0}) Degree = {1}.", counter, GraphDegree);
                counter++;
                tw.WriteLine("{0}) Clustering coefficient = {1}.", counter, GraphClusteringCoefficient);
                counter++;

                //check if it has the BarabasiMetric set to true, i.e. it is an iterative call to write out the Barabasi values
                if (SetUpBarabasi)
                {
                    tw.WriteLine("{0}) Relative size of largest component = {1}.", counter,
                        CalculatedBarabasiRelativeSizeOfOriginalLargestComponent);
                    counter++;
                    tw.WriteLine("{0}) Mean size of remaining components = {1}.", counter,
                        CalculatedBarabasiMeanSizeOfAllComponentsMinusLargest);
                    counter++;
                    tw.WriteLine("{0}) Number of all components = {1}.", counter,
                        NumberOfAllComponents);
                    counter++;
                    tw.WriteLine("{0}) Current size of current largest component = {1}.", counter,
                        CurrentSizeOfCurrentLargestComponent);
                    counter++;
                    tw.WriteLine("{0}) Highest Degree = {1}.", counter, HighestVertexDegree);
                    counter++;
                    tw.WriteLine("{0}) Percentage of Network Deleted = {1}.", counter, PercentageOfNetworkDeleted);

                }
                tw.Close();
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }

            //wait until the files have been written before returning true.
            //The reason being if this is being called many times in a loop, 
            //then some files will not be written pending release of the file writer.
            //AwaitFile(GraphOutputResultFileURI);//Resolved by writing multiple files in same folder
            return true;
        }

        public bool DeleteVertex(int vertexId)
        {
            try
            {
                _nodeXLControl.Graph.Vertices.Remove(vertexId);
                VertexStartId++;
                NumberOfVertices--;
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(@"Something went wrong while deleting node " + vertexId + @". Program terminating!" +
                                e.Message);
                return false;
            }
        }

        public bool DeleteVertex(IVertex vertex)
        {
            try
            {
                _nodeXLControl.Graph.Vertices.Remove(vertex);
                VertexStartId++;
                NumberOfVertices--;
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(@"Something went wrong while deleting node " + vertex.ID + @". Program terminating!" +
                                e.Message);
                return false;
            }
        }


        public bool DeleteVerticesWithDegree(int vertexId)
        {
            try
            {
                //get all the vertices with degree = degreeToRemove
                var listOfVericesToRemove = _nodeXLControl.Graph.Vertices.Where(v => v.Degree == vertexId);

                List<int> hitList = new List<int>();
                foreach (var vertex in listOfVericesToRemove)
                {
                    hitList.Add(vertex.ID);
                }

                listOfVericesToRemove = null;

                foreach (var vertexID in hitList)
                {
                    _nodeXLControl.Graph.Vertices.Remove(vertexID);
                    //VertexStartID++;
                    NumberOfVertices--;
                }

                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(@"Something went wrong while deleting nodes with degree " + vertexId +
                                @". Program terminating!" + e.Message);
                return false;
            }
        }


        //public void DetermineTheConnectedComponents(ClusterAlgorithm clusterAlgorithm)
        public void DetermineTheConnectedComponents(int runId)
        {
            ConnectedComponentCalculator connectedComponentCalculator = new ConnectedComponentCalculator();
            var lists = connectedComponentCalculator.CalculateStronglyConnectedComponents(Graph, true);

            int largestListSize = 0;
            var largestList = lists.OrderByDescending(m => m.Count).FirstOrDefault();
            if (largestList != null)
                largestListSize = largestList.Count;


            //set the original largest sized component on the first run
            if (runId == 1)
                BarabasiMetrics.OriginalSizeOfLargestComponent = largestListSize;


            var sizeOfAllLists = lists.Sum(childList => childList.Count);

            BarabasiSingleRunResults = new BarabasiSingleRunResults(runId, lists.Count, largestListSize,
                sizeOfAllLists, BarabasiMetrics.OriginalSizeOfLargestComponent);

            BarabasiMetrics.AddNewEntry(BarabasiSingleRunResults);

            CalculatedBarabasiRelativeSizeOfOriginalLargestComponent = BarabasiMetrics.CalculatedRelativeSizeOfLargestComponent;
            CalculatedBarabasiMeanSizeOfAllComponentsMinusLargest = BarabasiMetrics.CalculatedMeanSizeOfAllComponentsMinusLargest;
            CurrentSizeOfCurrentLargestComponent = BarabasiMetrics.CurrentSizeOfCurrentLargestComponent;
            NumberOfAllComponents = BarabasiMetrics.NumberOfAllComponents;
        }

        //Create a global list that keeps the components information from the different runs

        public void ClearEdges()
        {
            _nodeXLControl.Graph.Edges.Clear();
            Matrix = new string[_nodeXLControl.Graph.Edges.Count][];
            Degree = new List<double>();
            EigenvectorCentrality = new List<double>();
            BetweennessCentrality = new List<double>();
            ClosenessCentrality = new List<double>();
            ClusteringCoefficient = new List<double>();
        }
    }
}


#region commented out

//This was commented out but I brought it back 
//public void ClearEdges()
//{
//    //_nodeXLControl.Graph.Vertices.Clear();
//    //_nodeXLControl.Graph.Edges.Clear();   
//    //_nodeXLControl.ClearGraph();

//    //Graph.Vertices.
//    //Graph.Edges.Clear();

//    //_nodeXLControl.Graph.Vertices.Clear();

//    var size = _nodeXLControl.Graph.Edges.Count;
//    _nodeXLControl.Graph.Edges.Clear();
//    //_nodeXLControl.ClearGraph();
//    //Graph = new Graph();

//    //_nodeXLControl.ClearGraph();

//    Matrix = new string[size][];
//    Degree = new List<double>();
//    EigenvectorCentrality = new List<double>();
//    BetweennessCentrality = new List<double>();
//    ClosenessCentrality = new List<double>();
//    ClusteringCoefficient = new List<double>();
//}


//public string CalculatePlotRank(List<double> entries)
//{
//    var result = "";
//    //List<NumberRank> numberRanks = entries.OrderByDescending(n => n).Select((n, i) => new NumberRank(n, i + 1)).ToList();

//    //// check it worked
//    //foreach (NumberRank nr in numberRanks)
//    //    result += string.Format("{0} : {1}", nr.Rank, nr.Number) + "\r\n";
//    //return result;

//    //var numberRanks = entries.GroupBy(d => d)
//    //    .SelectMany(g => g.OrderByDescending(y => y)
//    //        .Select((x, i) => new {g.Key, Item = x, Rank = i + 1}));

//    //List<NumberRank> numberRanks = (List<NumberRank>) entries.GroupBy(d => d)
//    //    .SelectMany(g => g.OrderByDescending(y => y)
//    //        .Select((x, i) => new NumberRank(g.Key, i + 1))).ToList();


//    List<NumberRank> numberRanks = (List<NumberRank>)entries.OrderByDescending(x => x)
//        .GroupBy(x => x)
//        .SelectMany((g, i) =>
//            g.Select(e => new NumberRank(e, i + 1))).ToList();
//    var rank = numberRanks.Distinct(new NumberRankComparer()).ToList();


//    //http://stackoverflow.com/questions/7285714/linq-with-groupby-and-count


//    //            After calling GroupBy, you get a series of groups IEnumerable<Grouping>, where each Grouping itself exposes the Key used to create the group and also is an IEnumerable<T> of whatever items are in your original data set. You just have to call Count() on that Grouping to get the subtotal.

//    //foreach (var line in data.GroupBy(info => info.metric)
//    //                        .Select(group => new {
//    //                            Metric = group.Key,
//    //                            Count = group.Count()
//    //                        })
//    //                        .OrderBy(x => x.Metric)
//    //{
//    //                Console.WriteLine("{0} {1}", line.Metric, line.Count);
//    //            }

//    return result;

//}
//public void PopulateGraphWithMatrixValuesNoHeaders()
//{
//    throw new System.NotImplementedException();
//}


//void AwaitFile(string fileURI)
//{
//    //Your File
//    var file = new FileInfo(fileURI);

//    //While File is not accesable because of writing process
//    while (IsFileLocked(file)) { }

//    //File is available here

//}
//private bool IsFileLocked(FileInfo file)
//{
//    FileStream stream = null;

//    try
//    {
//        stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
//    }
//    catch (IOException)
//    {
//        //the file is unavailable because it is:
//        //still being written to
//        //or being processed by another thread
//        //or does not exist (has already been processed)
//        return true;
//    }
//    finally
//    {
//        if (stream != null)
//            stream.Close();
//    }

//    //file is not locked
//    return false;
//}


//Testing the modularity and other metrics. These metrics are in a private method that is 
//called inside the Modularity
#region testing before I figured out the components:

////////int iConnectedComponents;
////////int iSingleVertexConnectedComponents;
////////int iMaximumConnectedComponentVertices;
////////int iMaximumConnectedComponentEdges;

//////OverallMetricCalculator overallMetricCalculator = new OverallMetricCalculator();
//////int number = overallMetricCalculator.CalculateGraphMetrics(Graph).ConnectedComponents;

//////if (overallMetricCalculator.CalculateGraphMetrics(Graph).Modularity == null)
//////{
//////    Modularity modularity = new Modularity(clusterAlgorithm, Graph, ConnectedEdges, VertexStartID);
//////    var x = modularity.ClusteringAlgorithm;   
//////    var y = modularity.CommunitiesCount;      
//////    var z = modularity.ModularityValue;       
//////}
/// 
///             //OverallMetricCalculator overallMetricCalculator = new OverallMetricCalculator();

//overallMetricCalculator.CalculateConnectedComponentMetrics(_nodeXLControl.Graph, out iConnectedComponents,
//    out iSingleVertexConnectedComponents, out iMaximumConnectedComponentEdges,
//    out iMaximumConnectedComponentEdges);

#endregion
#endregion
