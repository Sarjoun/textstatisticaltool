﻿namespace SDNodeXLEngineLibrary
{
    public class NumberRank
    {
        public double Number { get; set; }
        public int Rank { get; set; }

        public NumberRank(double number, int rank)
        {
            Number = number;
            Rank = rank;
        }
    }
}
