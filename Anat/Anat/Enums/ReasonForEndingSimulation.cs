﻿using System.ComponentModel;

namespace Anat.Enums
{
    public enum ReasonForEndingSimulation
    {
        [Description("None")]
        None,
        [Description("Number of TaggedDocuments < threshold.")]
        NumberOfTaggedDocumentsLessThanThreshold,
        [Description("All documents where tagged.")]
        AllDocumentsWhereTagged,
        [Description("Ratio of (TaggedDocuments/TotalDocuments) < threshold.")]
        RatioOfTaggedDocumentsToTotalLessThanThreshold,
        [Description("Ratio of (Remaining UnTaggedDocuments)/TotalDocuments < (threshold x TotalDocuments).")]
        RemainingUntaggedDocumentsLessThanThreshold,
        [Description("Current code is not available for public sharing.")]
        CodeNotForPublicSharing

    }
}
