﻿using System.Windows;
using Anat.ViewModel;

namespace Anat.Views
{
    /// <summary>
    /// Interaction logic for TextStatisticsWindow.xaml
    /// </summary>
    public partial class TextStatisticsWindow : Window
    {
        public static readonly DependencyProperty ContextProperty = DependencyProperty.Register(
            "Context",
            typeof(ITextStatisticsWindowContext),
            typeof(TextStatisticsWindow),
            new PropertyMetadata(default(ITextStatisticsWindowContext)));

        public ITextStatisticsWindowContext Context
        {
            get { return (ITextStatisticsWindowContext)GetValue(ContextProperty); }
            set { SetValue(ContextProperty, value); }
        }

        public TextStatisticsWindow()
        {
            InitializeComponent();
            Context = new TextStatisticsWindowContext();
        }
    }
}
