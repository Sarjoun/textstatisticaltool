﻿using System.Windows;
using Anat.ViewModel;

namespace Anat.Views
{
    /// <summary>
    /// Interaction logic for AnatWindow.xaml
    /// </summary>
    public partial class AnatWindow : Window
    {
        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContextProperty = DependencyProperty.Register("Context", 
            typeof(IAnatWindowContext), 
            typeof(AnatWindow), 
            new PropertyMetadata(default(IAnatWindowContext)));

        public IAnatWindowContext Context
        {
            get { return (IAnatWindowContext)GetValue(ContextProperty); }
            set { SetValue(ContextProperty, value); }
        }

        
        public AnatWindow()
        {
            InitializeComponent();
            Context = new AnatWindowContext();
        }
    }
}
