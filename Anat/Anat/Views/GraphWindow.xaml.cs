﻿using System.Windows;
using Anat.ViewModel;

namespace Anat.Views
{
    /// <summary>
    /// Interaction logic for GraphWindow.xaml
    /// </summary>
    public partial class GraphWindow : Window
    {
        public static readonly DependencyProperty ContextProperty = DependencyProperty.Register(
            "Context", 
            typeof(IGraphWindowContext), 
            typeof(GraphWindow), 
            new PropertyMetadata(default(IGraphWindowContext)));

        public IGraphWindowContext Context
        {
            get { return (IGraphWindowContext) GetValue(ContextProperty); }
            set { SetValue(ContextProperty, value); }
        }

        public GraphWindow()
        {
            InitializeComponent();
            Context = new GraphWindowContext();
        }
    }
}
