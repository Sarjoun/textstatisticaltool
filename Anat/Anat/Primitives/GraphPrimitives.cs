﻿using SDParser.Primitives;

namespace Anat.Primitives
{
    public class GraphsOutputFolderPath : OneWayTypeDef<string, GraphsOutputFolderPath>
    {
        public GraphsOutputFolderPath(string value) : base(value) { }
    }
}

