﻿using System;
using System.Diagnostics;
using System.IO;
using Anat.Extensions;
using Anat.Primitives;

namespace Anat.Helpers
{
    public static class FileHelper
    {
        public static string SolutionName => Path.
            GetFileName(
                Process.
                    GetCurrentProcess().
                    MainModule.
                    FileName.
                    Replace(".vshost.exe", ""));

        public static string StartupFolderPath => Environment.GetFolderPath(
            Environment.SpecialFolder.Desktop);
        public static string OutputFolderPath => Environment.GetFolderPath(
           Environment.SpecialFolder.Desktop) + @"\Anat" + ".Output";
      //Environment.SpecialFolder.Desktop) + @"\" + SolutionName + ".Output";


      //The output for now should be on a folder on the Desktop
      public static GraphsOutputFolderPath CheckForOutputFolderAndCreateItIfNotFound()
        {
            try
            {
                if (!Directory.Exists(OutputFolderPath))
                {
                    Directory.CreateDirectory(OutputFolderPath);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Catastrophic error in creating output folder CheckOutputFolder()." + e.Message);
                throw;
            }
            return GraphsOutputFolderPath.FromValue(OutputFolderPath);
        }


        public static bool CheckForFolderAndCreateItIfNotFound(string folderPath)
        {
            try
            {
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Catastrophic error in creating output folder " + folderPath + ". " + e.Message);
                return false;
            }
            return true;
        }

        public static string GenerateARandomFileForOutput(string fileExtension) => OutputFolderPath
                + @"\"
                + SolutionName
                + fileExtension
                .AppendTimeStamp();


        public static string[] ReadFile(string fileName)
        {
            return File.ReadAllLines(fileName);
            //var fileContents = "";
            //try
            //{
            //    TextReader tr = new StreamReader(fileName);
            //    string line;
            //    // read all lines of text
            //    while ((line = tr.ReadLine()) != null)
            //    {
            //        fileContents += line + "\r\n";
            //    }
            //}
            //catch (Exception exc)
            //{
            //    Debug.WriteLine("Error in reading filename. " + exc.Message);
            //    if (exc.InnerException != null)
            //    {
            //        Debug.WriteLine("Inner exception: " + exc.InnerException.Message);
            //    }
            //}
            //return fileContents;
        }

    }

}
