﻿using System;
using Anat.Enums;
using SDParser.Primitives;

namespace Anat.Helpers
{
    public static class SimulationRunMetricsHelper
    {
        public static bool CheckIfStartAndTaggedDocumentsCountsAreNotEqual(float taggedDocuments, float totalDocuments, float threshold)
        {
            return Math.Abs(taggedDocuments - totalDocuments) >= threshold;
        }
        public static bool CheckIfTaggedDocumentsCountMoreThanThreshold(float taggedDocuments, float totalDocuments, float threshold)
        {
            return (taggedDocuments / totalDocuments >= threshold);
        }
        public static bool CheckIfRemainingUnTaggedDocumentsCountLessThanThreshold(float originalStartDocuments, float totalTaggedDocuments, float threshold)
        {
            return (originalStartDocuments - totalTaggedDocuments >= threshold * originalStartDocuments);
        }

        public static ReasonForEndingSimulation CheckConditionsToKeepGoing(float countOfTierStartingDocuments, float countOfTierTaggedDocuments)
        {
            if (!(countOfTierTaggedDocuments >= 100))
                return ReasonForEndingSimulation.NumberOfTaggedDocumentsLessThanThreshold;

            if (!CheckIfStartAndTaggedDocumentsCountsAreNotEqual(countOfTierTaggedDocuments, countOfTierStartingDocuments, 0.1f))
                return ReasonForEndingSimulation.AllDocumentsWhereTagged;

            if (CheckIfTaggedDocumentsCountMoreThanThreshold(countOfTierTaggedDocuments, countOfTierStartingDocuments, 0.1f))
            {
                return CheckIfRemainingUnTaggedDocumentsCountLessThanThreshold(countOfTierStartingDocuments, countOfTierTaggedDocuments, 0.2f)
                    ? ReasonForEndingSimulation.None
                    : ReasonForEndingSimulation.RemainingUntaggedDocumentsLessThanThreshold;
            }

            return ReasonForEndingSimulation.RatioOfTaggedDocumentsToTotalLessThanThreshold;
        }

        public static string GetTheSimulationTimeFileName(DocumentsLinksFileName documentsLinksFileName)
        {
            var temp = documentsLinksFileName.Value.
                Substring(0, documentsLinksFileName.Value.LastIndexOf(@"\", StringComparison.Ordinal));

            return temp.Substring(0, temp.LastIndexOf(@"\", StringComparison.Ordinal) + 1) + "SimulationTrace.info";
        }

    }
}
