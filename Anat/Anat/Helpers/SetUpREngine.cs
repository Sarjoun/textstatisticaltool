﻿using System;
using RDotNet;

namespace Anat.Helpers
{
    public static class SetupREngine
    {
        const string RVersion1 = @"R-3.4.4"; //Laptop

        const string RVersion2 = @"R-3.4.4"; //Desktop
        //const string RVersion3 = @"R-3.5.1"; //is not supported in RDotNet

        public static REngine GetREngine()
        {
            var RVersion = "";
            //RVersion = RVersion2;
            RVersion = RVersion1;

            var rhome = @"C:/Program Files/R/" + RVersion;
            var rpath = @"C:/Program Files/R/" + RVersion + @"/bin/x64";
            Console.Write("\r\nSet-up REngine.");
            REngine.SetEnvironmentVariables(rpath, rhome);
            Console.Write("\r\nCreate REngine.");
            return REngine.GetInstance();
        }
    }
}

