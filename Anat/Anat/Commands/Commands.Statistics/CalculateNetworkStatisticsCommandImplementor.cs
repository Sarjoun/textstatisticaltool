﻿using System;
using System.Windows.Input;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Statistics
{
    public class CalculateNetworkStatisticsCommandImplementor : ICommand
    {
        private readonly AnatWindowContext _context;
        private bool _canExecute;

        public CalculateNetworkStatisticsCommandImplementor(AnatWindowContext context)
        {
            _context = context;
            _canExecute = true;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute;
        }

        public void Execute(object parameter)
        {
            if (!CanExecute(parameter)) return;
            _context.CalculateNetworkStatisticsAndWriteOutput();
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}
