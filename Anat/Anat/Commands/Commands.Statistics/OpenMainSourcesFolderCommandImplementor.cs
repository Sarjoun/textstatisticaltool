﻿using System;
using System.Windows.Input;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Statistics
{
    public class OpenMainSourcesFolderCommandImplementor : ICommand
    {
        private readonly TextStatisticsWindowContext _context;

        public OpenMainSourcesFolderCommandImplementor(TextStatisticsWindowContext context)
        {
            _context = context;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _context.OpenMainFolderAndReturnAllSourcesPaths();
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}