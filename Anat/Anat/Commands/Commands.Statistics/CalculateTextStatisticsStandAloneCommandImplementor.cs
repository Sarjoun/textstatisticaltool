﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Statistics
{
    public class CalculateTextStatisticsStandAloneCommandImplementor : ICommand
    {
        private readonly TextStatisticsWindowContext _context;
        private bool _canExecute;

        public CalculateTextStatisticsStandAloneCommandImplementor(TextStatisticsWindowContext context)
        {
            _context = context;
            _canExecute = true;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute;
        }

        public async void Execute(object parameter)
        {
            try
            {
                await Task.Run(async () =>
                {
                    await _context.ProcessTextStatisticsFromFoldersOriginalAsync();
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Application.Current.Shutdown();
            }
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}
