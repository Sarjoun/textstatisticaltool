﻿using System;
using System.Windows.Input;
using Anat.Enums;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Statistics
{
    class ParseIONASettingsFileAndExtractTextStatisticsTypesCommandImplementor : ICommand
    {
        private readonly TextStatisticsWindowContext _context;

        public ParseIONASettingsFileAndExtractTextStatisticsTypesCommandImplementor(TextStatisticsWindowContext context)
        {
            _context = context;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public async void Execute(object parameter)
        {
            await _context.OpenAndParseSimulationFileAndExtractTextStatisticsSettingsAsync(ShowParsedFile.Yes);
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}
