﻿using System;
using System.Windows.Input;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Statistics
{
    internal class OpenTextStatisticsWindowCommandImplementor : ICommand
    {
        private readonly AnatWindowContext _context;

        public OpenTextStatisticsWindowCommandImplementor(AnatWindowContext context)
        {
            _context = context;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _context.OpenTextStatisticsWindow();
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}