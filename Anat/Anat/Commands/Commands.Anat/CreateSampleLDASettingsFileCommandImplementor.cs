﻿using System;
using System.Windows.Input;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Anat
{
    public class CreateSampleLDASettingsFileCommandImplementor : ICommand
    {
        private readonly AnatWindowContext _context;
        private bool _canExecute;

        public CreateSampleLDASettingsFileCommandImplementor(AnatWindowContext context)
        {
            _context = context;
            _canExecute = true;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute;
        }

        public void Execute(object parameter)
        {
            if (!CanExecute(parameter)) return;
            _context.WriteLDASampleRunFileOnDesktop();
            //await _context.WriteLDASampleRunFileOnDesktop();
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}