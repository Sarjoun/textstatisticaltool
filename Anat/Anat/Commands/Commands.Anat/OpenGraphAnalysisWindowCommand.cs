﻿using System;
using System.Windows.Input;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Anat
{
    internal class OpenGraphAnalysisWindowCommand : ICommand
    {
        private readonly AnatWindowContext _context;

        public OpenGraphAnalysisWindowCommand(AnatWindowContext context)
        {
            _context = context;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _context.OpenGraphicsWindow();
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}