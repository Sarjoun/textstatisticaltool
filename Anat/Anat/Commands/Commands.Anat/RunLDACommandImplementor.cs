﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Anat
{
    public class RunLDACommandImplementor : ICommand
    {
        private readonly AnatWindowContext _context;

        public RunLDACommandImplementor(AnatWindowContext context)
        {
            _context = context;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public async void Execute(object parameter)
        {
            try
            {
                await Task.Run(async () =>
                {
//                    await _context.RunLDAAsync();
                });

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Application.Current.Shutdown();
                //throw;
            }
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}
