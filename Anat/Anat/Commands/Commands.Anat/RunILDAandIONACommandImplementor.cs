﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Anat
{
    public class RunILDAandIONACommandImplementor : ICommand
    {
        private readonly AnatWindowContext _context;

        public RunILDAandIONACommandImplementor(AnatWindowContext context)
        {
            _context = context;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public async void Execute(object parameter)
        {
            try
            {
                await Task.Run(async () =>
                {
                    //await _context.RunILDAandIONAandStatisticsAsync();
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Application.Current.Shutdown();
                //throw;
            }
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}
