﻿using System;
using System.Windows.Input;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Anat
{
    public class OpenDataFileCommandImplementor : ICommand
    {
        private readonly AnatWindowContext _context;

        public OpenDataFileCommandImplementor(AnatWindowContext context)
        {
            _context = context;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _context.OpenFileAndDisplayContent();
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}
