﻿using System;
using System.Windows.Input;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Anat
{
    public class ParseLDASettingsFileAndTransformIntoTypesCommandImplementor : ICommand
    {
        private readonly AnatWindowContext _context;

        public ParseLDASettingsFileAndTransformIntoTypesCommandImplementor(AnatWindowContext context)
        {
            _context = context;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public async void Execute(object parameter)
        {
            await _context.OpenAndParseLDAFileIntoTypes();
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}
