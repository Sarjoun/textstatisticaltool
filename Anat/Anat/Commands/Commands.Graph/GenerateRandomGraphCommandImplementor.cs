﻿using System;
using System.Windows.Input;
using Anat.GraphWindow.GraphViz.GraphViz.TextParser;
using Anat.Helpers;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Graph
{
    public class GenerateRandomGraphCommandImplementor : ICommand
    {
        private readonly GraphWindowContext _context;
        private bool _canExecute;

        public GenerateRandomGraphCommandImplementor(GraphWindowContext context)
        {
            _context = context;
            _canExecute = true;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute;
        }

        public async void Execute(object parameter)
        {
            if (!CanExecute(parameter)) return;

            _canExecute = false;
            _canExecute =
                await _context.RenderAndDisplayRandomGraphTaskAsync(FileHelper.GenerateARandomFileForOutput(".png"));
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}
