﻿using System;
using System.Windows.Input;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Graph
{
    public class ConvertDataFileIntoGraphCommandImplementor : ICommand
    {
        private readonly GraphWindowContext _context;

        public ConvertDataFileIntoGraphCommandImplementor(GraphWindowContext context)
        {
            _context = context;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public async void Execute(object parameter)
        {
            await _context.OpenFileAndGenerateGraphAsync();
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}
