﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Graph
{
    public class ConvertDataFileIntoGraphThenMergeCommandImplementor : ICommand
    {
        private readonly GraphWindowContext _context;

        public ConvertDataFileIntoGraphThenMergeCommandImplementor(GraphWindowContext context)
        {
            _context = context;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public async void Execute(object parameter)
        {
            await _context.OpenFileAndGenerateGraphThenMergeAsync();
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}