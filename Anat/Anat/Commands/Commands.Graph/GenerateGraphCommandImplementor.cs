﻿using System;
using System.Windows.Input;
using Anat.GraphWindow.GraphViz.GraphViz.TextParser;
using Anat.Helpers;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Graph
{
    public class GenerateGraphCommandImplementor : ICommand
    {
        private readonly GraphWindowContext _context;
       
        public GenerateGraphCommandImplementor(GraphWindowContext context)
        {
            _context = context;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public async void Execute(object parameter)
        {
            await _context.RenderAndDisplayDefaultStaticGraphTaskAsync(FileHelper.GenerateARandomFileForOutput(".png"));
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}
