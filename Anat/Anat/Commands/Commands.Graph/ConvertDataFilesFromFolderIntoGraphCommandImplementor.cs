﻿using System;
using System.Windows.Input;
using Anat.ViewModel;

namespace Anat.Commands.Commands.Graph
{
    public class ConvertDataFilesFromFolderIntoGraphCommandImplementor : ICommand
    {
        private readonly GraphWindowContext _context;

        public ConvertDataFilesFromFolderIntoGraphCommandImplementor(GraphWindowContext context)
        {
            _context = context;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _context.OpenFolderAndGenerateGraphAsync();
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }
}
