﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Anat.Extensions
{
    public static class ListExtensions
    {
        public static List<T> FluentAdd<T>(this List<T> list, T item)
        {
            list.Add(item);
            return list;
        }

        public static List<T> FluentClear<T>(this List<T> list)
        {
            list.Clear();
            return list;
        }

        public static List<T> FluentForEach<T>(this List<T> list, Action<T> action)
        {
            list.ForEach(action);
            return list;
        }

        public static List<T> FluentInsert<T>(this List<T> list, int index, T item)
        {
            list.Insert(index, item);
            return list;
        }

        public static List<T> FluentRemoveAt<T>(this List<T> list, int index)
        {
            list.RemoveAt(index);
            return list;
        }

        public static List<T> FluentReverse<T>(this List<T> list)
        {
            list.Reverse();
            return list;
        }

        public static IEnumerable<TResult> SelectTwo<TSource, TResult>(this IEnumerable<TSource> source,
            Func<TSource, TSource, TResult> selector)
        {
            var enumerable = source as TSource[] ?? source.ToArray();
            return enumerable.Zip(enumerable.Skip(1), selector);
            /*
             * Example: Enumerable.Range(1,5).SelectTwo((a,b) => $"({a},{b})");
             * Results in:(1,2) (2,3) (3,4) (4,5)
             */
        }
    }
}

