﻿using System;
using System.Collections.Generic;

namespace Anat.Extensions
{
    public static class EnumerableExtensions
    {
        //public static string[] ForEach<T>(this IEnumerable<T> source, Action<T> action)
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (T item in source)
                action(item);
            //return null;
        } // [uses]   => myList.Where( ... ).ForEach( ... );
    }
}


