﻿using System;
using System.IO;
using System.Linq;
using Anat.GraphWindow.GraphViz.GraphViz.Structure;

namespace Anat.Extensions
{
    public static class StringExtensions
    {
        private static readonly char[] BufferChars = { '-', ' ' };
        private static readonly string[] EdgeSeparators = { "+" };

        public static string SafeRemove(this string s, int numCharactersToRemove)
        {
            if (numCharactersToRemove > s.Length)
            {
                throw new ArgumentException("numCharactersToRemove");
            }

            // other validation here

            return s.Remove(s.Length - numCharactersToRemove);
        }

        public static string AppendTimeStamp(this string fileName)
        {
            return string.Concat(
                Path.GetFileNameWithoutExtension(fileName),
                DateTime.Now.ToString("-MMMdd-htt-mmss"),
                Path.GetExtension(fileName)
            );
        }

        public static string GenerateRandomNodeName(this string name, int numCharacters)
        {
            var randomGenerator = new Random();
            byte[] randomBytes = new byte[randomGenerator.Next(numCharacters)];
            randomGenerator.NextBytes(randomBytes);
            return Convert.ToBase64String(randomBytes);
        }
        
        public static string StripExtraCharsForGraphLine(this string name)
        {
            return name.Trim(BufferChars);
        }

        public static string[] ExtractNgrams(this string name)
        {
            return name.
                Split(BufferChars, StringSplitOptions.None)
                .Where(x => x != string.Empty).ToArray();
        }

        //The before the first + symbol: all nodes values
        //Between the first + and second +: all edges values
        //after the second +: all the graph properties
        public static SdGraphNewDataStructure ExtractNgramsUsingNewFormats(this string data)
        {
            var splitData = data.Split('+');
            return new SdGraphNewDataStructure(
                splitData[0].Trim().Split(' '),
                splitData[1].Trim().Split(' '),
                splitData[2].Trim().Split(' '),
                splitData[3].Trim().Split(' '));
        }
        
        //public static string[] ExtractUniqueNodes(this string name)
        //{
        //    return name.
        //        Split(EdgeSeparators, StringSplitOptions.None)
        //        .Where(x => x != string.Empty)
        //        .Distinct().ToArray();
        //}

        public static string[] ExtractNodesFromNgram(this string name)
        {
            return name.
                Split(EdgeSeparators, StringSplitOptions.None)
                .Where(x => x != string.Empty).ToArray();
        }

        public static string[] ExtractUniqueNodesDisregardNGramSeparations(this string name)
        {
            return name.
                Split(EdgeSeparators, StringSplitOptions.RemoveEmptyEntries)
                .Distinct()
                .ToArray();
        }

        public static string[] ExtractAllNodes(this string name)
        {
            return name.Split(EdgeSeparators, StringSplitOptions.None);
        }
        
    }
}