﻿using System;

namespace Anat.GraphWindow.GraphViz.GraphViz.Structure
{
    public class SdEdge : ISdEdge
    {
        public string Name { get; set; }
        public double? Weight { get; set; }
        //public string WeightText { get; set; }
        public SdNode HeadNode { get; set; }
        public SdNode TailNode { get; set; }


        public SdEdge()
        { }


        public SdEdge(string name, double? weight)
        {
            Name = name;
            Weight = weight;
            //WeightText = weight.ToString();
        }

        public SdEdge(string name, string weightText)
        {
            Name = name;
            //WeightText = weightText;
            Weight = Convert.ToDouble(weightText);
        }

        public SdEdge Update(double value)
        {
            if (Weight.HasValue)
                Weight += value;
            else
                Weight = value;
            return this;
        }


    }
}