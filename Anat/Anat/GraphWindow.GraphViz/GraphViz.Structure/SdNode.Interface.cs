namespace Anat.GraphWindow.GraphViz.GraphViz.Structure
{
   public interface ISdNode
   {
       string Name { get; set; }
       double? Degree { get; set; }
       double? InDegree { get; set; }
       double? OutDegree { get; set; }
        string Style { get; set; }
        string Color { get; set; }
        //TODO: Add shape
    }
}

