﻿namespace Anat.GraphWindow.GraphViz.GraphViz.Structure
{
    public enum NodeTypeEnum
    {
        Head = 0,
        Tail = 1,
        None = 3 //Doesn't apply or end node
    }
}
