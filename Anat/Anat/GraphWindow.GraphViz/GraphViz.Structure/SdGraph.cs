﻿using System.Collections.Generic;
using Shields.GraphViz.Models;

namespace Anat.GraphWindow.GraphViz.GraphViz.Structure
{
    public class SdGraph
    {
        public SdNode[] Nodes { get; set; }
        public SdEdge[] Edges { get; set; }
        public IEnumerable<Statement> DecorationStatements { get; set; }

        //private Graph _graph;

        public SdGraph()
        { }

        public SdGraph(SdNode[] nodes, SdEdge[] edges)//, Graph graph)
        {
            //_graph = graph;
            Nodes = nodes;
            Edges = edges;
        }

        public SdGraph(SdNode[] nodes, SdEdge[] edges, IEnumerable<Statement> decorationStatements)
        {
            Nodes = nodes;
            Edges = edges;
            DecorationStatements = decorationStatements;
        }
    }
}
