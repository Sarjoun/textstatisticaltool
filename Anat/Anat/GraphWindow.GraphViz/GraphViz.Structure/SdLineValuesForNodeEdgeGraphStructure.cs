﻿using System.Linq;

namespace Anat.GraphWindow.GraphViz.GraphViz.Structure
{
    public class SdGraphNewDataStructure
    {
        public string[] Nodes { get; set; }
        public string[] NodesDecorations { get; set; }
        public string[] Edges { get; set; }
        public string[] EdgesDecorations { get; set; }

        public SdGraphNewDataStructure(string[] nodes,
            string[] edges, string[] nodesDecorations, string[] edgesDecorations)
        {
            Nodes = nodes.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            Edges = edges.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            NodesDecorations = nodesDecorations.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            EdgesDecorations = edgesDecorations.Where(x => !string.IsNullOrEmpty(x)).ToArray();
        }
    }
}
