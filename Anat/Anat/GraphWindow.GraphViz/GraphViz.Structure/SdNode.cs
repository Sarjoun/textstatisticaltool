﻿namespace Anat.GraphWindow.GraphViz.GraphViz.Structure
{
    public class SdNode : ISdNode
    {
        public SdNode()
        { }

        public SdNode(string name, NodeTypeEnum nodeTypeEnum)
        {
            Name = name;
            Degree = 1;
            switch (nodeTypeEnum)
            {
                case NodeTypeEnum.None:
                    break;
                case NodeTypeEnum.Head:
                    OutDegree = 1;
                    InDegree = 0;
                    break;
                case NodeTypeEnum.Tail:
                    InDegree = 1;
                    OutDegree = 0;
                    break;
            }
        }

        public SdNode(string name, double? degree)
        {
            Name = name;
            Degree = degree;
            OutDegree = 0;
            InDegree = 0;
        }

        public SdNode(string name, double? inDegree, double? outDegree)
        {
            Name = name;
            InDegree = inDegree;
            OutDegree = outDegree;
            Degree = InDegree + OutDegree;
        }

        public SdNode(string name, double? inDegree, double? outDegree, string style, string color)
        {
            Name = name;
            InDegree = inDegree;
            OutDegree = outDegree;
            Style = style;
            Color = color;
        }

        public SdNode Update(NodeTypeEnum nodeTypeEnum)
        {
            Degree++;
            switch (nodeTypeEnum)
            {
                case NodeTypeEnum.None:
                    break;
                case NodeTypeEnum.Head:
                    OutDegree++;
                    break;
                case NodeTypeEnum.Tail:
                    InDegree++;
                    break;
            }
            return this;
        }

        public string Name { get; set; }
        public double? Degree { get; set; }
        public double? InDegree { get; set; }
        public double? OutDegree { get; set; }
        public string Style { get; set; }
        public string Color { get; set; }
    }
}