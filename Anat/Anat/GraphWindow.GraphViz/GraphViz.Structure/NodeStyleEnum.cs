﻿namespace Anat.GraphWindow.GraphViz.GraphViz.Structure
{
    /// <summary>
    /// The enum style names are case-sensitive
    /// </summary>
    public enum NodeStyle
    {
        wedged = 0,
        // ReSharper disable once RedundantCommaInEnumDeclaration
        filled = 1,
        //striped = 2, << bug with graphviz 
        //radial = 1
    }
}