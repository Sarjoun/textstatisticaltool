namespace Anat.GraphWindow.GraphViz.GraphViz.Structure
{
    public interface ISdEdge
    {
        string Name { get; set; }
        double? Weight { get; set; }
        SdNode HeadNode { get; set; }
        SdNode TailNode { get; set; }
    }
}

