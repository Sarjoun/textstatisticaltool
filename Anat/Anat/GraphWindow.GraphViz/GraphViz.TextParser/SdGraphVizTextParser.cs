﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms.VisualStyles;
using Anat.Extensions;
using Anat.GraphWindow.GraphViz.GraphViz.Structure;
using Shields.GraphViz.Models;

namespace Anat.GraphWindow.GraphViz.GraphViz.TextParser
{
    public static class SdGraphVizTextParser
    {
        /*
         * The file format for now is the following:
         * 
         * 1. Process the file line-by-line.
         * 
         * 2. Each word encountered is a node. Create one word object for each occurrence and update its weight.
         * 
         * 3. Each '+' existing between 2 words is a considerd as an edge. 
         * For every (new) '+' connecting 2 words, I will create one edge object for its pair of words. 
         * If the same 2 words are seen connected, then find and update that edge's object's weight.
         * 
         * 
         */

        private static List<SdNode> SdNodes { get; set; }
        private static List<SdEdge> SdEdges { get; set; }
        private static List<string> NodeNames { get; set; }
        public static SdGraph SdGraph { get; set; }


        #region Node operations
        private static SdNode ProcessNode(string word, NodeTypeEnum nodeType)
        {
            return !NodeExists(word)
                ? NewNode(word, nodeType)
                : UpdateNode(word, nodeType);
        }

        private static SdNode ProcessNodeNewFormat(string word, NodeTypeEnum nodeType)
        {
            return !NodeExists(word)
                ? NewNodeNewFormat(word, nodeType)
                : UpdateNodeNewFormat(word, nodeType);
        }

        private static bool NodeExists(string word)
        {
            return SdNodes.Any(n => string.Equals(n.Name.ToLower(), word.ToLower(), StringComparison.Ordinal));
        }

        private static SdNode UpdateNode(string word, NodeTypeEnum nodeTypeEnum)
        {
            Debug.Assert(SdNodes != null, "SdNodes != null");
            var firstOrDefault = SdNodes
                .FirstOrDefault(n => string.Equals(n.Name.ToLower(), word.ToLower(), StringComparison.Ordinal));
            return firstOrDefault?.Update(nodeTypeEnum);
        }

        private static SdNode UpdateNodeNewFormat(string word, NodeTypeEnum nodeTypeEnum)
        {
            Debug.Assert(SdNodes != null, "SdNodes != null");
            var firstOrDefault = SdNodes
                .FirstOrDefault(n => string.Equals(n.Name.ToLower(), word.ToLower(), StringComparison.Ordinal));
            return firstOrDefault?.Update(nodeTypeEnum);
        }

        private static SdNode NewNode(string word, NodeTypeEnum nodeTypeEnum)
        {
            var newNode = new SdNode(word, nodeTypeEnum);
            SdNodes.Add(newNode);
            return newNode;
        }
        private static SdNode NewNodeNewFormat(string word, NodeTypeEnum nodeTypeEnum)
        {
            var newNode = new SdNode(word, nodeTypeEnum);
            SdNodes.Add(newNode);
            return newNode;
        }

        #endregion

        #region Edge operations
        //private static SdEdge ProcessEdge(string headWord, string tailWord)
        //{
        //    return !EdgeExists(headWord, tailWord)
        //        ? AddNewEdge(headWord, tailWord)
        //        : UpdateEdge(headWord, tailWord);
        //}
        private static void ProcessEdgeNoWeights(string headWord, string tailWord)
        {
            if (!EdgeExists(headWord, tailWord))
                NewEdgeNoWeight(headWord, tailWord);
            //else
            //    UpdateEdge(headWord, tailWord);
        }

        private static void ProcessEdgeUpdateUsingCurrentGraphFileEdges(string headWord, string tailWord)
        {
            if (!EdgeExists(headWord, tailWord))
                AddNewEdge(headWord, tailWord);
            else
                UpdateEdge(headWord, tailWord);
        }

        private static void ProcessEdge(string headWord, string tailWord)
        {
            if (!EdgeExists(headWord, tailWord))
                AddNewEdge(headWord, tailWord);
            else
                UpdateEdge(headWord, tailWord);
        }

        //private static void ProcessEdge(string headWord, string tailWord, string weightString)
        //{
        //    if (!EdgeExists(headWord, tailWord))
        //        AddNewEdgeUsingNewFormat(headWord, tailWord, weightString);
        //    else
        //        UpdateEdge(headWord, tailWord);
        //}

        private static void AddNewEdgeUsingNewFormat(string headWord, string tailWord, string weightText)
        {
            SdEdges.Add(new SdEdge
            {
                Name = "",
                Weight = Convert.ToDouble(weightText),
                HeadNode = ProcessNodeNewFormat(headWord, NodeTypeEnum.Head),
                TailNode = ProcessNodeNewFormat(tailWord, NodeTypeEnum.Tail)
            });
        }

        private static void AddNewEdge(string headWord, string tailWord)
        {
            SdEdges.Add(new SdEdge
            {
                Name = "",
                Weight = 1,
                HeadNode = ProcessNode(headWord, NodeTypeEnum.Head),
                TailNode = ProcessNode(tailWord, NodeTypeEnum.Tail)
            });
        }
        private static bool EdgeExists(string headWord, string tailWord)
        {
            return SdEdges.Any(e => e.HeadNode.Name == headWord && e.TailNode.Name == tailWord);
        }

        private static void NewEdgeNoWeight(string headWord, string tailWord)
        {
            SdEdges.Add(new SdEdge
            {
                Name = "",
                HeadNode = ProcessNode(headWord, NodeTypeEnum.Head),
                TailNode = ProcessNode(tailWord, NodeTypeEnum.Tail)
            });
        }

        //private static SdEdge AddNewEdge(string headWord, string tailWord)
        //{
        //    return new SdEdge
        //    {
        //        Name = "",
        //        Weight = 1,
        //        HeadNode = ProcessNode(headWord, NodeTypeEnum.Head),
        //        TailNode = ProcessNode(tailWord, NodeTypeEnum.Tail)
        //    };
        //}

        private static void UpdateEdge(string headWord, string tailWord)
        {
            var sdEdge = SdEdges.FirstOrDefault(e =>
             string.Equals(e.HeadNode.Name.ToLower(), headWord.ToLower(), StringComparison.Ordinal) &&
             string.Equals(e.TailNode.Name.ToLower(), tailWord.ToLower(), StringComparison.Ordinal));
            Debug.Assert(sdEdge != null, "sdEdge != null");

            sdEdge.Weight++;
            sdEdge.HeadNode.Update(NodeTypeEnum.Head);
            sdEdge.TailNode.Update(NodeTypeEnum.Tail);
        }
        #endregion


        #region Main operations that processess all the graph from file

        public static SdGraph PopulateSDGraphUsingNewFormatStringArray(string[] _dataLines)
        {
            SdNodes = new List<SdNode>();
            SdEdges = new List<SdEdge>();

            var dataLines = _dataLines.Where(x => !string.IsNullOrEmpty(x)).ToArray();

            foreach (var line in dataLines)
            {
                ProcessOneLineNodesWithEdgesUsingNewFormat(line);
            }

            //var decorations = AddDecorationsToNodesThatBelongToMultipleAgents(GetAllNodesFromDataLines(dataLines)).ToList();//Later
            var decorations = new List<Statement>();

            if (GetAllNodesFromDataLines(dataLines).Length > 1 &&
                GetAllNodeDecorationsFromDataLines(dataLines).Length > 1)
            {
                try
                {
                    decorations.AddRange(
                        AddDecorationsToNodesIfTheyAreHighCentralityNodes(
                            GetAllNodesFromDataLines(dataLines),
                            GetAllNodeDecorationsFromDataLines(dataLines)).ToList());
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            return SdGraph = new SdGraph(
                SdNodes.ToArray(),
                SdEdges.ToArray(),
                decorations);
        }

        private static string[] GetAllNodesFromDataLines(string[] dataLines)
        {
            var nodes = new List<string>();
            foreach (var line in dataLines)
            {
                nodes.AddRange(line.StripExtraCharsForGraphLine().ExtractNgramsUsingNewFormats().Nodes);
            }
            return nodes.ToArray();
        }

        private static string[] GetAllNodeDecorationsFromDataLines(string[] dataLines)
        {
            var nodes = new List<string>();
            foreach (var line in dataLines)
            {
                nodes.AddRange(line.StripExtraCharsForGraphLine().ExtractNgramsUsingNewFormats().NodesDecorations);
            }
            return nodes.ToArray();
        }

        public static IEnumerable<Statement> AddDecorationsToNodesThatBelongToMultipleAgents(string[] nodeNames)
        {
            var statements = new List<Statement>();
            var groups = nodeNames.GroupBy(v => v);
            foreach (var group in groups)
            {
                if (group.Count() > 1)
                    statements.Add(NodeStatement.For(group.Key).Set("shape", "box")
                        .Set("font", "bold").Set("width", "1").Set("penwidth", "2.5"));
            }
            return statements;
            //var groups = nodeNames.GroupBy(v => v);
            //return (from @group in groups where @group.Count() > 1
            //        select NodeStatement.For(@group.Key).Set("shape", "box").Set("font", "bold"))
            //        .Cast<Statement>()
            //        .ToList();
        }

        public static IEnumerable<Statement> AddDecorationsToNodesIfTheyAreHighCentralityNodes(string[] nodeNames, string[] nodeGraphProperties)
        {
            var statements = new List<Statement>();
            for (var i = 0; i < nodeGraphProperties.Length; i++)
            {
                if (Convert.ToInt32(nodeGraphProperties[i]) > 0)
                    statements.Add(NodeStatement.For(nodeNames[i]).Set("shape", "doubleoctagon")
                        .Set("font", "bold").Set("style", "filled").Set("fillcolor", "gray").Set("penwidth", "2.5"));
                //if (Convert.ToInt32(nodeGraphProperties[i]) == 3)
                //    statements.Add(NodeStatement.For(nodeNames[i]).Set("shape", "tripleoctagon")
                //        .Set("style", "filled").Set("color", "gray2").Set("fontcolor", "white"));
            }
            return statements;
        }


        public static SdGraph ProcessFileIntoNewNodeNamesAndEdges(string[] dataLines)
        {
            SdNodes = new List<SdNode>();
            SdEdges = new List<SdEdge>();
            NodeNames = new List<string>();
            foreach (var line in dataLines)
            {
                ProcessOneLineNodesWithEdges(line);
            }

            return SdGraph = new SdGraph(SdNodes.ToArray(), SdEdges.ToArray());
        }
        public static SdGraph ProcessFileIntoExistingNodeNamesAndEdges(string[] dataLines)
        {
            if (SdNodes == null || SdNodes.Count <= 0)
                return ProcessFileIntoNewNodeNamesAndEdges(dataLines);

            foreach (var line in dataLines)
            {
                ProcessOneLineNodesWithEdges(line);
            }

            return SdGraph = new SdGraph(SdNodes.ToArray(), SdEdges.ToArray());
        }

        public static void ProcessOneLineNodesWithEdges(string rawDataLine)
        {
            //Get all the text (strings) between the dashed lines --- which represents different n-grams
            var ngrams = rawDataLine.StripExtraCharsForGraphLine().ExtractNgrams();

            foreach (var ngram in ngrams)
            {
                var indexer = 0;
                var nodes = ngram.ExtractNodesFromNgram();

                while (indexer < nodes.Length - 1)
                {
                    //SdEdges.Add(ProcessEdge(nodes[indexer], nodes[indexer + 1]));
                    ProcessEdge(nodes[indexer], nodes[indexer + 1]);
                    indexer++;
                }
            }
        }

        public static void ProcessOneLineNodesWithEdgesUsingNewFormat(string rawDataLine)
        {
            //Get all the text (strings) between the dashed lines --- which represents different n-grams
            //var ngrams = rawDataLine.StripExtraCharsForGraphLine().ExtractNgrams();
            var dataStruct = rawDataLine.StripExtraCharsForGraphLine().ExtractNgramsUsingNewFormats();
            var ngrams = dataStruct.Nodes;
            var edges = dataStruct.Edges;
            if (edges.Length <= 1)
            {
                edges = Enumerable.Range(0, 40).Select(_ => "0").ToArray();
            }

            var ngramsMax = ngrams.Length;
            for (var i = 0; i < ngramsMax - 1; i++)
            {
                if (edges[i] != "0")
                    AddNewEdgeUsingNewFormat(ngrams[i], ngrams[i + 1], edges[i]);
            }

        }

        public static List<string> ProcessFileIntoUniqueNodeNamesNoEdges(string[] dataLines)
        {
            NodeNames = new List<string>();

            foreach (string line in dataLines)
            {
                NodeNames.AddRange(ProcessOneLineUniqueNodesNoEdges(line));
            }
            return NodeNames;
        }
        public static string[] ProcessOneLineUniqueNodesNoEdges(string rawDataLine)
        {
            //Get all the text (strings) between the dashed lines, which represents different n-grams
            return rawDataLine.StripExtraCharsForGraphLine().ExtractUniqueNodesDisregardNGramSeparations();
        }
        public static string[] GetAllValuesBetweenSeparations(string rawData)
        {
            return rawData.Split(new[] { "-" }, StringSplitOptions.None);
        }
        #endregion
    }
}
