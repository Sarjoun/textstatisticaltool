﻿using System;
using Anat.GraphWindow.GraphViz.GraphViz.Structure;

namespace Anat.GraphWindow.GraphViz.GraphViz.TextParser
{
    public class ParsedGraphFileContainer
    {
        public string FileName { get; set; }

        public SdNode[] Nodes { get; }

        public SdEdge[] Edges { get; }

        public SdGraph Graph { get; set; }

        public DateTime Date { get; }

        public int? Index { get; set; }
        public string Name { get; set; }

        public ParsedGraphFileContainer()
        {
            Date = DateTime.Now;
        }

        public ParsedGraphFileContainer(SdGraph graph, string fileName)
        {
            Graph = graph;
            Nodes = graph.Nodes;
            Edges = graph.Edges;
            FileName = fileName;
            Date = DateTime.Now;
        }


        //public ParsedGraphFileContainer(SdNode[] nodes, SdEdge[] edges, string fileName)
        //{
        //    Nodes = nodes;
        //    Edges = edges;
        //    FileName = fileName;
        //    Date = DateTime.Now;
        //}

        //public ParsedGraphFileContainer(SdNode[] nodes, SdEdge[] edges, string fileName, int index, string name)
        //{
        //    Nodes = nodes;
        //    Edges = edges;
        //    FileName = fileName;
        //    Index = index;
        //    Name = name;
        //    Date = DateTime.Now;
        //}
    }
}
