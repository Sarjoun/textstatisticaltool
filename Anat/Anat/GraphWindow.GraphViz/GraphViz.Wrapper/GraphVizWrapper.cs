﻿using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Anat.Extensions;
using Anat.GraphWindow.GraphViz.GraphViz.Structure;
using Shields.GraphViz.Components;
using Shields.GraphViz.Models;
using Shields.GraphViz.Services;

namespace Anat.GraphWindow.GraphViz.GraphViz.Wrapper
{
    public class GraphVizWrapper
    {
        private Graph _graph;
        public string OutputImageFile { get; set; }
        public string InputDataFile { get; set; }

        public GraphVizWrapper(string outputImageFile)
        {
            OutputImageFile = outputImageFile;
        }

        public GraphVizWrapper(string outputImageFile, string inputDataFile)
        {
            OutputImageFile = outputImageFile;
            InputDataFile = inputDataFile;
            DoStuffWithGraphBuilder();
        }

        public void DoStuffWithGraphBuilder()
        {
            //var results = new GraphBuilder(InputDataFile).RemoveBufferSymbols().BreakUpIntoChunks();
        }

        //public async Task<bool> CreateGraphFormatPng2()
        //{
        //    //_graph = new Graph<State>();
        //    _graph = Graph.OfKind(GraphKinds.Directed);

        //    //_graph = new Graph();
        //    //var a = new State()
        //    //{
        //    //    Status = "Ready",
        //    //    AllowedPurchaserOperations = "operation1, operation2",
        //    //    AllowedSupplierOperations = "operarion1, operation 3"
        //    //};
        //    //var b = new State()
        //    //{
        //    //    Status = "Paused",
        //    //    AllowedPurchaserOperations = "operation1, operation2",
        //    //    AllowedSupplierOperations = "operarion1, operation 3"
        //    //};

        //    //Graph.AddVertex(a);
        //    //Graph.AddVertex(b);

        //    //Graph.AddEdge(new Edge<State>(a, b) { Label = "pause()" });
        //    //Graph.AddEdge(new Edge<State>(b, a) { Label = "continue()" });
        //}

        public async Task<bool> CreateRenderAndSaveDefaultStaticGraphFormatPng()
        {
            var pngFile = CheckFileEndsWithPng(OutputImageFile);
            if (string.IsNullOrEmpty(InputDataFile))
                _graph = Graph.Directed
                    .Add(EdgeStatement.For("a", "b").Set("label", "5"))
                    .Add(EdgeStatement.For("b", "b"))
                    .Add(EdgeStatement.For("b", "c").Set("label", "0.2"))
                    .Add(EdgeStatement.For("a", "c"))
                    .Add(NodeStatement.For("a").Set("style", "filled").Set("color", "cadetblue1"))
                    .Add(NodeStatement.For("b").Set("shape", "box").Set("color", "orange").Set("font", "bold"))
                    .Add(NodeStatement.For("B").Set("shape", "diamond").Set("font", "bold"))
                    .Add(NodeStatement.For("c").Set("shape", "doublecircle").Set("font", "bold"))
                    .Add(NodeStatement.For("d").Set("shape", "doubleoctagon").Set("font", "bold"))
                    .Add(NodeStatement.For("E").Set("shape", "tripleoctagon").Set("font", "bold"))
                    .Add(EdgeStatement.For("c", "d"))
                    .Add(EdgeStatement.For("a", "B"))
                    .Add(EdgeStatement.For("B", "B"))
                    .Add(EdgeStatement.For("B", "C"))
                    .Add(EdgeStatement.For("c", "E"))
                    .Add(EdgeStatement.For("1b", "1b"))
                    .Add(EdgeStatement.For("1b", "1c"))
                    .Add(EdgeStatement.For("1c", "1d"))
                    .Add(EdgeStatement.For("b2", "b"))
                    .Add(EdgeStatement.For("b2", "c"))
                    .Add(EdgeStatement.For("c2", "d"))
                    .Add(EdgeStatement.For("a2", "B"))
                    .Add(EdgeStatement.For("B2", "B"))
                    .Add(EdgeStatement.For("B2", "C"))
                    .Add(EdgeStatement.For("c2", "E"))
                    .Add(EdgeStatement.For("1b2", "1b"))
                    .Add(EdgeStatement.For("1b2", "1c"))
                    .Add(EdgeStatement.For("1c2", "1d"))
                    .Add(EdgeStatement.For("b3", "b"))
                    .Add(EdgeStatement.For("b3", "c"))
                    .Add(EdgeStatement.For("c3", "d"))
                    .Add(EdgeStatement.For("a3", "B"))
                    .Add(EdgeStatement.For("B3", "B"))
                    .Add(EdgeStatement.For("B3", "C"))
                    .Add(EdgeStatement.For("c3", "E"))
                    .Add(EdgeStatement.For("1b3", "1b"))
                    .Add(EdgeStatement.For("1b3", "1c"))
                    .Add(EdgeStatement.For("1c3", "1d"));

            IRenderer renderer = new Renderer(SettingsUpdater.GetGraphVizBinFolder());
            using (Stream file = File.Create(pngFile))
            {
                await renderer.RunAsync(
                    _graph, file,
                    RendererLayouts.Dot,
                    RendererFormats.Png,
                    CancellationToken.None);
            }
            return true;
        }

        public async Task<bool> CreateAndSaveGraphUsingFormatPng_FirstIterationAsync(List<string> nodeNames)
        {
            //Random _random = new Random();
            var pngFile = CheckFileEndsWithPng(OutputImageFile);


            GraphBuilder.AddNodesAndSetupGraph(nodeNames.ToArray(), GraphKinds.Directed);

            _graph = GraphBuilder.GetGraph;
            IRenderer renderer = new Renderer(SettingsUpdater.GetGraphVizBinFolder());
            using (Stream file = File.Create(pngFile))
            {
                await renderer.RunAsync(
                    _graph, file,
                    RendererLayouts.Dot,
                    RendererFormats.Png,
                    CancellationToken.None);
            }
            return true;
        }

        public async Task<bool> CreateAndSaveGraphUsingFormatPngAsync(SdGraph graphContainer, GraphKinds graphKinds)
        {
            var pngFile = CheckFileEndsWithPng(OutputImageFile);
            _graph = GraphBuilder.CreateGraph(graphContainer, graphKinds);
            IRenderer renderer = new Renderer(SettingsUpdater.GetGraphVizBinFolder());
            using (Stream file = File.Create(pngFile))
            {
                await renderer.RunAsync(
                    _graph, file,
                    RendererLayouts.Dot,
                    RendererFormats.Png,
                    CancellationToken.None);
            }
            
            return true;
        }

        public async Task<bool> CreateRenderAndSaveRandomGraphFormatPngAsync(int? numberOfNodes)
        {
            var pngFile = CheckFileEndsWithPng(OutputImageFile);
            if (!numberOfNodes.HasValue)
                return true;
            var numberOfEdges = numberOfNodes.Value;//_random.Next(50, 80);

            GraphBuilder.AddRandomNodes(numberOfNodes.Value, GraphKinds.Directed);
            GraphBuilder.AddRandomEdges(numberOfNodes.Value, numberOfEdges, GraphKinds.Directed);
            _graph = GraphBuilder.GetGraph;

            IRenderer renderer = new Renderer(SettingsUpdater.GetGraphVizBinFolder());
            using (Stream file = File.Create(pngFile))
            {
                await renderer.RunAsync(
                    _graph, file,
                    RendererLayouts.Dot,
                    RendererFormats.Png,
                    CancellationToken.None);
            }
            return true;
        }

        private static string CheckFileEndsWithPng(string fileName)
        {
            var temp = fileName;
            if (fileName.Substring(fileName.Length - 3).ToLower() == "png")
                return temp;
            return fileName.SafeRemove(3) + "png";
        }

    }
}