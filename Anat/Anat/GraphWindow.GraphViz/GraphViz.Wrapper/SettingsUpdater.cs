﻿using System;

namespace Anat.GraphWindow.GraphViz.GraphViz.Wrapper
{
   public static class SettingsUpdater
   {
      public const string MainFolderName = @"\anat\";
      public const string GraphVizFolderName = "Graphviz.2.38.0.2";

      public static string GetGraphVizBinFolder()
      {
         var mainFolderNameSize = MainFolderName.Length;
         var graphVizBinFolderPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
         var location = graphVizBinFolderPath.ToLower().IndexOf(MainFolderName, StringComparison.Ordinal) +
                        mainFolderNameSize;

         return graphVizBinFolderPath.Substring(0, location) +
                @"\packages\" +
                GraphVizFolderName;
      }
   }
}
