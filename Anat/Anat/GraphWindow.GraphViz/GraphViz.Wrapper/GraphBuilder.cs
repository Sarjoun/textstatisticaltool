﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Anat.Extensions;
using Anat.GraphWindow.GraphViz.GraphViz.Structure;
using Shields.GraphViz.Models;

namespace Anat.GraphWindow.GraphViz.GraphViz.Wrapper
{
    /// <summary>
    /// This class holds the functions that consume the intructions for the graph components
    /// and execute the steps required to build the graph.
    /// </summary>
    public static class GraphBuilder
    {
        private static readonly string _nodeName = "";
        private static Graph _graph;

        public static Graph GetGraph => _graph;

        private static Random _random;


        public static Graph CreateGraph(SdGraph graph)
        {
            return _graph = new Graph(GraphKinds.Undirected, "Test", MakeGraphStatements(graph));
        }


        public static Graph CreateGraph(SdGraph graph, GraphKinds graphKind)
        {
            return _graph = new Graph(graphKind, "Test", MakeGraphStatements(graph));
        }

        //private static IImmutableList<Statement> MakeClusters(SdGraph graph) {}

        private static IImmutableList<Statement> MakeGraphStatements(SdGraph graph)
        {
            if (graph.DecorationStatements == null)
                return _statements = MakeNodesStatements(graph.Nodes)
                .AddRange(MakeEdgesStatements(graph.Edges));

            return _statements = MakeNodesStatements(graph.Nodes)
                .AddRange(MakeEdgesStatements(graph.Edges))
                .AddRange(graph.DecorationStatements);
        }

        private static IImmutableList<Statement> MakeEdgesStatements(IEnumerable<SdEdge> edges)
        {
            ImmutableList<Statement> l1 = ImmutableList.Create<Statement>();
            //_random = new Random();

            foreach (var edge in edges)
            {
                var ns = EdgeStatement
                    .For(edge.HeadNode.Name, edge.TailNode.Name)
                    .Set("label", edge.Weight.ToString());
                l1 = l1.Add(ns);
            }
            return l1;
        }

        private static IImmutableList<Statement> MakeNodesStatements(IEnumerable<SdNode> nodes)
        {
            var l1 = ImmutableList.Create<Statement>();

            foreach (var node in nodes)
            {
                var ns = NodeStatement
                    .For(node.Name);
                //.Set("style", "filled")
                //.Set("color", "White");

                l1 = l1.Add(ns);
            }
            //_statements = l1;
            return l1;
        }

        //.Add(EdgeStatement.For("a", "b").Set("label", "5"))
        //.Add(EdgeStatement.For("a", "c"))
        //.Add(NodeStatement.For("a").Set("style", "filled").Set("color", "cadetblue1"))


        private static IImmutableList<Statement> MakeEdgesStatements(int numberOfNodes, int numberOfEdges)
        {
            ImmutableList<Statement> l1 = ImmutableList.Create<Statement>();
            _random = new Random();

            for (var i = 0; i < numberOfEdges; i++)
            {
                var firstNode = _random.Next(0, numberOfNodes).ToString();
                var secondNode = _random.Next(0, numberOfNodes).ToString();

                var ns = EdgeStatement.For(firstNode, secondNode).Set("label", (i % 5).ToString());
                //.Add(EdgeStatement.For("a", "b").Set("label", "5"))

                //check if this edge exists, if so modify by adding 1 to its value
                //if (l1.Find(x => (EdgeStatement)x.))
                //    l1 = l1.Add(ns);
                //var ns = EdgeStatement.For(firstNode, secondNode);
                l1 = l1.Add(ns);
            }
            return l1;
        }

        private static IImmutableList<Statement> _statements;
        private static IImmutableList<Statement> MakeRandomNodesStatements(int numberOfNodes)
        {
            var l1 = ImmutableList.Create<Statement>();

            for (var i = 0; i < numberOfNodes; i++)
            {
                var ns = NodeStatement
                    .For(i.ToString())
                    .Set("style", Enum.GetName(typeof(NodeStyle), i % 2))
                    .Set("color", Enum.GetName(typeof(NodeColor), i % 29));

                l1 = l1.Add(ns);
            }
            _statements = l1;
            return l1;
        }

        private static IImmutableList<Statement> MakeNodesStatements(string[] nodeNames)
        {
            var l1 = ImmutableList.Create<Statement>();

            foreach (var nodeName in nodeNames)
            {
                var ns = NodeStatement
                    .For(nodeName)
                    .Set("style", Enum.GetName(typeof(NodeStyle), 0 % 2))
                    .Set("color", Enum.GetName(typeof(NodeColor), 0 % 29));

                l1 = l1.Add(ns);
            }
            _statements = l1;
            return l1;
        }

        //public static IImmutableList<Statement> MakeEdgesStatements(IEnumerable<SdEdge> edges)
        //{
        //    _statements = _statements.AddRange(Task.Run(() => MakeEdgesStatements(numberOfNodes, numberOfEdges)).Result);
        //    _graph = new Graph(GraphKinds.Directed, "Test", _statements);
        //    return _statements;
        //}

        public static void AddNodesAndSetupGraph(string[] nodeNames, GraphKinds graphKinds)
        {
            var temp1 = Task.Run(() => MakeNodesStatements(nodeNames));
            //var temp2 = Task.Run(() => MakeEdgesStatements(numberOfNodes, numberOfEdges));

            //_statements = temp1.Result.AddRange(temp2.Result);
            _statements = temp1.Result;
            //_graph = new Graph(GraphKinds.Directed, "Test", _statements);
            //_graph = new Graph(GraphKinds.Undirected, "Test", _statements);
            _graph = new Graph(graphKinds, "Test", _statements);
        }


        public static void AddRandomNodesAndEdgesAndSetupGraph(int numberOfNodes, int numberOfEdges, GraphKinds graphKinds)
        {
            var temp1 = Task.Run(() => MakeRandomNodesStatements(numberOfNodes));
            var temp2 = Task.Run(() => MakeEdgesStatements(numberOfNodes, numberOfEdges));

            _statements = temp1.Result.AddRange(temp2.Result);
            //_graph = new Graph(GraphKinds.Directed, "Test", _statements);
            //_graph = new Graph(GraphKinds.Undirected, "Test", _statements);
            _graph = new Graph(graphKinds, "Test", _statements);
        }


        public static IImmutableList<Statement> AddRandomNodes(int numberOfNodes, GraphKinds graphKinds)
        {
            _statements = Task.Run(() => MakeRandomNodesStatements(numberOfNodes)).Result;
            _graph = new Graph(graphKinds, "Test", _statements);
            return _statements;
        }
        public static IImmutableList<Statement> AddRandomEdges(int numberOfNodes, int numberOfEdges, GraphKinds graphKinds)
        {
            _statements = _statements.AddRange(
                Task.Run(() => MakeEdgesStatements(numberOfNodes, numberOfEdges)).Result);
            _graph = new Graph(graphKinds, "Test", _statements);
            return _statements;
        }


        public static SdNode[] CreateRandomNodes(int numberOfNodes)
        {
            return Enumerable.Range(0, numberOfNodes).
                Select(n => new SdNode(_nodeName.GenerateRandomNodeName(3), 1)).ToArray();
        }

        public static SdNode[] CreateNodesFromNodeNames(string[] nodeNames)
        {
            return nodeNames.Select(s => new SdNode(s, 1)).ToArray();
        }


        public static SdNode[] CreateRandomNodes(int numberOfNodes, bool randomColorFills)
        {
            throw new NotImplementedException();
        }


        public static SdEdge[] CreateRandomEdges(int numberOfEdges, bool randomWeights)
        {
            throw new NotImplementedException();
        }

        public static void AssociateNodes(SdNode tailNode, SdNode headNode, double value)
        {
            throw new NotImplementedException();
        }

        public static void AssociateNodes(SdNode tailNode, Color tailNodeFillColor, SdNode headNode, Color headNodeFillColor, double value)
        {
            throw new NotImplementedException();
        }

        public static void UploadNodes(SdNode[] nodes)
        {
            //_statements = _statements.AddRange(Task.Run(() => MakeEdgesStatements(numberOfNodes, numberOfEdges)).Result);
            //_graph = new Graph(GraphKinds.Directed, "Test", _statements);
            //return _statements;
        }

        public static void UloadEdges(SdEdge[] edges)
        {
            throw new NotImplementedException();
        }
    }
}



//_statements = l1;
////ImmutableList<Int32> l1 = ImmutableList.Create<Int32>();
////ImmutableList<Int32> l2 = l1.Add(1);
////ImmutableList<Int32> l5 = l4.Replace(2, 4);
