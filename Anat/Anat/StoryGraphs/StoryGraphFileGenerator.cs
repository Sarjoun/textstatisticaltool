﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IONA.Helpers;
using SDParser.ClusteringTypes;
using SDParser.Enums;
using SDParser.Notifications;
using SDParser.Structures;
using SDParser.TextProcessingCentral;

namespace Anat.StoryGraphs
{
    public static class StoryGraphFileGenerator
    {
        private static string OutputFileName { get; set; }
        private static List<Agent> Agents { get; set; }
        private static List<string> Top10Words { get; set; }
        private static decimal[,] AgentsMatrixValues { get; set; }
        //private static List<StemWord> SyncedStemWordsIds { get; set; }
        //private static Dictionary<StemWord, int> StemWordDictionaryOrderedByWeight { get; set; }
        private static Dictionary<string, StemWord> StemWordDictionaryOrderedByWeight { get; set; }



        //public FileGenerator(string outputFileName, List<Agent> agents, List<string> top10Words, decimal[,] agentsMatrixValues)
        //{
        //    OutputFileName = outputFileName;
        //    Agents = agents;
        //    Top10Words = top10Words;
        //    AgentsMatrixValues = agentsMatrixValues;
        //}

        //private static void Organize
        
        public static async Task WriteStemWordNGramsToGraphFileUsingMostPopularVariantAsync(
            string outputFileName,
            List<Agent> agents,
            //List<StemWord> syncedStemWordsIds,
            List<string> top10Words,
            StatisticsTypeEnum statisticsTypeEnum,
            decimal[,] matrixEdgeValues)
        {
            OutputFileName = outputFileName;
            Agents = agents;
            Top10Words = top10Words;
            AgentsMatrixValues = matrixEdgeValues;
            //SyncedStemWordsIds = syncedStemWordsIds;

            var streamWriter = new StreamWriter(outputFileName);
            try
            {
                if (IsSymmetric(statisticsTypeEnum))
                {
                    foreach (var agent in agents)
                    {
                        await streamWriter.WriteLineAsync(agent.GramToStringUsingMostPopularUsingNewFormat()
                                                          + " + " + GetEdgesValuesForOneAgent(agent) + " + " +
                                                          GetNodeDecorationsBasedOnVocabularyPopularity(agent) + " +");
                    }
                }
                else
                {
                    foreach (var agent in agents)
                    {
                        await streamWriter.WriteLineAsync(agent.GramToStringUsingMostPopularUsingNewFormat()
                                                          + " + " + GetEdgesValuesForOneAgent(agent) + " + " +
                                                          GetNodeDecorationsBasedOnVocabularyPopularity(agent) + " +");
                        //for the reverse direction values
                        await streamWriter.WriteLineAsync(
                            agent.GramToStringUsingMostPopularUsingNewFormatInReverseOrder() + " + " +
                            GetEdgesValuesForOneAgentInReverseOrderForAgentWords(agent) + " + " +
                            GetNodeDecorationsBasedOnVocabularyPopularityInReverseOrderForAgentWords(agent) + " +");
                    }
                }
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        private static bool IsSymmetric(StatisticsTypeEnum statisticsTypeEnum)
        {
            switch (statisticsTypeEnum)
            {
                case StatisticsTypeEnum.ConditionalProbability: return false;
                case StatisticsTypeEnum.CorrelationCoefficient: return false;
                case StatisticsTypeEnum.JointFrequency: return true;
                case StatisticsTypeEnum.JointProbability: return true;
                case StatisticsTypeEnum.LogOddsRatio: return false;
                case StatisticsTypeEnum.PointWiseMutualInformation: return false;
                default: return true;
            }
        }

        private static string GetEdgesValuesForOneAgent(Agent agent)
        {
            var stemWords = agent.Grams;
            var result = "";
            for (var i = 0; i < stemWords.Count - 1; i++)
            {
                try
                {
                    result += GetEdgeValueBetween2StemmedWordsIDsRounded2Places(
                        stemWords[i].StatsMatrixId, 
                        stemWords[i + 1].StatsMatrixId);
                }
                catch (Exception e)
                {
                    ShowMessage.ShowErrorMessageAndExit("Error in GetEdgesValuesForOneAgent(Agent agent). ", e);
                }
            }
            return result;
        }

        private static string GetEdgesValuesForOneAgentInReverseOrderForAgentWords(Agent agent)
        {
            //var stemWords = agent.Grams;
            var gramsReverse = agent.Grams.AsEnumerable().Reverse().ToList();
            var result = "";
            for (var i = 0; i < gramsReverse.Count - 1; i++)
            {
                try
                {
                    result += GetEdgeValueBetween2StemmedWordsIDsRounded2Places(gramsReverse[i].StatsMatrixId, gramsReverse[i + 1].StatsMatrixId);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
            return result;
        }

        //public static int GetClusteredStemWordId(StemWord agentStemWord)
        //{
        //    //return VocabularyDictionary.FirstOrDefault(x => x.Key.Root == stemWord.Root).Value;
        //    //return VocabularyDictionary.FirstOrDefault(x => x.Key.Root == stemWord.Root).Key.Id;
        //    var firstOrDefault = SyncedStemWordsIds.FirstOrDefault(x => (x.Root == agentStemWord.Root)
        //                                                                || (x.Root.Contains(agentStemWord.Root))
        //                                                                || (agentStemWord.Root.Contains(x.Root)));
        //    if (firstOrDefault != null)
        //        return firstOrDefault.Id;
        //    return -1;
        //}

        private static string GetEdgeValueBetween2StemmedWordsIDs(int id1, int id2)
        {
            try
            {
                return Convert.ToString(AgentsMatrixValues[id1, id2], CultureInfo.InvariantCulture) + " ";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private static string GetEdgeValueBetween2StemmedWordsIDsRounded2Places(int id1, int id2)
        {
            try
            {
                if (id1 == id2)
                    WindowNotifications.DisplayErrorMessage($"Bug in GetEdgeValueBetween2StemmedWordsIDsRounded2Places(id1,id2)", new Exception("id1 and id2 should not have the same value. In the logic I do not compare the edge between a node and itself!"));
                return Convert.ToString(
                    Math.Round(AgentsMatrixValues[id1, id2], 2), 
                    CultureInfo.InvariantCulture) + " ";
            }
            catch (Exception e)
            {
                if (AgentsMatrixValues == null)
                    WindowNotifications.DisplayErrorMessage($"Bug in StoryGraphFileGenerator: AgentsMatrixValues is null", new ArgumentNullException($@"AgentsMatrixValues is null!"));
                else
                    WindowNotifications.DisplayErrorMessage($"Bug in StoryGraphFileGenerator:  GetEdgeValueBetween2StemmedWordsIDsRounded2Places(id1,id2)", e);
                throw;
            }
        }

        private static string GetEdgeValueBetween2StemmedWordsIDs(int id1, int id2, bool round, bool addPercentage)
        {
            try
            {
                if (!round)
                    return Convert.ToString(AgentsMatrixValues[id1, id2], CultureInfo.InvariantCulture) + " ";

                if (addPercentage)
                    return "%" + Convert.ToString(Math.Round(AgentsMatrixValues[id1, id2], 2), CultureInfo.InvariantCulture) + " ";

                return Convert.ToString(AgentsMatrixValues[id1, id2], CultureInfo.InvariantCulture) + " ";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private static string GetNodeDecorationsBasedOnVocabularyPopularity(Agent agent)
        {
            string result = "";

            foreach (var agentStemWord in agent.Grams)
            {
                var exists = Top10Words.Exists(x =>
                (x == agentStemWord.Root) || (x.Contains(agentStemWord.Root)) || (agentStemWord.Root.Contains(x)));

                if (exists)
                    result += " 1 ";
                else
                    result += " 0 ";
            }

            return result;
        }

        private static string GetNodeDecorationsBasedOnVocabularyPopularityInReverseOrderForAgentWords(Agent agent)
        {
            string result = "";

            foreach (var agentStemWord in agent.Grams)
            {
                //var exists = Top10Words.Exists(x =>
                //    (x == agentStemWord.Root) || (x.Contains(agentStemWord.Root)) || (agentStemWord.Root.Contains(x)));

                //if (exists)
                //    result += " 1 ";
                //else
                result += " 0 ";
            }

            return result;
        }


        private static int GetTotalNumberOfEdgesInMediaGraph(List<Agent> agents, StatisticsTypeEnum statisticsTypeEnum)
        {
            var count = agents.Select(
                    agent => GetEdgesValuesForOneAgent(agent)
                        .Trim()
                        .Split(' ')
                        .ToList())
                .Select(temp => temp.Count(entry => entry != "0"))
                .Sum();

            if (!IsSymmetric(statisticsTypeEnum))
            {
                count += agents.Select(
                        agent => GetEdgesValuesForOneAgentInReverseOrderForAgentWords(agent)
                            .Trim()
                            .Split(' ')
                            .ToList())
                    .Select(temp => temp.Count(entry => entry != "0"))
                    .Sum();
            }

            return count;
        }




        public static double GetRatioOFEdgesToNodeNumber(int numberOfEdgesInMediaGraph, int numberOfNodesInMediaGraph)
        {
            return (double)numberOfEdgesInMediaGraph / (numberOfEdgesInMediaGraph * (numberOfEdgesInMediaGraph - 1));
            //String.Format("{0:0.00}", someValue);
        }

        public static async Task WriteOutStoryGraphsAnalysisAsync(string outputFileName, 
            StatisticsTypeEnum statisticsTypeEnum,
            Dictionary<string, StemWord> stemWordDictionaryOrderedByWeight,
            List<Agent> clusteredAgents)
        {
            GlobalSubStoryCounter = 1;
            OutputFileName = outputFileName;
            StemWordDictionaryOrderedByWeight = stemWordDictionaryOrderedByWeight;
            //Agents = agents;
            //Top10Words = top10Words;
            //AgentsMatrixValues = matrixEdgeValues;
            //SyncedStemWordsIds = syncedStemWordsIds;

            var streamWriter = new StreamWriter(outputFileName);
            try
            {
                var result = AnalysisOnFinalStoryGraph(clusteredAgents);
                var numberOfEdges = GetTotalNumberOfEdgesInMediaGraph(clusteredAgents, statisticsTypeEnum);

                await streamWriter.WriteLineAsync("Final graph analysis:");
                await streamWriter.WriteLineAsync("Total weight of words in stories in media graph:" + TotalNumberOfWordsInMediaGraph);
                await streamWriter.WriteLineAsync("Total weight of words in stories in media graph:" + TotalWeightOfAllWordsInMediaGraph);
                await streamWriter.WriteLineAsync("Total number of stories in media graph before GV merge:" + result.Count);
                await streamWriter.WriteLineAsync("Total number of edges in media graph:" + numberOfEdges);
                await streamWriter.WriteLineAsync("The (number of edges in media graph) / n(n-1)  where n is number of node in media graph:" +
                                                  $"{GetRatioOFEdgesToNodeNumber(numberOfEdges, TotalNumberOfWordsInMediaGraph):0.0000}");



                foreach (var storyGraph in result)
                {
                    await streamWriter.WriteLineAsync(storyGraph.Id + ") " + storyGraph.ShowWords() +
                                                      " . Relative Word Count = " + storyGraph.RelativeWordCount +
                                                      " . Relative Weight Count = " + storyGraph.RelativeWeight);
                }

                //foreach (var agent in agents)
                //{
                //    await streamWriter.WriteLineAsync(agent.GramToStringUsingMostPopularUsingNewFormat()
                //                                      + " + " + GetEdgesValuesForOneAgent(agent) + " + " +
                //                                      GetNodeDecorationsBasedOnVocabularyPopularity(agent) + " +");
                //}
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        private static List<StoryGraphAnalysis> AnalysisOnFinalStoryGraph(List<Agent> agents)
        {
            //Get all the words in media graph
            var mediaGraphWords = GetAllWordsFromTotalStoryGraph(agents);
            TotalNumberOfWordsInMediaGraph = mediaGraphWords.Count;
            try
            {
                TotalWeightOfAllWordsInMediaGraph = GetTotalWeightOfWords(mediaGraphWords);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            

            var result = new List<StoryGraphAnalysis>();
            foreach (var agent in agents)
            {
                result.AddRange(ExtractStoryGraphAnalysisFromSingleAgent(agent, TotalNumberOfWordsInMediaGraph, TotalWeightOfAllWordsInMediaGraph));
            }

            return result;
        }

        private static List<int> IndicesOfAllEdgesWithValue(string[] stringArray, string match)
        {
            //return stringArray.Select((x, i) => x.Contains(match) ? i : -1)
            //    .Where(x => x != -1)
            //    .ToList();
            return stringArray.Select((x, i) => x == match ? i : -1)
                .Where(x => x != -1)
                .ToList();

        }

        private static List<List<string>> ExtractSubAgentsFromIndicesOfZero(List<string> words, List<int> indicesOfZero)
        {
            var startIndex = 0;
            var previousIndex = startIndex;
            var result = new List<List<string>>();
            indicesOfZero.Add(words.Count);

            foreach (var index in indicesOfZero)
            {
                result.Add(ExtractSubAgentWordsFromIndices(words, previousIndex, index));
                previousIndex = index;
                //result.Add(ExtractSubAgentWordsFromIndices(words, previousIndex, indicesOfZero[index]));
                //previousIndex = indicesOfZero[index]+1;
                //if (previousIndex == 0)
                //    previousIndex = 1;
                previousIndex++;
            }
            return result;
        }

        private static List<string> ExtractSubAgentWordsFromIndices(List<string> words, int startIndex, int endIndex)
        {
            var res = new List<string>();
            if (startIndex == endIndex)
            {
                res.Add(words[startIndex]);
            }
            else
            {
                res = words.Skip(startIndex).Take(endIndex - startIndex).ToList();
            }
            return res;
        }


        private static int GlobalSubStoryCounter { get; set; }
        public static List<StoryGraphAnalysis> ExtractStoryGraphAnalysisFromSingleAgent(Agent agent,
            int totalNumberOfWordsInStoryGraph, double totalWeightOfAllWordsInStoryGraph)
        {
            //If there are no edges with 0 value then this agent becomes a single story graph
            var nodes = agent.GramToStringUsingMostPopularUsingNewFormat();
            var edges = GetEdgesValuesForOneAgent(agent);
            var result = new List<StoryGraphAnalysis>();

            var indicesOfZero = IndicesOfAllEdgesWithValue(edges.Trim().Split(' ').ToArray(), "0");
            var nodeWords = nodes.Trim().Split(' ').ToList();
            if (indicesOfZero.Count > 0)
            {
                var res = ExtractSubAgentsFromIndicesOfZero(nodeWords, indicesOfZero);
                foreach (var subList in res)
                {
                    result.Add(new StoryGraphAnalysis
                    {
                        Id = GlobalSubStoryCounter,
                        Words = subList,
                        TotalNumberOfWordsInStoryGraph = totalNumberOfWordsInStoryGraph,
                        WeightOfWords = GetTotalWeightOfWords(subList),
                        TotalWeightOfAllWordsInStoryGraph = totalWeightOfAllWordsInStoryGraph
                    });
                    GlobalSubStoryCounter++;
                }

            }
            else
            {
                var words = nodes.Trim().Split(' ').ToList();
                result.Add(new StoryGraphAnalysis
                {
                    Id = GlobalSubStoryCounter,
                    Words = words,
                    TotalNumberOfWordsInStoryGraph = totalNumberOfWordsInStoryGraph,
                    WeightOfWords = GetTotalWeightOfWords(words),
                    TotalWeightOfAllWordsInStoryGraph = totalWeightOfAllWordsInStoryGraph
                });
                GlobalSubStoryCounter++;
            }

            return result;
        }

        public static int TotalNumberOfWordsInMediaGraph { get; set; }
        public static double TotalWeightOfAllWordsInMediaGraph { get; set; }
        public static int TotalNUmberOfStoriesInMediaGraph { get; set; }

        private static List<string> GetAllWordsFromTotalStoryGraph(IEnumerable<Agent> agents)
        {
            var words = new List<string>();

            foreach (var agent in agents)
            {
                var grams = agent.Grams;
                foreach (var gram in grams)
                {
                    if (!words.Contains(gram.TopVariantWord))
                        words.Add(gram.TopVariantWord);
                }
            }
            return words;
        }
        
        private static int GetWordWeightFromStemWordDictionary(string word)
        {
            //This will break only when the word no longer exists in both dictionaries (IONA and ALAN)
            try
            {
                return StemWordDictionaryOrderedByWeight[SentenceProcessor.ExtractStemVersionFromWord(word)].Frequency;
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit("The word '"+ word + "' no longer exists in both dictionaries IONA and ALAN (only in IONA). \r\n" + 
                    "This is the source of the bug." , e);
                Console.WriteLine(e);
                throw;
            }
        }

        private static int GetWordWeightFromStemWordDictionary_OldMoreComplexThatMatchesOntheActualVariant(string word)
        {
            //(x == agentStemWord.Root) || (x.Contains(agentStemWord.Root)) || (agentStemWord.Root.Contains(x)));

            //var result = StemWordDictionaryOrderedByWeight.Where(p => p.Key.TopVariantWord == word
            //|| p.Key.TopVariantWord.Contains(word) || word.Contains(p.Key.TopVariantWord)).Select(p => p.Value).ToList()[0];

            try
            {
                //var result = StemWordDictionaryOrderedByWeight.FirstOrDefault(p =>
                //        p.Value.TopVariantWord == word.ToLower() ||
                //        p.Value.TopVariantWord.Contains(word.ToLower()) ||
                //        p.Value.DoesWordExistInVariants(word.ToLower()))
                //    .Value.Frequency;
                //    //word.ToLower().Contains(p.Value.TopVariantWord)).Value.Frequency;
                //return result;
                var result = StemWordDictionaryOrderedByWeight.FirstOrDefault(p =>
                        p.Value.TopVariantWord == word ||
                        //p.Value.TopVariantWord.Contains(word.ToLower()) ||
                        p.Value.DoesWordExistInVariants(word))
                    .Value.Frequency;
                //word.ToLower().Contains(p.Value.TopVariantWord)).Value.Frequency;

                // There is could be a condition where a Variant of the word that was pulled from one 
                // dictionary no longer exists in the tagged corpus and therefore cannot be found in this 
                // particular variation. The solution is to look for the stem version of this word and 
                // then match on that.

                ////this is pseudocode the way it should be
                //if (result == null)
                //{
                //    var t = StemWordDictionaryOrderedByWeight[SentenceProcessor.ExtractStemVersionFromWord(word)]
                //        .Frequency;
                //    return t;
                //}
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private static double GetTotalWeightOfWords(IEnumerable<string> words)
        {
            var result = words.Sum(GetWordWeightFromStemWordDictionary);
            return result;
        }

        //private List<string> BuildNewAgentOutput(Agent agent)
        //{
        //    var edges = GetEdgesValuesForOneAgent(agent).Split(' ');
        //    var ngrams = agent.GramToStringUsingMostPopularUsingNewFormat();
        //    var result = new List<string>();

        //    if (edges.Length <= 1)
        //    {
        //        edges = Enumerable.Range(0, 40).Select(_ => "0").ToArray();
        //    }

        //    var ngramsMax = ngrams.Length;
        //    for (var i = 0; i < ngramsMax - 1; i++)
        //    {
        //        if (edges[i] != "0")
        //            result.Count AddNewEdgeUsingNewFormat(ngrams[i], ngrams[i + 1], edges[i]);
        //    }
        //}


    }
}
