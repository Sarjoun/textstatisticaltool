﻿using System.Collections.Generic;

namespace Anat.StoryGraphs
{
    public class StoryGraphAnalysis
    {
        public int Id { get; set; }
        public List<string> Words { get; set; }
        public List<int> NumberOfEdges { get; set; }
        public int NumberOfWords => Words.Count;
        public int TotalNumberOfWordsInStoryGraph { get; set; }
        public double RelativeWordCount => (double)NumberOfWords / TotalNumberOfWordsInStoryGraph;  // NumberOfWords/TotalNumberOfWordsInStoryGraph
        public double WeightOfWords { get; set; }
        public double TotalWeightOfAllWordsInStoryGraph { get; set; }
        public double RelativeWeight => WeightOfWords / TotalWeightOfAllWordsInStoryGraph;

        public StoryGraphAnalysis()
        { }

        public StoryGraphAnalysis(int id, List<string> words, int totalNumberOfWordsInStoryGraph,
            double weightOfWords, double totalWeightOfAllWordsInStoryGraph)
        {
            Id = id;
            Words = words;
            TotalNumberOfWordsInStoryGraph = totalNumberOfWordsInStoryGraph;
            WeightOfWords = weightOfWords;
            TotalWeightOfAllWordsInStoryGraph = totalWeightOfAllWordsInStoryGraph;
        }

        public string ShowWords()
        {
            var result = "";
            foreach (var word in Words)
            {
                result += word + " ";
            }
            result = result.Trim();
            return result;
        }

    }
}
