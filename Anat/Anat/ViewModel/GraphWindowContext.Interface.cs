using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Anat.ViewModel
{
    public interface IGraphWindowContext
    {
        string WindowTitle { get; set; }
        bool CanGenerateRandomGraph { get; }
        bool ImageVisibility { get; }
        string DisplayedOutputFileName { get; set; }
        string GraphTextVersion { get; set; }
        string OutputFile { get; set; }
        BitmapImage GraphImage { get; }
        int NumberOfRandomNodesToGenerate { get; set; }

        ICommand GenerateGraphFromMemoryCommand { get; }
        ICommand GenerateRandomGraphCommand { get; }
        ICommand ConsumeDataFileAndGenerateGraphCommand { get; }
        ICommand ConsumeDataFileAndGenerateGraphCommandFromFolder { get; }
        ICommand ConsumeDataFileAndGenerateGraphCommandUsingNewFormat { get; }
        ICommand ConsumeDataFileThenAddToCurrentGraphCommand { get; }
        Task RenderAndDisplayDefaultStaticGraphTaskAsync(string outputGraphFile);
        Task<bool> RenderAndDisplayRandomGraphTaskAsync(string outputGraphFile);
        Task<bool> RenderAndDisplayGraphTaskAsync(string outputGraphFile);
        Task OpenFileAndGenerateGraphAsync();
        Task OpenFileAndGenerateGraphThenMergeAsync();
        Task OpenAndParseLDAFileIntoTypesAsync();
    }
}

//ICommand ConsumeDataFileCommand { get; }
//ICommand ParseLDASettingsFileAndTransformIntoTypesCommand { get; }
//ICommand CreateFileForLDASimulationSettingsCommand { get; }
//ICommand RunILDACommand { get; }
//ICommand ParseIONASettingsFileAndTransformIntoTypesCommand { get; }
//ICommand RunIONACommand { get; }
//Task<bool> CreateDataSpanCommand(string outputGraphFile);
