using System.Threading.Tasks;
using System.Windows.Input;

namespace Anat.ViewModel
{
    public interface IAnatWindowContext
    {
        string WindowTitle { get; set; }
        string Version { get; }
        ICommand OpenGraphicsWindowCommand { get; }
        ICommand CreateFileForLDASimulationSettingsCommand { get; }
        ICommand ParseLDASettingsFileAndTransformIntoTypesCommand { get; }
        ICommand ParseIONASettingsFileAndTransformIntoTypesCommand { get; }
        //ICommand RunILDACommand { get; }
        //ICommand RunIONACommand { get; }
        //ICommand RunILDAandIONACommand { get; }
        ICommand CalculateNetworkStatisticsAndWriteOutputCommand { get; }
        string OutputFile { get; set; }
        ICommand ConsumeDataFileCommand { get; }
        Task OpenAndParseLDAFileIntoTypes();
        string DisplayedOutputFileName { get; set; }
        string GraphTextVersion { get; set; }

        //bool ImageVisibility { get; }
        //BitmapImage GraphImage { get; }
    }
}

