﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using Anat.Annotations;
using Anat.Commands.Commands.Statistics;
using Anat.Enums;
using Anat.Helpers;
using LeagueOfMonads;
using SDParser;
using SDParser.ComplexTypes;
using SDParser.Primitives;
using static System.Windows.Application;
using Cursors = System.Windows.Input.Cursors;

namespace Anat.ViewModel
{
    public class TextStatisticsWindowContext : INotifyPropertyChanged, ITextStatisticsWindowContext
    {
        public ICommand CalculateTextStatisticsAndWriteResultsStandAloneCommand { get; }
        public ICommand CalculateTextStatisticsAndWriteResultsFromFileCommand { get; }
        public ICommand OpenMainFolderAndIterateThroughSourcesCommand { get; }
        public ICommand ParseSettingsFileAndExtractTextStatisticsSettingsValues { get; }
        public string StatisticsWindowTitle { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        string _textStatisticsOutputWindow;
        string _startupFolder;
        readonly FolderBrowserDialog _folderBrowserDialog;

        public string TextStatisticsOutputWindow
        {
            get { return _textStatisticsOutputWindow; }
            set
            {
                _textStatisticsOutputWindow = value;
                OnPropertyChanged();
            }
        }
        public TextStatisticsWindowContext()
        {
            _startupFolder = FileHelper.StartupFolderPath;
            StatisticsWindowTitle = "Text Statistics";
            _folderBrowserDialog = new FolderBrowserDialog();
            //commands
            CalculateTextStatisticsAndWriteResultsStandAloneCommand = new CalculateTextStatisticsStandAloneCommandImplementor(this);
            CalculateTextStatisticsAndWriteResultsFromFileCommand = new CalculateTextStatisticsFromFileCommandImplementor(this);
            OpenMainFolderAndIterateThroughSourcesCommand = new OpenMainSourcesFolderCommandImplementor(this);
            ParseSettingsFileAndExtractTextStatisticsSettingsValues = new ParseIONASettingsFileAndExtractTextStatisticsTypesCommandImplementor(this);
        }

        public async Task ProcessTextStatisticsFromFoldersOriginalAsync()//Using text only and no HTML
        {
            //register with the events
            SDStatistics.Orchestrator.OnProcessingNewSource += OrchestratorOnProcessingNewSource;

            MouseWait();
            TextStatisticsOutputWindow += SDStatistics.Orchestrator.RunTextStatisticsUsingDataStraightFromSourcesFolder(
                await OpenAndParseSimulationFileAndExtractTextStatisticsSettingsAsync(ShowParsedFile.Yes))
                ? "Text statistics analysis completed successfully!"
                : "Text statistics analysis failed.";
            MouseReady();
        }

        public async Task ProcessTextStatisticsFromFileAsync()
        {
            //register with the events
            SDStatistics.Orchestrator.OnProcessingNewSource += OrchestratorOnProcessingNewSource;

            MouseWait();
            TextStatisticsOutputWindow += SDStatistics.Orchestrator.RunTextStatisticsUsingSourcesFile(
                await OpenAndParseSimulationFileAndExtractTextStatisticsSettingsAsync(ShowParsedFile.No))
                ? "Text statistics analysis completed successfully!"
                : "Text statistics analysis failed.";
            MouseReady();
        }

        private void OrchestratorOnProcessingNewSource(object sender, SDStatistics.Events.ProcessingSourceEventArgs e) =>
            TextStatisticsOutputWindow += "Processing now [" + e.Id + "/" + e.TotalSourcesCount + "] "
                                       + e.Source + " at " + e.TimeStarted.ToString("h:mm:ss tt") + "\r\n";

        public Option<string[]> OpenMainFolderAndReturnAllSourcesPaths()
        {
            return _folderBrowserDialog.ShowDialog() != DialogResult.OK
                ? null
                : Directory.GetDirectories(_folderBrowserDialog.SelectedPath);
        }

        public async Task<TextStatisticsSettings> OpenAndParseSimulationFileAndExtractTextStatisticsSettingsAsync(ShowParsedFile showParsedFile)
        {
            var ionaRunSettingsFileName = ReturnFilePathFromDialog();
            if (string.IsNullOrEmpty(ionaRunSettingsFileName))
                return null;
            try
            {
                var allSettings = await MainParserForSettingsFile.GetIONASettingsFromFileAsync(
                   IONARunSettingsFileName.FromValue(ionaRunSettingsFileName));
                var textStatsSettings = allSettings.ExtractTextStatisticsSettings;

                if (showParsedFile != ShowParsedFile.Yes)
                {
                    if (!Directory.Exists(textStatsSettings.StatisticsSources.Value))
                    {
                        TextStatisticsOutputWindow = "ERROR: Statistics source directories do not exist!\r\n";
                        return null;
                    }
                    return allSettings.ExtractTextStatisticsSettings;
                }

                TextStatisticsOutputWindow = "RunStatistics is [" + textStatsSettings.RunStatistics.Value + "]\r\n";

                var statisticsSourcesValue = string.IsNullOrEmpty(textStatsSettings.StatisticsSources.Value)
                    ? "[Auto-Determined]"
                    : textStatsSettings.StatisticsSources.Value;
                TextStatisticsOutputWindow += "StatisticsSources is [" + statisticsSourcesValue + "]\r\n";

                var statisticsTargetValue = !string.IsNullOrEmpty(textStatsSettings.StatisticsTarget)
                    ? textStatsSettings.StatisticsTarget.Value
                    : "[Auto-Determined]";
                TextStatisticsOutputWindow += "StatisticsTarget is [" + statisticsTargetValue + "]\r\n";

                TextStatisticsOutputWindow += "ReductionInNetworkEdgeValue is [" + textStatsSettings.ReductionInNetworkEdgeValue.Value + "]\r\n";
                TextStatisticsOutputWindow += "RunNetworkStatistics is [" +
                                              textStatsSettings.RunNetworkStatistics.Value + "]\r\n";
                TextStatisticsOutputWindow += "ReductionInWordFrequency is [" +
                                              textStatsSettings.ReductionInWordFrequency.Value + "]\r\n";
                TextStatisticsOutputWindow += "RemoveLowWordFrequencyValue is [" +
                                              textStatsSettings.RemoveLowWordFrequencyValue.Value + "]\r\n";
                TextStatisticsOutputWindow += "ReductionOfNetworkEdgesWeight is [" +
                                              textStatsSettings.ReductionOfNetworkEdgesWeight.Value + "]\r\n";
                TextStatisticsOutputWindow += "TextPartioningAlgorithm is [" +
                                              textStatsSettings.TextPartioningAlgorithm.Value + "]\r\n";
                TextStatisticsOutputWindow += "SplitTheSentences is [" + textStatsSettings.SplitTheSentences.Value +
                                              "]\r\n";
                TextStatisticsOutputWindow += "IncludeTitleInSetup is [" + textStatsSettings.IncludeTitleInSetup.Value + "]\r\n";

                TextStatisticsOutputWindow += "HorizonSize is [" + textStatsSettings.HorizonSize.Value + "]\r\n";
                TextStatisticsOutputWindow += "UseStemmingInTextStatistics is [" +
                                              textStatsSettings.UseStemmingInTextStatistics.Value + "]\r\n";
                TextStatisticsOutputWindow += "CalculateConditionalProbability is [" +
                                              textStatsSettings.CalculateConditionalProbability.Value + "]\r\n";
                TextStatisticsOutputWindow += "CalculatePointWiseMutualInformation is [" +
                                              textStatsSettings.CalculatePointWiseMutualInformation.Value + "]\r\n";
                TextStatisticsOutputWindow += "CalculateCorrelationCoefficient is [" +
                                              textStatsSettings.CalculateCorrelationCoefficient.Value + "]\r\n";

                if (!Directory.Exists(textStatsSettings.StatisticsSources.Value))
                    TextStatisticsOutputWindow = "ERROR: Statistics source directories do not exist!\r\n";

                return allSettings.ExtractTextStatisticsSettings;
            }
            catch (Exception e)
            {
                TextStatisticsOutputWindow =
                   @"Some bug happend while processing the statistics settings from the settings file " + ionaRunSettingsFileName
                   + ". The exception is : " + e;
                return null;
                //Console.WriteLine(@"Some bug happend while writing the output of the text statistics settings file. " + e);
                //throw;
            }
        }

        public string ReturnFilePathFromDialog()
        {
            var fileDialog = new Microsoft.Win32.OpenFileDialog
            {
                Title = "Look for the LDA or IONA simulation settings file (.txt)",
                DefaultExt = ".txt",
                Filter = "TXT Files|*.txt",
                InitialDirectory = _startupFolder,
                RestoreDirectory = true
            };

            var result = fileDialog.ShowDialog();
            if (result != true) return "";

            _startupFolder = Path.GetDirectoryName(fileDialog.FileName);//fileDialog.InitialDirectory;
            return fileDialog.FileName;
        }

        private static void MouseWait() => Current.Dispatcher.Invoke(() => { Mouse.OverrideCursor = Cursors.Wait; });

        private static void MouseReady() => Current.Dispatcher.Invoke(() => { Mouse.OverrideCursor = null; });


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}



//public string OpenFile()
//{
//    // Create OpenFileDialog
//    var fileDialog = new Microsoft.Win32.OpenFileDialog
//    {
//        // Set filter for file extension and default file extension
//        DefaultExt = ".txt",
//        Filter = "TXT Files|*.txt",
//        InitialDirectory = _startupFolder,
//        RestoreDirectory = true
//    };

//    var result = fileDialog.ShowDialog();
//    if (result != true) return "";

//    _startupFolder = Path.GetDirectoryName(fileDialog.FileName);//fileDialog.InitialDirectory;
//    return fileDialog.FileName;
//}