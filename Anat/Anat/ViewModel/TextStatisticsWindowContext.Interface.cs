using System.Threading.Tasks;
using System.Windows.Input;
using Anat.Enums;
using SDParser.ComplexTypes;

namespace Anat.ViewModel
{
    public interface ITextStatisticsWindowContext
    {
        ICommand CalculateTextStatisticsAndWriteResultsStandAloneCommand { get; }
        ICommand CalculateTextStatisticsAndWriteResultsFromFileCommand { get; }
        ICommand OpenMainFolderAndIterateThroughSourcesCommand { get; }
        ICommand ParseSettingsFileAndExtractTextStatisticsSettingsValues { get; }
        Task ProcessTextStatisticsFromFoldersOriginalAsync();
        Task ProcessTextStatisticsFromFileAsync();
        Task<TextStatisticsSettings> OpenAndParseSimulationFileAndExtractTextStatisticsSettingsAsync(ShowParsedFile showParsedFile);
        string StatisticsWindowTitle { get; set; }
        string TextStatisticsOutputWindow { get; set; }
    }
}

