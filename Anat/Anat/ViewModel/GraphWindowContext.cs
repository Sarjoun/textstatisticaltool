﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Anat.Annotations;
using Anat.Commands.Commands.Graph;
using Anat.Enums;
using Anat.GraphWindow.GraphViz.GraphViz.Structure;
using Anat.GraphWindow.GraphViz.GraphViz.TextParser;
using Anat.Helpers;
using Anat.Libraries;
using Anat.Primitives;
using SDParser;
using SDParser.Primitives;
using Shields.GraphViz.Models;
using Graph = Anat.GraphWindow.GraphViz.GraphViz.Wrapper.GraphVizWrapper;

namespace Anat.ViewModel
{
    public class GraphWindowContext : INotifyPropertyChanged, IGraphWindowContext
    {
        public ICommand GenerateGraphFromMemoryCommand { get; }
        public ICommand GenerateRandomGraphCommand { get; }
        public ICommand ConsumeDataFileAndGenerateGraphCommand { get; }
        public ICommand ConsumeDataFileAndGenerateGraphCommandFromFolder { get; }
        public ICommand ConsumeDataFileThenAddToCurrentGraphCommand { get; }
        public ICommand ConsumeDataFileAndGenerateGraphCommandUsingNewFormat { get; }

        private bool _imageVisibility;
        private bool _canGenerateRandomGraph;// => GenerateRandomGraphCommand.CanExecute(this);
        private string _startupFolder;
        private string _outputFilename;
        private string _graphTextVersion;
        private string _outputFileName;
        private string _windowTitle;
        private Graph _graph;
        private BitmapImage _image;
        private int _numberOfRandomNodesToGenerate;
        private readonly FolderBrowserDialog _folderBrowserDialog;

        public event PropertyChangedEventHandler PropertyChanged;
        public const string DefaultWindowName = @"ANAT ⌂ Sarjoun.com ";//©

        public GraphWindowContext()
        {
            GraphsOutputFolderPath = FileHelper.CheckForOutputFolderAndCreateItIfNotFound();
            _startupFolder = FileHelper.StartupFolderPath;
            _folderBrowserDialog = new FolderBrowserDialog();
            //commands
            GenerateGraphFromMemoryCommand = new GenerateGraphCommandImplementor(this);
            ConsumeDataFileAndGenerateGraphCommand = new ConvertDataFileIntoGraphCommandImplementor(this);
            ConsumeDataFileAndGenerateGraphCommandFromFolder = new ConvertDataFilesFromFolderIntoGraphCommandImplementor(this);
            ConsumeDataFileThenAddToCurrentGraphCommand = new ConvertDataFileIntoGraphThenMergeCommandImplementor(this);
            GenerateRandomGraphCommand = new GenerateRandomGraphCommandImplementor(this);
            ConsumeDataFileAndGenerateGraphCommandUsingNewFormat = new ConsumeDataFileAndGenerateGraphCommandUsingNewFormatImplementor(this);

            NumberOfRandomNodesToGenerate = 10;//initialize
            CanGenerateRandomGraph = true;
            ImageVisibility = true;
            DisplayedOutputFileName = "No file loaded";
            WindowTitle = "";
        }

        public GraphsOutputFolderPath GraphsOutputFolderPath { get; set; }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string WindowTitle
        {
            get { return _windowTitle; }
            set
            {
                _windowTitle = value.Length > 0 ? DefaultWindowName + "  ▲ " + value + "  ▼ " : DefaultWindowName;//○ 8 ~ ≈[-A╥Σ~A6▼⌂A┼╞
                OnPropertyChanged();
            }
        }
        public bool CanGenerateRandomGraph
        {
            get { return _canGenerateRandomGraph; }
            set
            {
                _canGenerateRandomGraph = value;
                ImageVisibility = true;
                OnPropertyChanged();
            }
        }
        public string GraphTextVersion
        {
            get { return _graphTextVersion; }
            set
            {
                ImageVisibility = false;
                _graphTextVersion = value;
                OnPropertyChanged();
            }
        }
        public bool ImageVisibility
        {
            get { return _imageVisibility; }
            set
            {
                _imageVisibility = value;
                OnPropertyChanged();
            }
        }
        public BitmapImage GraphImage
        {
            get { return _image; }
            private set
            {
                _image = value;
                OnPropertyChanged();
            }
        }
        public string DisplayedOutputFileName
        {
            get { return _outputFileName; }
            set
            {
                WindowTitle = value.Trim();
                _outputFileName = value.Trim();
                OnPropertyChanged();
            }
        }
        public string OutputFile
        {
            get { return _outputFilename; }
            set
            {
                _outputFilename = value;
                OnPropertyChanged();
            }
        }
        public int NumberOfRandomNodesToGenerate
        {
            get { return _numberOfRandomNodesToGenerate; }
            set
            {
                if (!int.TryParse(value.ToString(), out _numberOfRandomNodesToGenerate))
                    return;

                if (_numberOfRandomNodesToGenerate > 500)
                    _numberOfRandomNodesToGenerate = 1;
                OnPropertyChanged();
            }
        }
        public string OpenFile()
        {
            // Create OpenFileDialog
            var fileDialog = new Microsoft.Win32.OpenFileDialog
            {
                // Set filter for file extension and default file extension
                DefaultExt = ".txt",
                Filter = "TXT Files|*.txt",
                InitialDirectory = _startupFolder,
                RestoreDirectory = true
            };

            var result = fileDialog.ShowDialog();
            if (result != true) return "";

            _startupFolder = Path.GetDirectoryName(fileDialog.FileName);//fileDialog.InitialDirectory;
            return fileDialog.FileName;
        }
        public void OpenFileAndDisplayContent()
        {
            DisplayedOutputFileName = OpenFile();
            if (!string.IsNullOrEmpty(DisplayedOutputFileName))
            {
                GraphTextVersion = FileHelper
                    .ReadFile(DisplayedOutputFileName)
                    .Aggregate((current, next) => current + "\r\n" + next);
            }
        }
        public void DisplayContent()
        {
            if (!string.IsNullOrEmpty(DisplayedOutputFileName))
            {
                GraphTextVersion = FileHelper
                    .ReadFile(DisplayedOutputFileName)
                    .Aggregate((current, next) => current + "\r\n" + next);
            }
        }
        public async Task OpenFileAndGenerateGraphThenMergeAsync()
        {
            DisplayedOutputFileName = OpenFile();
            if (!string.IsNullOrEmpty(DisplayedOutputFileName))
                await GenerateGraphFromFileThenMergeAsync();
        }
        public async Task OpenFileAndGenerateGraphAsync()
        {
            DisplayedOutputFileName = OpenFile();
            if (!string.IsNullOrEmpty(DisplayedOutputFileName))
                await GenerateGraphFromFileAsync();
        }

        public async Task GenerateFileFromNewFormatGraphAsync()
        {
            DisplayedOutputFileName = OpenFile();
            if (!string.IsNullOrEmpty(DisplayedOutputFileName))
                await GenerateGraphFromFileNewFormatAsync();
        }

        public async Task OpenFolderAndGenerateGraphAsync()
        {
            DisplayedOutputFileName = OpenMainFolderAndReturnFolderPath();
            if (!string.IsNullOrEmpty(DisplayedOutputFileName))
                await GenerateGraphFromFolderAsync(DisplayedOutputFileName);
        }
        public async Task OpenAndParseLDAFileIntoTypesAsync()
        {
            DisplayedOutputFileName = OpenFile();
            if (!string.IsNullOrEmpty(DisplayedOutputFileName))
                await ParseLDASettingsFileIntoTypesAsync(
                    CurrentSimulationSettingsFileName.FromValue(DisplayedOutputFileName), ShowParsedFile.Yes);
        }

        public string OpenMainFolderAndReturnFolderPath()
        {
            return _folderBrowserDialog.ShowDialog() != DialogResult.OK
                ? null
                : _folderBrowserDialog.SelectedPath;
        }

        public async Task<bool> GenerateGraphFromFileThenMergeAsync()
        {
            var result = SdGraphVizTextParser.ProcessFileIntoExistingNodeNamesAndEdges(
                FileHelper.ReadFile(DisplayedOutputFileName));

            ImageVisibility = true;
            DisplayedOutputFileName = FileHelper.GenerateARandomFileForOutput(".png");
            CanGenerateRandomGraph = false;
            _graph = new Graph(DisplayedOutputFileName);
            try
            {
                CanGenerateRandomGraph = await _graph.CreateAndSaveGraphUsingFormatPngAsync(result, GraphKinds.Directed);
                GraphImage = GraphicalLibrary.RenderImage(DisplayedOutputFileName);
            }
            catch (Exception e)
            {
                DisplayedOutputFileName = "Error in displaying graph! " + e.Message;
            }
            return true;
        }

        public async Task<bool> GenerateGraphFromFileNewFormatAsync()
        {
            var resultingSDGraph = SdGraphVizTextParser.PopulateSDGraphUsingNewFormatStringArray(
                FileHelper.ReadFile(DisplayedOutputFileName));

            ImageVisibility = true;
            DisplayedOutputFileName = FileHelper.GenerateARandomFileForOutput(".png");
            CanGenerateRandomGraph = false;
            _graph = new Graph(DisplayedOutputFileName);
            try
            {
                CanGenerateRandomGraph = await _graph.CreateAndSaveGraphUsingFormatPngAsync(resultingSDGraph, GraphKinds.Directed);
                GraphImage = GraphicalLibrary.RenderImage(DisplayedOutputFileName);
            }
            catch (Exception e)
            {
                DisplayedOutputFileName = "Error in displaying graph! " + e.Message;
            }
            return true;
        }

        public async Task<bool> GenerateGraphFromFileAsync()
        {
            var result = SdGraphVizTextParser.ProcessFileIntoNewNodeNamesAndEdges(
                FileHelper.ReadFile(DisplayedOutputFileName));

            ImageVisibility = true;
            DisplayedOutputFileName = FileHelper.GenerateARandomFileForOutput(".png");
            CanGenerateRandomGraph = false;
            _graph = new Graph(DisplayedOutputFileName);
            try
            {
                CanGenerateRandomGraph = await _graph.CreateAndSaveGraphUsingFormatPngAsync(result, GraphKinds.Undirected);
                GraphImage = GraphicalLibrary.RenderImage(DisplayedOutputFileName);
            }
            catch (Exception e)
            {
                DisplayedOutputFileName = "Error in displaying graph! " + e.Message;
            }
            return true;
        }

        //private static string GetDirectoryPathNameFromFile(Option<string> filePath)
        //{
        //    return filePath.HasValue ?
        //        Path.GetDirectoryName(filePath.Value) :
        //        "";
        //}
        public async Task<bool> GenerateGraphFromFolderAsync(string filePath)
        {
            //var extractedFolderPath = Path.GetDirectoryName(filePath);

            //if (string.IsNullOrEmpty(extractedFolderPath))
            //    return false;

            var filesGraphs = new List<string>();
            var folders = Directory.GetDirectories(filePath);

            foreach (var folder in folders)
            {
                filesGraphs.AddRange(Directory.GetFiles(folder, "*SecondMergeOfAgentsGraph*MostPopular*.txt"));
            }

            var result = new SdGraph();
            foreach (var fileGraph in filesGraphs)
            {
                result = SdGraphVizTextParser.ProcessFileIntoExistingNodeNamesAndEdges(
                    FileHelper.ReadFile(fileGraph));
            }

            ImageVisibility = true;
            DisplayedOutputFileName = FileHelper.GenerateARandomFileForOutput(".png");
            CanGenerateRandomGraph = false;
            _graph = new Graph(DisplayedOutputFileName);
            try
            {
                CanGenerateRandomGraph = await _graph.CreateAndSaveGraphUsingFormatPngAsync(result, GraphKinds.Undirected);
                GraphImage = GraphicalLibrary.RenderImage(DisplayedOutputFileName);
            }
            catch (Exception e)
            {
                DisplayedOutputFileName = "Error in displaying graph! " + e.Message;
            }
            return true;
        }

        //  Three things to note in the signature: 
        //   - The method has an async modifier.  
        //   - The return type is Task or Task<T>. (See "Return Types" section.)
        //     Here, it is Task because the return statement is void.
        //   - The method name ends in "Async."

        public async Task RenderAndDisplayDefaultStaticGraphTaskAsync(string outputGraphFile)
        {
            ImageVisibility = true;
            DisplayedOutputFileName = outputGraphFile;
            _graph = new Graph(outputGraphFile);
            await _graph.CreateRenderAndSaveDefaultStaticGraphFormatPng();
            GraphImage = GraphicalLibrary.RenderImage(outputGraphFile);
        }
        public async Task<bool> RenderAndDisplayRandomGraphTaskAsync(string outputGraphFile)
        {
            ImageVisibility = true;
            DisplayedOutputFileName = outputGraphFile;
            CanGenerateRandomGraph = false;
            _graph = new Graph(outputGraphFile);
            try
            {
                CanGenerateRandomGraph = await _graph.
                    CreateRenderAndSaveRandomGraphFormatPngAsync(NumberOfRandomNodesToGenerate);
                GraphImage = GraphicalLibrary.RenderImage(outputGraphFile);
            }
            catch (Exception e)
            {
                DisplayedOutputFileName = "Error in displaying graph! " + e.Message;
            }
            return true;
        }
        public async Task<bool> RenderAndDisplayGraphTaskAsync(string outputGraphFile)
        {
            ImageVisibility = true;
            DisplayedOutputFileName = outputGraphFile;
            CanGenerateRandomGraph = false;
            _graph = new Graph(outputGraphFile);
            try
            {
                CanGenerateRandomGraph = await _graph.
                    CreateRenderAndSaveRandomGraphFormatPngAsync(NumberOfRandomNodesToGenerate);
                GraphImage = GraphicalLibrary.RenderImage(outputGraphFile);
            }
            catch (Exception e)
            {
                DisplayedOutputFileName = "Error in displaying graph! " + e.Message;
            }
            return true;
        }

        public async Task<ParsedSimulationSettings> ParseLDASettingsFileIntoTypesAsync(
            CurrentSimulationSettingsFileName currentSimulationSettingsFileName, ShowParsedFile showParsedFile)
        {
            var result = await MainParserForSettingsFile.GetLDASettingsFromFile(currentSimulationSettingsFileName);

            if (showParsedFile == ShowParsedFile.No)
            {
                GraphTextVersion = "Parsed LDA settings file successfully.";
                return result;
            }
            //Display some contents of the types in the file:
            try
            {
                GraphTextVersion = "The value of the start date is " + result.StartDate.Value + "\r\n";
                GraphTextVersion += "The value of the finish date is " + result.FinishDate.Value + "\r\n";
                GraphTextVersion += "The value of the Media is " + result.MediaName.Value + "\r\n";
                GraphTextVersion += "The value of the random seed is " + result.RandomSeed.Value + "\r\n";
                GraphTextVersion += "The value of the output folder is " + result.OutputFolder.Value + "\r\n";
                GraphTextVersion += "The value of the root folder (path) is " + result.RootFolderPath.Value + "\r\n";
                GraphTextVersion += "The value of the root database folder (path) is " + result.DatabaseRootFolderPath.Value + "\r\n";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return result;
        }
    }
}
