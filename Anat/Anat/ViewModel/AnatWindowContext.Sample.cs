#if DEBUG || SAMPLE_BEHIND

// place using directives here to properly exclude the entire file based on #if directive
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Anat.Annotations;

namespace Anat.ViewModel
{
    public class AnatWindowContextSample : INotifyPropertyChanged, IAnatWindowContext
    {
        private string _outputFileName;
        private string _windowTitle;

        public ICommand ConsumeDataFileCommand { get; } = null;
        public ICommand ParseLDASettingsFileAndTransformIntoTypesCommand { get; } = null;
        public string Version { get; } = "v.1.1.1.1";
        public ICommand OpenGraphicsWindowCommand { get; } = null;
        public ICommand OpenTextStatisticsWindowCommand { get; } = null;

        public ICommand CreateFileForLDASimulationSettingsCommand { get; } = null;
        public ICommand RunILDACommand { get; } = null;
        public ICommand ParseIONASettingsFileAndTransformIntoTypesCommand { get; } = null;
        public ICommand RunIONACommand { get; } = null;
        public ICommand RunILDAandIONACommand { get; } = null;
        public ICommand CalculateNetworkStatisticsAndWriteOutputCommand { get; } = null;
        public ICommand CalculateTextStatisticsAndWriteOutputCommand { get; } = null;

        public Task RenderAndDisplayDefaultStaticGraphTaskAsync(string outputGraphFile)
        {
            throw new NotImplementedException();
        }

        public Task<bool> RenderAndDisplayRandomGraphTaskAsync(string outputGraphFile)
        {
            throw new NotImplementedException();
        }

        public Task<bool> RenderAndDisplayGraphTaskAsync(string outputGraphFile)
        {
            throw new NotImplementedException();
        }

        public Task OpenFileAndGenerateGraphAsync()
        {
            throw new NotImplementedException();
        }

        public Task OpenFileAndGenerateGraphThenMergeAsync()
        {
            throw new NotImplementedException();
        }

        public Task OpenAndParseLDAFileIntoTypes()
        {
            throw new NotImplementedException();
        }

        public string DisplayedOutputFileName { get; set; }
        public string GraphTextVersion { get; set; }
        //public bool ImageVisibility { get; }

        public AnatWindowContextSample()
        {
            /*
            The resource file is embedded, meaning it is incapsulated inside a DLL assembly. 
            Therefore one cannot get the real path, the solution would be to change your approach.
            You would probably want to load the resource in memory and write it down to a temp file, 
            then link it from there. Once the icon is changed on the destination file, you can delete 
            the icon file itself.
            */
            WindowTitle = "Anat Test";
            GraphTextVersion = "Lebanon-Beirut-Political-Climate\r\nLebanon-Beirut-Demonstrations";
            //ImageVisibility = false;
        }

        public const string DefaultWindowName = @"Anat Control Window";//@"Anat Control Window - {Anat} - � Sarjoun.com";

        public string WindowTitle
        {
            get { return _windowTitle; }
            set
            {
                _outputFileName = DefaultWindowName;
                _windowTitle = DefaultWindowName;//+ "  " + value;
                OnPropertyChanged();
            }
        }

        public string OutputFile
        {
            get { return _outputFileName; }
            set
            {
                _outputFileName = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}

#endif
