#if DEBUG || SAMPLE_BEHIND

// place using directives here to properly exclude the entire file based on #if directive
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Anat.Annotations;
using Image = System.Windows.Controls.Image;

namespace Anat.ViewModel
{
    public class GraphWindowContextSample : INotifyPropertyChanged, IGraphWindowContext
    {
        private string _outputFileName;
        private BitmapImage _image;
        private string _windowTitle;

        public ICommand GenerateGraphFromMemoryCommand { get; } = null;
        public ICommand GenerateRandomGraphCommand { get; } = null;
        public ICommand ConsumeDataFileCommand { get; } = null;
        public ICommand ConsumeDataFileAndGenerateGraphCommand { get; } = null;
        public ICommand ConsumeDataFileAndGenerateGraphCommandFromFolder { get; } = null;
        public ICommand ConsumeDataFileThenAddToCurrentGraphCommand { get; } = null;
        public ICommand ConsumeDataFileAndGenerateGraphCommandUsingNewFormat { get; } = null;
        public ICommand ParseLDASettingsFileAndTransformIntoTypesCommand { get; } = null;
        public ICommand CreateFileForLDASimulationSettingsCommand { get; } = null;
        public ICommand RunILDACommand { get; } = null;
        public ICommand CreateFileForIONASimulationSettingsCommand { get; } = null;
        public ICommand ParseIONASettingsFileAndTransformIntoTypesCommand { get; } = null;
        public ICommand RunIONACommand { get; } = null;

        public Task RenderAndDisplayDefaultStaticGraphTaskAsync(string outputGraphFile)
        {
            throw new NotImplementedException();
        }

        public Task<bool> RenderAndDisplayRandomGraphTaskAsync(string outputGraphFile)
        {
            throw new NotImplementedException();
        }

        public Task<bool> RenderAndDisplayGraphTaskAsync(string outputGraphFile)
        {
            throw new NotImplementedException();
        }

        public Task OpenFileAndGenerateGraphAsync()
        {
            throw new NotImplementedException();
        }

        public Task OpenFileAndGenerateGraphThenMergeAsync()
        {
            throw new NotImplementedException();
        }

        public Task OpenAndParseLDAFileIntoTypesAsync()
        {
            throw new NotImplementedException();
        }

        public bool CanGenerateRandomGraph { get; } = true;
        public string DisplayedOutputFileName { get; set; }
        public string GraphTextVersion { get; set; }
        public bool ImageVisibility { get; }
        public int NumberOfRandomNodesToGenerate { get; set; }

        public GraphWindowContextSample()
        {
            /*
            The resource file is embedded, meaning it is incapsulated inside a DLL assembly. 
            Therefore one cannot get the real path, the solution would be to change your approach.
            You would probably want to load the resource in memory and write it down to a temp file, 
            then link it from there. Once the icon is changed on the destination file, you can delete 
            the icon file itself.
            */

            OutputFile = @"C:/Users/sdoumit/Documents/Projects/Anat/Anat/Resources/SampleGraph.png";
            //OutputFile = GetAnatResourcesFolder();
            //OutputFile = "/Resources/SampleGraph.png";

            //@"Resources/SampleGraph.png";//"pack://application:,,,/Resources/SampleGraph.png";
            GraphImage = RenderImage();
            NumberOfRandomNodesToGenerate = 20;
            WindowTitle = "SampleGraph.png";
            GraphTextVersion = "Lebanon-Beirut-Political-Climate\r\nLebanon-Beirut-Demonstrations";

            ImageVisibility = true;

            WindowTitle = "Test";
        }

        //public const string MainFolderName = @"\anat\";
        public const string DefaultWindowName = @"Main Control Window - {Anat} - � Sarjoun.com";

        //public static string GetAnatResourcesFolder()
        //{
        //    var mainFolderNameSize = MainFolderName.Length;
        //    var graphVizBinFolderPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
        //    var location = graphVizBinFolderPath.ToLower().IndexOf(MainFolderName, StringComparison.Ordinal) +
        //                   mainFolderNameSize;

        //    return graphVizBinFolderPath.Substring(0, location) +
        //           @"\Anat\Resources\SampleGraph.png";
        //}

        public string WindowTitle
        {
            get { return _windowTitle; }
            set
            {
                _windowTitle = DefaultWindowName + "  " + value;
                OnPropertyChanged();
            }
        }

        public string OutputFile
        {
            get { return _outputFileName; }
            set
            {
                _outputFileName = value;
                OnPropertyChanged();
            }
        }

        public BitmapImage GraphImage
        {
            get { return _image; }
            set
            {
                _image = value;
                OnPropertyChanged();
            }
        }

        private BitmapImage RenderImage()
        {
            var image = new BitmapImage();

            try
            {
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
                image.UriSource = new Uri(OutputFile, UriKind.Relative);
                image.EndInit();
            }
            catch
            {
                return (BitmapImage)DependencyProperty.UnsetValue;
            }
            return image;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

#endif
