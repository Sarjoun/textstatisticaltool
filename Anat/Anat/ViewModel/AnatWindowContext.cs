﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Anat.Annotations;
using Anat.Commands.Commands.Anat;
using Anat.Commands.Commands.Statistics;
using Anat.Enums;
using Anat.Extensions;
using Anat.Helpers;
using Anat.StoryGraphs;
using Anat.Views;
//using ILDA;
//using ILDA.Algorithm;
using IONA.Algorithm.Algorithm.Helpers;
using SDParser;
using SDParser.ClusteringTypes;
using SDParser.ComplexTypes;
using SDParser.Enums;
using SDParser.FoldersManagement;
using SDParser.Notifications;
using SDParser.Primitives;

namespace Anat.ViewModel
{
    public class AnatWindowContext : INotifyPropertyChanged, IAnatWindowContext
    {
        public ICommand ConsumeDataFileCommand { get; }
        public ICommand ParseLDASettingsFileAndTransformIntoTypesCommand { get; }
        public ICommand ParseIONASettingsFileAndTransformIntoTypesCommand { get; }
        //public ICommand RunILDACommand { get; }
        //public ICommand RunIONACommand { get; }
        //public ICommand RunILDAandIONACommand { get; }
        public ICommand OpenGraphicsWindowCommand { get; }
        public ICommand OpenTextStatisticsWindowCommand { get; }
        public ICommand CreateFileForLDASimulationSettingsCommand { get; }
        public ICommand CalculateNetworkStatisticsAndWriteOutputCommand { get; }

        private string _startupFolder;
        private string _outputFilename;
        private string _graphTextVersion;
        private string _outputFileName;
        private string _windowTitle;
        public event PropertyChangedEventHandler PropertyChanged;
        public const string DefaultWindowName = @"ANAT";//@"ANAT ⌂ Sarjoun.com ";
        private static float _countOfTierStartingDocuments;
        private static float _countOfTierTaggedDocuments;
        private static float _originalStartDocumentsCount;
        public static DocumentsLinksFileName OriginalDocumentsLinksFileName;
        public List<Agent> ClusteredAgents { get; set; }

        public AnatWindowContext()
        {
            _startupFolder = FileHelper.StartupFolderPath;
            DisplayedOutputFileName = "No file loaded";
            WindowTitle = "";
            GraphTextVersion = "Hello " + Environment.UserName; //WindowsIdentity.GetCurrent().Name;

            //commands
            ConsumeDataFileCommand = new OpenDataFileCommandImplementor(this);
            CreateFileForLDASimulationSettingsCommand = new CreateSampleLDASettingsFileCommandImplementor(this);
            ParseLDASettingsFileAndTransformIntoTypesCommand = new ParseLDASettingsFileAndTransformIntoTypesCommandImplementor(this);
            ParseIONASettingsFileAndTransformIntoTypesCommand = new ParseIONASettingsFileAndTransformIntoTypesCommandImplementor(this);
            //RunILDACommand = new RunLDACommandImplementor(this);
            //RunIONACommand = new RunIONACommandImplementor(this);
            //RunILDAandIONACommand = new RunILDAandIONACommandImplementor(this);
            OpenGraphicsWindowCommand = new OpenGraphAnalysisWindowCommand(this);
            OpenTextStatisticsWindowCommand = new OpenTextStatisticsWindowCommandImplementor(this);
            CalculateNetworkStatisticsAndWriteOutputCommand = new CalculateNetworkStatisticsCommandImplementor(this);
        }

        private StatisticsSources UpdateStatisticsSources()
        {
            return StatisticsSources.FromValue(FileReader.GetSettingValueFromFileAsync(DisplayedOutputFileName, "StatisticsSources").Result);
        }

        public void CodeDemoDefaultAnswer()
        {
            WindowTitle = GraphTextVersion = "ANAT analysis not available for the public yet!";
            DisplayOutputForConditionsForStoppingMet(DateTime.Now, ReasonForEndingSimulation.CodeNotForPublicSharing);
            MouseReady();
        }
        
        public async Task OpenAndParseLDAFileIntoTypes()
        {
            DisplayedOutputFileName = ReturnFilePathFromDialog();
            if (!string.IsNullOrEmpty(DisplayedOutputFileName))
                await ParseLDASettingsFileIntoTypesAsync(
                    CurrentSimulationSettingsFileName.FromValue(DisplayedOutputFileName), ShowParsedFile.Yes);
        }
        public async Task OpenAndParseIONAFileIntoTypesAsync()
        {
            DisplayedOutputFileName = ReturnFilePathFromDialog();
            if (!string.IsNullOrEmpty(DisplayedOutputFileName))
                await ParseIONASettingsFileIntoTypesAsync(
                IONARunSettingsFileName.FromValue(DisplayedOutputFileName),
                ShowParsedFile.Yes);
        }

        public string WindowTitle
        {
            get { return _windowTitle; }
            set
            {
                _windowTitle = value.Length > 0
                    ? DefaultWindowName + " " + Version + "  ▲ " + value + "  ▼ "
                    : DefaultWindowName + " " + Version;
                OnPropertyChanged();
            }
        }
        public string Version
        {
            get
            {
                var fullName = Assembly.GetExecutingAssembly().FullName;
                var startLocation = fullName.IndexOf('=') + 1;
                var endLocation = fullName.IndexOf(", Culture", StringComparison.Ordinal);
                return "v." + fullName.Substring(startLocation, endLocation - startLocation);
            }
        }
        public string DisplayedOutputFileName
        {
            get { return _outputFileName; }
            set
            {
                WindowTitle = value.Trim();
                _outputFileName = value.Trim();
                OnPropertyChanged();
            }
        }
        public string GraphTextVersion
        {
            get { return _graphTextVersion; }
            set
            {
                //ImageVisibility = false;
                _graphTextVersion = value;
                OnPropertyChanged();
            }
        }
        public string OutputFile
        {
            get { return _outputFilename; }
            set
            {
                _outputFilename = value;
                OnPropertyChanged();
            }
        }
        public string ReturnFilePathFromDialog()
        {
            var fileDialog = new Microsoft.Win32.OpenFileDialog
            {
                Title = "Look for the LDA or IONA simulation settings file (.txt)",
                DefaultExt = ".txt",
                Filter = "TXT Files|*.txt",
                InitialDirectory = _startupFolder,
                RestoreDirectory = true
            };

            var result = fileDialog.ShowDialog();
            if (result != true) return "";

            _startupFolder = Path.GetDirectoryName(fileDialog.FileName);//fileDialog.InitialDirectory;
            return fileDialog.FileName;
        }
        public CurrentSimulationSettingsFileName CurrentSimulationSettingsFileName { get; set; }

        public void OpenFileAndDisplayContent()
        {
            DisplayedOutputFileName = ReturnFilePathFromDialog();
            if (!string.IsNullOrEmpty(DisplayedOutputFileName))
            {
                GraphTextVersion = FileHelper
                    .ReadFile(DisplayedOutputFileName)
                    .Aggregate((current, next) => current + "\r\n" + next);
            }
        }
        public void DisplayContentOfDisplayedOutputFileName()
        {
            if (!string.IsNullOrEmpty(DisplayedOutputFileName))
            {
                GraphTextVersion = FileHelper
                    .ReadFile(DisplayedOutputFileName)
                    .Aggregate((current, next) => current + "\r\n" + next);
            }
        }

        public bool WriteLDASampleRunFileOnDesktop()
        {
            return FolderManager.CreateOrOverwriteLDASampleSettingsFileOnDesktop();
        }
        public bool CalculateNetworkStatisticsAndWriteOutput()
        {
            return true;//FolderManager.CreateOrOverwriteLDASampleSettingsFileOnDesktop();
        }

        public async Task<ParsedSimulationSettings> ParseLDASettingsFileIntoTypesAsync(CurrentSimulationSettingsFileName currentSimulationSettingsFileName,
            ShowParsedFile showParsedFile)
        {
            //Display some results in shape of contents of the types in the file:
            try
            {
                var result = await MainParserForSettingsFile.GetLDASettingsFromFile(currentSimulationSettingsFileName);

                if (!result.CheckForMissingSettingInSimulationFile())
                {
                    ShowMessage.ShowWarningMessageAndExit("The LDA settings file has a missing parameter "
                        + result.MissingSettingName +
                        ". The simulation cannot proceed.");
                }

                if (showParsedFile == ShowParsedFile.No)
                    return result;

                string displayName;
                if (UserPrincipal.Current.DisplayName == null)
                    displayName = "Anonymous @ " + UserPrincipal.Current.Context.ConnectedServer;
                else
                    displayName = UserPrincipal.Current.DisplayName;

                if (result.StartDate == null)
                    GraphTextVersion = "Hey " + displayName + ":\r\nThe file does not conform to the LDA format!";
                else
                {
                    GraphTextVersion = "The value of the tier is " + result.Tier.Value + "\r\n";
                    GraphTextVersion += "The value of the start date is " + result.StartDate.Value + "\r\n";
                    GraphTextVersion += "The value of the finish date is " + result.FinishDate.Value + "\r\n";
                    GraphTextVersion += "The value of the media is " + result.MediaName.Value + "\r\n";
                    GraphTextVersion += "The value of the horizon size is " + result.HorizonSize.Value + "\r\n";
                    GraphTextVersion += "The value for CalculateCorrelationCoefficient is " + result.CalculateCorrelationCoefficient.Value + "\r\n";
                }
                return result;
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit("Something went wrong when parsing the LDA file", e);
                Console.WriteLine(e);
            }
            throw new Exception("Error parsing LDA");
        }
        public async Task<AllSettings> ParseIONASettingsFileIntoTypesAsync(IONARunSettingsFileName ionaRunSettingsFileName, ShowParsedFile showParsedFile)
        {
            var result = await MainParserForSettingsFile.GetIONASettingsFromFileAsync(ionaRunSettingsFileName);

            if (showParsedFile == ShowParsedFile.No)
            {
                return result;
            }
            try
            {
                GraphTextVersion = "The value of the tier is " + result.Tier.Value + "\r\n";
                GraphTextVersion += "The value of the start date is " + result.StartDate.Value + "\r\n";
                GraphTextVersion += "The value of the finish date is " + result.FinishDate.Value + "\r\n";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return result;
        }

        private void DisplayOutputForConditionsForStoppingNotMet(AllSettings generalSettingsForIONA)
        {
            GraphTextVersion += "\r\nTier starting documents = " + _countOfTierStartingDocuments;
            GraphTextVersion += "\r\nTier tagged documents = " + _countOfTierTaggedDocuments;
            GraphTextVersion += "\r\nTier untagged documents = " + (_countOfTierStartingDocuments - _countOfTierTaggedDocuments);
            GraphTextVersion += "\r\nTier percentage tagging = %" + (_countOfTierTaggedDocuments / _countOfTierStartingDocuments) * 100;
            GraphTextVersion += GetStartedWorkingString(generalSettingsForIONA.Tier);
        }
        private void DisplayOutputForConditionsForStoppingMet(DateTime simulationStartedTime, ReasonForEndingSimulation reasonForEnding)
        {
            GraphTextVersion += "\r\nTier starting documents = " + _countOfTierStartingDocuments;
            GraphTextVersion += "\r\nTier tagged documents = " + _countOfTierTaggedDocuments;
            GraphTextVersion += "\r\nTier untagged documents = " + (_countOfTierStartingDocuments - _countOfTierTaggedDocuments);
            GraphTextVersion += "\r\nTier percentage tagging = %" + (_countOfTierTaggedDocuments / _countOfTierStartingDocuments) * 100;
            GraphTextVersion += "\r\nReason for ending simulation = " + reasonForEnding.DescriptionAttr();
            GraphTextVersion += GetDivider();
            GraphTextVersion += "\r\nSimulation completed successfully at [" + DateTime.Now.ToString("h:mm:ss tt") + "]";
            GraphTextVersion += "\r\nTotal starting documents = " + _originalStartDocumentsCount;
            var totalTaggedDocuments = (_originalStartDocumentsCount - _countOfTierStartingDocuments + _countOfTierTaggedDocuments);
            GraphTextVersion += "\r\nTotal tagged documents = " + totalTaggedDocuments;
            GraphTextVersion += "\r\nTotal untagged documents = " + (_countOfTierStartingDocuments - _countOfTierTaggedDocuments);
            GraphTextVersion += "\r\nTotal percentage tagging = % " + (totalTaggedDocuments / _originalStartDocumentsCount) * 100 + "\r\n";

            WindowTitle = "Simulation completed successfully!";

            var totalSimulationTime = $"{(DateTime.Now - simulationStartedTime).Days} days, " +
                                      $"{(DateTime.Now - simulationStartedTime).Hours} hours, " +
                                      $"{(DateTime.Now - simulationStartedTime).Minutes} minutes, " +
                                      $"{(DateTime.Now - simulationStartedTime).Seconds} seconds";
            GraphTextVersion += "\r\n-- Total Simulation time --";
            GraphTextVersion += "\r\n" + totalSimulationTime;
        }

        public void OpenGraphicsWindow()
        {
            var graphWindow = new Views.GraphWindow();
            graphWindow.Show();
        }
        public void OpenTextStatisticsWindow()
        {
            var textStatisticsWindow = new TextStatisticsWindow();
            textStatisticsWindow.Show();
        }

        private static string PrintOutSimulationStartedInfo()
        {
            var graphTextVersion = "Simulation started at [" + DateTime.Now.ToString("h:mm:ss tt") + "]";
            graphTextVersion += "\r\n\r\nStarted working on Tier 1 [" + DateTime.Now.ToString("h:mm:ss tt") + "]";
            return graphTextVersion;
        }
        private static string GetStartedWorkingString(Tier tier)
        {
            return "\r\n\r\nStarted working on Tier " + (tier.Value + 1) + " [" + DateTime.Now.ToString("h:mm:ss tt") + "]";
        }
        private static string GetDivider()
        {
            return "\r\n____________________________________";
        }

        private static void MouseWait()
        {
            Application.Current.Dispatcher.Invoke(() => { Mouse.OverrideCursor = Cursors.Wait; });
        }
        private static void MouseReady()
        {
            Application.Current.Dispatcher.Invoke(() => { Mouse.OverrideCursor = null; });
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
