using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Anat.Annotations;
using Anat.Enums;
using SDParser.ComplexTypes;

#if DEBUG || SAMPLE_BEHIND

// place using directives here to properly exclude the entire file based on #if directive
namespace Anat.ViewModel
{
    public class TextStatisticsWindowContextSample : INotifyPropertyChanged, ITextStatisticsWindowContext
    {
        public ICommand CalculateTextStatisticsAndWriteResultsStandAloneCommand { get; } = null;
        public ICommand CalculateTextStatisticsAndWriteResultsFromFileCommand { get; } = null;
        public ICommand OpenMainFolderAndIterateThroughSourcesCommand { get; } = null;
        public ICommand ParseSettingsFileAndExtractTextStatisticsSettingsValues { get; } = null;
        public Task ProcessTextStatisticsFromFoldersOriginalAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task ProcessTextStatisticsFromFileAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task<TextStatisticsSettings> OpenAndParseSimulationFileAndExtractTextStatisticsSettingsAsync(ShowParsedFile showParsedFile)
        {
            throw new System.NotImplementedException();
        }

        public string StatisticsWindowTitle { get; set; }
        public string TextStatisticsOutputWindow { get; set; }

        public TextStatisticsWindowContextSample()
        {
            StatisticsWindowTitle = "Text Statistics";
            TextStatisticsOutputWindow = "Discovered 10 Sources";
            //OnPropertyChanged();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

#endif
