﻿using System.Linq;

namespace Anat.Libraries
{
    public static class TextModifier
    {
        private static readonly string[] CharsToRemove = { "@", ",", ".", ";", "'" };

        public static string RemoveCharactersFromText(string original)
        {
            return CharsToRemove.Aggregate(original, (current, c) => current.Replace(c, string.Empty));
        }

        public static string RemoveCharactersFromText(string original, string[] charsToRemove)
        {
            return charsToRemove.Aggregate(original, (current, c) => current.Replace(c, string.Empty));
        }
    }
}
