using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Anat.Libraries
{
    public static class GraphicalLibrary
    {
        public static BitmapImage RenderImage(string outputFile)
        {
            var image = new BitmapImage();

            try
            {
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
                image.UriSource = new Uri(outputFile, UriKind.RelativeOrAbsolute);
                image.EndInit();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error in RenderImage()." + e.Message);
                return (BitmapImage)DependencyProperty.UnsetValue;
            }
            return image;
        }
    }
}
