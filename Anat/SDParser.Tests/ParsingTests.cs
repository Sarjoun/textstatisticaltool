﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using SDParser.Primitives;

namespace SDParser.Tests
{
    [TestClass]
    public class ParsingTest
    {
        private const string File = @"C:\Users\sdoumit\Desktop\LDARunSettings.txt";
        private readonly CurrentSimulationSettingsFileName _simulationSettingsFileName;
        private const int RandomSeed = 77;
        private readonly DateTime _startDate = new DateTime(2010, 12, 5);


        public ParsingTest()
        {
            _simulationSettingsFileName = CurrentSimulationSettingsFileName.FromValue(File);
        }

        public IEnumerable<string> GetLines()
        {
            return System.IO.File.ReadAllLines(_simulationSettingsFileName);
        }

        [TestMethod]
        public void CheckThatRandomSeedIsCorrectlyProcessed()
        {
            Assert.AreEqual(
               Primitives.RandomSeed.FromValue(RandomSeed),
               MainParserForSettingsFile.ProcessAllSimulationSettingsValues(_simulationSettingsFileName).RandomSeed);
        }

        [TestMethod]
        public void CheckThatStartDateIsCorrectlyProcessed()
        {
            Assert.AreEqual(
               Primitives.StartDate.FromValue(_startDate),
               MainParserForSettingsFile.ProcessAllSimulationSettingsValues(_simulationSettingsFileName).StartDate);
        }

        [TestMethod]
        public void CheckThatFinishDateDoesNotHaveTheStartValue()
        {
            Assert.AreNotEqual(
               Primitives.FinishDate.FromValue(_startDate),
               MainParserForSettingsFile.ProcessAllSimulationSettingsValues(_simulationSettingsFileName).FinishDate);
        }
    }
}
