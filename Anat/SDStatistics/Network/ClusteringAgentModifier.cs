﻿using System;
using System.Collections.Generic;
using System.Linq;
using LeagueOfMonads;
using LeagueOfMonads.NoLambda;
using SDParser.ClusteringTypes;
using SDParser.Notifications;
using SDParser.Structures;

namespace SDStatistics.Network
{
    public static class ClusteringAgentModifier
    {
        //public static Dictionary<StemWord, int> VocabularyDictionary { get; set; }
        public static Dictionary<string, StemWord> VocabularyDictionary { get; set; }

        public static IEnumerable<StemWord> ProcessAgentsStemWordsFromANAT(List<Agent> clusteredAgents, List<string> stemmedWords)
        {
            return Identity.Create(ExtractStemWordsFromClusteringsOrderedById(clusteredAgents))
               .Map(SynchronizeClusteredAgentsStemWordsIdsWithStatisticsList, stemmedWords)
               .Value;
        }

        public static IEnumerable<StemWord> ExtractStemWordsFromClusteringsOrderedById(List<Agent> clusteredAgents)
        {
            var stemWords = new List<StemWord>();
            return clusteredAgents.Aggregate(stemWords, (current, agent) => current.Union(agent.Grams).ToList()).OrderBy(x => x.Id);
        }

        private static IEnumerable<StemWord> SynchronizeClusteredAgentsStemWordsIdsWithStatisticsList(IEnumerable<StemWord> stemWords, List<string> stemmedWords)
        {
            //The stemmedWords' order is the actual order in the matrix
            var stemWordsListVersion = stemWords as IList<StemWord> ?? stemWords.ToList();
            foreach (var stemWord in stemWordsListVersion.ToList())
            {
                stemWord.Id = stemmedWords.FindIndex(x => x == stemWord.TopVariantWord);
            }
            return stemWordsListVersion;
        }

        //public static IEnumerable<StemWord> SynchronizeClusteredStemWordsIdsWithStatisticsDictionary(IEnumerable<StemWord> stemWords)
        //{
        //    var stemWordsListVersion = stemWords as IList<StemWord> ?? stemWords.ToList();
        //    foreach (var stemWord in stemWordsListVersion.ToList())
        //    {
        //        stemWord.Id = VocabularyDictionary.FirstOrDefault(x => x.V.Root == stemWord.Root).Value;
        //    }
        //    return stemWordsListVersion;
        //}

        //If ID is -1, it means delete
        public static int GetClusteredStemWordId(StemWord stemWord)
        {
            try
            {
                return VocabularyDictionary.ContainsKey(stemWord.Root) 
                    ? VocabularyDictionary[stemWord.Root].Id 
                    : -1;

                //return stemWord.Root == "abbas�"
                //    ? -1
                //    : VocabularyDictionary.FirstOrDefault(x => x.Value.Root == stemWord.Root
                //                                            || x.Value.Root.Contains(stemWord.Root)
                //                                            || stemWord.Root.Contains(x.Value.Root)).Value.Id;
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit("The word " + stemWord.TopVariantWord +
                    " is found in IONA but not in the tagged ALAN corpus.\r\n" +
                    "Make a record of this word and add it to the conditions until it's fixed.", e);
                return -1;
            }
        }
    }
}
