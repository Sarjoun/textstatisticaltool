﻿using System;

namespace SDStatistics.Events
{
    public class ProcessingSourceEventArgs : EventArgs
    {
        public string Source { get; set; }
        public int Id { get; set; }
        public int TotalSourcesCount { get; set; }
        public DateTime TimeStarted { get; set; }
        
        public ProcessingSourceEventArgs(string name, int id, int totalSourcesCount, DateTime time)
        {
            Source = name;
            Id = id;
            TotalSourcesCount = totalSourcesCount;
            TimeStarted = time;
        }

    }
}
