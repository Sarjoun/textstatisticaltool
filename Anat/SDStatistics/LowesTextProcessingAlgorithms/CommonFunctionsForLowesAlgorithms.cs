﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SDParser.Structures;

namespace SDStatistics.LowesTextProcessingAlgorithms
{
    public static class CommonFunctionsForLowesAlgorithms
    {
        //public static List<StemWord> StemWords { get; set; }
        public static void PrintWordListToFile(List<StemWord> horizonWordList, StreamWriter file)
        {
            try
            {
                foreach (var StemWord in horizonWordList)
                {
                    //file.Write(StemWord.OriginalName + " ");
                    file.Write(StemWord.Root + " ");
                }
                file.WriteLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        //public static StemWord ReturnStemWord(string name)
        //{
        //    return StemWords.FirstOrDefault(w => w.Root == name);
        //}
    }
}
