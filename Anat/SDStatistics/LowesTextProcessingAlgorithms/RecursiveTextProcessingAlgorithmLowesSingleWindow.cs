﻿using System;
using System.Collections.Generic;
using System.IO;
using SDParser.Notifications;
using SDParser.Structures;
using SDStatistics.TextCorpus.DataSources;
using SDStatistics.TextCorpus.Helpers;
using SDStatistics.TextCorpus.SentenceReadingAlgorithms;

namespace SDStatistics.LowesTextProcessingAlgorithms
{
    public static class RecursiveTextProcessingAlgorithmLowesSingleWindow
    {
        public static Dictionary<int, int> FrameCounterDictionary { get; set; }
        public static int[][] StemWordMatrix { get; set; }
        public static string[] GoSingleWindow(int horizonSize, bool printLowesSingleWindowSentencesToFile, string[] allSentencesWithStemWords,
           string poetAllPoemsHorizonAlgorithmSentencesFileName, string folderPath, int[][] stemWordMatrix)
        {
            StemWordMatrix = stemWordMatrix; //check that it is not by reference and just by value// HERE 
            var debugCounter = 0;
            StreamWriter file = null;
            if (printLowesSingleWindowSentencesToFile)
            {
                try
                {
                    file = new StreamWriter(folderPath + poetAllPoemsHorizonAlgorithmSentencesFileName);
                }
                catch (Exception e)
                {
                    ShowMessage.ShowErrorMessageAndExit("Could not write file: " + folderPath + poetAllPoemsHorizonAlgorithmSentencesFileName + ".", e);
                }
            }

            FrameCounterDictionary = new Dictionary<int, int>();
            foreach (var sentenceLineString in allSentencesWithStemWords)
            {
                var start = 0;
                debugCounter++;
                string[] stemWords = sentenceLineString.Split(' ');

                if (stemWords.Length <= 0) //=> empty sentence
                    continue;  //The continue statement passes control to the next iteration of the enclosing while, do, for, 
                               //or foreach statement in which it appears.

                int end = start + stemWords.Length;

                while (start <= end)
                {
                    var lowesWindowWordList = new List<StemWord>();

                    //While sentence is still larger than the horizon
                    LowesSingleWindowAlgorithm lowesSingleWindow;
                    if ((horizonSize + start) <= end)
                    {
                        for (var i = start; i < start + horizonSize; i++)
                        {
                            if (stemWords[i] != "")
                                lowesWindowWordList.Add(StemWordsManager.ReturnStemWord(stemWords[i]));
                        }

                        if (printLowesSingleWindowSentencesToFile)
                            LowesHelpers.PrintWordListToFileAsOneLine(lowesWindowWordList, file);

                        //add to _frameDictionary
                        var frameSize = lowesWindowWordList.Count;
                        if (FrameCounterDictionary.ContainsKey(frameSize))
                            FrameCounterDictionary[frameSize] = FrameCounterDictionary[frameSize] + 1;
                        else
                            FrameCounterDictionary.Add(frameSize, 1);

                        //here is a potential bug
                        if (lowesWindowWordList.Count < 1)
                        {
                            var debugCounterInt = debugCounter;
                            ShowMessage.ShowErrorMessageAndExit($"{debugCounterInt} Is the location of the bug.");
                        }

                        lowesSingleWindow = new LowesSingleWindowAlgorithm(horizonSize, lowesWindowWordList, true);

                        List<Tuple<int, int>> matrixUpdate = lowesSingleWindow.UpdateMatrixCountsAll();

                        Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
                                                             lowesWindowWordList.Count - 1;

                        Helper.LowesTotalNumberOfWindows = Helper.LowesTotalNumberOfWindows + horizonSize - 1;
                        foreach (Tuple<int, int> t in matrixUpdate)
                        {
                            StemWordMatrix[t.Item1][t.Item2] = StemWordMatrix[t.Item1][t.Item2] + 1;
                        }
                    }
                    //When sentence is smaller than the  horizon
                    else
                    {
                        int newHorizonSize = end - start;
                        for (int i = start; i < end; i++)
                            lowesWindowWordList.Add(StemWordsManager.ReturnStemWord(stemWords[i]));

                        //The horizonWordList is the list of remaining words. If Count > 1, means last word not to be counted in pairs (but frames are still there?)
                        if (lowesWindowWordList.Count > 1) //In case you DON'T want the final word to be a pair on its own!
                                                           //if (horizonWordList.Count > 0)  //In case you DO want the final word to be a pair on its own!
                        {
                            Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
                                                                 lowesWindowWordList.Count - 1;

                            Helper.LowesTotalNumberOfWindows = Helper.LowesTotalNumberOfWindows + newHorizonSize - 1;

                            //print out the value of the horizonWordList which is the horizon sentence 
                            if (printLowesSingleWindowSentencesToFile)
                                LowesHelpers.PrintWordListToFileAsOneLine(lowesWindowWordList, file);

                            //add to _frameDictionary
                            var frameSize = lowesWindowWordList.Count;
                            if (FrameCounterDictionary.ContainsKey(frameSize))
                                FrameCounterDictionary[frameSize] = FrameCounterDictionary[frameSize] + 1;
                            else
                                FrameCounterDictionary.Add(frameSize, 1);

                            lowesSingleWindow = new LowesSingleWindowAlgorithm(newHorizonSize, lowesWindowWordList, true);
                            List<Tuple<int, int>> matrixUpdate = lowesSingleWindow.UpdateMatrixCountsAll();

                            foreach (Tuple<int, int> t in matrixUpdate)
                            {
                                StemWordMatrix[t.Item1][t.Item2] = StemWordMatrix[t.Item1][t.Item2] + 1;
                            }
                        }
                    }
                    start++;
                }
            }
            file?.Close();
            return allSentencesWithStemWords;
        }

    }
}
