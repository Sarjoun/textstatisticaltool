﻿/*
LowesSingleWindowAlgorithm horizon;
The code divides the sentences into 2 types.
1. Those where the length of the sentence is > the horizon, and
2. those that are smaller or equal.

The continue statement passes control to the next iteration of the enclosing while, do, for, or foreach statement 
in which it appears.

This variable is designed so that only the first iteration of the SingleWindowLowes counts
the ReadingFrame and the subsequent ones do not.
That's because of how the SingleWindowLowes does the counting and we don't want to over-count.

The horizonWordList is the list of remaining words. If Count > 1, means last word not to be
counted in pairs(but frames are still there ?)
In case you DON'T want the final word to be a pair on its own!
 
 */

using System;
using System.Collections.Generic;
using System.IO;
using SDParser.Notifications;
using SDParser.Structures;
using SDStatistics.TextCorpus.DataSources;
using SDStatistics.TextCorpus.Helpers;
using SDStatistics.TextCorpus.SentenceReadingAlgorithms;

namespace SDStatistics.LowesTextProcessingAlgorithms
{
    public static class RecursiveTextProcessingAlgorithmLowesDoubleWindow
    {
        public static Dictionary<int, int> FrameCounterDictionary { get; set; }
        public static int[][] StemWordMatrix { get; set; }

        public static string[] GoDoubleWindow(int horizonSize, bool printLowesDoubleWindowSentencesToFile,
              string[] allSentencesWithStemWords, string poetAllPoemsHorizonAlgorithmSentencesFileName,
              string folderPath, int[][] stemWordMatrix)
        {
            StemWordMatrix = stemWordMatrix;
            FrameCounterDictionary = new Dictionary<int, int>();
            horizonSize = horizonSize / 2 + 1;

            var file = GetOutputStreamWriter(printLowesDoubleWindowSentencesToFile,
                poetAllPoemsHorizonAlgorithmSentencesFileName, folderPath);

            foreach (var sentenceLineString in allSentencesWithStemWords)
            {
                if (string.IsNullOrEmpty(sentenceLineString.Trim()))
                    continue;
                var allStemmedWordsInSentence = sentenceLineString.Trim().Split(' ');
                var center = 0;
                var end = allStemmedWordsInSentence.Length;
                try
                {
                    while (center <= end) //While sentence is still larger than the horizon
                    {
                        center = ProcessRightHorizon(allStemmedWordsInSentence, horizonSize, center, end, printLowesDoubleWindowSentencesToFile, file);
                    }
                }
                catch (Exception e)
                {
                    ShowMessage.ShowErrorMessageAndExit("Error in RecursiveTextProcessingAlgorithmLowesDoubleWindow line 63.", e);
                }
                
                center = 0;
                Array.Reverse(allStemmedWordsInSentence);
                var controlSoThatFirstIterationSingleWindowLowesToCountReadingFramesOnly = true;
                try
                {
                    while (center <= end) //While sentence is still larger than the horizon
                    {
                        center = ProcessLeftHorizon(allStemmedWordsInSentence, horizonSize, center, end, printLowesDoubleWindowSentencesToFile, 
                            file, controlSoThatFirstIterationSingleWindowLowesToCountReadingFramesOnly);
                        
                        //here is where I reset the countFrames bool value so that the ReadingFrames do not get added.
                        if (controlSoThatFirstIterationSingleWindowLowesToCountReadingFramesOnly)
                            controlSoThatFirstIterationSingleWindowLowesToCountReadingFramesOnly = false;
                    }
                }
                catch (Exception e)
                {
                    ShowMessage.ShowErrorMessageAndExit("Error in RecursiveTextProcessingAlgorithmLowesDoubleWindow line 82.", e);
                }
            }
            file?.Close();

            var actualReadingFrames = Helper.GetReadingFrames() / 2 + 1;
            Helper.SetReadingFrames(actualReadingFrames);
            return allSentencesWithStemWords;
        }

        private static int ProcessRightHorizon(
            IReadOnlyList<string> stemWords, 
            int horizonSize, int center, int end, 
            bool printLowesDoubleWindowSentencesToFile, StreamWriter file)
        {
            var horizonWordList = new List<StemWord>();
            var centerRightHorizon = center;
            if (horizonSize + centerRightHorizon <= end)
            {
                for (var i = centerRightHorizon; i < centerRightHorizon + horizonSize; i++)
                {
                    if (stemWords[i] != "")
                        horizonWordList.Add(StemWordsManager.ReturnStemWord(stemWords[i]));
                }
                //print out the value of the horizonWordList which is the horizon sentence 
                try
                {
                    if (printLowesDoubleWindowSentencesToFile)
                        LowesHelpers.PrintWordListToFileAsOneLine(horizonWordList, file);
                }
                catch (Exception e)
                {
                    ShowMessage.ShowErrorMessageAndExit("Error in RecursiveTextProcessingAlgorithmLowesDoubleWindow " +
                                                        "line 110.", e);
                }

                //add to _frameDictionary
                var frameSize = horizonWordList.Count;
                if (FrameCounterDictionary.ContainsKey(frameSize))
                    FrameCounterDictionary[frameSize] = FrameCounterDictionary[frameSize] + 1;
                else
                    FrameCounterDictionary.Add(frameSize, 1);

                var horizon = new LowesSingleWindowAlgorithm(horizonSize, horizonWordList, true);
                List<Tuple<int, int>> matrixUpdate = horizon.GetAllStemWordsIdsTuplesFromCenterStemWordGoingInOneDirection();

                Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
                                                     horizonWordList.Count - 1;
                Helper.LowesTotalNumberOfWindows = Helper.LowesTotalNumberOfWindows +
                                                   horizonSize - 1;
                foreach (Tuple<int, int> t in matrixUpdate)
                {
                    StemWordMatrix[t.Item1][t.Item2] = StemWordMatrix[t.Item1][t.Item2] + 1;
                }
            }
            //When sentence is smaller than the  horizon
            else
            {
                var newHorizonSize = end - centerRightHorizon;
                for (var i = centerRightHorizon; i < end; i++)
                    horizonWordList.Add(StemWordsManager.ReturnStemWord(stemWords[i]));

                if (horizonWordList.Count > 1)
                {
                    Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
                                                         horizonWordList.Count - 1;
                    Helper.LowesTotalNumberOfWindows = Helper.LowesTotalNumberOfWindows + newHorizonSize - 1;

                    //print out the value of the horizonWordList which is the horizon sentence 
                    if (printLowesDoubleWindowSentencesToFile)
                        LowesHelpers.PrintWordListToFileAsOneLine(horizonWordList, file);

                    //add to _frameDictionary
                    var frameSize = horizonWordList.Count;
                    if (FrameCounterDictionary.ContainsKey(frameSize))
                        FrameCounterDictionary[frameSize] = FrameCounterDictionary[frameSize] + 1;
                    else
                        FrameCounterDictionary.Add(frameSize, 1);

                    var horizon = new LowesSingleWindowAlgorithm(newHorizonSize, horizonWordList, true);
                    List<Tuple<int, int>> matrixUpdate = horizon.GetAllStemWordsIdsTuplesFromCenterStemWordGoingInOneDirection();

                    foreach (Tuple<int, int> t in matrixUpdate)
                    {
                        StemWordMatrix[t.Item1][t.Item2] = StemWordMatrix[t.Item1][t.Item2] + 1;
                    }
                }
            }
            centerRightHorizon++;
            return centerRightHorizon;
        }

        private static int ProcessLeftHorizon(IReadOnlyList<string> allStemWordsInSentence, int horizonSize, int center, int end,
            bool printLowesDoubleWindowSentencesToFile, StreamWriter file,
            bool controlSoThatFirstIterationSingleWindowLowesToCountReadingFramesOnly)
        {
            var horizonWordList = new List<StemWord>();
            var centerLeftHorizon = center;
            var controlSoThatFirstIterationSingleWindowLowesToCountReadingFramesOnlyLeft = controlSoThatFirstIterationSingleWindowLowesToCountReadingFramesOnly;

            if (horizonSize + centerLeftHorizon <= end)
            {
                for (var i = centerLeftHorizon; i < centerLeftHorizon + horizonSize; i++)
                {
                    if (allStemWordsInSentence[i] != "")
                        horizonWordList.Add(StemWordsManager.ReturnStemWord(allStemWordsInSentence[i]));
                }

                if (printLowesDoubleWindowSentencesToFile)
                    LowesHelpers.PrintWordListToFileAsOneLine(horizonWordList, file);

                //add to _frameDictionary
                var frameSize = horizonWordList.Count;
                if (FrameCounterDictionary.ContainsKey(frameSize))
                    FrameCounterDictionary[frameSize] = FrameCounterDictionary[frameSize] + 1;
                else
                    FrameCounterDictionary.Add(frameSize, 1);

                var horizon = new LowesSingleWindowAlgorithm(horizonSize, horizonWordList, controlSoThatFirstIterationSingleWindowLowesToCountReadingFramesOnlyLeft);

                List<Tuple<int, int>> matrixUpdate = horizon.GetAllStemWordsIdsTuplesFromCenterStemWordGoingInOneDirection();
                Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames + horizonWordList.Count - 1;
                Helper.LowesTotalNumberOfWindows = Helper.LowesTotalNumberOfWindows + horizonSize - 1;

                foreach (Tuple<int, int> t in matrixUpdate)
                {
                    StemWordMatrix[t.Item1][t.Item2] = StemWordMatrix[t.Item1][t.Item2] + 1;
                }
            }
            //When sentence is smaller than the horizon
            else
            {
                var newHorizonSize = end - centerLeftHorizon;
                for (var i = centerLeftHorizon; i < end; i++)
                    horizonWordList.Add(StemWordsManager.ReturnStemWord(allStemWordsInSentence[i]));

                if (horizonWordList.Count > 0)  //In case you DO want the final word to be a pair on its own!
                    if (horizonWordList.Count > 1)
                {
                    Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
                                                         horizonWordList.Count - 1;
                    Helper.LowesTotalNumberOfWindows = Helper.LowesTotalNumberOfWindows + newHorizonSize - 1;
                    //print out the value of the horizonWordList which is the horizon sentence 
                    if (printLowesDoubleWindowSentencesToFile)
                        LowesHelpers.PrintWordListToFileAsOneLine(horizonWordList, file);

                    //add to _frameDictionary
                    var frameSize = horizonWordList.Count;
                    if (FrameCounterDictionary.ContainsKey(frameSize))
                        FrameCounterDictionary[frameSize] = FrameCounterDictionary[frameSize] + 1;
                    else
                        FrameCounterDictionary.Add(frameSize, 1);

                    var horizon = new LowesSingleWindowAlgorithm(newHorizonSize, horizonWordList, false);
                    List<Tuple<int, int>> matrixUpdate = horizon.GetAllStemWordsIdsTuplesFromCenterStemWordGoingInOneDirection();

                    foreach (Tuple<int, int> t in matrixUpdate)
                    {
                        StemWordMatrix[t.Item1][t.Item2] = StemWordMatrix[t.Item1][t.Item2] + 1;
                    }
                }
            }
            centerLeftHorizon++;
            return centerLeftHorizon;
        }

        private static StreamWriter GetOutputStreamWriter(bool printLowesDoubleWindowSentencesToFile,
            string poetAllPoemsHorizonAlgorithmSentencesFileName, string folderPath)
        {
            if (!printLowesDoubleWindowSentencesToFile) return null;
            try
            {
                return new StreamWriter(folderPath + poetAllPoemsHorizonAlgorithmSentencesFileName);
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit(
                    "Error in in RecursiveTextProcessingAlgorithmLowesDoubleWindow." + "Could not write file: " +
                    folderPath + poetAllPoemsHorizonAlgorithmSentencesFileName + ".", e);
            }
            return null;
        }

    }
}
