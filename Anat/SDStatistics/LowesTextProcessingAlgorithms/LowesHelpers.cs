﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SDParser.Notifications;
using SDParser.Structures;
using SDStatistics.Helpers;

namespace SDStatistics.LowesTextProcessingAlgorithms
{

    public static class LowesHelpers
    {
        public static int[][] StemWordMatrix { get; set; }
        public static List<string> StemWordsString { get; set; }

        //public static List<StemWord> StemWords { get; set; }
        //public static StemWord ReturnStemWord(string name)
        //{
        //    return StemWords.FirstOrDefault(w => w.Root == name);
        //}

        public static void PrintWordListToFileAsOneLine(List<StemWord> horizonWordList, StreamWriter file)
        {
            try
            {
                foreach (var stemWord in horizonWordList)
                {
                    file.Write(stemWord.Root + " ");
                }
                file.WriteLine();
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit("Error in PrintWordListToFile in LowesHelper.", e);
            }
        }

        /// <summary>
        /// Create a list of StemWord where the index of each word in the list
        /// corresponds to the orderID of that word, and each StemWord contains 
        /// the frequency of occurence value which is the key in the dictionary and
        /// total sum of the frequency of occurence of each of its variation words.
        /// </summary>
        //public static int[][] CreateStemWordsMatrix(Dictionary<StemWord, int> stemWordDictionaryOrderedByWeight)
        public static int[][] CreateStemWordsMatrix(Dictionary<string, StemWord> stemWordDictionaryOrderedByWeight)
        {
            StemWordsString = new List<string>();

            //The ordering id of the StemWord is their index in the sortedVocabulary
            //This broke because the number of words is too small and I was doing reduction of node words with value 1
            //I removed the need to reduce words based on the 'connection/weight/occurrence' of the word 
            //in the settings simulation file and the code worked. Should/will revise later on.
            var idCounter = 0;
            foreach (var entry in stemWordDictionaryOrderedByWeight)
            {
                //var tempStemWord = entry.Key;
                //tempStemWord.Id = idCounter;
                //idCounter++;
                //StemWords.Add(tempStemWord);
                //StemWordsString.Add(tempStemWord.Root);
                var tempStemWord = entry.Value;
                tempStemWord.Id = idCounter;
                idCounter++;
                //StemWords.Add(tempStemWord);
                StemWordsString.Add(tempStemWord.Root);
            }

            ////StemWordMatrix = MatrixOperations.CreateAndInitializeIntegerMatrix(stemWordDictionaryOrderedByWeight.Count + 1,
            ////   stemWordDictionaryOrderedByWeight.Count + 1);

            //StemWordMatrix = MatrixOperations.CreateAndInitializeIntegerMatrix(stemWordDictionaryOrderedByWeight.Count + 1,
            //    stemWordDictionaryOrderedByWeight.Count + 1);

            //TODO: FIX!! THIS IS WHERE THE PROBLEM IS
            StemWordMatrix = MatrixOperations.CreateAndInitializeIntegerMatrix(stemWordDictionaryOrderedByWeight.Count + 1,
                stemWordDictionaryOrderedByWeight.Count + 1);


            return StemWordMatrix;
        }


    }
}
