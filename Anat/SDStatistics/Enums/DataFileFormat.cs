﻿namespace SDStatistics.Enums
{
    //The data file source extension type (html or text)
    public enum DataFileFormat
    {
        TextFormat,
        HtmlFormat,
        AllSupportedText
    }

}
