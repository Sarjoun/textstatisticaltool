﻿using System;
using System.Text;

namespace SDStatistics.Helpers
{
   public static class TextHelper
   {
      public static string GetSourceNameFromFile(string fileName)
      {
         var start = fileName.LastIndexOf(@"]", StringComparison.Ordinal) + 2;
         var end = (fileName.LastIndexOf(@"\Statistics", System.StringComparison.Ordinal));
         return fileName.Substring(start, end - start);
      }

       public static string ConvertStringArrayToString(string[] array)
       {
           //
           // Concatenate all the elements into a StringBuilder.
           //
           var builder = new StringBuilder();
           foreach (var value in array)
           {
               builder.Append(value);
               builder.Append(' ');
           }
           return builder.ToString();
       }
    }
}
