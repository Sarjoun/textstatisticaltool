﻿using System;

namespace SDStatistics.Helpers
{
   public static class ExtractTextFromStrings
   {
      public static string GetSourceName(string folderPath)
      {
         return folderPath.Substring(folderPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1,
             folderPath.Length - folderPath.LastIndexOf(@"\", StringComparison.Ordinal) - 1);
      }

      public static string GetFolderPath(string filePath)
      {
         return filePath.Substring(0, filePath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);
      }

      public static string GetStatisticsFolder(string sourceFolderPath, string dataSourceName)
      {
         return sourceFolderPath + @"\statistics_" + dataSourceName;
      }
    }
}
