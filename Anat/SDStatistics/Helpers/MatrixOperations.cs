﻿using System;
using System.Linq;

namespace SDStatistics.Helpers
{
    public static class MatrixOperations
    {
        public static decimal[][] CreateAndInitializeDecimalMatrix(int rows, int columns)
        {
            var returnMatrix = Enumerable.Range(0, rows).Select(i => new decimal[columns]).ToArray();
            Array.Clear(returnMatrix, 0, returnMatrix.Length);

            for (var i = 0; i < rows; i++)
            {
                returnMatrix[i] = new decimal[columns];
                Array.Clear(returnMatrix[i], 0, returnMatrix[i].Length);
            }
            return returnMatrix;
        }
        public static int[][] CreateAndInitializeIntegerMatrix(int rows, int columns)
        {
            //this statement caused an out-of-memory error which according to the debugger is caused by either
            //an array that is not well defined or ran out of internal memory
#pragma warning disable
            //int[][] zeroIntegerMatrix = Enumerable.Range(0, rows).Select(i => new int[columns]).ToArray();
            var zeroIntegerMatrix = Enumerable.Range(0, rows).Select(i => new int[columns]).ToArray();

            Array.Clear(zeroIntegerMatrix, 0, zeroIntegerMatrix.Length);

            for (var i = 0; i < rows; i++)
            {
                zeroIntegerMatrix[i] = new int[columns];
                Array.Clear(zeroIntegerMatrix[i], 0, zeroIntegerMatrix.Length);
            }

            return zeroIntegerMatrix;
        }
    }
}
