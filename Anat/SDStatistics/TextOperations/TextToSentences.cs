﻿using System.Collections.Generic;
using System.IO;
using SDStatistics.Enums;
using SDStatistics.TextCorpus.SentenceSplitters;

namespace SDStatistics.TextOperations
{
    public static class TextToSentences
    {
        public static List<string> ProcessTextSourceFileAndWriteResultToTxtFile(string poemFile, SentenceSetUp sentenceSetUp)
        {
            // Read the file as one string.
            var myFile = new StreamReader(poemFile);
            var sourceFileContent = myFile.ReadToEnd();
            myFile.Close();

            //Remove all \r\n
            sourceFileContent = sourceFileContent
                .Replace("\r\n", " ")
                .Replace("--", " ")
                .Trim();

            switch (sentenceSetUp)
            {
                case SentenceSetUp.NoSplit:
                    return SentenceSplitter.ProcessPoemTextVersionAsOneBigSentence(sourceFileContent);

                case SentenceSetUp.Split:
                    return SentenceSplitter.ProcessPoemTextVersionUsingOrbit(sourceFileContent);

                default:
                    return SentenceSplitter.ProcessPoemTextVersionUsingOrbit(sourceFileContent);
            }
        }
    }
}
