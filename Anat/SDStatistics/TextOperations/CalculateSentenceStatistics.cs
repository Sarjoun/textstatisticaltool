﻿using System.Collections.Generic;
using System.Linq;
using Iveonik.Stemmers;
using SDParser.Structures;

namespace SDStatistics.TextOperations
{
    public static class CalculateSentenceStatistics
    {
       public static Dictionary<StemWord, int> VocabularyDictionary { get; private set; }

       public static List<string> CalculateWordStatisticsAndReplaceWordWithStemRoot(bool useStemming, 
           List<string> allSentencesMinusStopWords)
        {
            VocabularyDictionary = new Dictionary<StemWord, int>();
            var allSentencesWithStemWords = new List<string>();
            IStemmer stemmer = new EnglishStemmer();
            string tempStemWordString;

            foreach (var sentenceMinusStopWords in allSentencesMinusStopWords)
            {
                var tempWordSentenceList = new List<string>();
                var inputStringArray = sentenceMinusStopWords.Split(" ".ToCharArray()).ToList();

                foreach (var wordString in inputStringArray)
                {
                    if (wordString.Length <= 1) continue;

                    tempStemWordString = useStemming
                        ? stemmer.Stem(wordString.ToLower())
                        : wordString.ToLower();

                    var stemWord = VocabularyDictionary.FirstOrDefault(x => x.Key.Root == tempStemWordString).Key;
                    if (stemWord != null)
                    {
                        VocabularyDictionary.FirstOrDefault(x => x.Key.Root == tempStemWordString).Key.AddVariantWord(wordString.ToLower());
                        VocabularyDictionary[stemWord] = VocabularyDictionary[stemWord] + 1;
                    }
                    else
                    {
                        stemWord = new StemWord(tempStemWordString, new List<VariantWord>(), 0, null);
                        stemWord.AddVariantWord(wordString);

                        VocabularyDictionary.Add(stemWord, 1);
                    }
                    tempWordSentenceList.Add(tempStemWordString);
                }
                var results = string.Join(" ", tempWordSentenceList);

                if (results.Count(c => !char.IsWhiteSpace(c)) > 0) // returns the number of non-whitespace characters
                    allSentencesWithStemWords.Add(results);
            }
            return allSentencesWithStemWords;
        }
    }
}
