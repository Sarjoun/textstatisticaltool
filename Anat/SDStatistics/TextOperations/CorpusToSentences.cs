﻿using System;
using System.Collections.Generic;
using SDParser.Notifications;
using SDStatistics.Enums;

namespace SDStatistics.TextOperations
{
    public static class CorpusToSentences
    {
        public static string[] ProcessCorpusToSentences(string[] poemsStringArray, SentenceSetUp sentenceSetup)
        {
            var allSentences = new List<string>();
            try
            {
                foreach (var dataFilePath in poemsStringArray)
                {
                    allSentences.AddRange(TextToSentences.ProcessTextSourceFileAndWriteResultToTxtFile(dataFilePath, sentenceSetup));
                }
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit($"Bug in CorpusToSentences.Process ", e);
            }

           return allSentences.ToArray();
        }
    }
}


//In order to combine the 2 systems (IONA and ALAN) into using the same text processing rule.
//I need to convert the function (ProcessTextSourceFileAndWriteResultToTxtFile) into using
//the same rules that the IONA is using and which is used in the function before this
//which is MakeWordStreamAndVariantWordsReturnDictionaryOrderedAlphabeticallyFromListOfFileNames.
