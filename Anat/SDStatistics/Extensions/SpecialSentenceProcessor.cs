﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text.RegularExpressions;
//using SDParser.Notifications;

//namespace SDStatistics.Extensions
//{
//    public static class SpecialSentenceProcessor
//    {
//        private static readonly Regex Regex = new Regex(@"[ ]{2,}", RegexOptions.None);
//        public static List<string> StopWords = new List<string>();//needs to be initialized

//        public static List<string> RemoveStopWordsAndNumeralsFromSentences(this List<string> allSentences)
//        {
//            if (StopWords == null)
//                ShowMessage.ShowErrorMessageAndExit("The stopwords were not populated in SpecialSentenceProcessor. " +
//                                                    "The program will now exit", new NullReferenceException());

//            var allSentencesMinusStopWords = new List<string>();
//            foreach (var sentenceRawString in allSentences)
//            {
//                var tempWordSentenceList = new List<string>();
//                var sentenceString = Regex.Replace(sentenceRawString, @" ");

//                //replace multiple spaces with a single space
//                var inputStringArray = sentenceString.Split(" ".ToCharArray()).ToList();
//                inputStringArray = inputStringArray.ConvertAll(d => d.ToLower());

//                //Can't use this because it treats both as list, meaning that duplicates are removed: [W1 W2 W1 W3] & [W3] => [W1 W2]
//                //_tempWordSentenceList = inputStringArray.Except(_stopWords).ToList();

//                //instead compare word by word
//                foreach (var word in inputStringArray)
//                {
//                    //var word2 = word.Replace("’", " ");
//                    //word2 = word2.Replace("”", " ");
//                    //word2 = word2.Replace("“", " ");
//                    //word2 = word2.Replace("'", " ");
//                    //word2 = word2.Replace("\"", " ");
//                    //word2 = word2.Replace("�", " ");
//                    var word2 = RemoveSpecialChars(word).Trim();

//                    if (!StopWords.Contains(word2) && !word2.HasNumber())
//                    {
//                        tempWordSentenceList.Add(word2);
//                    }
//                }

//                string resultingSentence = string.Join(" ", tempWordSentenceList);

//                //Added a check to make sure that the resultingSentence has at least 1 word in it.
//                if (string.IsNullOrEmpty(resultingSentence) || string.IsNullOrWhiteSpace(resultingSentence))
//                { }
//                else
//                    allSentencesMinusStopWords.Add(resultingSentence);
//            }
//            return allSentencesMinusStopWords;
//        }

//        public static string RemoveSpecialChars(this string input)
//        {
//            return Regex.Replace(input, @"[^0-9a-zA-Z\._]", string.Empty);
//        }


//    }
//}
