﻿using System.IO;

namespace SDStatistics.Extensions
{
    public static class ListExtensions
    {
        public static string[] WriteToFileAsync(this string[] sentencesToWrite, string fileName)
        {
            StreamWriter file;
            using (file = new StreamWriter(fileName))
            {
                foreach (var sentenceWithStemWords in sentencesToWrite)
                {
                    file.WriteLine(sentenceWithStemWords);
                }
                file.Close();
            }
            return sentencesToWrite;
        }
    }
}
