﻿using System.Collections.Generic;
using System.Linq;
using SDParser.Extensions;
using SDParser.Structures;
using SDParser.TextProcessingCentral;

namespace SDStatistics.Extensions
{
    public static class ArrayExtensions
    {
        public static string[] ScanEntriesAndBreakUpOnPunctuation(
           this string[] sentences, int occurrences)
        {
            return sentences;
        }

        private static Dictionary<string, StemWord> _dictionary;
        //public static string[] ReplaceAllWordsWithStemRemoveStopWordsRemoveEmptySentences(
        //   this string[] sentences, Dictionary<string, StemWord> dictionary)
        //{
        //   _dictionary = dictionary;
        //   return sentences.Select(sentence => sentence.Split(' '))
        //      .Select(words => words.Select(GetStemVersionOfWord)
        //         .Where(result => result != null)
        //         .Aggregate("", (current, result) => current + (result + " "))).ToArray();
        //}
        public static string[] ReplaceAllWordsWithStemRemoveStopWordsRemoveEmptySentences(
            this string[] sentences, Dictionary<string, StemWord> dictionary)
        {
            //_dictionary = dictionary;
            //return sentences
            //    .Select(sentence => sentence.Split(' '))
            //    .Select(words => words.Select(word => GetStemVersionOfWordFromDictionary(GetStemVersionOfWordFromStemmer(word)))
            //    .Where(result => result != null)
            //    .Aggregate("", (current, result) => current + (result + " ")))
            //    .Where(tempSentence => tempSentence.Trim().Length > 0)
            //    .ToArray();
            ////previous version
            ////return sentences.Select(sentence => sentence.Split(' '))
            ////   .Select(words => words.Select(GetStemVersionOfWordFromDictionary)
            ////      .Where(result => result != null)
            ////      .Aggregate("", (current, result) => current + (result + " ")))
            ////   .Where(tempSentence => !string.IsNullOrEmpty(tempSentence.Trim())).ToArray();


            _dictionary = dictionary;
            List<string> resultSentences = new List<string>();
            foreach (var sentence in sentences)
            {
                var words = sentence.Split(' ').Where(s => !string.IsNullOrEmpty(s)).ToArray();
                var tempSentence = "";
                foreach (var word in words)
                {
                    var result = GetStemVersionOfWordFromDictionary(word);
                    if (result != null)
                        tempSentence += result + " ";
                }
                if (tempSentence.Trim().Length > 0)
                    resultSentences.Add(tempSentence);
            }
            return resultSentences.ToArray();
        }

        private static string GetStemVersionOfWordFromDictionary(string word)
        {
            var temp = GetStemVersionOfWordFromStemmer(word);
            var result = _dictionary.FirstOrDefault(x => x.Value.Root == temp).Key;
            return result;
            //return _dictionary.FirstOrDefault(x => x.Value.Root == word).Key;
        }

        private static string GetStemVersionOfWordFromStemmer(string word)
        {
            var x = SentenceProcessor.ExtractStemVersionFromWord(word);
            return x;
            //var x = SentenceProcessor
            //    .ProcessSentence(word
            //        .Trim()
            //        .ReplaceBracketsAndPunctuationsWithSpace()
            //        .Trim()
            //        .Split(' ', '-')
            //        .StripStopWords())
            //        .FirstOrDefault();
            //return x;
        }
    }
}


/******************************************************/
//Last non-Linq version
//_dictionary = dictionary;
//List<string> resultSentences = new List<string>();
//foreach (var sentence in sentences)
//{
//var words = sentence.Split(' ');
//var tempSentence = "";
//    foreach (var word in words)
//{
//    var result = GetStemVersionOfWordFromDictionary(GetStemVersionOfWordFromStemmer(word));
//    if (result != null)
//        tempSentence += result + " ";
//}
//if (tempSentence.Trim().Length > 0)
//resultSentences.Add(tempSentence);
//}
//return resultSentences.ToArray();
/******************************************************/
//public static string[] ReplaceAllWordsWithStemRemoveStopWordsRemoveEmptySentences(
//this string[] sentences, Dictionary<string, StemWord> dictionary)
//{
//_dictionary = dictionary;
//List<string> resultSentences = new List<string>();
//   foreach (var sentence in sentences)
//{
//   var words = sentence.Split(' ');
//   var tempSentence = words
//      .Select(GetStemVersionOfWord)
//      .Where(result => result != null)
//      .Aggregate("", (current, result) => current + (result + " "));
//   if (!string.IsNullOrEmpty(tempSentence.Trim()))
//      resultSentences.Add(tempSentence);
//}
//return resultSentences.ToArray();
//}
/******************************************************/
//foreach (var sentence in sentences)
//{
//var words = sentence.Split(' ');
////var tempSentence = "";
////foreach (var word in words)
////{
////   var result = GetStemVersionOfWord(word);
////   if (result != null)
////      tempSentence += result + " ";
////}
//var tempSentence = words
//   .Select(GetStemVersionOfWord)
//   .Where(result => result != null)
//   .Aggregate("", (current, result) => current + (result + " "));
//resultSentences.Add(tempSentence);
//}
/******************************************************/
