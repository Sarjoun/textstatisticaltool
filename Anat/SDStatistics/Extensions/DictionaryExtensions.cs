﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SDParser.Structures;
using SDStatistics.TextCorpus.Helpers;
using SDStatistics.TextCorpus.OldCode;

namespace SDStatistics.Extensions
{
    public static class DictionaryExtensions
    {
        public static Dictionary<string, StemWord> RemoveEntriesIfStemWordFrequencyHasLessOrEqualOccurrence(this Dictionary<string, StemWord> dictionary, int occurrences)
        {
            foreach (var entry in dictionary.Where(x => x.Value.Frequency <= occurrences).ToList())
            {
                dictionary.Remove(entry.Key);
            }
            return dictionary;
        }

        public static Dictionary<string, StemWord> OrderByFrequency(this Dictionary<string, StemWord> dictionary)
        {
            return (from entry in dictionary
                    orderby entry.Value.Frequency descending
                    select entry)
                .ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        public static Dictionary<string, StemWord> Print(this Dictionary<string, StemWord> dictionary, string fileName)
        {
            using (var streamWriter2 = new StreamWriter(fileName))
            {
                streamWriter2.WriteLine("[{0} {1}]", "Most common word", "Frequency");
                foreach (var entry in dictionary)
                {
                    streamWriter2.WriteLine(entry.Value.GetRootAndVariantsAndAllFrequenciesStringRepresentation());
                }
                streamWriter2.Close();
            }
            return dictionary;
        }

        public static Dictionary<string, StemWord> PrintMarginalProbability(this Dictionary<string, StemWord> dictionary, string fileName)
        {
            using (var streamWriter2 = new StreamWriter(fileName))
            {
                //streamWriter2.WriteLine("[{0} {1}]", "Most common word", "Frequency");
                foreach (var entry in dictionary)
                {
                    streamWriter2.WriteLine(entry.Value.PrintRootItsFrequencyAndItsMarginalProbability());
                }
                streamWriter2.Close();
            }
            return dictionary;
        }

        public static Dictionary<string, StemWord> UpdateStemWordMarginalProbabilityUsingStemWordMatrix(
            this Dictionary<string, StemWord> dictionary,
            int[][] stemWordMatrix,
            decimal totalNumberOfPairsInFrames)
        {
            foreach (var pair in dictionary)
            {
                //Get the total sum of all the occurrences of this word with every other word 
                pair.Value.MarginalProbability = Convert.ToDecimal(
                    stemWordMatrix[pair.Value.Id].Sum() / totalNumberOfPairsInFrames);
            }
            return dictionary;
        }

        //public static Dictionary<string, StemWord> PrintMarginalProbabilityFromDictionary(this Dictionary<string, StemWord> dictionary, string folderPath, string fileName)
        //{
        //    //Update the stem words with the new marginal probability based on the RFs
        //    //The marginal probability is the total number of times that word appeared in a pair, 
        //    //over the total number of possible pairs.

        //    TextWriter tw = new StreamWriter(folderPath + fileName);
        //    try
        //    {
        //        tw.WriteLine("{0}: {1}", "Word", "Value");

        //        tw.WriteLineAsync();
        //        foreach (var pair in dictionary)
        //        {
        //            //Get the total sum of all the occurrences of this word with every other word 
        //            tw.WriteLine("{0}: {1,0:0.####}",
        //                pair.Value.Root.PadRight(5),
        //                pair.Value.MarginalProbability);
        //        }
        //        tw.Close();
        //    }
        //    finally
        //    {
        //        tw.Dispose();
        //    }
        //    return dictionary;
        //}


        //Original implementation that used to join both updating the vocabulary and printing it.
        //Keeping it for record. Do not cleanup unless necessary.
        //public static void PrintJointProbabilityMatrixFromDictionary(string folderPath, string fileName,
        //    decimal denominator, int[][] stemWordMatrix, Dictionary<string, StemWord> dictionary)
        //{
        //     //Update the stem words with the new marginal probability based on the RFs
        //     //The marginal probability is the total number of times that word appeared in a pair, 
        //     //over the total number of possible pairs.

        //    TextWriter tw = new StreamWriter(folderPath + fileName);
        //    try
        //    {
        //        tw.WriteLine("{0}: {1} / {2} = {3}.", "Word", "Sum of Word-Pairs", "Total Number of Pairs", "Value");

        //        tw.WriteLineAsync();
        //        foreach (var pair in dictionary)
        //        {
        //            //Get the total sum of all the occurrences of this word with every other word 
        //            var sum = stemWordMatrix[pair.Value.Id].Sum();
        //            var result = (decimal)sum / denominator;// totalNumberOfPairsWRTFrames;
        //            tw.WriteLine("{0}: {1}/{2} = {3,0:0.####}",
        //                pair.Value.Root.PadRight(5),
        //                sum.ToString().PadLeft(5),
        //                denominator.ToString(CultureInfo.InvariantCulture).PadRight(5),
        //                result);
        //        }
        //        tw.Close();
        //    }
        //    finally
        //    {
        //        tw.Dispose();
        //    }
        //    Helper.StemWordsCoOccurenceMatrixIntValue = _stemWordMatrix;
        //}


    }
}
