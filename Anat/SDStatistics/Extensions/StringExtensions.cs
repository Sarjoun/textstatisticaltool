﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace SDStatistics.Extensions
{
    public static class StringExtensions
    {
        public static string CreateIfFolderDoesNotExistAndReturnPath(this string folderPath)
        {
            try
            {
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Catastrophic error in creating output folder " + folderPath + ". " + e.Message);
                return folderPath;
            }
            return folderPath;
        }

        public static bool HasNumber(this string input)
        {
            return input.Any(char.IsDigit);
        }
    }
}
