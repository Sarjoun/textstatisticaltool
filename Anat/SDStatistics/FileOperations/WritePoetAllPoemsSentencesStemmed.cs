﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SDStatistics.FileOperations
{
   public static class WritePoetAllPoemsSentencesStemmed
   {
      public static string PoetAllPoemsSentencesStemWordsFileName = @"/GlobalPoemsFileStemmedWords.txt";
      public static async Task GoAsync(string folderPath, string[] allSentencesWithStemWords)
      {
         StreamWriter file;
         using (file = new StreamWriter(folderPath + PoetAllPoemsSentencesStemWordsFileName))
         {
            foreach (string sentenceWithStemWords in allSentencesWithStemWords)
            {
               await file.WriteLineAsync(sentenceWithStemWords);
            }
            file.Close();
         }
      }
   }
}
