﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SDStatistics.FileOperations
{
    public static class WritePoetAllPoemsSentencesNoStopWordsFile
    {
        public static string PoetAllPoemsSentencesNoStopWordsFileName = @"/GlobalPoemsFileNoStopWords.txt";
        public static async Task GoAsync(string folderPath, List<string> allSentencesMinusStopWords)
        {
            StreamWriter file;
            using (file = new StreamWriter(folderPath + PoetAllPoemsSentencesNoStopWordsFileName))
            {
                foreach (var sentenceMinusStopWords in allSentencesMinusStopWords)
                {
                    await file.WriteLineAsync(sentenceMinusStopWords);
                }
                file.Close();
            }
        }
    }
}
