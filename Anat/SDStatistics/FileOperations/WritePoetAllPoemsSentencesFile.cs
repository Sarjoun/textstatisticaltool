﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SDStatistics.FileOperations
{
    public static class WritePoetAllPoemsSentencesFile
    {
        public static string PoetAllPoemsSentencesFileName = @"/GlobalPoemsFile.txt";
        public static async Task<string> GoAsync(string folderPath, List<string> allSentences)
        {
            StreamWriter file;
            //Add the contents to a global file containing all poems                        
            using (file = new StreamWriter(folderPath + PoetAllPoemsSentencesFileName))
            {
                foreach (var sentence in allSentences)
                {
                    await file.WriteLineAsync(sentence);
                }
                file.Close();
            }
            return folderPath;
        }
    }
}


//public static async Task WriteListToFileAsync(
//List<string> generalContent,
//string fileName)
//{
//var streamWriter = new StreamWriter(fileName);
//    try
//{
//    foreach (var content in generalContent)
//    {
//        await streamWriter.WriteLineAsync(content);
//    }
//}
//finally
//{
//streamWriter.Close();
//streamWriter.Dispose();
            