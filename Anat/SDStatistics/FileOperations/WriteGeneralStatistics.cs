﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SDParser.Structures;
using SDStatistics.TextCorpus.Helpers;

namespace SDStatistics.FileOperations
{
    public static class WriteGeneralStatistics
    {
        public static string PoetAllPoemsGeneralStatisticsFileName = @"\GeneralStatistics.txt";

        public static async Task GoAsync(string folderPath, 
            Dictionary<string, StemWord> vocabularyDictionary) 
        {
            //Write out simple statistics about the corpus 
            using (var file = new StreamWriter(folderPath + PoetAllPoemsGeneralStatisticsFileName))
            {
                //the +1,-1 is to fix the display and so for not including the '/' in the poet's name
                await file.WriteLineAsync("General statistics for " + 
                    folderPath.Substring(folderPath.LastIndexOf('\\') + 1, 
                    folderPath.Length - folderPath.LastIndexOf('\\') - 1));
                await file.WriteLineAsync("");
                await file.WriteLineAsync("The total number of reading frames is : " 
                    + Helper.GetReadingFrames());
                await file.WriteLineAsync("The total number of (nonstop) word pairs wrt to reading frames is : " 
                    + Helper.TotalNumberOfPairsWrtFrames);
                await file.WriteLineAsync("The total number of (stemmed) words is : " 
                    + vocabularyDictionary.Sum(x => x.Value.Frequency));
                await file.WriteLineAsync("The total number of (stemmed) vocabulary words is : " + vocabularyDictionary.Count);
            }
        }
    }
}
