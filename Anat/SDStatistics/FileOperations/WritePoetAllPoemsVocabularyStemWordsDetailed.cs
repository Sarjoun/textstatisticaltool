﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using SDParser.Structures;

namespace SDStatistics.FileOperations
{
    public static class WritePoetAllPoemsVocabularyStemWordsDetailed
    {
        //The marginal probability is using as denominatory the total number of ReadingFrames
        public static string PoetAllPoemsVocabularyStemWordsDetailedOldFileName = @"/VocabularyStemmedWordsDetailed_UsingRF.txt";
        //public static async Task GoAsync(string folderPath, Dictionary<StemWord, int> stemmedWordsDictionaryByWeight)
        public static async Task GoAsync(string folderPath, Dictionary<string, StemWord> stemmedWordsDictionaryByWeight)
        {
            StreamWriter file;
            //Write the vocabulary with details
            using (file = new StreamWriter(folderPath + PoetAllPoemsVocabularyStemWordsDetailedOldFileName))
            {
                foreach (var entry in stemmedWordsDictionaryByWeight)
                {
                    await file.WriteLineAsync(entry.Value.GetRootAndVariantsAndAllFrequenciesStringRepresentation());
                }
                file.Close(); //in MSN example it is missing.                        
            }
        }
    }
}
