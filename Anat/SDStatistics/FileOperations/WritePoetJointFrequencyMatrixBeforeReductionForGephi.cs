﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SDStatistics.FileOperations
{
    public static class WritePoetJointFrequencyMatrixBeforeReductionForGephi
    {
        public static string PoetJointFrequencyMatrixBeforeReductionGephi = @"\JointFrequencyBeforeReductionGephi.csv";

        public static async Task GoAsync(int totalWords, int[][] matrixValue, string folder, List<string> stemmedWords)
        {
            TextWriter tw = new StreamWriter(folder + PoetJointFrequencyMatrixBeforeReductionGephi);
            try
            {
                var temp = stemmedWords.Aggregate(";", (current, pair) => current + (pair + ";"));
                await tw.WriteLineAsync(temp);

                for (var i = 1; i <= totalWords; i++)
                {
                    await tw.WriteAsync(stemmedWords[i - 1] + ";");
                    for (var j = 1; j <= totalWords; j++)
                        await tw.WriteAsync(matrixValue[i][j] + ";");
                    await tw.WriteLineAsync();
                }
                tw.Close();
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }
        }
    }
}
