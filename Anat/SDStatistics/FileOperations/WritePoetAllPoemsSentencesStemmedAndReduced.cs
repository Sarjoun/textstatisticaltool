﻿using System.IO;
using System.Threading.Tasks;

namespace SDStatistics.FileOperations
{
   public static class WritePoetAllPoemsSentencesStemmedAndReduced
   {
      public static string PoetAllPoemsSentencesNoStopWordsFileName = @"/GlobalPoemsFileStemmedAndReduced.txt";
      public static async Task GoAsync(string folderPath, string[] allSentencesMinusStopWords)
      {
         StreamWriter file;
         using (file = new StreamWriter(folderPath + PoetAllPoemsSentencesNoStopWordsFileName))
         {
            foreach (var sentenceMinusStopWords in allSentencesMinusStopWords)
            {
               await file.WriteLineAsync(sentenceMinusStopWords);
            }
            file.Close();
         }
      }
   }
}
