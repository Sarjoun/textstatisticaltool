﻿using System;
using System.Collections.Generic;
using System.Linq;
using SDParser.Structures;

namespace SDStatistics.MathLibrary
{
    public static class ProbabilityCalculations
    {
        //public static Dictionary<StemWord, int> OrderedVocabularyDictionary;
        public static Dictionary<string, StemWord> OrderedVocabularyDictionary;

        public static int[][] StemWordsCoOccurenceMatrixIntValue;
        public static decimal TotalNumberOfPairsWrtFrames;//to represent the value WxN (4.2.2016)

        //After 4/2/2016 - Meeting, _totalNumberOfPairsWRTFrames is now representative of WxN
        public static decimal LowesTotalNumberOfWindows; //to represent the value WxN,(not anymore!)... FIX THIS 

        //where W = size of horizon and N total number of words.
        //This is calculated because at the end, the number of words in W shrinks. 
        //The addition is done programmatically in the code.

        static readonly decimal ValueDecimalPostv1 = new decimal(1);
        static readonly decimal ValueDecimalNegtv1 = new decimal(-1);
        static readonly decimal ValueDecimalZero = new decimal(0);


        public static decimal GetWordFrequency(int id)
        {
            //return OrderedVocabularyDictionary.ElementAt(id-1).Key.MarginalProbabilityUsingRFCount;
            //return OrderedVocabularyDictionary.ElementAt(id - 1).Value.Frequency;
            return OrderedVocabularyDictionary.ElementAt(id).Value.Frequency;
        }

        public static decimal GetWordProbability(int id)
        {
            //return OrderedVocabularyDictionary.ElementAt(id-1).Key.MarginalProbabilityUsingRFCount;
            //return OrderedVocabularyDictionary.ElementAt(id - 1).Value.MarginalProbability;
            return OrderedVocabularyDictionary.ElementAt(id).Value.MarginalProbability;
        }
        public static decimal GetConditionalProbability(int id1, int id2)
        {
            // HERE UPDATE THIS VARIABLE!!!!!
            //decimal coOccurenceProbability = (StemWordsCoOccurenceMatrixIntValue[id1][id2] / (decimal)_readingFrames);//old implementation
                decimal coOccurenceProbability = (StemWordsCoOccurenceMatrixIntValue[id1][id2] / TotalNumberOfPairsWrtFrames);

                //decimal probability = GetWordFrequency(id2);
                decimal probability = GetWordProbability(id2);

                //If the value is so small that it is zero (0), then conditionalProbability is 0
                    if (decimal.Compare(coOccurenceProbability, ValueDecimalZero) == 0 &&
                        decimal.Compare(probability, ValueDecimalZero) == 0)
                        return 0;

                return coOccurenceProbability / probability;

            //DEBUGGING
            //if ((Decimal.Compare(conditionalProbability, ValueDecimalPostv1) > 0) || (conditionalProbability < 0))
            //{
            //    var x = 1;
            //}
            //return conditionalProbability;
        }

        public static decimal GetCorrelationCoefficientOld(int id1, int id2)
        {
            decimal coOccurenceProbability =
                decimal.Round((StemWordsCoOccurenceMatrixIntValue[id1][id2] / (decimal)TotalNumberOfPairsWrtFrames), 9);//7);
            //decimal probability = GetWordFrequency(id2);
            decimal probability1 = decimal.Round(GetWordProbability(id1), 9);//7);
            decimal probability2 = decimal.Round(GetWordProbability(id2), 9);//7);
            //decimal correlationCoefficient = (coOccurenceProbability - (probability1 * probability2)) / 
            //                                 (decimal) Math.Sqrt((double) ((probability1*(1-probability1))*(probability2*(1-probability2))));

            decimal correlationCoefficientNumerator = (coOccurenceProbability - (probability1 * probability2));
            decimal nDecimal = decimal.Round(correlationCoefficientNumerator, 9);//7); // result = 1.234

            decimal correlationCoefficientDenominator = SimpleCalculations.Sqrt((probability1 * (1 - probability1)) * (probability2 * (1 - probability2)));
            decimal dDecimal = decimal.Round(correlationCoefficientDenominator, 9);//7); // result = 1.

            decimal correlationCoefficient = nDecimal / dDecimal;

            if ((Decimal.Compare(correlationCoefficient, ValueDecimalPostv1) > 0) || (Decimal.Compare(correlationCoefficient, ValueDecimalNegtv1) > 0))
            {
                var x = 1;
            }

            return correlationCoefficient;
        }

        public static decimal GetCorrelationCoefficient(int id1, int id2)
        {
            //All variables are decimals
            var coOccurenceProbability = (StemWordsCoOccurenceMatrixIntValue[id1][id2] / (decimal)TotalNumberOfPairsWrtFrames);
            //decimal probability = GetWordFrequency(id2);
            var probability1 = GetWordProbability(id1);
            var probability2 = GetWordProbability(id2);
            //decimal correlationCoefficient = (coOccurenceProbability - (probability1 * probability2)) / 
            //                                 (decimal) Math.Sqrt((double) ((probability1*(1-probability1))*(probability2*(1-probability2))));


            var correlationCoefficientNumerator = (coOccurenceProbability - (probability1 * probability2));
            var numerator = Truncate(correlationCoefficientNumerator, 9);//7); // result = 1.234

            var tempCorrelationCoefficientDenominator = (probability1 * (1 - probability1)) * (probability2 * (1 - probability2));
            Decimal correlationCoefficientDenominator = 0;
            correlationCoefficientDenominator = Decimal.Compare(tempCorrelationCoefficientDenominator, ValueDecimalZero) == 0 ? 0 : SimpleCalculations.Sqrt(tempCorrelationCoefficientDenominator);

            //double correlationCoefficientDenominator2 = Math.Sqrt((double) ((probability1 * (1 - probability1)) * (probability2 * (1 - probability2))));

            Decimal denominator = Truncate(correlationCoefficientDenominator, 9);//7); // result = 1.

            Decimal correlationCoefficient = ValueDecimalZero;

            //var correlationCoefficient = numerator / denominator;

            if ((Decimal.Compare(numerator, ValueDecimalZero) == 0) && (Decimal.Compare(denominator, ValueDecimalZero) == 0))
                return correlationCoefficient = 0;
            else
                correlationCoefficient = numerator / denominator;


            if (Decimal.Compare(correlationCoefficient, ValueDecimalPostv1) > 0)
            {
                var x = 1;
            }

            return correlationCoefficient;
        }

        public static decimal Truncate(decimal number, int digits)
        {
            decimal stepper = (decimal)(Math.Pow(10.0, (double)digits));
            int temp = (int)(stepper * number);
            return (decimal)temp / stepper;
        }

        //public static decimal GetPointWiseMutualInformation(int id1, int id2)
        //{
        //    decimal coOccurenceProbability = (StemWordsCoOccurenceMatrixIntValue[id1][id2] / (decimal)_frameCounter);            
        //    decimal probability1 = GetWordProbability(id1);
        //    decimal probability2 = GetWordProbability(id2);

        //    //if (coOccurenceProbability == 0)
        //    //    return 0;
        //    var pointWiseMutualInformationTemp = (double)(coOccurenceProbability / (probability1 * probability2));
        //    decimal pointWiseMutualInformation = Math.Log(pointWiseMutualInformationTemp);
        //    return pointWiseMutualInformation;
        //}

        public static double GetLogOddsRatio(int id1, int id2, int horizon)
        {
            //horizon = W, id1 = b, and id2 = t (w.r.t. Lowe's paper)
            //To calculate the LogOddsRatio, (please refer to resources: Lowe's paper "towards-a-theory" pg.3)

            decimal coOccurenceProbability = (StemWordsCoOccurenceMatrixIntValue[id1][id2] / (decimal)TotalNumberOfPairsWrtFrames);

            //These 4 variables have to be calculated for each 2 words.

            //(Paper)Expected co-occurrence frequency summed over each possible position in the window
            //(My) Joint Probability

            //fW(b;t)
            decimal fW_B_T = coOccurenceProbability * TotalNumberOfPairsWrtFrames;

            //(Paper) fW(b;'t) = W f(b) - fW(b; t)
            //fW(b;'t)
            decimal fW_B_notT = (horizon * GetWordFrequency(id2)) - fW_B_T;

            //(Paper) W('b; t) = W f(t) - fW(b; t)
            //fW('b;t)
            decimal fW_notB_T = (horizon * GetWordFrequency(id1)) - fW_B_T; ;

            //(Paper)  WN - (fW(b;'t)+ fW('b;t) + fW(b; t))
            //fW('b;'t)
            decimal fW_notB_notT = LowesTotalNumberOfWindows - (fW_B_notT + fW_notB_T + fW_B_T);


            var odds_ratio = (fW_B_T * fW_notB_notT) / (fW_B_notT * fW_notB_T);


            return Convert.ToDouble(odds_ratio);
            //decimal probability1 = GetWordProbability(id1);
            //decimal probability2 = GetWordProbability(id2);

            //double pointWiseMutualInformationTemp = 0;

            //if (Decimal.Compare(coOccurenceProbability, ValueDecimalZero) != 0)
            //    pointWiseMutualInformationTemp = (double)(coOccurenceProbability / (probability1 * probability2));
            //var pointWiseMutualInformation = Math.Log(pointWiseMutualInformationTemp);


            ////return -99 instead of -Infinity per Minai's request
            //if (Double.IsNegativeInfinity(pointWiseMutualInformation))
            //    return -99;
            //return pointWiseMutualInformation;
        }

        public static double GetPointWiseMutualInformation(int id1, int id2)
        {
            decimal coOccurenceProbability = (StemWordsCoOccurenceMatrixIntValue[id1][id2] / (decimal)TotalNumberOfPairsWrtFrames);
            decimal probability1 = GetWordProbability(id1);
            decimal probability2 = GetWordProbability(id2);

            //if (coOccurenceProbability == 0)
            //    return 0;

            double pointWiseMutualInformationTemp = 0;

            if (Decimal.Compare(coOccurenceProbability, ValueDecimalZero) != 0)
                pointWiseMutualInformationTemp = (double)(coOccurenceProbability / (probability1 * probability2));
            var pointWiseMutualInformation = Math.Log(pointWiseMutualInformationTemp);

            //return -99 instead of -Infinity per Minai's request
            if (Double.IsNegativeInfinity(pointWiseMutualInformation))
                return -99;
            return pointWiseMutualInformation;
        }
    }
}
