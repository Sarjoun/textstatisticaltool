﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using LeagueOfMonads;
//using SDNodeXLEngineLibrary;
using SDParser.ComplexTypes;
using SDParser.CustomFileParsers;
using SDParser.Primitives;
using SDStatistics.Events;
using SDStatistics.Helpers;
using SDStatistics.TextCorpus.OldCode;
using LeagueOfMonads.NoLambda;
using SDParser.ClusteringTypes;
using SDParser.FileFunctions;
using SDParser.Notifications;
using SDParser.Structures;
using SDStatistics.Enums;
using SDStatistics.Network;
using SDStatistics.TextCorpus.DataSources;
using static System.Windows.Application;

namespace SDStatistics
{
    public static class Orchestrator
    {
        public delegate void ProcessingSourceUpdateHandler(object sender, ProcessingSourceEventArgs e);
        public static event ProcessingSourceUpdateHandler OnProcessingNewSource;
        public static Dictionary<string, StemWord> StemWordDictionaryOrderedByWeight { get; set; }
        public static decimal[,] JointFrequencyClusteredAgentsMatrixValues { get; set; }
        public static decimal[,] JointProbabilityClusteredAgentsMatrixValues;
        public static decimal[,] ConditionalProbabilityClusteredAgentsMatrixValues { get; set; }
        public static List<string> Top10StemWordsFromMatrix { get; set; }
        public static List<StemWord> SyncedIdsStemWords { get; set; }
        public static string StaticSources { get; set; }


        public static async Task<List<Agent>> SetUpDataAndRunStatisticsForAnatAsync(
            TextStatisticsSettings textStatsSettings,
            DocumentsLinksFileName originalSourceDocumentsFile,
            UnTaggedDocumentsSubCorporaFileName remainingUntaggedDocumentsFile,
            string documentsFilePath,
            List<Agent> clusteredAgents)
        {
            return Identity
            .Create(await SetUpDocumentsFileFromTaggedFilesAndOriginalFilesAsync(
                            originalSourceDocumentsFile, remainingUntaggedDocumentsFile, documentsFilePath))
            .Map(RunTextStatisticsWithAnat,
                            textStatsSettings,
                            clusteredAgents,
                            documentsFilePath).Value;
        }

        private static async Task<IEnumerable<string>> SetUpDocumentsFileFromTaggedFilesAndOriginalFilesAsync(
            DocumentsLinksFileName originalSourceDocumentsFile,
            UnTaggedDocumentsSubCorporaFileName remainingUntaggedDocumentsFile,
            string documentsFilePath)
        {
            try
            {
                return await CreateTaggedDocumentsFileFromOriginalAndUntagged
                    .GetResultingTaggedDocumentsFileAsync(originalSourceDocumentsFile, remainingUntaggedDocumentsFile)
                    .Tea(x => SDParser.FileFunctions.FileWriter.WriteListToFileAsync(x.ToList(), documentsFilePath));
            }
            catch (Exception e)
            {
                throw new Exception("Could not create the base file for the TextStatistics part. " + e.Message);
            }
        }

        private static string[] GetDataFilesForProcessingFromIONA(string documentsFilePath)
        {
            return FileReader.ReadFileAsync(documentsFilePath).Result.ToArray();
        }
        private static string[] GetDataFilesForProcessingFromFolder(string documentsFilePath)
        {
            return Directory.GetFiles(documentsFilePath);
        }
        private static IEnumerable<StemWord> GetClusteredStemWordsForDataSourceTextFormat()
        {
            return null;
        }

        private static List<Agent> RunTextStatisticsWithAnat(
            IEnumerable<string> poemsStringArray,
            TextStatisticsSettings textStatsSettings,
            List<Agent> agentsFromIONA,
            string documentsFilePath)
        {
            //var newAgents = agentsFromIONA;
            var clusteredStemWords = ClusteringAgentModifier.ExtractStemWordsFromClusteringsOrderedById(agentsFromIONA);

            const bool calculateNetworkGraphFiles = false;
            var sourceFolderPath = textStatsSettings.StatisticsSources.Value;
            var calculateRemainingProbabilities =
                textStatsSettings.CalculateConditionalProbability.Value 
                || textStatsSettings.CalculatePointWiseMutualInformation.Value;

            var dataSource = new DataSourceTextFormat(
                GetDataFilesForProcessingFromIONA(documentsFilePath),
                clusteredStemWords,//GetClusteredStemWordsForDataSourceTextFormat(),
                sourceFolderPath,
                ExtractTheSourceNameFromTheFolderPath(sourceFolderPath),
                textStatsSettings.HorizonSize.Value,
                textStatsSettings.IncludeTitleInSetup,
                textStatsSettings.TextPartioningAlgorithm.Value,
                textStatsSettings.SplitTheSentences.Value ? SentenceSetUp.Split : SentenceSetUp.NoSplit,
                textStatsSettings.UseStemmingInTextStatistics.Value,
                calculateRemainingProbabilities,
                DataFileFormat.TextFormat,
                textStatsSettings.ReductionInWordFrequency.Value,
                textStatsSettings.ReductionInWordFrequency.Value ? textStatsSettings.RemoveLowWordFrequencyValue.Value : 0,
                textStatsSettings.ReductionInNetworkEdgeValue.Value,
                textStatsSettings.ReductionInNetworkEdgeValue.Value
                                    ? textStatsSettings.ReductionOfNetworkEdgesWeight.Value
                                    : 0,
                WordStreamFileName.FromValue(documentsFilePath));

            clusteredStemWords = dataSource.UpdatedClusteredStemWords;

            //update the agents by removing any word that might not have made it from IONA to ALAN due to having low statistical properties
            var newAgents = UpdateAgentsWordsFromTheStatisticsResultsByRemovingStemWordsThatNoLongerExist(agentsFromIONA, clusteredStemWords);

            #region Needed for ANAT's final weighted and decorated story graph
            JointFrequencyClusteredAgentsMatrixValues = dataSource.JointFrequencyClusteredAgentsMatrixValues;
            JointProbabilityClusteredAgentsMatrixValues = dataSource.JointProbabilityClusteredAgentsMatrixValues;
            ConditionalProbabilityClusteredAgentsMatrixValues = dataSource.ConditionalProbabilityClusteredAgentsMatrixValues;
            Top10StemWordsFromMatrix = dataSource.Top10StemWordsFromMatrix;
            SyncedIdsStemWords = dataSource.SyncedIdsStemWords;
            #endregion

            #region Needed for ANAT's final data analysis
            //relative word count for every story in the media graph
            //total # of words in the entire media graph
            //relative weight for every story = (weight of story words)/(total weight of all words in the media graph)
            //StemWordDictionaryOrderedByWeight = dataSource.StemWordDictionaryOrderedByWeight;
            StemWordDictionaryOrderedByWeight = dataSource.FinalDictionary;
            #endregion

            var firstRunMatrix = true;

            if (!textStatsSettings.RunNetworkStatistics.Value)
                return agentsFromIONA;

            var networkFolder = CreateFolderForNetworkResults(sourceFolderPath);

            var matricesFiles = Directory
                                .GetFiles(ExtractTextFromStrings.GetStatisticsFolder(
                                    sourceFolderPath, 
                                    ExtractTheSourceNameFromTheFolderPath(sourceFolderPath)), 
                                    Constants.FileExtensionCSV)
                                 .ToList();

            //Do not calculate the JointFrequencyNetworkGraph.txt file as it is almost identical (except for a very minor change in Modularity)
            //The same can be said for the values in JointProbabilityMatrixNetworkGraph
            matricesFiles.RemoveAll(x => x.Contains("JointFrequencyGephi"));

            //#region NodeXL
            //SDNodeXLEngine sDNodeXLEngine = null;
            //try
            //{
            //    Current?.Dispatcher.Invoke(delegate
            //    {
            //        //This is needed so that a new Graph Id is created so that I can get the new ID of newly added vertices.
            //        sDNodeXLEngine = new SDNodeXLEngine();
            //    });
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //    throw;
            //}

            //matricesFiles.Remove(matricesFiles.SingleOrDefault(s => s.Contains("BeforeReduction")));
            //matricesFiles.RemoveAll(s => s.Contains("ClusteringAgents"));

            //foreach (var matrix in matricesFiles)
            //{
            //    var currentMatrixName = matrix.
            //        Substring(matrix.LastIndexOf('\\') + 1, matrix.Length - matrix.LastIndexOf('\\') - 1)
            //        .Replace(".csv", "")
            //        .Replace("Gephi", "Network");
            //    try
            //    {
            //        if (sDNodeXLEngine == null)
            //        {
            //            ShowMessage.ShowWarningMessage("sDNodeXLEngine failed to load the matrix file. No network analysis.");
            //            return agentsFromIONA;
            //            //return false;
            //        }
            //        sDNodeXLEngine.SourceFileUri = matrix;
            //        sDNodeXLEngine.NodeOutputResultFileUri = networkFolder + "\\" + currentMatrixName + "Nodes.txt";
            //        sDNodeXLEngine.GraphOutputResultFileUri = networkFolder + "\\" + currentMatrixName + "Graph.txt";
            //        sDNodeXLEngine.ImportDictionary = dataSource.ExportDictionaryToNodeXL;

            //        if (!sDNodeXLEngine.LoadMatrixFile())
            //        {
            //            ShowMessage.ShowWarningMessage("sDNodeXLEngine failed to load the matrix file. No network analysis.");
            //            return agentsFromIONA;
            //        }

            //        if (firstRunMatrix)
            //        {
            //            sDNodeXLEngine.CreateAndPopulateGraph(false);
            //            firstRunMatrix = false;
            //        }
            //        else
            //        {
            //            sDNodeXLEngine.ReassignTheEdgesValues(false);
            //        }

            //        sDNodeXLEngine.RunAllNetworkMetricsCalculations();
            //        try
            //        {
            //            sDNodeXLEngine.WriteOutAllNodesStatisticsToOutputFile();
            //        }
            //        catch (Exception sDNodeXLEngineException)
            //        {
            //            ShowMessage.ShowErrorMessageAndExit(@"An exception occurred: " + 
            //                sDNodeXLEngineException.Message, sDNodeXLEngineException);
            //        }

            //        if (calculateNetworkGraphFiles)
            //        {

            //            try
            //            {
            //                sDNodeXLEngine.WriteOutGeneralGraphStatisticsToOutputFile("WakitaTsurumi");
            //            }
            //            catch (Exception sDNodeXLEngineException)
            //            {
            //                ShowMessage.ShowErrorMessageAndExit(@"An exception occurred: " + sDNodeXLEngineException.Message, sDNodeXLEngineException);
            //                //MessageBox.Show(@"An exception occurred: " + sDNodeXLEngineException.Message);
            //                //return null;
            //                //return false;
            //            }
            //        }

            //        sDNodeXLEngine.ClearEdges();
            //    }
            //    catch (Exception sDNodeXLEngineException)
            //    {
            //        ShowMessage.ShowErrorMessageAndExit(@"An exception occurred: " + 
            //            sDNodeXLEngineException.Message, sDNodeXLEngineException);
            //    }
            //}
            //#endregion
            return newAgents;
        }

        private static List<Agent> UpdateAgentsWordsFromTheStatisticsResultsByRemovingStemWordsThatNoLongerExist(List<Agent> currentAgents,
            IEnumerable<StemWord> listOfAlanStemWords)
        {
            var stemmedWords = listOfAlanStemWords.ToArray().Select(x => x.Root).ToList();
            var newAgents = new List<Agent>();

            foreach (var agent in currentAgents)
            {
                var newAgent = agent;
                //newAgents.Add(newAgent.SyncAgentWithListOfCurrentStemmedWordsToRemoveDiscontinuedOnes(stemmedWords));
                newAgent.SyncAgentWithListOfCurrentStemmedWordsToRemoveDiscontinuedOnes(stemmedWords);
                newAgents.Add(newAgent);
            }
            return newAgents;
        }

        public static bool RunTextStatisticsUsingSourcesFile(TextStatisticsSettings textStatsSettings)
        {
            return false;
        }
    
        #region temporarily commented out v2
        //public static bool RunTextStatisticsUsingSourcesFile(TextStatisticsSettings textStatsSettings)
        //{
        //    var sourceFolderPath = textStatsSettings.StatisticsSources.Value;
        //    var calculateRemainingProbabilities = textStatsSettings.CalculateConditionalProbability.Value ||
        //                                          textStatsSettings.CalculatePointWiseMutualInformation.Value;

        //    //var dataSource = new DataSourceTextFormat();
        //    //sourceFolderPath = dataSource._folderPath;

        //    var firstRunMatrix = true;

        //    if (!textStatsSettings.RunNetworkStatistics.Value)
        //        return true;
        //    var networkFolder = CreateFolderForNetworkResults(sourceFolderPath);

        //    //Get the all the files of the matrices that need processing 
        //    //(basically all the files meant for Gephi or the statistics_corpus folder)
        //    var matricesFiles = Directory.GetFiles(
        //            ExtractTextFromStrings
        //            .GetStatisticsFolder(sourceFolderPath, sourceFolderPath),//dataSource.DataSourceName),
        //                    Constants.FileExtensionCSV).ToList();

        //    SDNodeXLEngine sDNodeXLEngine = null;
        //    try
        //    {
        //        Current?.Dispatcher.Invoke(delegate
        //        {
        //            //This is needed so that a new Graph Id is created so that I can get the new ID of newly added vertices.
        //            sDNodeXLEngine = new SDNodeXLEngine();
        //        });
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw;
        //    }

        //    //Do not include the BeforeReduction file for analysis because the Dictionary that 
        //    //holds the new reduced values has already been populated and has less words than
        //    //the matrix and will break the code.

        //    //Ugly fix that assumes there is only one file with size issue and has BeforeReduction
        //    if (matricesFiles.Any(s => s.Contains("BeforeReduction")))
        //        matricesFiles.Remove(matricesFiles.SingleOrDefault(s => s.Contains("BeforeReduction")));

        //    foreach (var matrix in matricesFiles)
        //    {
        //        var currentMatrixName = matrix.
        //            Substring(matrix.LastIndexOf('\\') + 1, matrix.Length - matrix.LastIndexOf('\\') - 1)
        //            .Replace(".csv", "")
        //            .Replace("Gephi", "Network");
        //        try
        //        {
        //            if (sDNodeXLEngine == null)
        //                return false;
        //            sDNodeXLEngine.SourceFileUri = matrix;
        //            sDNodeXLEngine.NodeOutputResultFileUri = networkFolder + "\\" + currentMatrixName + "Nodes.txt";
        //            sDNodeXLEngine.GraphOutputResultFileUri = networkFolder + "\\" + currentMatrixName + "Graph.txt";
        //            sDNodeXLEngine.ImportDictionary = null;//dataSource.ExportDictionaryToNodeXL;

        //            if (!sDNodeXLEngine.LoadMatrixFile())
        //                return false;

        //            if (firstRunMatrix)
        //            {
        //                sDNodeXLEngine.CreateAndPopulateGraph(false);
        //                firstRunMatrix = false;
        //            }
        //            else
        //            {
        //                sDNodeXLEngine.ReassignTheEdgesValues(false);
        //            }

        //            sDNodeXLEngine.RunAllNetworkMetricsCalculations();
        //            try
        //            {
        //                sDNodeXLEngine.WriteOutAllNodesStatisticsToOutputFile();
        //            }
        //            catch (Exception sDNodeXLEngineException)
        //            {
        //                MessageBox.Show(@"An exception occurred: " + sDNodeXLEngineException.Message);
        //                return false;
        //            }

        //            try
        //            {
        //                sDNodeXLEngine.WriteOutGeneralGraphStatisticsToOutputFile("WakitaTsurumi");
        //            }
        //            catch (Exception sDNodeXLEngineException)
        //            {
        //                MessageBox.Show(@"An exception occurred: " + sDNodeXLEngineException.Message);
        //                return false;
        //            }

        //            sDNodeXLEngine.ClearEdges();
        //        }
        //        catch (Exception sDNodeXLEngineException)
        //        {
        //            MessageBox.Show(@"An exception occurred: " + sDNodeXLEngineException.Message);
        //            return false;
        //        }
        //    }
        //    return true;
        //}
        #endregion

        public static string ExtractTheSourceNameFromTheFolderPath(string folderName)
        {
            return folderName.Substring(folderName.LastIndexOf(@"\", StringComparison.Ordinal) + 1,
                folderName.Length - folderName.LastIndexOf(@"\", StringComparison.Ordinal) - 1);
        }
        public static bool RunTextStatisticsUsingDataStraightFromSourcesFolder(TextStatisticsSettings textStatsSettings)
        {
            if (!Directory.Exists(textStatsSettings.StatisticsSources.Value))
                return false;

            var dataSourcesStringArrayFolders = Directory.GetDirectories(textStatsSettings.StatisticsSources.Value);
            var dataSourceCounter = 0;

            //check if this is a folder containing all sources' folders or a single source's poems
            foreach (var sourceFolderPath in dataSourcesStringArrayFolders)
            {
                dataSourceCounter++;

                SetupEventingForProcessingNewDataSource(new ProcessingSourceEventArgs(
                    ExtractTextFromStrings.GetSourceName(sourceFolderPath),
                    dataSourceCounter,
                    dataSourcesStringArrayFolders.Length,
                    DateTime.Now));

                var calculateRemainingProbabilities = textStatsSettings.CalculateConditionalProbability.Value ||
                                                      textStatsSettings.CalculatePointWiseMutualInformation.Value;

                var dataSource = new DataSourceTextFormat(
                    GetDataFilesForProcessingFromFolder(sourceFolderPath),
                    GetClusteredStemWordsForDataSourceTextFormat(),
                    sourceFolderPath,
                    ExtractTheSourceNameFromTheFolderPath(sourceFolderPath),
                    textStatsSettings.HorizonSize.Value,
                    textStatsSettings.IncludeTitleInSetup,
                    textStatsSettings.TextPartioningAlgorithm,
                    textStatsSettings.SplitTheSentences.Value ? SentenceSetUp.Split : SentenceSetUp.NoSplit,
                    textStatsSettings.UseStemmingInTextStatistics.Value,
                    calculateRemainingProbabilities,
                    DataFileFormat.TextFormat,
                    textStatsSettings.ReductionInWordFrequency.Value,
                    textStatsSettings.ReductionInWordFrequency.Value ? textStatsSettings.RemoveLowWordFrequencyValue.Value : 0,
                    textStatsSettings.ReductionInNetworkEdgeValue.Value,
                    textStatsSettings.ReductionInNetworkEdgeValue.Value ? textStatsSettings.ReductionOfNetworkEdgesWeight.Value : 0,
                    WordStreamFileName.FromValue(
                        Environment.GetFolderPath(
                            Environment.SpecialFolder.Desktop) + @"\TempWordStreamFileOutput.txt"));

                if (!textStatsSettings.RunNetworkStatistics) continue;

                var firstRunMatrix = true;

                if (!textStatsSettings.RunNetworkStatistics.Value)
                    return true;
                var networkFolder = CreateFolderForNetworkResults(sourceFolderPath);
                var matricesFiles = Directory
                                    .GetFiles(
                                        ExtractTextFromStrings.GetStatisticsFolder(
                                            sourceFolderPath, ExtractTheSourceNameFromTheFolderPath(sourceFolderPath)),
                                        //dataSource.FolderPath, dataSource.DataSourceName),
                                        Constants.FileExtensionCSV).ToList();

                //SDNodeXLEngine sDNodeXLEngine = null;
                //try
                //{
                //    Current?.Dispatcher.Invoke(delegate
                //    {
                //        //This is needed so that a new Graph Id is created so that I can get the new ID of newly added vertices.
                //        sDNodeXLEngine = new SDNodeXLEngine();
                //    });
                //}
                //catch (Exception e)
                //{
                //    Console.WriteLine(e);
                //    throw;
                //}

                if (matricesFiles.Any(s => s.Contains("BeforeReduction")))
                    matricesFiles.Remove(matricesFiles.SingleOrDefault(s => s.Contains("BeforeReduction")));

                foreach (var matrix in matricesFiles)
                {
                    var currentMatrixName = matrix.
                        Substring(matrix.LastIndexOf('\\') + 1, matrix.Length - matrix.LastIndexOf('\\') - 1)
                        .Replace(".csv", "")
                        .Replace("Gephi", "Network");
                    try
                    {
                        //if (sDNodeXLEngine == null)
                        //    return false;

                        //sDNodeXLEngine.SourceFileUri = matrix;
                        //sDNodeXLEngine.NodeOutputResultFileUri = networkFolder + "\\" + currentMatrixName + "Nodes.txt";
                        //sDNodeXLEngine.GraphOutputResultFileUri = networkFolder + "\\" + currentMatrixName + "Graph.txt";
                        //sDNodeXLEngine.ImportDictionary = dataSource.ExportDictionaryToNodeXL;

                        //if (!sDNodeXLEngine.LoadMatrixFile())
                        //    return false;

                        //if (firstRunMatrix)
                        //{
                        //    sDNodeXLEngine.CreateAndPopulateGraph(false);
                        //    firstRunMatrix = false;
                        //}
                        //else
                        //{
                        //    sDNodeXLEngine.ReassignTheEdgesValues(false);
                        //}

                        //sDNodeXLEngine.RunAllNetworkMetricsCalculations();
                        //try
                        //{
                        //    sDNodeXLEngine.WriteOutAllNodesStatisticsToOutputFile();
                        //}
                        //catch (Exception sDNodeXLEngineException)
                        //{
                        //    MessageBox.Show(@"An exception occurred: " + sDNodeXLEngineException.Message);
                        //    return false;
                        //}

                        //try
                        //{
                        //    sDNodeXLEngine.WriteOutGeneralGraphStatisticsToOutputFile("WakitaTsurumi");
                        //}
                        //catch (Exception sDNodeXLEngineException)
                        //{
                        //    MessageBox.Show(@"An exception occurred: " + sDNodeXLEngineException.Message);
                        //    return false;
                        //}
                        //sDNodeXLEngine.ClearEdges();
                    }
                    catch (Exception sDNodeXLEngineException)
                    {
                        MessageBox.Show(@"An exception occurred: " + sDNodeXLEngineException.Message);
                        return false;
                    }
                }
                //GC.Collect();//GC.WaitForPendingFinalizers();
            }
            return true;
        }

        private static void SetupEventingForProcessingNewDataSource(ProcessingSourceEventArgs e)
        {
            OnProcessingNewSource?.Invoke(null, e);
        }
        private static string CreateFolderForNetworkResults(string sourceFolderPath)
        {
            if (!Directory.Exists(sourceFolderPath + @"\network"))
                Directory.CreateDirectory(sourceFolderPath + @"\network");
            return sourceFolderPath + @"\network";
        }
    }
}
