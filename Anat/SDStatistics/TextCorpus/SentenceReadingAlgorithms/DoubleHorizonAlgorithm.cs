﻿using System;
using System.Collections.Generic;
using SDParser.Structures;
using SDStatistics.TextCorpus.Helpers;

namespace SDStatistics.TextCorpus.SentenceReadingAlgorithms
{
    public class DoubleHorizonAlgorithm : IAlgorithm
    {
        public int Size { get; set; }//number of all words
        public StemWord Root { get; set; }
        public List<StemWord> Range { get; set; } //all the words in the horizon

        /// <summary>
        /// Empty constructor for the horizon algorithm
        /// </summary>
        public DoubleHorizonAlgorithm()
        { }

        public DoubleHorizonAlgorithm(int size, List<StemWord> allWords)
        {
            Size = size;
            Root = allWords[0];
            Range = allWords;
            Helper.AddReadingFrames(1);
        }

        /// <summary>
        /// Returns a list of all the pairs in the matrix that need to be updated
        /// </summary>
        /// <returns></returns>
        public List<Tuple<int, int>> UpdateMatrix()
        {
            //Added the condition that if a root word encounters the same word twice, it is 
            //only counted once
            var wordsAlreadyConnectedWithRootWord = new List<int>();
            var resultList = new List<Tuple<int, int>>();
            var startingPoint = 0;
            while (startingPoint <= Size)
            {
                for (var i = startingPoint; i < Size - 1; i++)
                {
                    for (var j = startingPoint + 1; j < Size; j++)
                    {
                        //check if Range[j].Id has been already added
                        if (wordsAlreadyConnectedWithRootWord.Contains(Range[j].Id)) continue;
                        wordsAlreadyConnectedWithRootWord.Add(Range[j].Id);
                        resultList.Add(new Tuple<int, int>(Range[i].Id, Range[j].Id));
                    }
                    startingPoint++;
                }
            }
            return resultList;
        }

        //Added the condition that if a root word encounters the same word n times, then it is counted n times
        public List<Tuple<int, int>> UpdateMatrixCountsSameWordToWordPairTwice()
        {
            var resultList = new List<Tuple<int, int>>();
            for (var j = 1; j < Size; j++)
            {
                resultList.Add(new Tuple<int, int>(Range[0].Id, Range[j].Id));
            }
            return resultList;
        }


        //Added the condition that if a root word encounters the same word n times, then it is counted n times
        public List<Tuple<int, int>> UpdateMatrixCountNWordsNTimes()
        {
            List<Tuple<int, int>> resultList = new List<Tuple<int, int>>();
            for (var j = 1; j < Size; j++)
            {
                if (Range[0].Id != Range[j].Id)
                {
                    resultList.Add(new Tuple<int, int>(Range[0].Id, Range[j].Id));
                }
                else
                {
                    resultList.Add(new Tuple<int, int>(Range[j].Id, Range[0].Id));
                }
            }
            return resultList;
        }

        public string TestOutput()
        {
            var text = "AA BB CC DD EE FF GG HH. AA BB CC DD EE.";
            Size = 5;

            var allWords = new List<StemWord>
            {
                new StemWord("AA", 2),
                new StemWord("BB", 2),
                new StemWord("CC", 2),
                new StemWord("DD", 2),
                new StemWord("EE", 2),
                new StemWord("FF", 1),
                new StemWord("GG", 1),
                new StemWord("HH", 1)
            };
            Root = allWords[0];
            Range = allWords;
            Helper.AddReadingFrames(1);

            return text;
        }
    }
}
