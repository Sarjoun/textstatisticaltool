﻿using System;
using System.Collections.Generic;

namespace SDStatistics.TextCorpus.SentenceReadingAlgorithms
{
   interface IAlgorithm
   {
      /// <summary>
      /// Updates the corpus' word-word matrix with algorithm results of the algorithm. 
      /// The algorithm determines how to count the co-occurences of the words in a set of words. 
      /// </summary>
      /// <returns></returns>
      List<Tuple<int, int>> UpdateMatrix();


      /// <summary>
      /// An example to show the resulting output of the co-occurrences counting of a speciffic algorithm
      /// on a predefined set of words. 
      /// </summary>
      /// <returns></returns>
      string TestOutput();
   }
}
