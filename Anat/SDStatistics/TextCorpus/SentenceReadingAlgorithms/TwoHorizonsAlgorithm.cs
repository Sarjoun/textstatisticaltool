﻿using System;
using System.Collections.Generic;
using SDParser.Structures;
using SDStatistics.TextCorpus.Helpers;

namespace SDStatistics.TextCorpus.SentenceReadingAlgorithms
{
    public class TwoHorizonsAlgorithm : IAlgorithm
    {
        public int Size { get; set; }//number of all words
        public StemWord Root { get; set; }
        public List<StemWord> WordsInRange { get; set; } //all the words in the horizon

        /// <summary>
        /// Empty constructor for the horizon algorithm
        /// </summary>
        public TwoHorizonsAlgorithm()
        { }


        /// <summary>
        /// Main constructor for the TwoHorizonsAlgorithm algorithm
        /// </summary>
        /// <param name="size">Number of words in the horizon</param>
        /// <param name="allWords">List of all the words (stemmed words in this case).</param>
        public TwoHorizonsAlgorithm(int size, List<StemWord> allWords)
        {
            Size = size;
            Root = allWords[0];
            WordsInRange = allWords;
            Helper.AddReadingFrames(1);
        }


        /// <summary>
        /// 
        /// Added the condition that if a root word encounters the same word n times, then it is counted n times.
        /// </summary>
        /// <returns>An updated 2x2 matrix of the list of words with their occurrences.</returns>
        public List<Tuple<int, int>> UpdateMatrix()
        {
            List<Tuple<int, int>> resultList = new List<Tuple<int, int>>();
            for (var j = 1; j < Size; j++)
            {
                if (WordsInRange[0].Id != WordsInRange[j].Id)
                {
                    resultList.Add(new Tuple<int, int>(WordsInRange[0].Id, WordsInRange[j].Id));
                    resultList.Add(new Tuple<int, int>(WordsInRange[j].Id, WordsInRange[0].Id));
                }
                else
                {
                    resultList.Add(new Tuple<int, int>(WordsInRange[j].Id, WordsInRange[0].Id));
                }
            }
            return resultList;
        }


        /// <summary>
        /// An test example of what this algorithm does.
        /// </summary>
        /// <returns></returns>
        public string TestOutput()
        {
            var text = "Input: AA BB CC DD EE FF GG HH. AA BB CC DD EE.\r\n";

            text += "Output: Not implemented.\r\n";

            return text;
        }
    }
}
