﻿using System;
using System.Collections.Generic;
using SDParser.Structures;
using SDStatistics.TextCorpus.Helpers;

namespace SDStatistics.TextCorpus.SentenceReadingAlgorithms
{
    //gives double the probability value
    public class LowesSingleWindowAlgorithm : IAlgorithm
    {
        public int Size { get; set; }//number of all words
        public StemWord Root { get; set; }
        public List<StemWord> AllWordsContainedWithinTheHorizon { get; set; } //all the words in the horizon

        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        /// <param name="allWords"></param>
        /// <param name="addReadingFrames">This parameter is used to see if the ReadingFrames need to be updated or not.
        /// Each reading frame is embodied by 1 instance of the Algorithm. In case a double-window (2-horizon) algorithm
        /// is being simulated by using 2 LowesSingleWindowAlgorithm (one for each side), then one side (or set of algorithm instances)
        /// needs to keep count of the window frames and the other should not (only for the 1st one - algorithm explanation attached) 
        /// and that's what the bool does. </param>
        public LowesSingleWindowAlgorithm(int size, List<StemWord> allWords, bool addReadingFrames)
        {
            Size = size;
            Root = allWords[0];
            AllWordsContainedWithinTheHorizon = allWords;
            //if (addReadingFrames)
            Helper.AddReadingFrames(1);
        }

        /// <summary>
        /// Returns a list of all the pairs in the matrix that need to be updated
        /// </summary>
        /// <returns></returns>
        public List<Tuple<int, int>> UpdateMatrixCountSameWordOnce()
        {
            //Added the condition that if a root word encounters the same word twice, it is 
            //only counted once
            List<int> wordsAlreadyConnectedWithRootWord = new List<int>();
            List<Tuple<int, int>> resultList = new List<Tuple<int, int>>();
            int startingPoint = 0;
            while (startingPoint <= Size)
            {
                for (int i = startingPoint; i < Size - 1; i++)
                {
                    for (int j = startingPoint + 1; j < Size; j++)
                    {
                        //check if Range[j].Id has been already added
                        if (wordsAlreadyConnectedWithRootWord.Contains(AllWordsContainedWithinTheHorizon[j].Id)) continue;
                        wordsAlreadyConnectedWithRootWord.Add(AllWordsContainedWithinTheHorizon[j].Id);
                        resultList.Add(new Tuple<int, int>(AllWordsContainedWithinTheHorizon[i].Id, AllWordsContainedWithinTheHorizon[j].Id));
                        resultList.Add(new Tuple<int, int>(AllWordsContainedWithinTheHorizon[j].Id, AllWordsContainedWithinTheHorizon[i].Id));
                    }
                    startingPoint++;
                }
            }
            return resultList;
        }

        //Added the condition that if a root word encounters the same word n times, then it is counted n times
        public List<Tuple<int, int>> UpdateMatrixCountsSameWordToWordPairTwice()
        {
            List<Tuple<int, int>> resultList = new List<Tuple<int, int>>();
            for (var j = 1; j < Size; j++)
            {
                resultList.Add(new Tuple<int, int>(AllWordsContainedWithinTheHorizon[0].Id, AllWordsContainedWithinTheHorizon[j].Id));
                resultList.Add(new Tuple<int, int>(AllWordsContainedWithinTheHorizon[j].Id, AllWordsContainedWithinTheHorizon[0].Id));
            }
            return resultList;
        }


        //Added the condition that if a root word encounters the same word n times, then it is counted n times
        public List<Tuple<int, int>> UpdateMatrixCountsAll()
        {
            List<Tuple<int, int>> resultList = new List<Tuple<int, int>>();
            for (var j = 1; j < Size; j++)
            {
                resultList.Add(new Tuple<int, int>(AllWordsContainedWithinTheHorizon[0].Id, AllWordsContainedWithinTheHorizon[j].Id));
                resultList.Add(new Tuple<int, int>(AllWordsContainedWithinTheHorizon[j].Id, AllWordsContainedWithinTheHorizon[0].Id));
            }
            return resultList;
        }

        //Updates in one direction (directional)
        public List<Tuple<int, int>> GetAllStemWordsIdsTuplesFromCenterStemWordGoingInOneDirection()
        {
            var stemwordsTupleList = new List<Tuple<int, int>>();
            for (var j = 1; j < Size; j++)
            {
                stemwordsTupleList.Add(
                    new Tuple<int, int>(AllWordsContainedWithinTheHorizon[0].Id, AllWordsContainedWithinTheHorizon[j].Id));
            }
            return stemwordsTupleList;
        }

        //Added the condition that if a root word encounters the same word n times, --- (not sure about this: then it is counted n times)
        public List<Tuple<int, int>> UpdateMatrix()
        {
            List<Tuple<int, int>> resultList = new List<Tuple<int, int>>();
            for (var j = 1; j < Size; j++)
            {
                resultList.Add(new Tuple<int, int>(AllWordsContainedWithinTheHorizon[0].Id, AllWordsContainedWithinTheHorizon[j].Id));
                resultList.Add(new Tuple<int, int>(AllWordsContainedWithinTheHorizon[j].Id, AllWordsContainedWithinTheHorizon[0].Id));
            }
            return resultList;

            //List<Tuple<int, int>> resultList = new List<Tuple<int, int>>();
            //for (var j = 1; j < Size; j++)
            //{
            //    if (Range[0].Id != Range[j].Id)
            //    {
            //        resultList.Add(new Tuple<int, int>(Range[0].Id, Range[j].Id));
            //        resultList.Add(new Tuple<int, int>(Range[j].Id, Range[0].Id));
            //    }
            //    else
            //    {
            //        resultList.Add(new Tuple<int, int>(Range[j].Id, Range[0].Id));
            //        // or resultList.Add(new Tuple<int, int>(Range[0].Id, Range[0].Id));
            //        // or resultList.Add(new Tuple<int, int>(Range[j].Id, Range[j].Id));
            //    }
            //}
            //return resultList;
        }

        public string TestOutput()
        {
            var text = "AA BB CC DD EE FF GG HH. AA BB CC DD EE.";
            Size = 5;

            List<StemWord> allWords = new List<StemWord>();
            allWords.Add(new StemWord("AA", 2));
            allWords.Add(new StemWord("BB", 2));
            allWords.Add(new StemWord("CC", 2));
            allWords.Add(new StemWord("DD", 2));
            allWords.Add(new StemWord("EE", 2));
            allWords.Add(new StemWord("FF", 1));
            allWords.Add(new StemWord("GG", 1));
            allWords.Add(new StemWord("HH", 1));
            Root = allWords[0];
            AllWordsContainedWithinTheHorizon = allWords;
            Helper.AddReadingFrames(1);

            return text;
        }
    }
}
