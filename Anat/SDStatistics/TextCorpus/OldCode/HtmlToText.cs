using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using HtmlAgilityPack;
using SDStatistics.Helpers;
using SDStatistics.TextCorpus.Helpers;

namespace SDStatistics.TextCorpus.OldCode
{
    public class HtmlToText
    {
        //private const int NumberOfWordsInSentence = 25;//15; //100; //20;//(number of) words in sentence.            

        public string Convert(string path)
        {
            var htmlDoc = new HtmlDocument();
            htmlDoc.Load(path);

            var stringWriter = new StringWriter();
            ConvertTo(htmlDoc.DocumentNode, stringWriter);
            stringWriter.Flush();
            return stringWriter.ToString();
        }

        public string ConvertHtml2TxtFile(string path)
        {
            var htmlDoc = new HtmlDocument();
            htmlDoc.Load(path);

            using (var stringWriter = new StringWriter())
            {
                ConvertTo(htmlDoc.DocumentNode, stringWriter);
                stringWriter.Flush();

                //Write contents to text file
                File.WriteAllText(path.Replace(".html", ".txt"), stringWriter.ToString());

                return stringWriter.ToString();
            }
        }

        /// <summary>
        /// HTML rules:
        /// a.	Cut-off all sentences where the "<p/>" marker is met.
        /// b.	If a sentence is > x words, then use the 20-word thresholding rule 
        /// (rounding up or down using the <br/> marker).
        /// </summary>
        /// <param name="path"></param>
        /// <returns>processed text/poem in sentences</returns>
        public List<string> ConvertHtml2TxtFileSpecial(string path)
        {
            var doc = new HtmlDocument();
            doc.Load(path);

            //StringWriter sw = new StringWriter();
            string rawPoem = doc.DocumentNode.OuterHtml;

            rawPoem = rawPoem.Trim();
            rawPoem = rawPoem.Replace("  ", " ");
            var stringSeparatorsPArray = new[] { "<p>", "</p>" };            
            var stringSeparatorsBrArray = new[] { "<br>" };

            var splitSentences = rawPoem.Split(stringSeparatorsPArray, StringSplitOptions.None);

            var sentencesList = new List<string>();
            foreach (var sentenceString in splitSentences)
            {
                if (sentenceString.Trim().WordCount() == 0) continue;

                var tempSentence = sentenceString;
                tempSentence = tempSentence.Replace("<p>", " "); //not needed
                tempSentence = tempSentence.Replace("</p>", " "); //not needed
                tempSentence = tempSentence.Trim();
                //var sentenceBR = tempSentence.Replace("<br>", " ");
                //tempSentence = tempSentence.Replace("<br>", " ");

                //int countSpaces = tempSentence.Count(Char.IsWhiteSpace);
                //int countWords = tempSentence.Split().Length;
                var wordCount = tempSentence.WordCount();

                if (wordCount > Constants.NumberOfWordsInSentence)
                {
                    //Count 20 words until the next <br>
                    string[] sbrSentences = tempSentence.Split(stringSeparatorsBrArray, StringSplitOptions.None);


                    //var wordCounterInt = 0;

                    //var newSentenceString = "";
                    //for (var i = 0; i < sbrSentences.Count(); i++)
                    //{
                    //    newSentenceString = newSentenceString + " " + sbrSentences[i];
                    //    wordCounterInt += sbrSentences[i].Split().Length;

                    //    if (wordCounterInt < Constants.NumberOfWordsInSentence) continue;

                        
                    //    wordCounterInt = 0;
                    //    sentencesList.Add(newSentenceString);
                    //    newSentenceString = "";
                    //}

                    for (var i = 0; i < sbrSentences.Count(); i++)
                    {
                        if (sbrSentences[i].WordCount() == 0) continue;

                        if (sbrSentences[i].WordCount() <= Constants.NumberOfWordsInSentence)
                            sentencesList.Add(sbrSentences[i]);
                        else
                        {
                            var sentencesSplit = sbrSentences[i].SplitSentence(Constants.NumberOfWordsInSentence);
                            sentencesList.AddRange(sentencesSplit);
                        }

                    }

                    //sentencesList.Add(newSentenceString);//add the last words
                }
                else
                {
                    //sentencesList.Add(sentenceBR);
                    sentencesList.Add(tempSentence);
                }
            }

            using (var file = new StreamWriter(path.Replace(".html", ".txt")))
            {
                foreach (var lineString in sentencesList)
                {
                    file.WriteLine(lineString);
                }
                file.Close();//in MSN example it is missing.
            }
            return sentencesList;
        }

        public string ConvertHtml(string html)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            StringWriter sw = new StringWriter();
            ConvertTo(doc.DocumentNode, sw);
            sw.Flush();
            return sw.ToString();
        }

        private void ConvertContentTo(HtmlNode node, TextWriter outText)
        {
            foreach (HtmlNode subnode in node.ChildNodes)
            {
                ConvertTo(subnode, outText);
            }
        }

        public void ConvertTo(HtmlNode node, TextWriter outText)
        {
            switch (node.NodeType)
            {
                case HtmlNodeType.Comment:
                    // don't output comments
                    break;

                case HtmlNodeType.Document:
                    ConvertContentTo(node, outText);
                    break;

                case HtmlNodeType.Text:
                    // script and style must not be output
                    string parentName = node.ParentNode.Name;
                    if ((parentName == "script") || (parentName == "style"))
                        break;

                    // get text
                    var htmlString = ((HtmlTextNode)node).Text;

                    // is it in fact a special closing node output as text?
                    if (HtmlNode.IsOverlappedClosingElement(htmlString))
                        break;


                    // check the text is meaningful and not a bunch of whitespaces
                    htmlString = htmlString.Trim();

                    //Code for splitting sentence
                    if (htmlString.Length > 0)
                    {
                        //If htmlString number of words is larger than NumberOfWordsInSentence
                        //int countSpaces = htmlString.Count(Char.IsWhiteSpace);
                        
                        //var countWords = htmlString.Split().Length;
                        //var sentenceLengthMultipleCounts = countWords / NumberOfWordsInSentence;
                        var parts = Array.ConvertAll(htmlString.Split(' '), p => p.Trim());
                       //rebuild a string with the empty spaces removed
                        htmlString = TextHelper.ConvertStringArrayToString(parts);

                        var countWords = parts.Length;
                        var sentenceLengthMultipleCounts = countWords / Constants.NumberOfWordsInSentence;
                        
                        if (sentenceLengthMultipleCounts < 1)
                        {
                            try
                            {
                                outText?.Write(HtmlEntity.DeEntitize(htmlString));
                            }
                            catch (Exception exception)
                            {
                                Debug.WriteLine("Error in converting html to text." + exception.Message);
                            }
                        }
                        else
                        {
                            var htmlStringArraySplit = htmlString.Split(new char[0]);

                            
                            //check that it is not a case of a sentence "\r\n                       I" because
                            //in this case it will be considered a '31' worded string array.
                            
                            var counter = 1;
                            var end = 0;
                            string sentenceString;
                            while (sentenceLengthMultipleCounts > 0)
                            {
                                int start = end;
                                end = counter*sentenceLengthMultipleCounts;
                                counter++;
                                sentenceString = htmlString.Substring(start, end);
                                if (outText != null) outText.Write(HtmlEntity.DeEntitize(sentenceString));
                                sentenceLengthMultipleCounts--;
                            }

                            //for the remaining words (fraction of sentenceLengthMultipleCounts)
                            sentenceString = htmlString.Substring(end, htmlStringArraySplit.Count());
                            if (outText != null) outText.Write(HtmlEntity.DeEntitize(sentenceString));
                        }
                    }
                    break;

                case HtmlNodeType.Element:
                    switch (node.Name)
                    {
                        case "p":
                            // treat paragraphs as crlf
                            //outText.Write("\r\n");
                            outText.Write(" <p> ");
                            break;
                        case "br":
                            // treat line breaks as space
                            outText.Write(" ");
                            break;
                    }

                    if (node.HasChildNodes)
                    {
                        ConvertContentTo(node, outText);
                    }
                    break;
            }
        }
    }
}