﻿using System;
using System.Linq;

namespace SDStatistics.TextCorpus.OldCode
{
    public static class Extensions
    {

        /// <summary>
        /// Sentences can count the number of word in it
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int WordCount(this String str)
        {
            return str.Split(new char[] { ' ', '.', '?' }, StringSplitOptions.RemoveEmptyEntries).Length;
        }

        /// <summary>
        /// Sentences check if the number of words in it is less than or equal to the length value
        /// </summary>
        /// <param name="str"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static bool Conforming(this String str, int length)
        {
            return str.WordCount() <= length;
        }


        public static string[] SplitSentence(this String str, int length)
        {
            return str.Split(new[] { " ", "  " }, 3, StringSplitOptions.None);
        }

    }
}


