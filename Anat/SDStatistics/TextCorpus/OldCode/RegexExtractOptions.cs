namespace SDStatistics.TextCorpus.OldCode
{
    public enum RegexExtractOptions
    {
        Point = 1,
        QuestionExclamation = 2,
        Semicolumn = 3,
        Comma = 4,
        FullStopQuestionExclamation = 5
    }
}