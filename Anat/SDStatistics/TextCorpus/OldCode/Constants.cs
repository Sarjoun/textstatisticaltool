namespace SDStatistics.TextCorpus.OldCode
{
   public static class Constants
   {
      public static string Precision = "{0:0.00000000}";//"{0:0.00000}";
      public static bool Gui = false;
      public const int NumberOfWordsInSentence = 15; //25;//100; //20;//(number of) words in sentence.            

      //global variables
      public static string PoetAllPoemsSentencesTopVariantWordsFileName = @"/GlobalPoemsFileTopVariantWords.txt";
      //public static string PoetAllPoemsVocabularyStemWordsAfterReduction = @"/VocabularyStemmedWords_Reduced.txt";
      public static string PoetAllPoemsVocabularyStemWordsOldFileName = @"/VocabularyStemmedWords_UsingRF.txt"; //The marginal probability is using as denominatory the total number of ReadingFrames

      //This is the new Marginal probability and should be used in the place of the previous one
      //public static string PoetAllPoemsVocabularyStemWordsFileName = @"/VocabularyStemmedWords.txt";//The marginal probability is using the summation of all probabilities from the Joint matrix
      public static string PoetAllPoemsVocabularyStemWordsDetailedFileName = @"/VocabularyStemmedWordsDetailed.txt";//The marginal probability is using the summation of all probabilities from the Joint matrix

      //public static string PoetAllPoemsGeneralStatisticsFileName = @"/GeneralStatistics.txt";
      public static string PoetAllPoemsGeneralStatisticsFileNameNoSlash = @"GeneralStatistics.txt";
      public static string PoetAllPoemsGeneralStatisticsFileNameAfterReduction = @"/GeneralStatisticsAfterReduction.txt";

      //public static string PoetMarginalProbability = @"\MarginalProbability.txt";
      //public static string PoetMarginalProbabilityReduced = @"/MarginalProbabilityReduced.txt";

      public static string PoetJointFrequencyMatrixGephi = @"/JointFrequencyGephi.csv";
      //public static string PoetJointFrequencyMatrix = @"\JointFrequencyMatrixNeverReduced.txt";
      public static string PoetJointFrequencyMatrixDistributionPlot = @"/JointFrequencyMatrixDistributionPlot.txt";

      //public static string PoetJointFrequencyMatrixBeforeReduction = @"/JointFrequencyMatrixBeforeReduction.txt";
      //public static string PoetJointFrequencyMatrixZero = @"/JointFrequencyMatrixWithZeroReplacedEdges.txt";

      public static string PoetJointProbabilityMatrixGephi = @"/JointProbabilityMatrixGephi.csv";
      public static string PoetJointProbabilityMatrix = @"/JointProbabilityMatrix.txt";
      public static string PoetJointProbabilityMatrixDistributionPlot = @"/JointProbabilityMatrixDistributionPlot.txt";

      public static string PoetConditionalProbabilityMatrixGephi = @"/ConditionalProbabilityGephi.csv";
      public static string PoetConditionalProbabilityMatrix = @"/ConditionalProbability.txt";
      public static string PoetConditionalProbabilityMatrixDistributionPlot = @"/ConditionalProbabilityDistributionPlot.txt";

      public static string PoetCorrelationCoefficientMatrixGephi = @"/CorrelationCoefficientGephi.csv";
      public static string PoetCorrelationCoefficientMatrix = @"/CorrelationCoefficient.txt";
      public static string PoetCorrelationCoefficientMatrixDistributionPlot = @"/CorrelationCoefficientDistributionPlot.txt";

      public static string PoetPMIMatrixGephi = @"/PointWiseMutualInformationGephi.csv";
      public static string PoetPMIMatrix = @"/PointWiseMutualInformation.txt";
      public static string PoetPMIMatrixDistributionPlot = @"/PointWiseMutualInformationDistributionPlot.txt";

      public static string PoetLogOddsRatioMatrixGephi = @"/LogOddsRatioGephi.csv";
      public static string PoetLogOddsRatioMatrix = @"/LogOddsRatio.txt";
      public static string PoetLogOddsRatioMatrixDistributionPlot = @"\LogOddsRatioDistributionPlot.txt";

      public static string ClusteredAgentsJointFrequencyMatrix = @"\ClusteringAgentsJointFrequencyMatrix.csv";
      public static string ClusteredAgentsJointProbabilityMatrix = @"\ClusteringAgentsJointProbabilityMatrix.csv";
      public static string ClusteredAgentsConditionalProbabilityMatrix = @"\ClusteringAgentsConditionalProbabilityMatrix.csv";

      public static string FileExtensionTxt = "*.txt";
      public static string FileExtensionHtml = "*.html";
      public static string FileExtensionCSV = "*.csv";


      public static void AddPoetNameToOutputFiles(string poetName)
      {
         PoetAllPoemsSentencesTopVariantWordsFileName = PoetAllPoemsSentencesTopVariantWordsFileName + poetName;
         PoetAllPoemsVocabularyStemWordsOldFileName = PoetAllPoemsVocabularyStemWordsOldFileName + poetName;
         //PoetAllPoemsGeneralStatisticsFileName = PoetAllPoemsGeneralStatisticsFileName + poetName;
         PoetAllPoemsVocabularyStemWordsDetailedFileName = PoetAllPoemsVocabularyStemWordsDetailedFileName + poetName;

         PoetJointFrequencyMatrixGephi = PoetJointFrequencyMatrixGephi + poetName;
         //PoetJointFrequencyMatrix = PoetJointFrequencyMatrix + poetName;
         PoetJointFrequencyMatrixDistributionPlot = PoetJointFrequencyMatrixDistributionPlot + poetName;

         PoetJointProbabilityMatrixGephi = PoetJointProbabilityMatrixGephi + poetName;
         PoetJointProbabilityMatrix = PoetJointProbabilityMatrix + poetName;
         PoetJointProbabilityMatrixDistributionPlot = PoetJointProbabilityMatrixDistributionPlot + poetName;

         PoetConditionalProbabilityMatrixGephi = PoetConditionalProbabilityMatrixGephi + poetName;
         PoetConditionalProbabilityMatrix = PoetConditionalProbabilityMatrix + poetName;
         PoetConditionalProbabilityMatrixDistributionPlot = PoetConditionalProbabilityMatrixDistributionPlot + poetName;

         PoetCorrelationCoefficientMatrixGephi = PoetCorrelationCoefficientMatrixGephi + poetName;
         PoetCorrelationCoefficientMatrix = PoetCorrelationCoefficientMatrix + poetName;
         PoetCorrelationCoefficientMatrixDistributionPlot = PoetCorrelationCoefficientMatrixDistributionPlot + poetName;

         PoetPMIMatrixGephi = PoetPMIMatrixGephi + poetName;
         PoetPMIMatrix = PoetPMIMatrix + poetName;
         PoetPMIMatrixDistributionPlot = PoetPMIMatrixDistributionPlot + poetName;
      }
   }
}
