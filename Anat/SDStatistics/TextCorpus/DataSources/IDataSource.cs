//using System.Collections.Generic;

//namespace SDStatistics.TextCorpus.DataSources
//{
//    interface IDataSource
//    {
//        // Assigns the variables that are used.
//        //void InitializeVariables(List<string> stopWordsStringList);
//        void InitializeVariables();

//        // Resets all the variables used. Useful when there are more than 1 source exists. The poet or source's name
//        void ResetVariables(string poet);
//        void CalculateWordStatisticsAndReplaceWordWithStemRoot(bool stemOption, List<string> allSentencesMinusStopWords);    
//        void ReplaceStemWithTopVariantAndWriteOutVocabularyWithStatisticsToFiles();
//        void WriteOutGeneralAnalysisResultsToFiles();
//        void CreateDictionary();
//        List<string> RecursiveTextProcessingAlgorithm(int horizonSize, bool simulationTest, List<string> allSentencesWithStemmedWords);
//        //void CalculateTheSumOfAllLinksInAllFrames();

//        //That is the old way. We still calculate it (for now) but will be using the value of the sum of a 
//        //word's probabilities from the joint matrix
//        //void UpdateStemWordsMarginalProbabilityUsingRfCount();

//        void UpdateStemWordsMarginalProbabilityFromJointProbabilityMatrix(decimal denominator, string fname);
//        void WriteBasicResultsToFiles();
//        void CalculateAndWriteOutTheConditionalProbability();
//        void CalculateAndWriteOutTheCorrelationCoefficient();
//        void CalculateAndWriteOutThePointWiseMutualInformation();
//        void CalculateAndWriteOutTheLogOddsRatio();
//        void MoveResultFilesToCorrespondingFoldersAndCleanUp();
//    }
//}
