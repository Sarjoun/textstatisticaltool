﻿using System.Collections.Generic;

namespace SDStatistics.TextCorpus.DataSources
{
    interface IDataSourceTextFormat
    {
        void InitializeVariables();
        // Resets all the variables used. Useful when there are more than 1 source exists. The poet or source's name
        void ResetVariables(string poet); 
        void CalculateWordStatisticsAndReplaceWordWithStemRoot(bool stemOption, List<string> allSentencesMinusStopWords);
        void ReplaceStemWithTopVariantAndWriteOutVocabularyWithStatisticsToFiles();
        //void WriteOutGeneralAnalysisResultsToFiles(List<string> allSentences, Dictionary<StemWord, int> stemmedWordDictionaryOrderedByWeight);
        void CreateDictionary();
        List<string> RecursiveTextProcessingAlgorithm(int horizonSize, bool simulationTest, List<string> allSentencesWithStemmedWords, string globalPoemsFileHorizonSentences);
        void UpdateStemWordsMarginalProbabilityFromJointProbabilityMatrix(decimal denominator, string fname, int[][] stemWordMatrix);
        //void WriteBasicResultsToFiles();
        void CalculateAndWriteOutTheConditionalProbability();
        void CalculateAndWriteOutTheCorrelationCoefficient();
        void CalculateAndWriteOutThePointWiseMutualInformation();
        void CalculateAndWriteOutTheLogOddsRatio();
        void MoveResultFilesToCorrespondingFoldersAndCleanUp();
    }
}

//Interfaces can't have static methods. 
//A class that implements an interface needs to implement them all as instance methods. 
//Static classes can't have instance methods.
//   QED.Aug 12, 2009
