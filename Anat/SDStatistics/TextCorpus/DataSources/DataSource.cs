﻿//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.IO;
//using System.Linq;
//using System.Text.RegularExpressions;
//using Iveonik.Stemmers;
//using SDParser.Enums;
//using SDParser.Extensions;
//using SDParser.Primitives;
//using SDParser.StopWords;
//using SDParser.Structures;
//using SDParser.TextProcessingCentral;
//using SDStatistics.Enums;
//using SDStatistics.Helpers;
//using SDStatistics.TextCorpus.Helpers;
//using SDStatistics.TextCorpus.OldCode;
//using SDStatistics.TextCorpus.SentenceReadingAlgorithms;
//using SDStatistics.TextCorpus.SentenceSplitters;

//namespace SDStatistics.TextCorpus.DataSources
//{
//    public class DataSource : IDataSource
//    {
//        #region export variables
//        /* Represents the value in the dictionary to export to the network statistics analysis:
//        Format: word frequency probability */
//        public List<Tuple<string, string, string>> ExportDictionaryToNodeXL { get; set; }
//        #endregion

//        #region local variables

//        public string DataSourceName = "";
//        public string FolderPath = "";

//        private HtmlToText _htmlToText;
//        private SentenceSplitter _sentenceSplitter;
//        private List<string> _allSentences;
//        private List<string> _allSentencesMinusStopWords;
//        private List<string> _allSentencesWithStemWords;
//        private List<string> _allSentencesWithTopVariantWords;
//        //which is similar to _allSentencesWithStemWords with stemRoot replaced with top variant

//        private List<StemWord> StemWords { get; set; }
//        public List<string> StemWordsString;
//        private List<string> _stemWordsTopVariantString;
//        private int _totalNumberOfWords;

//        public int Horizon { get; set; }
//        //private List<Horizon> horizons;
//        private int[][] _stemWordMatrix;

//        private List<string> _stopWords = new List<string>();
//        private List<string> _sentences = new List<string>();
//        private List<string> _totalWords = new List<string>();
//        public List<string> TopFrequentWords = new List<string>();
//        private Dictionary<StemWord, int> _vocabularyDictionary = new Dictionary<StemWord, int>();
//        private Dictionary<StemWord, int> _vocabularyLowWordFrequencyDictionary = new Dictionary<StemWord, int>();
//        public Dictionary<StemWord, int> StemWordDictionaryOrderedByWeight = new Dictionary<StemWord, int>();

//        public List<string> LowWordFrequencyList { get; set; }


//        /* copy of _stemWordDictionaryOrderedByWeight but with the key replaced by the top popular variant word */
//        private Dictionary<StemWord, int> _topVariantDictionaryOrderedByWeight = new Dictionary<StemWord, int>();

//        //Since we need to calculate (sum of all frame sizes in the corpus) - (number of frames in the corpus). 
//        //then this dictionary uses for a key the frame size and for value the number of that frames in the corpus
//        private Dictionary<int, int> _frameCounterDictionary = new Dictionary<Int32, Int32>();
//        private int _frameCounter; // (sum of all frame sizes in the corpus) - (number of frames in the corpus). 

//        private List<string> _tempWordSentenceList = new List<string>();
//        private Dictionary<StemWord, int> _tempvocabularyDictionary = new Dictionary<StemWord, int>();
//        private string[] _poemsStringArray = null;
//        private readonly Regex _regex = new Regex(@"[ ]{2,}", RegexOptions.None);

//        #endregion

//        public TextReaderAlgorithmEnum CurrentAlgorithm { get; set; }

//        public bool UseStemming { get; set; }
//        // Originally the DataSource class was meant to process a poet corpora in HTML format.
//        // Now it is a multi-purpose data source processor. 
//        public DataSource(
//            string poetFolderPath, int horizonSize, TextReaderAlgorithmEnum algorithm,
//            SentenceSetUp sentenceSetup, bool useStemming, bool calculateAllAvailableStatistics, 
//            DataFileFormat sourceDataFileFormat,
//            WordStreamFileName wordStreamFileName)
//        {
//            /* Initialization */
//            ExportDictionaryToNodeXL = new List<Tuple<string, string, string>>();
//            InitializeVariables();
//            Horizon = horizonSize;
//            ResetVariables(poetFolderPath);
//            UseStemming = useStemming;
//            CurrentAlgorithm = algorithm;
//                if (sourceDataFileFormat == DataFileFormat.AllSupportedText)
//                {
//                    /* Extract text */
//                    foreach (var dataFilePath in _poemsStringArray)
//                    {
//                        try
//                        {
//                            _allSentences.AddRange(ProcessHtmlSourceFileAndWriteResultToTxtFile(dataFilePath, sentenceSetup));
//                        }
//                        catch (Exception e)
//                        {
//                            Console.WriteLine(e);
//                            throw;
//                        }
//                    }
//                }
//                else
//                {
//                    //TODO: complete me
//                    MakeWordStreamAndVariantWordsReturnDictionaryOrderedAlphabeticallyFromListOfFileNames(
//                        _poemsStringArray, wordStreamFileName, IncludeTitleInSetup.FromValue(true));

//                    foreach (var dataFilePath in _poemsStringArray)
//                    {
//                        try
//                        {
//                        //In order to combine the 2 systems (IONA and ALAN) into using the same text processing rule.
//                        //I need to convert the function (ProcessTextSourceFileAndWriteResultToTxtFile) into using
//                        //the same rules that the IONA is using and which is used in the function before this
//                        //which is MakeWordStreamAndVariantWordsReturnDictionaryOrderedAlphabeticallyFromListOfFileNames.
                        
//                        _allSentences.AddRange(ProcessTextSourceFileAndWriteResultToTxtFile(dataFilePath, sentenceSetup));
//                        }
//                        catch (Exception e)
//                        {
//                            Console.WriteLine(e);
//                            throw;
//                        }
//                    }
//                }


//            /* Remove stop words and numerals*/
//            var extractedSents = RemoveStopWordsAndNumeralsFromSentences();

//            /* CalculateWordStatisticsAndReplaceWordWithStemRoot */
//            CalculateWordStatisticsAndReplaceWordWithStemRoot(UseStemming, extractedSents);

//            if (Helper.UseLowWordFrequencyReduction)
//            {
//                RemoveLowFrequencyWordOccurrencesAndUpdateWordStatistics(Helper.LowWordFrequencyValue);
//                RemoveLowFrequencyWordsFromSentences();
//            }
//            WriteOutGeneralAnalysisResultsToFiles();

//            //update
//            _totalNumberOfWords = _vocabularyDictionary.Sum(x => x.Key.Frequency);

//            CreateStemWordsMatrix();

//            _allSentencesWithStemWords = CurrentAlgorithm == TextReaderAlgorithmEnum.LoweSingleWindowAlgorithm
//                ? RecursiveTextProcessingAlgorithmLowesSingleWindow(horizonSize, false, _allSentencesWithStemWords)
//                : RecursiveTextProcessingAlgorithmLowesDoubleWindow(horizonSize, false, _allSentencesWithStemWords);
//            Helper.TotalNumberOfPairsWrtFrames = Helper.LowesTotalNumberOfWindows;

//            UpdateStemWordsMarginalProbabilityFromJointProbabilityMatrix(Helper.LowesTotalNumberOfWindows, Constants.PoetMarginalProbability);

//            ReplaceStemWithTopVariantAndWriteOutVocabularyWithStatisticsToFiles();
//            WriteOutGlobalFileUsingTopVariant();

//            #region Using network reduction

//            if (Helper.UseReduction)
//            {
//                //write words' frequencies for Gephi before reduction
//                Helper.write_sparse_int_gephi(StemWordDictionaryOrderedByWeight.Count, _stemWordMatrix,
//                    FolderPath + Constants.PoetJointFrequencyMatrixBeforeReductionGephi, _stemWordsTopVariantString);//_StemWordsString);


//                /* The task is to identify all the edges with values equal to the [Helper.ReductionValue]
//                in the probability matrix (which is not reduced) and then replace those values with 0,
//                while adding them to a file for later review.                               
//                */

//                //write out the original matrix file before the edge value reduction
//                SDParser.FileFunctions.FileWriter.WriteSparseIntArrayToFile(_stemWordMatrix, FolderPath + Constants.PoetJointFrequencyMatrixBeforeReduction);

//                //remove certain edge values 
//                var resultMatrix = Helper.write_sparse_int_replace_edgevalue(StemWordDictionaryOrderedByWeight.Count,
//                    _stemWordMatrix, FolderPath + Constants.PoetJointFrequencyMatrixZero, Helper.ReductionValue, 0);

//                _stemWordMatrix =
//                    Helper.RemoveZeroEntriesFromMatrixThenRemoveZeroColumnsAndRows(StemWordDictionaryOrderedByWeight.Count,
//                        resultMatrix, FolderPath + Constants.PoetJointFrequencyMatrixZeroRemoved);


//                //It is ok to assign the orderedDictionary to the regular one.
//                ReduceRegularDictionary(Helper.ListOfWordsToRemove);
//                //ReduceOrderedDictionary(Helper.ListOfWordsToRemove);

//                ReduceSizeOfVariousDictionariesAfterReduction();

//                //Rebuild the _StemWordMatrix !!!
//                UpdateStemWordsMarginalProbabilityFromJointProbabilityMatrix(Helper.TotalNumberOfPairsWrtFrames, Constants.PoetMarginalProbabilityReduced);

//                //Have to calculate _stemWordDictionaryOrderedByWeight. Generate new ordered_dictionary
//                ReplaceStemWithTopVariantAndWriteOutVocabularyWithStatisticsToFilesAfterReduction();
//            }
//            #endregion

//            WriteBasicResultsToFiles();

//            //Update MatrixValue with the updated StemWord matrix
//            Helper.StemWordsCoOccurenceMatrixIntValue = _stemWordMatrix;
//            if (calculateAllAvailableStatistics)
//            {
//                CalculateAndWriteOutTheConditionalProbability();
//                CalculateAndWriteOutTheCorrelationCoefficient();
//                CalculateAndWriteOutThePointWiseMutualInformation();
//                CalculateAndWriteOutTheLogOddsRatio();
//            }
//            MoveResultFilesToCorrespondingFoldersAndCleanUp();
//        }

//        //TODO Create matrices using the clusteredStemWords, files and in memory and return the values.
//        public DataSource(
//            string poetFolderPath, int horizonSize, TextReaderAlgorithmEnum algorithm,
//            SentenceSetUp sentenceSetup, bool useStemming, bool calculateAllAvailableStatistics,
//            bool directAnatVersion, IEnumerable<string> poemsStringArray,
//            string documentsFilePath,
//            IEnumerable<StemWord> clusteredStemWords,
//            DataFileFormat dataFileFormat)
//        {
//            UseStemming = useStemming;
//            /* Initialization */
//            ExportDictionaryToNodeXL = new List<Tuple<string, string, string>>();
//            InitializeVariables();
//            Horizon = horizonSize;

//            var stringArray = poemsStringArray as string[] ?? poemsStringArray.ToArray();
//            if (directAnatVersion)
//            {
//                poetFolderPath = ExtractTextFromStrings.GetFolderPath(documentsFilePath);
//                ResetVariablesAnat(poetFolderPath, stringArray);
//            }
//            else
//                ResetVariablesAnatFromFile(poetFolderPath);

//            CurrentAlgorithm = algorithm;

//            if (dataFileFormat == DataFileFormat.AllSupportedText)
//            {
//                /* Extract text */
//                foreach (var dataFilePath in _poemsStringArray)
//                {
//                    try
//                    {
//                        //HERE
//                        //Add the title to see the effect here
//                        var start = dataFilePath.LastIndexOf(@"\", StringComparison.Ordinal);
//                        var finish = dataFilePath.LastIndexOf(@"[", StringComparison.Ordinal);
//                        var fileTitle = dataFilePath.Substring(start + 1, finish - start - 1);

//                        _allSentences.Add(fileTitle);
//                        _allSentences.AddRange(
//                            ProcessHtmlSourceFileAndWriteResultToTxtFile(dataFilePath, sentenceSetup));
//                    }
//                    catch (Exception e)
//                    {
//                        //Should throw the error in a window here.
//                    }
//                }
//            }
//            else if (dataFileFormat == DataFileFormat.TextFormat)
//            {
//                /* Extract text */
//                foreach (var dataFilePath in _poemsStringArray)
//                {
//                    try
//                    {
//                        //HERE
//                        //Add the title to see the effect here
//                        var start = dataFilePath.LastIndexOf(@"\", StringComparison.Ordinal);
//                        var finish = dataFilePath.LastIndexOf(@"[", StringComparison.Ordinal);
//                        var fileTitle = dataFilePath.Substring(start + 1, finish - start - 1);
//                        _allSentences.Add(fileTitle);
//                        _allSentences.AddRange(ProcessTextSourceFileAndWriteResultToTxtFile(dataFilePath, sentenceSetup));
//                    }
//                    catch (Exception e)
//                    {
//                        //Should throw the error in a window here.
//                    }
//                }
//            }

//            /* Remove stop words and numerals*/
//            var extractedSentences = RemoveStopWordsAndNumeralsFromSentences();

//            /* CalculateWordStatisticsAndReplaceWordWithStemRoot */
//            CalculateWordStatisticsAndReplaceWordWithStemRoot(UseStemming, extractedSentences);

//            if (Helper.UseLowWordFrequencyReduction)
//            {
//                RemoveLowFrequencyWordOccurrencesAndUpdateWordStatistics(Helper.LowWordFrequencyValue);
//                RemoveLowFrequencyWordsFromSentences();
//            }

//            //TODO FIX THIS
//            WriteOutGeneralAnalysisResultsToFiles();

//            //update
//            _totalNumberOfWords = _vocabularyDictionary.Sum(x => x.Key.Frequency);

//            CreateStemWordsMatrix();

//            _allSentencesWithStemWords = CurrentAlgorithm == TextReaderAlgorithmEnum.LoweSingleWindowAlgorithm
//                ? RecursiveTextProcessingAlgorithmLowesSingleWindow(horizonSize, false, _allSentencesWithStemWords)
//                : RecursiveTextProcessingAlgorithmLowesDoubleWindow(horizonSize, false, _allSentencesWithStemWords);

//            Helper.TotalNumberOfPairsWrtFrames = Helper.LowesTotalNumberOfWindows;
//            UpdateStemWordsMarginalProbabilityFromJointProbabilityMatrix(Helper.LowesTotalNumberOfWindows, Constants.PoetMarginalProbability);

//            ReplaceStemWithTopVariantAndWriteOutVocabularyWithStatisticsToFiles();
//            WriteOutGlobalFileUsingTopVariant();

//            #region Using network reduction

//            if (Helper.UseReduction)
//            {
//                //write words' frequencies for Gephi before reduction
//                Helper.write_sparse_int_gephi(StemWordDictionaryOrderedByWeight.Count, _stemWordMatrix,
//                    FolderPath + Constants.PoetJointFrequencyMatrixBeforeReductionGephi, _stemWordsTopVariantString);//_StemWordsString);


//                /* The task is to identify all the edges with values equal to the [Helper.ReductionValue]
//                in the probability matrix (which is not reduced) and then replace those values with 0,
//                while adding them to a file for later review.                               
//                */

//                //write out the original matrix file before the edge value reduction
//                SDParser.FileFunctions.FileWriter.WriteSparseIntArrayToFile(_stemWordMatrix,
//                    FolderPath + Constants.PoetJointFrequencyMatrixBeforeReduction);

//                //remove certain edge values 
//                var resultMatrix = Helper.write_sparse_int_replace_edgevalue(StemWordDictionaryOrderedByWeight.Count,
//                    _stemWordMatrix, FolderPath + Constants.PoetJointFrequencyMatrixZero, Helper.ReductionValue, 0);

//                _stemWordMatrix =
//                    Helper.RemoveZeroEntriesFromMatrixThenRemoveZeroColumnsAndRows(StemWordDictionaryOrderedByWeight.Count,
//                        resultMatrix, FolderPath + Constants.PoetJointFrequencyMatrixZeroRemoved);


//                //It is ok to assign the orderedDictionary to the regular one.
//                ReduceRegularDictionary(Helper.ListOfWordsToRemove);
//                //ReduceOrderedDictionary(Helper.ListOfWordsToRemove);

//                ReduceSizeOfVariousDictionariesAfterReduction();

//                //Rebuild the _StemWordMatrix !!!
//                UpdateStemWordsMarginalProbabilityFromJointProbabilityMatrix(Helper.TotalNumberOfPairsWrtFrames, Constants.PoetMarginalProbabilityReduced);

//                //Have to calculate _stemWordDictionaryOrderedByWeight. Generate new ordered_dictionary
//                ReplaceStemWithTopVariantAndWriteOutVocabularyWithStatisticsToFilesAfterReduction();
//            }
//            #endregion

//            //WriteBasicResultsToFiles();

//            //Update the Ids of the clusteredStemWords with the indices of the probablility matrix,
//            //so that later I can correlate between values of the the values between the 
//            clusteredStemWords = Helper.UpdateClusteredStemWordsWithIdsCorrespondingToNewProbabilityMatrixIndexWeird(
//                clusteredStemWords, _topVariantDictionaryOrderedByWeight);//_vocabularyDictionary);


//            var stemWords = clusteredStemWords as StemWord[] ?? clusteredStemWords.ToArray();
//            WriteBasicResultsToFilesWithClusteredStemWords_AndUpdateJointFrequencyAndJointProbabilityClusteredStemWords(stemWords);

//            Top10StemWordsFromMatrix = _stemWordsTopVariantString.Take(10).ToList();
//            SyncedIdsStemWords = stemWords.ToList();

//            Helper.SetupProbabilityCalculationsEngine();//Needed before doing any probability calculations.
//            //CalculateAndWriteOutTheConditionalProbability();//iffy here .. TODO: fix this issue
//            ConditionalProbabilityClusteredAgentsMatrixValues = CalculateAndWriteOutTheConditionalProbabilityForClusteredAgents(stemWords);
//            //Update MatrixValue with the updated StemWord matrix
//            Helper.StemWordsCoOccurenceMatrixIntValue = _stemWordMatrix;

//            //CalculateAndWriteOutTheConditionalProbability();
//            //if (calculateAllAvailableStatistics)
//            //{
//            CalculateAndWriteOutTheConditionalProbability();
//            CalculateAndWriteOutTheCorrelationCoefficient();
//            CalculateAndWriteOutThePointWiseMutualInformation();
//            CalculateAndWriteOutTheLogOddsRatio();
//            //}
//            try
//            {
//                MoveResultFilesToCorrespondingFoldersAndCleanUp();
//            }
//            catch (Exception e)
//            {
//                Console.WriteLine(e);
//                throw;
//            }
//        }

//        public decimal[,] JointFrequencyClusteredAgentsMatrixValues { get; set; }
//        public decimal[,] JointProbabilityClusteredAgentsMatrixValues { get; set; }
//        public decimal[,] ConditionalProbabilityClusteredAgentsMatrixValues { get; set; }
//        public List<string> Top10StemWordsFromMatrix { get; set; }
//        public List<StemWord> SyncedIdsStemWords { get; set; }

//        //uses sentencesSplitter to implement the cutting off the sentences
//        public List<string> ProcessHtmlSourceFileAndWriteResultToTxtFile(string poem, SentenceSetUp sentenceSetUp)
//        {
//            //If poem has .htm or .html to it, then convert to html first, otherwise simply read it
//            var extension = Path.GetExtension(poem);
//            string poemTextVersion;

//            // Use the overload of the Equals method that specifies a StringComparison. 
//            // Ordinal is the fastest way to compare two strings. 
//            //bool result = root.Equals(root2, StringComparison.Ordinal);

//            if (extension != null && extension.Contains("htm"))
//            {
//                //Extract text from html and write it into a .txt file with same name.
//                poemTextVersion = _htmlToText.ConvertHtml2TxtFile(poem);
//                //var poemTextVersion = _htmlToText.ConvertHtml2TxtFileSpecial(poem);

//                //Remove all \r\n
//                poemTextVersion = poemTextVersion.Replace("\r\n", " ");
//                poemTextVersion = poemTextVersion.Replace("--", " ");
//                poemTextVersion = poemTextVersion.Trim();
//            }
//            else
//            {
//                // Read the file as one string.
//                var myFile = new StreamReader(poem);
//                poemTextVersion = myFile.ReadToEnd();
//                myFile.Close();

//                //Remove all \r\n
//                poemTextVersion = poemTextVersion.Replace("\r\n", " ");
//                poemTextVersion = poemTextVersion.Replace("--", " ");
//                poemTextVersion = poemTextVersion.Trim();
//            }

//            _sentenceSplitter = new SentenceSplitter(poemTextVersion);

//            switch (sentenceSetUp)
//            {
//                case SentenceSetUp.NoSplit:
//                    _sentenceSplitter.ProcessPoemTextVersionAsOneBigSentence();
//                    break;

//                case SentenceSetUp.Split:
//                    _sentenceSplitter.ProcessPoemTextVersionUsingOrbit();
//                    break;

//                default:
//                    _sentenceSplitter.ProcessPoemTextVersionUsingOrbit();
//                    break;
//            }

//            return _sentenceSplitter.ResultingSentencesList;
//            //_allSentences.AddRange(_sentenceSplitter.ResultingSentencesList);
//        }
//        public List<string> ProcessTextSourceFileAndWriteResultToTxtFile(string poem, SentenceSetUp sentenceSetUp)
//        {
//            // Read the file as one string.
//            var myFile = new StreamReader(poem);
//            var sourceFileContent = myFile.ReadToEnd();
//            myFile.Close();

//            //Remove all \r\n
//            sourceFileContent = sourceFileContent
//                .Replace("\r\n", " ")
//                .Replace("--", " ")
//                .Trim();

//            _sentenceSplitter = new SentenceSplitter(sourceFileContent);

//            switch (sentenceSetUp)
//            {
//                case SentenceSetUp.NoSplit:
//                    _sentenceSplitter.ProcessPoemTextVersionAsOneBigSentence();
//                    break;

//                case SentenceSetUp.Split:
//                    _sentenceSplitter.ProcessPoemTextVersionUsingOrbit();
//                    break;

//                default:
//                    _sentenceSplitter.ProcessPoemTextVersionUsingOrbit();
//                    break;
//            }

//            return _sentenceSplitter.ResultingSentencesList;
//        }
//        public static Dictionary<string, StemWord> UniqueWordsAndTheirVariants { get; set; }

//        public static Dictionary<string, StemWord> MakeWordStreamAndVariantWordsReturnDictionaryOrderedAlphabeticallyFromFile(
//            DocumentsLinksFileName documentsLinksFileName,
//            WordStreamFileName wordStreamFileName,
//            IncludeTitleInSetup includeTitleInSetup)
//        {
//            SentenceProcessor.Stemmer = new EnglishStemmer();
//            SentenceProcessor.StopWords = StopWordsHelper.GetStopWordsFromEmbeddedResource();
//            SentenceProcessor.NewWords = new List<string>();
//            SentenceProcessor.UniqueWordsAndTheirVariants = new Dictionary<string, StemWord>();

//            StreamReader file = null;
//            //TextWriter fileWriter = new StreamWriter(wordStreamFileName.Value);
//            try
//            {
//                file = new StreamReader(documentsLinksFileName.Value);
//                string line;
//                while ((line = file.ReadLine()) != null)
//                {
//                    var newWords = StoryProcessor.MakeWordStreamFromSingleStory(
//                        line.RemoveDocIdFromStartOfPathIfItExists(),
//                        includeTitleInSetup);

//                    if (newWords.Count <= 0) continue;

//                    //fileWriter.WriteLine("##SOD##" + line);
//                    //foreach (var word in newWords)
//                    //    fileWriter.WriteLine(word);
//                    //fileWriter.WriteLine("##EOD##");
//                }
//            }
//            finally
//            {
//                file?.Close();
//                //fileWriter.Close();
//            }
//            return UniqueWordsAndTheirVariants = SentenceProcessor.UpdatedUniqueWordsAndTheirVariants();
//        }

//        public static Dictionary<string, StemWord> MakeWordStreamAndVariantWordsReturnDictionaryOrderedAlphabeticallyFromListOfFileNames(
//            string[] documentsFileNames,
//            WordStreamFileName wordStreamFileName,
//            IncludeTitleInSetup includeTitleInSetup)
//        {
//            //Setup static class SentenceProcessor
//            SentenceProcessor.Stemmer = new EnglishStemmer();
//            SentenceProcessor.StopWords = StopWordsHelper.GetStopWordsFromEmbeddedResource();
//            SentenceProcessor.NewWords = new List<string>();
//            SentenceProcessor.UniqueWordsAndTheirVariants = new Dictionary<string, StemWord>();

//            TextWriter fileWriter = new StreamWriter(wordStreamFileName.Value);
//            try
//            {
//                foreach (var documentFileLink in documentsFileNames)
//                {
//                    try
//                    {
//                        var newWords = StoryProcessor.MakeWordStreamFromSingleStory(
//                            documentFileLink.RemoveDocIdFromStartOfPathIfItExists(),
//                            includeTitleInSetup);

//                        //fileWriter.WriteLine("##SOD##" + documentFileLink);
//                        //foreach (var word in newWords)
//                        //    fileWriter.WriteLine(word);
//                        //fileWriter.WriteLine("##EOD##");

//                        //if (newWords.Count <= 0)
//                        //    continue;

//                    }
//                    catch (Exception e)
//                    {
//                        Console.WriteLine(e);
//                        throw;
//                    }
//                }
//            }
//            finally
//            {
//                //file?.Close();
//                fileWriter.Close();
//            }
//            var test = SentenceProcessor.UpdatedUniqueWordsAndTheirVariants();
//            return UniqueWordsAndTheirVariants = SentenceProcessor.UpdatedUniqueWordsAndTheirVariants();
//        }


//        #region Initializing and resetting variables - should get rid of
//        //public void InitializeVariables(List<string> stopWordsStringList)
//        public void InitializeVariables()
//        {
//            //_stopWords = stopWordsStringList;
//            _stopWords = StopWordsHelper.GetStopWordsFromEmbeddedResource();
//            _totalNumberOfWords = 0;
//            Helper.OrderedVocabularyDictionary = null;
//            Helper.StemWordsCoOccurenceMatrixIntValue = null;
//            Helper.ResetReadingFrames();
//            Helper.TotalNumberOfPairsWrtFrames = 0;
//            Helper.LowesTotalNumberOfWindows = 0;
//            _sentences = new List<string>();
//            _totalWords = new List<string>();
//            TopFrequentWords = new List<string>();
//            StemWordDictionaryOrderedByWeight = new Dictionary<StemWord, int>();
//            _tempWordSentenceList = new List<string>();

//            _frameCounterDictionary = new Dictionary<int, int>();
//            _frameCounter = 0;
//            _vocabularyDictionary = new Dictionary<StemWord, int>();
//            _topVariantDictionaryOrderedByWeight = new Dictionary<StemWord, int>();
//            _stemWordsTopVariantString = new List<string>();
//        }

//        public void ResetVariables(string poet)
//        {
//            _stemWordMatrix = null;
//            //_uniqueWords = 0;
//            _totalNumberOfWords = 0;
//            Helper.OrderedVocabularyDictionary = null;
//            Helper.StemWordsCoOccurenceMatrixIntValue = null;
//            Helper.ResetReadingFrames();
//            Helper.TotalNumberOfPairsWrtFrames = 0;
//            Helper.LowesTotalNumberOfWindows = 0;
//            //stopWords = new List<string>();
//            _sentences = new List<string>();
//            _totalWords = new List<string>();
//            TopFrequentWords = new List<string>();
//            _vocabularyDictionary = new Dictionary<StemWord, int>();
//            StemWordDictionaryOrderedByWeight = new Dictionary<StemWord, int>();
//            StemWords = new List<StemWord>();

//            FolderPath = poet; //folderBrowserDialog1.SelectedPath;
//            DataSourceName = poet.Substring(poet.LastIndexOf(@"\", System.StringComparison.Ordinal) + 1,
//                poet.Length - poet.LastIndexOf(@"\", System.StringComparison.Ordinal) - 1);

//            //Added after I added categorization to the suspected type of poet (or source) which is space followed by 2 designation letters: POETNAME XX
//            DataSourceName = DataSourceName.Substring(0, DataSourceName.Length - 3);

//            //AddPoetNameToOutputFiles();
//            _poemsStringArray = Directory.GetFiles(FolderPath);
//            _htmlToText = new HtmlToText();
//            _allSentences = new List<string>();
//            _allSentencesMinusStopWords = new List<string>();
//            _allSentencesWithStemWords = new List<string>();
//            _allSentencesWithTopVariantWords = new List<string>();
//            StemWordsString = new List<string>();

//            _frameCounterDictionary = new Dictionary<int, int>();
//            _frameCounter = 0;
//            _vocabularyDictionary = new Dictionary<StemWord, int>();
//            StemWordDictionaryOrderedByWeight = new Dictionary<StemWord, int>();
//            _topVariantDictionaryOrderedByWeight = new Dictionary<StemWord, int>();
//            _stemWordsTopVariantString = new List<string>();
//        }

//        //TODO fix reset variables function
//        public void ResetVariablesAnatFromFile(string poet)
//        {
//            _stemWordMatrix = null;
//            //_uniqueWords = 0;
//            _totalNumberOfWords = 0;
//            Helper.OrderedVocabularyDictionary = null;
//            Helper.StemWordsCoOccurenceMatrixIntValue = null;
//            Helper.ResetReadingFrames();
//            Helper.TotalNumberOfPairsWrtFrames = 0;
//            Helper.LowesTotalNumberOfWindows = 0;
//            //stopWords = new List<string>();
//            _sentences = new List<string>();
//            _totalWords = new List<string>();
//            TopFrequentWords = new List<string>();
//            _vocabularyDictionary = new Dictionary<StemWord, int>();
//            StemWordDictionaryOrderedByWeight = new Dictionary<StemWord, int>();
//            StemWords = new List<StemWord>();

//            FolderPath = poet; //folderBrowserDialog1.SelectedPath;
//            DataSourceName = poet.Substring(poet.LastIndexOf(@"\", System.StringComparison.Ordinal) + 1,
//                poet.Length - poet.LastIndexOf(@"\", System.StringComparison.Ordinal) - 1);

//            //Added after I added categorization to the suspected type of poet (or source) which is space followed by 2 designation letters: POETNAME XX
//            DataSourceName = DataSourceName.Substring(0, DataSourceName.Length - 3);

//            //AddPoetNameToOutputFiles();
//            _poemsStringArray = GetAllTheDocumentsFromTheDocumentsFile(FolderPath);
//            _htmlToText = new HtmlToText();
//            _allSentences = new List<string>();
//            _allSentencesMinusStopWords = new List<string>();
//            _allSentencesWithStemWords = new List<string>();
//            _allSentencesWithTopVariantWords = new List<string>();
//            StemWordsString = new List<string>();

//            _frameCounterDictionary = new Dictionary<int, int>();
//            _frameCounter = 0;
//            _vocabularyDictionary = new Dictionary<StemWord, int>();
//            StemWordDictionaryOrderedByWeight = new Dictionary<StemWord, int>();
//            _topVariantDictionaryOrderedByWeight = new Dictionary<StemWord, int>();
//            _stemWordsTopVariantString = new List<string>();
//        }

//        public void ResetVariablesAnat(string poet, IEnumerable<string> poemsStringArray)
//        {
//            _stemWordMatrix = null;
//            _totalNumberOfWords = 0;
//            _sentences = new List<string>();
//            _totalWords = new List<string>();
//            _vocabularyDictionary = new Dictionary<StemWord, int>();
//            StemWordDictionaryOrderedByWeight = new Dictionary<StemWord, int>();

//            TopFrequentWords = new List<string>();
//            StemWords = new List<StemWord>();
//            StemWordsString = new List<string>();

//            Helper.OrderedVocabularyDictionary = null;
//            Helper.StemWordsCoOccurenceMatrixIntValue = null;
//            Helper.ResetReadingFrames();
//            Helper.TotalNumberOfPairsWrtFrames = 0;
//            Helper.LowesTotalNumberOfWindows = 0;


//            FolderPath = poet; //folderBrowserDialog1.SelectedPath;

//            DataSourceName = TextHelper.GetSourceNameFromFile(poet); //poet.Substring(poet.LastIndexOf(@"\", StringComparison.Ordinal) + 1, poet.Length - poet.LastIndexOf(@"\", System.StringComparison.Ordinal) - 1);
//                                                                     //Added after I added categorization to the suspected type of poet (or source) which is space followed by 2 designation letters: POETNAME XX
//                                                                     //DataSourceName = DataSourceName.Substring(0, DataSourceName.Length - 3);

//            _poemsStringArray = poemsStringArray.ToArray();

//            _htmlToText = new HtmlToText();
//            _allSentences = new List<string>();
//            _allSentencesMinusStopWords = new List<string>();
//            _allSentencesWithStemWords = new List<string>();
//            _allSentencesWithTopVariantWords = new List<string>();
//            _frameCounterDictionary = new Dictionary<int, int>();
//            _frameCounter = 0;
//            _vocabularyDictionary = new Dictionary<StemWord, int>();
//            StemWordDictionaryOrderedByWeight = new Dictionary<StemWord, int>();
//            _topVariantDictionaryOrderedByWeight = new Dictionary<StemWord, int>();
//            _stemWordsTopVariantString = new List<string>();
//        }
//        #endregion

//        public static string[] GetAllTheDocumentsFromTheDocumentsFile(string fileName)
//        {
//            var documentsLinks = SDParser.CustomFileParsers.LineByLineParsingFunctions.ReadLines(fileName);
//            return documentsLinks.ToArray();
//        }

//        public bool CheckIfWordIsLowFrequencyWord(string word)
//        {
//            return LowWordFrequencyList.Contains(word);
//        }

//        public void RemoveLowFrequencyWordsFromSentences()
//        {
//            _tempWordSentenceList = new List<string>();
//            _allSentencesMinusStopWords = new List<string>();

//            foreach (var sentenceRawString in _allSentencesWithStemWords)
//            {
//                _tempWordSentenceList = new List<string>();

//                var sentenceString = _regex.Replace(sentenceRawString, @" ");
//                //replace multiple spaces with a single space
//                var inputStringArray = sentenceString.Split(" ".ToCharArray()).ToList();
//                inputStringArray = inputStringArray.ConvertAll(d => d.ToLower());

//                //instead compare word by word
//                foreach (var word in inputStringArray)
//                {
//                    if (!CheckIfWordIsLowFrequencyWord(word))
//                        _tempWordSentenceList.Add(word);
//                }

//                string results = string.Join(" ", _tempWordSentenceList);
//                _allSentencesMinusStopWords.Add(results);
//            }
//            //_allSentencesMinusStopWords
//            //_tempWordSentenceList = _allSentencesWithStemWords;

//            _allSentencesWithStemWords = _allSentencesMinusStopWords;
//        }

//        /// <summary>
//        /// This method removes all occurrences of stop words from the corpus based on the loaded list of stop word file.
//        /// </summary>
//        public string RemoveSpecialChars(string input)
//        {
//            return Regex.Replace(input, @"[^0-9a-zA-Z\._]", string.Empty);
//        }

//        /// <summary>
//        /// This method removes all occurrences of stop words from the corpus based on the loaded list of stop word file.
//        /// Also removes all numbers.
//        /// </summary>
//        public List<string> RemoveStopWordsAndNumeralsFromSentences()
//        {
//            _tempWordSentenceList = new List<string>();
//            foreach (var sentenceRawString in _allSentences)
//            {
//                _tempWordSentenceList = new List<string>();
//                var sentenceString = _regex.Replace(sentenceRawString, @" ");

//                //replace multiple spaces with a single space
//                var inputStringArray = sentenceString.Split(" ".ToCharArray()).ToList();
//                inputStringArray = inputStringArray.ConvertAll(d => d.ToLower());

//                //Can't use this because it treats both as list, meaning that duplicates are removed: [W1 W2 W1 W3] & [W3] => [W1 W2]
//                //_tempWordSentenceList = inputStringArray.Except(_stopWords).ToList();

//                //instead compare word by word
//                foreach (var word in inputStringArray)
//                {
//                    //var word2 = word.Replace("’", " ");
//                    //word2 = word2.Replace("”", " ");
//                    //word2 = word2.Replace("“", " ");
//                    //word2 = word2.Replace("'", " ");
//                    //word2 = word2.Replace("\"", " ");
//                    //word2 = word2.Replace("�", " ");
//                    var word2 = RemoveSpecialChars(word).Trim();

//                    if ((!_stopWords.Contains(word2)) && (!word2.HasNumber()))
//                    {
//                        _tempWordSentenceList.Add(word2);
//                    }
//                }

//                string resultingSentence = string.Join(" ", _tempWordSentenceList);

//                //Added a check to make sure that the resultingSentence has at least 1 word in it.
//                if (string.IsNullOrEmpty(resultingSentence) || string.IsNullOrWhiteSpace(resultingSentence))
//                { }
//                else
//                    _allSentencesMinusStopWords.Add(resultingSentence);
//            }
//            return _allSentencesMinusStopWords;
//        }

//        /// <summary>
//        /// This should happen after stemming has happened (if it was an option) because single frequency
//        /// words can combine into a higher frequency stem word. This only removes the words that have a low
//        /// occurrence. It does not affect the edge it might have shared with another word. 
//        /// </summary>
//        /// <param name="frequency"></param>
//        public void RemoveLowFrequencyWordOccurrencesAndUpdateWordStatistics(int frequency)
//        {
//            _vocabularyLowWordFrequencyDictionary = new Dictionary<StemWord, int>();
//            LowWordFrequencyList = new List<string>();
//            foreach (var item in _vocabularyDictionary.Where(kvp => kvp.Value <= frequency).ToList())
//            {
//                _vocabularyLowWordFrequencyDictionary.Add(item.Key, item.Value);
//                LowWordFrequencyList.Add(item.Key.Root);
//                _vocabularyDictionary.Remove(item.Key);
//            }
//        }

//        // The noStem reference refers to the option of processing the data without stemming.
//        // Without disturbing the otherwise working perfectly code and output statistics, the efficient 
//        // way is to create a stem word (since the whole code depends on it) that takes only 1 value which
//        // it is itself!
//        public void CalculateWordStatisticsAndReplaceWordWithStemRoot(
//            bool useStemming, List<string> allSentencesMinusStopWords)
//        {
//            IStemmer stemmer = new EnglishStemmer();
//            var tempStemWordString = "";

//            foreach (var sentenceMinusStopWords in allSentencesMinusStopWords)
//            {
//                _tempWordSentenceList = new List<string>();
//                var inputStringArray = sentenceMinusStopWords.Split(" ".ToCharArray()).ToList();

//                foreach (var wordString in inputStringArray)
//                {
//                    if (wordString.Length <= 1) continue;

//                    tempStemWordString = useStemming
//                        ? stemmer.Stem(wordString.ToLower())
//                        : wordString.ToLower();

//                    var stemWord = _vocabularyDictionary.FirstOrDefault(x => x.Key.Root == tempStemWordString).Key;
//                    if (stemWord != null)
//                    {
//                        _vocabularyDictionary.FirstOrDefault(x => x.Key.Root == tempStemWordString).Key.AddVariantWord(wordString.ToLower());
//                        _vocabularyDictionary[stemWord] = _vocabularyDictionary[stemWord] + 1;
//                    }
//                    else
//                    {
//                        stemWord = new StemWord(tempStemWordString, new List<VariantWord>(), 0, null);
//                        stemWord.AddVariantWord(wordString);

//                        _vocabularyDictionary.Add(stemWord, 1);
//                    }
//                    _tempWordSentenceList.Add(tempStemWordString);
//                }
//                string results = string.Join(" ", _tempWordSentenceList);
//                if (results.Count(c => !char.IsWhiteSpace(c)) > 0) // returns the number of non-whitespace characters
//                    _allSentencesWithStemWords.Add(results);
//            }
//        }

//        public void ReplaceStemWithTopVariantAndWriteOutVocabularyWithStatisticsToFiles()
//        {
//            //Write out simple statistics about the corpus 
//            using (var file = new StreamWriter(FolderPath + Constants.PoetAllPoemsGeneralStatisticsFileName))
//            {
//                //the +1,-1 is to fix the display and so for not including the '/' in the poet's name
//                file.WriteLine("General statistics for " +
//                               FolderPath.Substring(FolderPath.LastIndexOf('\\') + 1,
//                                   FolderPath.Length - FolderPath.LastIndexOf('\\') - 1));
//                file.WriteLine("");

//                file.WriteLine("The total number of reading frames is : " + Helper.GetReadingFrames());
//                file.WriteLine("The total number of (nonstop) word pairs wrt to reading frames is : " +
//                               Helper.TotalNumberOfPairsWrtFrames);
//                //file.WriteLine("The total number of (stemmed) words is : " + vocabulary_dictionary.Sum(x => x.Key.Frequency - 1).ToString());
//                file.WriteLine("The total number of (stemmed) words is : " +
//                               _vocabularyDictionary.Sum(x => x.Key.Frequency));
//                file.WriteLine("The total number of (stemmed) vocabulary words is : " + _vocabularyDictionary.Count);
//            }

//            _stemWordsTopVariantString = new List<string>();

//            WriteTheVocabularyWithoutHeaders();
//        }

//        //TODO: fix this
//        private void WriteTheVocabularyWithoutHeaders()
//        {
//            #region Write the vocabulary (regular) without headers

//            using (var streamWriter
//                = new StreamWriter(FolderPath + Constants.PoetAllPoemsVocabularyStemWordsOldFileName))
//            {
//                //sort dictionary based in (total) entry value
//                //Using LINQ alphabetically entry value
//                StemWordDictionaryOrderedByWeight = (from entry in _vocabularyDictionary
//                                                     orderby entry.Value descending
//                                                     select entry).
//                    ToDictionary(pair => pair.Key, pair => pair.Value);

//                _allSentencesWithTopVariantWords = new List<string>();
//                _topVariantDictionaryOrderedByWeight = new Dictionary<StemWord, int>();

//                //Using LINQ alphabetically
//                foreach (var entry in StemWordDictionaryOrderedByWeight)
//                {
//                    //get the word's version with the highest score  ( get TopVariantWord )
//                    var mostPopularStemVersion = entry.Key.TopVariantWord;

//                    //streamWriter.WriteLine(entry.Key.Root + " " + entry.Value + " " + string.Format(Constants.Precision, (decimal)entry.Key.MarginalProbabilityUsingRFCount));
//                    streamWriter.WriteLine(mostPopularStemVersion + " " + entry.Value + " " +
//                                           string.Format(Constants.Precision, entry.Key.MarginalProbability));

//                    //Create new dictionary that has the most popular variant word as the Name (or main name) 
//                    var newStemWord = entry.Key;
//                    //newStemWord.Root = mostPopularStemVersion;
//                    _topVariantDictionaryOrderedByWeight.Add(newStemWord, entry.Value);
//                    _stemWordsTopVariantString.Add(mostPopularStemVersion);
//                    //added to list to replace header in .csv files
//                }
//                streamWriter.Close();

//                //Populate the allSentencesWithTopVariantWords list
//                //_allSentencesWithTopVariantWords = new List<string>();
//                //ReplaceStemRootWordWithTopVariantInSentences();
//            }

//            #endregion
//        }
//        public void ReplaceStemWithTopVariantAndWriteOutVocabularyWithStatisticsToFilesAfterReduction()
//        {
//            //Write out simple statistics about the corpus 
//            using (
//                var file = new StreamWriter(FolderPath + Constants.PoetAllPoemsGeneralStatisticsFileNameAfterReduction))
//            {
//                //the +1,-1 is to fix the display and so for not including the '/' in the poet's name
//                file.WriteLine("General statistics for " +
//                               FolderPath.Substring(FolderPath.LastIndexOf('\\') + 1,
//                                   FolderPath.Length - FolderPath.LastIndexOf('\\') - 1));
//                file.WriteLine("");

//                file.WriteLine("The total number of reading frames is : " + Helper.GetReadingFrames());
//                file.WriteLine("The total number of (nonstop) word pairs wrt to reading frames is : " +
//                               Helper.TotalNumberOfPairsWrtFrames);
//                //file.WriteLine("The total number of (stemmed) words is : " + vocabulary_dictionary.Sum(x => x.Key.Frequency - 1).ToString());
//                file.WriteLine("The total number of (stemmed) words is : " +
//                               _vocabularyDictionary.Sum(x => x.Key.Frequency));
//                file.WriteLine("The total number of (stemmed) vocabulary words is : " + _vocabularyDictionary.Count);
//            }

//            _stemWordsTopVariantString = new List<string>();

//            #region Write the vocabulary (regular) without headers

//            using (
//                var streamWriter =
//                    new StreamWriter(FolderPath + Constants.PoetAllPoemsVocabularyStemWordsAfterReduction))
//            {
//                //sort dictionary based in (total) entry value
//                //Using LINQ alphabetically entry value
//                StemWordDictionaryOrderedByWeight = (from entry in _vocabularyDictionary
//                                                     orderby entry.Value descending
//                                                     select entry).
//                    ToDictionary(pair => pair.Key, pair => pair.Value);

//                _allSentencesWithTopVariantWords = new List<string>();
//                _topVariantDictionaryOrderedByWeight = new Dictionary<StemWord, int>();

//                //Using LINQ alphabetically
//                foreach (var entry in StemWordDictionaryOrderedByWeight)
//                {
//                    //get the word's version with the highest score  ( get TopVariantWord )
//                    var mostPopularStemVersion = ((StemWord)entry.Key).TopVariantWord;

//                    //streamWriter.WriteLine(entry.Key.Root + " " + entry.Value + " " + string.Format(Constants.Precision, 
//                    //(decimal)entry.Key.MarginalProbabilityUsingRFCount));

//                    streamWriter.WriteLine(mostPopularStemVersion + " " + entry.Value + " " +
//                                           string.Format(Constants.Precision, entry.Key.MarginalProbability));

//                    //Create new dictionary that has the most popular variant word as the Name (or main name) 
//                    var newStemWord = entry.Key;
//                    //newStemWord.Root = mostPopularStemVersion;
//                    _topVariantDictionaryOrderedByWeight.Add(newStemWord, entry.Value);
//                    _stemWordsTopVariantString.Add(mostPopularStemVersion);
//                    //added to list to replace header in .csv files
//                }
//                streamWriter.Close();

//                //Populate the allSentencesWithTopVariantWords list
//                //_allSentencesWithTopVariantWords = new List<string>();
//                //ReplaceStemRootWordWithTopVariantInSentences();
//            }

//            #endregion
//        }

//        public void WriteBasicResultsToFiles()
//        {
//            //write words' frequencies 
//            #region write words' frequencies
//            Helper.write_sparse_int_wordsFrequencies(StemWordDictionaryOrderedByWeight.Count, _stemWordMatrix,
//                FolderPath + Constants.PoetJointFrequencyMatrix);
//            #endregion

//            //write words' frequencies for Gephi
//            #region write words' frequencies for Gephi
//            Helper.write_sparse_int_gephi(StemWordDictionaryOrderedByWeight.Count, _stemWordMatrix,
//                FolderPath + Constants.PoetJointFrequencyMatrixGephi, _stemWordsTopVariantString);//_StemWordsString);
//            #endregion

//            //write words' joint probability 
//            #region write words' joint probability
//            _vocabularyDictionary = Helper.write_sparse_decimal_gephi(
//                StemWordDictionaryOrderedByWeight.Count,
//                _stemWordMatrix,
//                FolderPath + Constants.PoetJointProbabilityMatrixGephi,
//                FolderPath + Constants.PoetJointProbabilityMatrix,
//                FolderPath + Constants.PoetJointProbabilityMatrixDistributionPlot,
//                _stemWordsTopVariantString, _vocabularyDictionary); //_StemWordsString
//            #endregion

//            //Update the ordered Vocabulary 
//            Helper.OrderedVocabularyDictionary = StemWordDictionaryOrderedByWeight;


//            #region Write the vocabulary (regular) without headers
//            using (var streamWriter = new StreamWriter(FolderPath + Constants.PoetAllPoemsVocabularyStemWordsFileName))
//            {
//                //sort dictionary based in (total) entry value
//                //Using LINQ alphabetically entry value
//                StemWordDictionaryOrderedByWeight = (from entry in _vocabularyDictionary orderby entry.Value descending select entry).
//                    ToDictionary(pair => pair.Key, pair => pair.Value);

//                _allSentencesWithTopVariantWords = new List<string>();
//                _topVariantDictionaryOrderedByWeight = new Dictionary<StemWord, int>();

//                //Using LINQ alphabetically
//                foreach (var entry in StemWordDictionaryOrderedByWeight)
//                {
//                    //get the word's version with the highest score  ( get TopVariantWord )
//                    var mostPopularStemVersion = ((StemWord)entry.Key).TopVariantWord;

//                    streamWriter.WriteLine(mostPopularStemVersion + " " + entry.Value + " " + string.Format(Constants.Precision, entry.Key.MarginalProbability));

//                    //To export to the network statistics analysis
//                    ExportDictionaryToNodeXL.Add(new Tuple<string, string, string>(mostPopularStemVersion, entry.Value.ToString(), string.Format(Constants.Precision, entry.Key.MarginalProbability)));


//                    //Create new dictionary that has the most popular variant word as the Name (or main name) 
//                    var newStemWord = entry.Key;
//                    //newStemWord.Root = mostPopularStemVersion;
//                    _topVariantDictionaryOrderedByWeight.Add(newStemWord, entry.Value);
//                }
//                streamWriter.Close();
//                streamWriter.Dispose();
//            }
//            #endregion
//            //TODO the next bug location
//            //Write the vocabulary (detailed) without headers  
//            #region Write the vocabulary (detailed) without headers  
//            using (var streamWriter2 = new StreamWriter(FolderPath + Constants.PoetAllPoemsVocabularyStemWordsDetailedFileName))
//            {
//                //sort dictionary based in (total) entry value//Using LINQ alphabetically entry value
//                StemWordDictionaryOrderedByWeight = (from entry in _vocabularyDictionary orderby entry.Value descending select entry).
//                    ToDictionary(pair => pair.Key, pair => pair.Value);

//                //Using LINQ alphabetically
//                //var sortedVocab = (from entry in vocabulary_dictionary orderby entry.Key ascending select entry).
//                //    ToDictionary(pair => pair.Key, pair => pair.Value);
//                streamWriter2.WriteLine("[{0} {1}]", "Most common word", "Frequency");
//                foreach (var entry in StemWordDictionaryOrderedByWeight)
//                {
//                    //streamWriter2.Write(entry.Key.Root + " [" + entry.Value + "] = ");
//                    streamWriter2.WriteLine(entry.Key.GetRootAndVariantsAndAllFrequenciesStringRepresentation());
//                    //foreach (var variation in entry.Key.WordVariations)
//                    //    streamWriter2.Write(variation.Key + "(" + variation.Value + "), ");
//                    //streamWriter2.WriteLine();
//                }
//                streamWriter2.Close();//in MSN example it is missing.     
//            }
//            #endregion

//        }

//        public void WriteBasicResultsToFilesWithClusteredStemWords_AndUpdateJointFrequencyAndJointProbabilityClusteredStemWords(IEnumerable<StemWord> clusteredStemWords)
//        {
//            ////Update the Ids of the clusteredStemWords with the indices of the probablility matrix,
//            ////so that later I can correlate between values of the the values between the 
//            //clusteredStemWords = Helper.UpdateClusteredStemWordsWithIdsCorrespondingToNewProbabilityMatrixIndexWeird(clusteredStemWords, _vocabularyDictionary);

//            //write words' frequencies 
//            #region write words' frequencies
//            Helper.write_sparse_int_wordsFrequencies(StemWordDictionaryOrderedByWeight.Count, _stemWordMatrix, FolderPath + Constants.PoetJointFrequencyMatrix);
//            #endregion

//            #region WriteClusteredValuesJointFrequency
//            var stemWords = clusteredStemWords as StemWord[] ?? clusteredStemWords.ToArray();
//            JointFrequencyClusteredAgentsMatrixValues = Helper.WriteJointFrequencyForClusteredAgents(
//                _stemWordMatrix, FolderPath + Constants.ClusteredAgentsJointFrequencyMatrix,
//               stemWords);
//            #endregion

//            //write words' frequencies for Gephi
//            #region write words' frequencies for Gephi
//            Helper.write_sparse_int_gephi(StemWordDictionaryOrderedByWeight.Count, _stemWordMatrix,
//               FolderPath + Constants.PoetJointFrequencyMatrixGephi, _stemWordsTopVariantString);//_StemWordsString);
//            #endregion

//            //write words' joint probability 
//            #region write words' joint probability
//            _vocabularyDictionary = Helper.write_sparse_decimal_gephi(
//               StemWordDictionaryOrderedByWeight.Count, _stemWordMatrix,
//               FolderPath + Constants.PoetJointProbabilityMatrixGephi,
//               FolderPath + Constants.PoetJointProbabilityMatrix,
//               FolderPath + Constants.PoetJointProbabilityMatrixDistributionPlot,
//               _stemWordsTopVariantString, _vocabularyDictionary);
//            #endregion

//            #region WriteClusteredValuesJointProbability
//            JointProbabilityClusteredAgentsMatrixValues = Helper.WriteReducedMatrix(_stemWordMatrix,
//                FolderPath + Constants.ClusteredAgentsJointProbabilityMatrix, stemWords);
//            #endregion

//            //Update the ordered Vocabulary 
//            Helper.OrderedVocabularyDictionary = StemWordDictionaryOrderedByWeight;

//            #region Write the vocabulary (regular) without headers
//            using (var streamWriter = new StreamWriter(FolderPath + Constants.PoetAllPoemsVocabularyStemWordsFileName))
//            {
//                //sort dictionary based in (total) entry value
//                //Using LINQ alphabetically entry value
//                StemWordDictionaryOrderedByWeight = (from entry in _vocabularyDictionary orderby entry.Value descending select entry).
//                   ToDictionary(pair => pair.Key, pair => pair.Value);

//                _allSentencesWithTopVariantWords = new List<string>();
//                _topVariantDictionaryOrderedByWeight = new Dictionary<StemWord, int>();

//                //Using LINQ alphabetically
//                foreach (var entry in StemWordDictionaryOrderedByWeight)
//                {
//                    //get the word's version with the highest score  ( get TopVariantWord )
//                    var mostPopularStemVersion = ((StemWord)entry.Key).TopVariantWord;

//                    streamWriter.WriteLine(mostPopularStemVersion + " " + entry.Value + " " + string.Format(Constants.Precision, entry.Key.MarginalProbability));

//                    //To export to the network statistics analysis
//                    ExportDictionaryToNodeXL.Add(new Tuple<string, string, string>(mostPopularStemVersion, entry.Value.ToString(), string.Format(Constants.Precision, entry.Key.MarginalProbability)));


//                    //Create new dictionary that has the most popular variant word as the Name (or main name) 
//                    var newStemWord = entry.Key;
//                    //newStemWord.Root = mostPopularStemVersion;
//                    _topVariantDictionaryOrderedByWeight.Add(newStemWord, entry.Value);
//                }
//                streamWriter.Close();
//                streamWriter.Dispose();
//            }
//            #endregion
//            //TODO the next bug location
//            //Write the vocabulary (detailed) without headers  
//            #region Write the vocabulary (detailed) without headers  
//            using (var streamWriter2 = new StreamWriter(FolderPath + Constants.PoetAllPoemsVocabularyStemWordsDetailedFileName))
//            {
//                //sort dictionary based in (total) entry value//Using LINQ alphabetically entry value
//                StemWordDictionaryOrderedByWeight = (from entry in _vocabularyDictionary orderby entry.Value descending select entry).
//                   ToDictionary(pair => pair.Key, pair => pair.Value);

//                //Using LINQ alphabetically
//                //var sortedVocab = (from entry in vocabulary_dictionary orderby entry.Key ascending select entry).
//                //    ToDictionary(pair => pair.Key, pair => pair.Value);
//                streamWriter2.WriteLine("[{0} {1}]", "Most common word", "Frequency");
//                foreach (var entry in StemWordDictionaryOrderedByWeight)
//                {
//                    //streamWriter2.Write(entry.Key.Root + " [" + entry.Value + "] = ");
//                    streamWriter2.WriteLine(entry.Key.GetRootAndVariantsAndAllFrequenciesStringRepresentation());
//                    //foreach (var variation in entry.Key.WordVariations)
//                    //    streamWriter2.Write(variation.Key + "(" + variation.Value + "), ");
//                    //streamWriter2.WriteLine();
//                }
//                streamWriter2.Close();//in MSN example it is missing.     
//            }
//            #endregion

//        }

//        private void ReduceSizeOfVariousDictionariesAfterReduction()
//        {
//            //sort dictionary based in (total) entry value
//            //Using LINQ alphabetically entry value
//            StemWordDictionaryOrderedByWeight = (from entry in _vocabularyDictionary
//                                                 orderby entry.Value descending
//                                                 select entry).
//                ToDictionary(pair => pair.Key, pair => pair.Value);

//            _allSentencesWithTopVariantWords = new List<string>();
//            _topVariantDictionaryOrderedByWeight = new Dictionary<StemWord, int>();

//            //Using LINQ alphabetically
//            foreach (var entry in StemWordDictionaryOrderedByWeight)
//            {
//                //get the word's version with the highest score  ( get TopVariantWord )
//                var mostPopularStemVersion = ((StemWord)entry.Key).TopVariantWord;
//                //Create new dictionary that has the most popular variant word as the Name (or main name) 
//                var newStemWord = entry.Key;
//                //newStemWord.Root = mostPopularStemVersion;
//                _topVariantDictionaryOrderedByWeight.Add(newStemWord, entry.Value);
//                _stemWordsTopVariantString.Add(mostPopularStemVersion);
//            }
//        }

//        /// <summary>
//        /// Global file containing all the poems in one file.
//        /// Sentences with original words but without stop words
//        /// Sentences with stemmed words and without stop words
//        /// etc...
//        /// </summary>
//        public void WriteOutGeneralAnalysisResultsToFiles()
//        {
//            StreamWriter file;
//            //Add the contents to a global file containing all poems                        
//            using (file = new StreamWriter(FolderPath + Constants.PoetAllPoemsSentencesFileName))
//            {
//                foreach (var sentence in _allSentences)
//                {
//                    file.WriteLine(sentence);
//                }
//                file.Close();
//            }

//            using (file = new StreamWriter(FolderPath + Constants.PoetAllPoemsSentencesNoStopWordsFileName))
//            {
//                foreach (var sentenceMinusStopWords in _allSentencesMinusStopWords)
//                {
//                    file.WriteLine(sentenceMinusStopWords);
//                }
//                file.Close();
//            }

//            using (file = new StreamWriter(FolderPath + Constants.PoetAllPoemsSentencesStemWordsFileName))
//            {
//                foreach (string sentenceWithStemWords in _allSentencesWithStemWords)
//                {
//                    file.WriteLine(sentenceWithStemWords);
//                }
//                file.Close();
//            }

//            //Write the vocabulary with details
//            using (file = new StreamWriter(FolderPath + Constants.PoetAllPoemsVocabularyStemWordsDetailedOldFileName))
//            {
//                StemWordDictionaryOrderedByWeight = (from entry in _vocabularyDictionary
//                                                     orderby entry.Value descending
//                                                     select entry).
//                                                      ToDictionary(pair => pair.Key, pair => pair.Value);

//                foreach (var entry in StemWordDictionaryOrderedByWeight)
//                {
//                    file.WriteLine(entry.Key.GetRootAndVariantsAndAllFrequenciesStringRepresentation());
//                }
//                file.Close(); //in MSN example it is missing.                        
//            }

//        }

//        public void WriteOutGlobalFileUsingTopVariant()
//        {
//            StreamWriter file;
//            using (file = new StreamWriter(FolderPath + Constants.PoetAllPoemsSentencesTopVariantWordsFileName))
//            {
//                foreach (string sentenceWithStemWords in _allSentencesWithTopVariantWords)
//                {
//                    file.WriteLine(sentenceWithStemWords);
//                }
//                file.Close();
//            }
//        }

//        public void CreateDictionary() //Create a temp dictionary that has list
//        {
//            _tempvocabularyDictionary = new Dictionary<StemWord, int>();

//            foreach (var pair in _vocabularyDictionary)
//            {
//                StemWord _temp = pair.Key;
//                _temp.MarginalProbabilityUsingRfCount = (decimal)pair.Key.Frequency / _totalNumberOfWords;
//                _tempvocabularyDictionary.Add(_temp, pair.Value);
//            }

//            _vocabularyDictionary = new Dictionary<StemWord, int>();
//            _vocabularyDictionary = _tempvocabularyDictionary;
//            _tempvocabularyDictionary = null;
//        }

//        //Since the dictionary is created here, it is only fit to update it here
//        /// <summary>
//        /// Not only should I remove the words that have been eliminated. I have to update
//        /// the Key.ID value to reflect the new counter.
//        /// </summary>
//        /// <param name="removedWordsIdList"></param>
//        public void ReduceRegularDictionary(List<int> removedWordsIdList) //Create a temp dictionary that has list
//        {
//            //_vocabularyDictionary = new Dictionary<StemWord, int>();
//            //_vocabularyDictionary = _stemWordDictionaryOrderedByWeight;
//            Dictionary<StemWord, int> _tmpvocabularyDictionary = new Dictionary<StemWord, int>();

//            //udpate _totalNumberOfWords
//            //_totalNumberOfWords -= removedWordsIdList.Count;
//            //List<int> counter = new List<int>();
//            //List<int> negativeCounter = new List<int>();
//            var counter = 0;
//            int numberOfWordsToRemoveFromTotalWords = 0;

//            //foreach (var pair in _vocabularyDictionary)
//            foreach (var pair in StemWordDictionaryOrderedByWeight)
//            {
//                if (!removedWordsIdList.Contains(pair.Key.Id))
//                {
//                    counter++;
//                    StemWord _temp = pair.Key;
//                    _temp.Id = counter;
//                    //_temp.MarginalProbabilityUsingRfCount = (decimal)pair.Key.Frequency / _totalNumberOfWords;
//                    _temp.MarginalProbability = -1;//pair.Key.Frequency / denominator;
//                    _temp.MarginalProbabilityUsingRfCount = -1;
//                    _tmpvocabularyDictionary.Add(_temp, pair.Value);
//                }
//                else
//                {
//                    numberOfWordsToRemoveFromTotalWords += pair.Key.Frequency;
//                }

//                //if (removedWordsIdList.Contains(pair.Key.Id))
//                //{
//                //    //negativeCounter.Add(pair.Key.Id);
//                //    //negativeCounter.Sort();
//                //}
//                //else
//                //{
//                //    //counter.Add(pair.Key.Id);
//                //    //counter.Sort();
//                //    StemWord _temp = pair.Key;
//                //    //_temp.Id = counter;
//                //    _temp.MarginalProbabilityUsingRfCount = (decimal)pair.Key.Frequency / _totalNumberOfWords;
//                //    _temp.MarginalProbability = -1;
//                //    _tmpvocabularyDictionary.Add(pair.Key, pair.Value);
//                //}
//            }
//            _vocabularyDictionary = new Dictionary<StemWord, int>();
//            _vocabularyDictionary = _tmpvocabularyDictionary;
//            _tmpvocabularyDictionary = null;

//            ////finding - difference - between - two - dictionaries
//            //var diff = _stemWordDictionaryOrderedByWeight.Except(_vocabularyDictionary).Concat(_vocabularyDictionary.Except(_stemWordDictionaryOrderedByWeight));
//        }

//        /// <summary>
//        /// Create a list of StemWord where the index of each word in the list
//        /// corresponds to the orderID of that word, and each StemWord contains 
//        /// the frequency of occurence value which is the key in the dictionary and
//        /// total sum of the frequency of occurence of each of its variation words.
//        /// </summary>
//        public void CreateStemWordsMatrix()
//        {
//            //The ordering id of the StemWord is their index in the sortedVocabulary
//            //This broke because the number of words is too small and I was doing reduction of node words with value 1
//            //I removed the need to reduce words based on the 'connection/weight/occurrence' of the word 
//            //in the settings simulation file and the code worked. Should/will revise later on.

//            var idCounter = 1;
//            foreach (var entry in StemWordDictionaryOrderedByWeight)
//            {
//                var tempStemWord = entry.Key;
//                tempStemWord.Id = idCounter;
//                idCounter++;
//                StemWords.Add(tempStemWord);
//                StemWordsString.Add(tempStemWord.Root);
//                //StemWords.Add(entry.Key);
//            }

//            _stemWordMatrix = MatrixOperations.CreateAndInitializeIntegerMatrix(StemWordDictionaryOrderedByWeight.Count + 1,
//                StemWordDictionaryOrderedByWeight.Count + 1);
//        }

//        public void PrintWordListToFile(List<StemWord> horizonWordList, StreamWriter file)
//        {
//            try
//            {
//                foreach (var StemWord in horizonWordList)
//                {
//                    //file.Write(StemWord.OriginalName + " ");
//                    file.Write(StemWord.Root + " ");
//                }
//                file.WriteLine();
//            }
//            catch (Exception e)
//            {
//                Console.WriteLine(e);
//                throw;
//            }
//        }

//        public List<string> RecursiveTextProcessingAlgorithm(int horizonSize, bool testSimulation, List<string> allSentencesWithStemWords)
//        {
//            int debugCounter = 0;
//            StreamWriter file = null;
//            if (!testSimulation)
//            {
//                try
//                {
//                    file = new StreamWriter(FolderPath + Constants.PoetAllPoemsHorizonAlgorithmSentencesFileName);
//                }
//                catch (Exception e)
//                {
//                    throw new Exception("Could not write file: " + FolderPath + Constants.PoetAllPoemsHorizonAlgorithmSentencesFileName + ". " + e.Message);
//                }
//            }

//            foreach (var sentenceLineString in allSentencesWithStemWords)
//            {
//                int start = 0;
//                debugCounter++;
//                string[] stemWords = sentenceLineString.Split(' ');

//                if (stemWords.Length <= 0) //=> empty sentence
//                    continue; //The continue statement passes control to the next iteration of the enclosing while, do, for, or foreach statement in which it appears.

//                int end = start + stemWords.Length;
//                //int otherEnd = 0;

//                List<StemWord> horizonWordList;
//                HorizonAlgorithm horizon;

//                while (start <= end)
//                {
//                    horizonWordList = new List<StemWord>();

//                    //The code divides the sentences into 2 types. Those where the length of the sentence is > the horizon and those that are smaller or equal.
//                    //While sentence is still larger than the horizon
//                    if ((horizonSize + start) <= end)
//                    {
//                        for (var i = start; i < start + horizonSize; i++)
//                        {
//                            if (stemWords[i] != "")
//                                horizonWordList.Add(ReturnStemWord(stemWords[i]));
//                        }

//                        //print out the value of the horizonWordList which is the horizon sentence 
//                        if (!testSimulation)
//                            PrintWordListToFile(horizonWordList, file);

//                        //add to _frameDictionary
//                        var frameSize = horizonWordList.Count;
//                        if (_frameCounterDictionary.ContainsKey(frameSize))
//                            _frameCounterDictionary[frameSize] = _frameCounterDictionary[frameSize] + 1;
//                        else
//                            _frameCounterDictionary.Add(frameSize, 1);

//                        //here is a potential bug
//                        if (horizonWordList.Count < 1)
//                        {
//                            var debugCounterInt = debugCounter;
//                            throw new Exception(String.Format("{0} Is the location of the bug.", debugCounterInt));
//                        }

//                        horizon = new HorizonAlgorithm(horizonSize, horizonWordList);
//                        List<Tuple<int, int>> matrixUpdate = horizon.UpdateMatrix();

//                        Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
//                                                              horizonWordList.Count - 1;

//                        foreach (Tuple<int, int> t in matrixUpdate)
//                        {
//                            _stemWordMatrix[t.Item1][t.Item2] = _stemWordMatrix[t.Item1][t.Item2] + 1;
//                            _stemWordMatrix[t.Item2][t.Item1] = _stemWordMatrix[t.Item1][t.Item2]; //added
//                        }
//                    }
//                    //When sentence is smaller than the  horizon
//                    else
//                    {
//                        int newHorizonSize = end - start;
//                        for (int i = start; i < end; i++)
//                            horizonWordList.Add(ReturnStemWord(stemWords[i]));

//                        //The horizonWordList is the list of remaining words. If Count > 1, means last word not to be counted in pairs (but frames are still there?)
//                        if (horizonWordList.Count > 1) //In case you DON'T want the final word to be a pair on its own!
//                                                       //if (horizonWordList.Count > 0)  //In case you DO want the final word to be a pair on its own!
//                        {
//                            Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
//                                                                  horizonWordList.Count - 1;

//                            //print out the value of the horizonWordList which is the horizon sentence 
//                            if (!testSimulation)
//                                PrintWordListToFile(horizonWordList, file);

//                            //add to _frameDictionary
//                            var frameSize = horizonWordList.Count;
//                            if (_frameCounterDictionary.ContainsKey(frameSize))
//                                _frameCounterDictionary[frameSize] = _frameCounterDictionary[frameSize] + 1;
//                            else
//                                _frameCounterDictionary.Add(frameSize, 1);

//                            horizon = new HorizonAlgorithm(newHorizonSize, horizonWordList);
//                            List<Tuple<int, int>> matrixUpdate = horizon.UpdateMatrix();

//                            foreach (Tuple<int, int> t in matrixUpdate)
//                            {
//                                _stemWordMatrix[t.Item1][t.Item2] = _stemWordMatrix[t.Item1][t.Item2] + 1;
//                            }
//                        }
//                    }
//                    start++;
//                }
//            }
//            file.Close();
//            return allSentencesWithStemWords;
//        }

//        public List<string> RecursiveTextProcessingAlgorithmLowesSingleWindow(int horizonSize, bool testSimulation, List<string> allSentencesWithStemWords)
//        {
//            int debugCounter = 0;
//            StreamWriter file = null;
//            if (!testSimulation)
//            {
//                try
//                {
//                    file = new StreamWriter(FolderPath + Constants.PoetAllPoemsHorizonAlgorithmSentencesFileName);
//                }
//                catch (Exception e)
//                {
//                    throw new Exception("Could not write file: " + FolderPath + Constants.PoetAllPoemsHorizonAlgorithmSentencesFileName + ". " + e.Message);
//                }
//            }

//            foreach (var sentenceLineString in allSentencesWithStemWords)
//            {
//                int start = 0;
//                debugCounter++;
//                string[] stemWords = sentenceLineString.Split(' ');

//                if (stemWords.Length <= 0) //=> empty sentence
//                    continue;  //The continue statement passes control to the next iteration of the enclosing while, do, for, 
//                               //or foreach statement in which it appears.

//                int end = start + stemWords.Length;
//                //int otherEnd = 0;

//                List<StemWord> lowesWindowWordList;
//                LowesSingleWindowAlgorithm lowesSingleWindow;

//                while (start <= end)
//                {
//                    lowesWindowWordList = new List<StemWord>();

//                    //While sentence is still larger than the horizon
//                    if ((horizonSize + start) <= end)
//                    {
//                        for (var i = start; i < start + horizonSize; i++)
//                        {
//                            if (stemWords[i] != "")
//                                lowesWindowWordList.Add(ReturnStemWord(stemWords[i]));
//                        }

//                        //print out the value of the horizonWordList which is the horizon sentence 
//                        if (!testSimulation)
//                            PrintWordListToFile(lowesWindowWordList, file);

//                        //add to _frameDictionary
//                        var frameSize = lowesWindowWordList.Count;
//                        if (_frameCounterDictionary.ContainsKey(frameSize))
//                            _frameCounterDictionary[frameSize] = _frameCounterDictionary[frameSize] + 1;
//                        else
//                            _frameCounterDictionary.Add(frameSize, 1);

//                        //here is a potential bug
//                        if (lowesWindowWordList.Count < 1)
//                        {
//                            var debugCounterInt = debugCounter;
//                            throw new Exception(String.Format("{0} Is the location of the bug.", debugCounterInt));
//                        }

//                        lowesSingleWindow = new LowesSingleWindowAlgorithm(horizonSize, lowesWindowWordList, true);

//                        List<Tuple<int, int>> matrixUpdate = lowesSingleWindow.UpdateMatrixCountsAll();

//                        Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
//                                                              lowesWindowWordList.Count - 1;

//                        Helper.LowesTotalNumberOfWindows = Helper.LowesTotalNumberOfWindows + horizonSize - 1;
//                        //Helper.LowesTotalNumberOfWindows = Helper.LowesTotalNumberOfWindows + lowesWindowWordList.Count;


//                        //here is an issue/bug..
//                        foreach (Tuple<int, int> t in matrixUpdate)
//                        {
//                            _stemWordMatrix[t.Item1][t.Item2] = _stemWordMatrix[t.Item1][t.Item2] + 1;
//                            //_StemWordMatrix[t.Item2][t.Item1] = _StemWordMatrix[t.Item2][t.Item1] + 1;//_StemWordMatrix[t.Item1][t.Item2]; //added
//                        }
//                    }
//                    //When sentence is smaller than the  horizon
//                    else
//                    {
//                        int newHorizonSize = end - start;
//                        for (int i = start; i < end; i++)
//                            lowesWindowWordList.Add(ReturnStemWord(stemWords[i]));

//                        //The horizonWordList is the list of remaining words. If Count > 1, means last word not to be counted in pairs (but frames are still there?)
//                        if (lowesWindowWordList.Count > 1) //In case you DON'T want the final word to be a pair on its own!
//                                                           //if (horizonWordList.Count > 0)  //In case you DO want the final word to be a pair on its own!
//                        {
//                            Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
//                                                                  lowesWindowWordList.Count - 1;

//                            Helper.LowesTotalNumberOfWindows = Helper.LowesTotalNumberOfWindows + newHorizonSize - 1;

//                            //print out the value of the horizonWordList which is the horizon sentence 
//                            if (!testSimulation)
//                                PrintWordListToFile(lowesWindowWordList, file);

//                            //add to _frameDictionary
//                            var frameSize = lowesWindowWordList.Count;
//                            if (_frameCounterDictionary.ContainsKey(frameSize))
//                                _frameCounterDictionary[frameSize] = _frameCounterDictionary[frameSize] + 1;
//                            else
//                                _frameCounterDictionary.Add(frameSize, 1);

//                            lowesSingleWindow = new LowesSingleWindowAlgorithm(newHorizonSize, lowesWindowWordList, true);
//                            List<Tuple<int, int>> matrixUpdate = lowesSingleWindow.UpdateMatrixCountsAll();

//                            foreach (Tuple<int, int> t in matrixUpdate)
//                            {
//                                _stemWordMatrix[t.Item1][t.Item2] = _stemWordMatrix[t.Item1][t.Item2] + 1;
//                            }
//                        }
//                    }
//                    start++;
//                }
//            }
//            file.Close();
//            return allSentencesWithStemWords;
//        }

//        //Bug starts here while adding the non-stemming approach
//        public List<string> RecursiveTextProcessingAlgorithmLowesDoubleWindow(
//            int horizonSize,
//            bool testSimulation,
//            List<string> allSentencesWithStemWords)
//        {
//            horizonSize = horizonSize / 2 + 1;
//            //int debugCounter = 0;
//            StreamWriter file = null;
//            if (!testSimulation)
//            {
//                try
//                {
//                    file = new StreamWriter(FolderPath + Constants.PoetAllPoemsHorizonAlgorithmSentencesFileName);
//                }
//                catch (Exception e)
//                {
//                    throw new Exception("Could not write file: " + FolderPath + Constants.PoetAllPoemsHorizonAlgorithmSentencesFileName + ". " + e.Message);
//                }
//            }

//            LowesSingleWindowAlgorithm horizon;

//            foreach (var sentenceLineString in allSentencesWithStemWords)
//            {
//                int center = 0;
//                int end = 0;

//                //debugCounter++;
//                string[] stemWords = sentenceLineString.Split(' ');

//                if (stemWords.Length <= 0)
//                    continue;

//                end = center + stemWords.Length;

//                List<StemWord> horizonWordList;
//                try
//                {
//                    while (center <= end)
//                    {
//                        horizonWordList = new List<StemWord>();

//                        //LowesSingleWindowAlgorithm horizon;

//                        //The code divides the sentences into 2 types. 
//                        //1. Those where the length of the sentence is > the horizon, and 
//                        //2. those that are smaller or equal.

//                        //While sentence is still larger than the horizon
//                        #region Right Horizon
//                        if ((horizonSize + center) <= end)
//                        {
//                            for (var i = center; i < center + horizonSize; i++)
//                            {
//                                if (stemWords[i] != "")
//                                    horizonWordList.Add(ReturnStemWord(stemWords[i]));
//                            }
//                            //print out the value of the horizonWordList which is the horizon sentence 
//                            try
//                            {
//                                if (!testSimulation)
//                                    PrintWordListToFile(horizonWordList, file);
//                            }
//                            catch (Exception e)
//                            {
//                                Console.WriteLine(e);
//                                throw;
//                            }

//                            //add to _frameDictionary
//                            var frameSize = horizonWordList.Count;
//                            if (_frameCounterDictionary.ContainsKey(frameSize))
//                                _frameCounterDictionary[frameSize] = _frameCounterDictionary[frameSize] + 1;
//                            else
//                                _frameCounterDictionary.Add(frameSize, 1);


//                            horizon = new LowesSingleWindowAlgorithm(horizonSize, horizonWordList, true);
//                            List<Tuple<int, int>> matrixUpdate = horizon.UpdateMatrixCountsOneDirection();

//                            Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
//                                                                  horizonWordList.Count - 1;
//                            Helper.LowesTotalNumberOfWindows = Helper.LowesTotalNumberOfWindows +
//                                                                horizonSize - 1;
//                            foreach (Tuple<int, int> t in matrixUpdate)
//                            {
//                                _stemWordMatrix[t.Item1][t.Item2] = _stemWordMatrix[t.Item1][t.Item2] + 1;
//                                //_StemWordMatrix[t.Item2][t.Item1] = _StemWordMatrix[t.Item1][t.Item2]; //added
//                            }
//                        }
//                        //When sentence is smaller than the  horizon
//                        else
//                        {
//                            int newHorizonSize = end - center;
//                            for (int i = center; i < end; i++)
//                                horizonWordList.Add(ReturnStemWord(stemWords[i]));

//                            //The horizonWordList is the list of remaining words. If Count > 1, means last word not to be counted in pairs (but frames are still there?)
//                            if (horizonWordList.Count > 1) //In case you DON'T want the final word to be a pair on its own!
//                                                           //if (horizonWordList.Count > 0)  //In case you DO want the final word to be a pair on its own!
//                            {
//                                Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
//                                                                      horizonWordList.Count - 1;
//                                Helper.LowesTotalNumberOfWindows = Helper.LowesTotalNumberOfWindows + newHorizonSize - 1;

//                                //print out the value of the horizonWordList which is the horizon sentence 
//                                if (!testSimulation)
//                                    PrintWordListToFile(horizonWordList, file);

//                                //add to _frameDictionary
//                                var frameSize = horizonWordList.Count;
//                                if (_frameCounterDictionary.ContainsKey(frameSize))
//                                    _frameCounterDictionary[frameSize] = _frameCounterDictionary[frameSize] + 1;
//                                else
//                                    _frameCounterDictionary.Add(frameSize, 1);

//                                horizon = new LowesSingleWindowAlgorithm(newHorizonSize, horizonWordList, true);
//                                List<Tuple<int, int>> matrixUpdate = horizon.UpdateMatrixCountsOneDirection();

//                                foreach (Tuple<int, int> t in matrixUpdate)
//                                {
//                                    _stemWordMatrix[t.Item1][t.Item2] = _stemWordMatrix[t.Item1][t.Item2] + 1;
//                                }
//                            }
//                        }
//                        center++;
//                        #endregion
//                    }
//                }
//                catch (Exception e)
//                {
//                    Console.WriteLine(e);
//                    throw;
//                }

//                center = 0;
//                stemWords = sentenceLineString.Split(' ');
//                if (stemWords.Length <= 0) //=> empty sentence
//                    continue; //The continue statement passes control to the next iteration of the enclosing while, do, for, or foreach statement in which it appears.
//                Array.Reverse(stemWords);
//                end = center + stemWords.Length;

//                //This variable is designed so that only the first iteration of the SingleWindowLowes counts the ReadingFrame and the subsequent ones do not.
//                //That's because of how the SingleWindowLowes does the counting and we don't want to over-count.
//                bool countFrames = true;

//                try
//                {
//                    while (center <= end)
//                    {
//                        horizonWordList = new List<StemWord>();

//                        //The code divides the sentences into 2 types. Those where the length of the sentence is > the horizon and those that are smaller or equal.
//                        //While sentence is still larger than the horizon
//                        #region Left Horizon
//                        if ((horizonSize + center) <= end)
//                        {
//                            for (var i = center; i < center + horizonSize; i++)
//                            {
//                                if (stemWords[i] != "")
//                                    horizonWordList.Add(ReturnStemWord(stemWords[i]));
//                            }
//                            //print out the value of the horizonWordList which is the horizon sentence 
//                            if (!testSimulation)
//                                PrintWordListToFile(horizonWordList, file);

//                            //add to _frameDictionary
//                            var frameSize = horizonWordList.Count;
//                            if (_frameCounterDictionary.ContainsKey(frameSize))
//                                _frameCounterDictionary[frameSize] = _frameCounterDictionary[frameSize] + 1;
//                            else
//                                _frameCounterDictionary.Add(frameSize, 1);


//                            horizon = new LowesSingleWindowAlgorithm(horizonSize, horizonWordList, countFrames);
//                            //here is where I reset the countFrames bool value so that the ReadingFrames do not get added.
//                            if (countFrames)
//                                countFrames = false;
//                            List<Tuple<int, int>> matrixUpdate = horizon.UpdateMatrixCountsOneDirection();

//                            Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
//                                                                  horizonWordList.Count - 1;
//                            Helper.LowesTotalNumberOfWindows = Helper.LowesTotalNumberOfWindows + horizonSize - 1;
//                            foreach (Tuple<int, int> t in matrixUpdate)
//                            {
//                                _stemWordMatrix[t.Item1][t.Item2] = _stemWordMatrix[t.Item1][t.Item2] + 1;
//                                //_StemWordMatrix[t.Item2][t.Item1] = _StemWordMatrix[t.Item1][t.Item2]; //added
//                            }

//                        }
//                        //When sentence is smaller than the horizon
//                        else
//                        {
//                            int newHorizonSize = end - center;
//                            for (int i = center; i < end; i++)
//                                horizonWordList.Add(ReturnStemWord(stemWords[i]));

//                            //The horizonWordList is the list of remaining words. If Count > 1, means last word not to be counted in pairs (but frames are still there?)
//                            if (horizonWordList.Count > 1) //In case you DON'T want the final word to be a pair on its own!
//                                                           //if (horizonWordList.Count > 0)  //In case you DO want the final word to be a pair on its own!
//                            {
//                                Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
//                                                                      horizonWordList.Count - 1;
//                                Helper.LowesTotalNumberOfWindows = Helper.LowesTotalNumberOfWindows + newHorizonSize - 1;
//                                //print out the value of the horizonWordList which is the horizon sentence 
//                                if (!testSimulation)
//                                    PrintWordListToFile(horizonWordList, file);

//                                //add to _frameDictionary
//                                var frameSize = horizonWordList.Count;
//                                if (_frameCounterDictionary.ContainsKey(frameSize))
//                                    _frameCounterDictionary[frameSize] = _frameCounterDictionary[frameSize] + 1;
//                                else
//                                    _frameCounterDictionary.Add(frameSize, 1);

//                                horizon = new LowesSingleWindowAlgorithm(newHorizonSize, horizonWordList, false);
//                                List<Tuple<int, int>> matrixUpdate = horizon.UpdateMatrixCountsOneDirection();

//                                foreach (Tuple<int, int> t in matrixUpdate)
//                                {
//                                    _stemWordMatrix[t.Item1][t.Item2] = _stemWordMatrix[t.Item1][t.Item2] + 1;
//                                }
//                            }
//                        }
//                        center++;
//                        #endregion
//                    }
//                }
//                catch (Exception e)
//                {
//                    Console.WriteLine(e);
//                    throw;
//                }
//            }
//            if (file != null)
//                file.Close();

//            var actualReadingFrames = Helper.GetReadingFrames() / 2 + 1;
//            Helper.SetReadingFrames(actualReadingFrames);
//            return allSentencesWithStemWords;
//        }

//        public void UpdateStemWordsMarginalProbabilityFromJointProbabilityMatrix(Decimal denominator, string fileName)
//        {
//            //Update the stem words with the new marginal probability based on the RFs
//            _tempvocabularyDictionary = new Dictionary<StemWord, int>();

//            //The marginal probability is the total number of times that word appeared in a pair, over the total number of possible pairs.
//            //Decimal totalNumberOfPairsWRTFrames = Helper.TotalNumberOfPairsWrtFrames; //replaced with denominator now (11.5.2015)
//            decimal totalValue = 0;

//            //TextWriter tw = new StreamWriter(FolderPath + Constants.PoetMarginalProbability);
//            TextWriter tw = new StreamWriter(FolderPath + fileName);
//            try
//            {
//                tw.WriteLine("{0}: {1} / {2} = {3}.", "Word", "Sum of Word-Pairs", "Total Number of Pairs", "Value");
//                tw.WriteLine();
//                foreach (var pair in StemWordDictionaryOrderedByWeight)
//                //foreach (var pair in _vocabularyDictionary)
//                {
//                    var stemWord = pair.Key;
//                    ////StemWord.MarginalProbabilityUsingRFCount = (decimal)pair.Key.Frequency / _frameCounter;
//                    //StemWord.MarginalProbability = (decimal)pair.Key.Frequency / totalNumberOfPairsWRTFrames;

//                    //Get the total sum of all the occurrences of this word with every other word 
//                    var sum = _stemWordMatrix[pair.Key.Id].Sum();
//                    totalValue += sum;
//                    var result = (decimal)sum / denominator;// totalNumberOfPairsWRTFrames;
//                    stemWord.MarginalProbability = result;
//                    //tw.WriteLine(String.Format("{0}: {1} / {2} = {3}.", pair.Key.Root, sum.ToString(), totalNumberOfPairsWRTFrames.ToString(), result.ToString()));
//                    //tw.WriteLine(String.Format("{0}: {1} / {2} = {3}.", pair.Key.Root, sum.ToString(), denominator.ToString(), result.ToString()));
//                    tw.WriteLine("{0}: {1}/{2} = {3,0:0.####}.",
//                        pair.Key.Root.PadRight(5),
//                        sum.ToString().PadLeft(5),
//                        denominator.ToString(CultureInfo.InvariantCulture).PadRight(5),
//                        result);

//                    _tempvocabularyDictionary.Add(stemWord, pair.Value);
//                }
//                tw.Close();

//            }
//            finally
//            {
//                tw.Dispose();
//            }

//            //Added because in the changes, StemWordsCoOccurenceMatrixIntValue was zero it seems.
//            Helper.StemWordsCoOccurenceMatrixIntValue = _stemWordMatrix;

//        }
//        public decimal[,] CalculateAndWriteOutTheConditionalProbabilityForClusteredAgents(IEnumerable<StemWord> stemWords)
//        {
//            return Helper.WriteConditionalProbabilityForClusteredAgents(
//                FolderPath + Constants.ClusteredAgentsConditionalProbabilityMatrix,
//                _stemWordsTopVariantString,
//                stemWords);
//        }

//        public void CalculateAndWriteOutTheConditionalProbability()
//        {
//            Helper.write_sparse_decimal_conditionalProbability_gephi(
//                StemWordDictionaryOrderedByWeight.Count,
//                _stemWordMatrix,
//                FolderPath + Constants.PoetConditionalProbabilityMatrixGephi,
//                FolderPath + Constants.PoetConditionalProbabilityMatrix,
//                FolderPath + Constants.PoetConditionalProbabilityMatrixDistributionPlot,
//                _stemWordsTopVariantString);
//        }
//        public void CalculateAndWriteOutTheCorrelationCoefficient()
//        {
//            Helper.write_sparse_decimal_correlationcoefficient_gephi(
//                StemWordDictionaryOrderedByWeight.Count,
//                _stemWordMatrix,
//                FolderPath + Constants.PoetCorrelationCoefficientMatrixGephi,
//                FolderPath + Constants.PoetCorrelationCoefficientMatrix,
//                FolderPath + Constants.PoetCorrelationCoefficientMatrixDistributionPlot,
//                _stemWordsTopVariantString);
//        }
//        public void CalculateAndWriteOutThePointWiseMutualInformation()
//        {
//            Helper.write_sparse_decimal_pointwisemutualinformation_gephi(
//                StemWordDictionaryOrderedByWeight.Count,
//                _stemWordMatrix,
//                FolderPath + Constants.PoetPMIMatrixGephi,
//                FolderPath + Constants.PoetPMIMatrix,
//                FolderPath + Constants.PoetPMIMatrixDistributionPlot,
//                _stemWordsTopVariantString);
//        }
//        public void CalculateAndWriteOutTheLogOddsRatio()
//        {
//            Helper.write_sparse_decimal_logoddsratio_gephi(
//                StemWordDictionaryOrderedByWeight.Count,
//                _stemWordMatrix,
//                FolderPath + Constants.PoetLogOddsRatioMatrixGephi,
//                FolderPath + Constants.PoetLogOddsRatioMatrix,
//                FolderPath + Constants.PoetLogOddsRatioMatrixDistributionPlot,
//                _stemWordsTopVariantString, Horizon);
//        }

//        public StemWord ReturnStemWord(string name)
//        {
//            return StemWords.FirstOrDefault(w => w.Root == name);
//        }
//        public void MoveResultFilesToCorrespondingFoldersAndCleanUp()
//        {
//            //final clean up: put all .txt and .html files into a folder called corpus
//            var corpusFolder = FolderPath + @"\corpus";
//            if (!Directory.Exists(corpusFolder))
//                Directory.CreateDirectory(corpusFolder);

//            var allFiles = Directory.GetFiles(FolderPath, Constants.FileExtensionTxt)
//                .Where(w => !w.Contains("DocumentsFile.txt")).ToArray();

//            string[] txtFiles;
//            //remove the files that make up the corpus from txtFiles: _poemsStringArray
//            if (_poemsStringArray != null && _poemsStringArray.Length > 0)
//                txtFiles = allFiles.Except(_poemsStringArray).ToArray();
//            else
//                txtFiles = allFiles;
//            try
//            {
//                foreach (var item in txtFiles)
//                {
//                    File.Move(item, Path.Combine(corpusFolder, Path.GetFileName(item)));
//                }
//            }
//            catch (Exception e)
//            {
//                throw new Exception(e.Message);
//            }

//            //put all the .csv and .txt files into a folder called statistics
//            string statisticsFolder = FolderPath + @"\statistics_" + DataSourceName;
//            const string fileExtensionCsv = "*.csv";
//            const string fileExtensionDat = "*_stat_*.txt";

//            if (!Directory.Exists(statisticsFolder))
//                Directory.CreateDirectory(statisticsFolder);

//            txtFiles = Directory.GetFiles(FolderPath, fileExtensionCsv);
//            try
//            {
//                foreach (var item in txtFiles)
//                {
//                    File.Move(item, Path.Combine(statisticsFolder, Path.GetFileName(item)));
//                }
//            }
//            catch (Exception excep)
//            {
//                var error = "There is an error: " + excep.Message;
//            }


//            txtFiles = Directory.GetFiles(FolderPath, fileExtensionDat);
//            try
//            {
//                foreach (var item in txtFiles)
//                {
//                    File.Move(item, Path.Combine(statisticsFolder, Path.GetFileName(item)));
//                }
//            }
//            catch (Exception excep)
//            {
//                throw excep.InnerException != null
//                    ? new Exception(excep.InnerException.ToString())
//                    : new Exception(excep.Message);
//            }
//        }
//    }
//}