﻿using System.Collections.Generic;
using System.Linq;
using SDParser.Structures;

namespace SDStatistics.TextCorpus.DataSources
{
    public static class StemWordsManager
    {
        public static List<StemWord> StemWords { get; set; }
        public static StemWord ReturnStemWord(string name)
        {
            return StemWords.First(w => w.Root == name);
        }
    }
}
