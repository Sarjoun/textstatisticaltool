﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using SDParser.Enums;
using SDParser.Notifications;
using SDParser.Primitives;
using SDParser.Structures;
using SDStatistics.Enums;
using SDStatistics.Extensions;
using SDStatistics.FileOperations;
using SDStatistics.LowesTextProcessingAlgorithms;
using SDStatistics.TextCorpus.Helpers;
using SDStatistics.TextCorpus.OldCode;
using SDStatistics.TextCorpus.SentenceReadingAlgorithms;
using SDStatistics.TextCorpus.SentenceSplitters;
using SDStatistics.TextOperations;

namespace SDStatistics.TextCorpus.DataSources
{
    public class DataSourceTextFormat
    {
        //Format used: word-frequency-probability(value) 
        public List<Tuple<string, string, string>> ExportDictionaryToNodeXL { get; set; }
        public Dictionary<string, StemWord> FinalDictionary => _vocabularyDictionary;
        public IEnumerable<StemWord> UpdatedClusteredStemWords { get; set; }
        public string DataSourceName { get; set; }
        public string FolderPath;
        public List<string> TopFrequentWords = new List<string>();

        //Since we need to calculate (sum of all frame sizes in the corpus) - (number of frames in the corpus). 
        //then this dictionary uses for a key the frame size and for value the number of that frames in the corpus
        private Dictionary<int, int> _frameCounterDictionary = new Dictionary<int, int>();
        private readonly string[] _documentsFileNames;
        private List<StemWord> StemWords { get; set; }
        private int[][] _stemWordMatrix;
        private Dictionary<string, StemWord> _vocabularyDictionary;
        private int _attemptsToMoveFolder = 0;

        public DataSourceTextFormat(
            string[] documentsFileNames,
            IEnumerable<StemWord> clusteredStemWords,
            string folderPath,
            string dataSourceName,
            int horizonSize,
            IncludeTitleInSetup includeTitleInSetup,
            TextReaderAlgorithmEnum algorithm,
            SentenceSetUp sentenceSetup,
            bool useStemming,
            bool calculateAllAvailableStatistics,
            DataFileFormat sourceDataFileFormat,
            bool useLowWordFrequencyReduction,
            int lowWordFrequencyValue,
            bool useNetworkReduction,
            int networkReductionValue,
            WordStreamFileName wordStreamFileName)
        {
            try
            {
                #region before network reduction
                ExportDictionaryToNodeXL = new List<Tuple<string, string, string>>();
                FolderPath = folderPath;
                DataSourceName = dataSourceName;

                _documentsFileNames = documentsFileNames;

                ResetVariables();

                if (sourceDataFileFormat != DataFileFormat.TextFormat)
                    throw new Exception("Currently ALAN (and IONA) can only process text data sources. \r\n" +
                                        "ALAN has the HTML processing component disabled.");

                _vocabularyDictionary = ProcessFileNamesAndReturnDictionary
                    .MakeWordStreamAndVariantWordsReturnDictionaryOrderedAlphabeticallyFromListOfFileNames(
                            _documentsFileNames, wordStreamFileName, false, includeTitleInSetup)
                    .Print(FolderPath + @"\Vocabulary_Step1_StemmedWords.txt");

                if (useLowWordFrequencyReduction)
                {
                    _vocabularyDictionary = _vocabularyDictionary
                        .RemoveEntriesIfStemWordFrequencyHasLessOrEqualOccurrence(lowWordFrequencyValue)
                        .Print(FolderPath + @"\Dictionary Step2-v1 RemovedLowFrequencyWords_Alphabetical.txt")
                        .OrderByFrequency()
                        .Print(FolderPath + @"\Dictionary Step2-v2 RemovedLowFrequencyWords_Frequency.txt");
                }

                var extractedSentences = CorpusToSentences
                    .ProcessCorpusToSentences(_documentsFileNames, sentenceSetup)
                    .WriteToFileAsync(FolderPath + @"\CorpusData_Original.txt")
                    .ReplaceAllWordsWithStemRemoveStopWordsRemoveEmptySentences(_vocabularyDictionary)
                    .WriteToFileAsync(useLowWordFrequencyReduction
                        ? FolderPath + @"\CorpusData_StemmedWords-StopWords-LowFrequencyWordsRemoved.txt"
                        : FolderPath + @"\CorpusData_StemmedWords-StopWords.txt");

                //needed to search for recorded StemWords
                StemWordsManager.StemWords = _vocabularyDictionary.Values.ToList();

                var allSentencesWithStemWords = algorithm == TextReaderAlgorithmEnum.LoweSingleWindowAlgorithm
                   ? RecursiveTextProcessingAlgorithmLowesSingleWindow.GoSingleWindow(horizonSize, true, extractedSentences,
                   @"\GlobalPoemsFileHorizonSentences.txt", folderPath, LowesHelpers.CreateStemWordsMatrix(_vocabularyDictionary))
                   : RecursiveTextProcessingAlgorithmLowesDoubleWindow.GoDoubleWindow(horizonSize, true, extractedSentences,
                    @"\GlobalPoemsFileHorizonSentences.txt", folderPath, LowesHelpers.CreateStemWordsMatrix(_vocabularyDictionary));

                Helper.TotalNumberOfPairsWrtFrames = Helper.LowesTotalNumberOfWindows;

                const string sourceMarginalProbability = @"\MarginalProbability.txt";
                _vocabularyDictionary = _vocabularyDictionary
                    .UpdateStemWordMarginalProbabilityUsingStemWordMatrix(
                        algorithm == TextReaderAlgorithmEnum.LoweSingleWindowAlgorithm
                                   ? RecursiveTextProcessingAlgorithmLowesSingleWindow.StemWordMatrix
                                   : RecursiveTextProcessingAlgorithmLowesDoubleWindow.StemWordMatrix,
                        Helper.LowesTotalNumberOfWindows);
                //.PrintMarginalProbability(folderPath + poetMarginalProbability);
                UpdateStemWordsMarginalProbabilityFromJointProbabilityMatrix(
                    Helper.TotalNumberOfPairsWrtFrames, sourceMarginalProbability, algorithm == TextReaderAlgorithmEnum.LoweSingleWindowAlgorithm
                        ? RecursiveTextProcessingAlgorithmLowesSingleWindow.StemWordMatrix
                        : RecursiveTextProcessingAlgorithmLowesDoubleWindow.StemWordMatrix);

                //UpdateStemWordsMarginalProbabilityFromJointProbabilityMatrix(
                //    Helper.LowesTotalNumberOfWindows,
                //    PoetMarginalProbability,
                //    algorithm == TextReaderAlgorithmEnum.LoweSingleWindowAlgorithm
                //               ? RecursiveTextProcessingAlgorithmLowesSingleWindow.StemWordMatrix
                //               : RecursiveTextProcessingAlgorithmLowesDoubleWindow.StemWordMatrix);

                var temp1 = WriteGeneralStatistics.GoAsync(folderPath, _vocabularyDictionary);

                var temp2 = WritePoetJointFrequencyMatrixBeforeReductionForGephi.GoAsync(
                    _vocabularyDictionary.Count,
                    algorithm == TextReaderAlgorithmEnum.LoweSingleWindowAlgorithm
                      ? RecursiveTextProcessingAlgorithmLowesSingleWindow.StemWordMatrix
                      : RecursiveTextProcessingAlgorithmLowesDoubleWindow.StemWordMatrix,
                    FolderPath,
                    _vocabularyDictionary.Select(d => d.Value.Root).ToList());

                /* The task is to identify all the edges with values equal to the [Helper.ReductionValue]
                in the probability matrix (which is not reduced) and then replace those values with 0,
                while adding them to a file for later review. */

                //write out the original matrix file before the edge value reduction
                const string poetJointFrequencyMatrixBeforeReduction = @"/JointFrequencyMatrixBeforeReduction.txt";
                SDParser.FileFunctions.FileWriter.WriteSparseIntArrayToFile(
                    algorithm == TextReaderAlgorithmEnum.LoweSingleWindowAlgorithm
                      ? RecursiveTextProcessingAlgorithmLowesSingleWindow.StemWordMatrix
                      : RecursiveTextProcessingAlgorithmLowesDoubleWindow.StemWordMatrix,
                    FolderPath + poetJointFrequencyMatrixBeforeReduction);
                #endregion
                #region Using network reduction
                if (useNetworkReduction)
                {
                    var poetJointFrequencyMatrixZeroFileName = @"/JointFrequencyMatrixReplacing[" + networkReductionValue + "]ValuesWithZero.txt";

                    var resultMatrix = Helper.write_sparse_int_replace_edgevalue(_vocabularyDictionary.Count,
                                       algorithm == TextReaderAlgorithmEnum.LoweSingleWindowAlgorithm
                                          ? RecursiveTextProcessingAlgorithmLowesSingleWindow.StemWordMatrix
                                          : RecursiveTextProcessingAlgorithmLowesDoubleWindow.StemWordMatrix,
                                        FolderPath + poetJointFrequencyMatrixZeroFileName, networkReductionValue, 0);

                    var poetJointFrequencyMatrixZeroRemovedFileName = @"\JointFrequencyMatrixWithZeroRowsNColumnsRemoved.txt";

                    _stemWordMatrix = Helper.RemoveZeroEntriesFromMatrixThenRemoveZeroColumnsAndRows(
                              _vocabularyDictionary.Count,
                              resultMatrix,
                              FolderPath + poetJointFrequencyMatrixZeroRemovedFileName,
                              _vocabularyDictionary);

                    //It is ok to assign the orderedDictionary to the regular one.
                    _vocabularyDictionary = ReduceRegularDictionary(_vocabularyDictionary, Helper.ListOfWordsToRemove);

                    //Rebuild the _StemWordMatrix !!!
                    var poetMarginalProbabilityReducedFileName = @"\MarginalProbabilityReduced.txt";
                    UpdateStemWordsMarginalProbabilityFromJointProbabilityMatrix(
                        Helper.TotalNumberOfPairsWrtFrames, poetMarginalProbabilityReducedFileName, _stemWordMatrix);

                    //Have to calculate _stemWordDictionaryOrderedByWeight. Generate new ordered_dictionary
                    ReplaceStemWithTopVariantAndWriteOutVocabularyWithStatisticsToFilesAfterReduction();
                }
                #endregion

                try
                {
                    WriteBasicResultsToFiles(_stemWordMatrix);
                }
                catch (Exception e)
                {
                    ShowMessage.ShowErrorMessageAndExit("Error in WriteBasicResultsToFiles().", e);
                }

                //Maybe this should be called earlier
                //This only is needed when ALAN is running in conjunction with IONA in a sense of updating IONA's story graphs.
                //and not as an independent run.
                
                //This means that it is being called from outside of ANAT and while the functionality has been
                //temporarily disabled, I will be restoring it soon. 
                if (clusteredStemWords == null)
                {
                    clusteredStemWords = _vocabularyDictionary.Select(x => x.Value);
                }
                
                clusteredStemWords = NormalizeIONAClusteredStemWordsUsingAlanDictionary(clusteredStemWords);

                var enumerable = clusteredStemWords as StemWord[] ?? clusteredStemWords.ToArray();
                UpdatedClusteredStemWords = enumerable.ToArray();

                Populate_TopVariantDictionary_StemWordsTopVariantString();
                //Update the Ids of the clusteredStemWords with the indices of the probablility matrix,
                //so that later I can correlate between values of the the values between the 
                clusteredStemWords = Helper.UpdateClusteredStemWordsWithIdsCorrespondingToNewProbabilityMatrixIndexWeird(
                    enumerable, _topVariantDictionaryOrderedByWeight);//_vocabularyDictionary);

                var words = clusteredStemWords as StemWord[] ?? clusteredStemWords.ToArray();
                UpdatedClusteredStemWords = words.ToArray();//To be used to update the agents.

                var stemWords = clusteredStemWords as StemWord[] ?? words.ToArray();
                WriteBasicResultsToFilesWithClusteredStemWords_AndUpdateJointFrequencyAndJointProbabilityClusteredStemWords(stemWords);

                Top10StemWordsFromMatrix = _stemWordsTopVariantString.Take(10).ToList();
                SyncedIdsStemWords = stemWords.ToList();

                Helper.SetupProbabilityCalculationsEngine();//Needed before doing any probability calculations.
                //CalculateAndWriteOutTheConditionalProbability();//iffy here .. TODO: fix this issue

                //TODO: Mysterious bug here!
                ConditionalProbabilityClusteredAgentsMatrixValues = 
                    CalculateAndWriteOutTheConditionalProbabilityForClusteredAgents(stemWords);

                //Update MatrixValue with the updated StemWord matrix
                Helper.StemWordsCoOccurenceMatrixIntValue = _stemWordMatrix;

                //CalculateAndWriteOutTheConditionalProbability();//could be removed if unneeded
                if (calculateAllAvailableStatistics)
                {
                    CalculateAndWriteOutTheConditionalProbability();
                    CalculateAndWriteOutTheCorrelationCoefficient();
                    CalculateAndWriteOutThePointWiseMutualInformation();
                    CalculateAndWriteOutTheLogOddsRatio(horizonSize);
                }
                MoveResultFilesToCorrespondingFoldersAndCleanUp();
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit("There is an error in DataSourceTextFormat. ", e);
            }
        }


        public decimal[,] JointFrequencyClusteredAgentsMatrixValues { get; set; }
        public decimal[,] JointProbabilityClusteredAgentsMatrixValues { get; set; }
        public decimal[,] ConditionalProbabilityClusteredAgentsMatrixValues { get; set; }
        public List<string> Top10StemWordsFromMatrix { get; set; }
        public List<StemWord> SyncedIdsStemWords { get; set; }
        public List<string> ProcessHtmlSourceFileAndWriteResultToTxtFile(string poem, SentenceSetUp sentenceSetUp)
        {
            var htmlToText = new HtmlToText();
            //If poem has .htm or .html to it, then convert to html first, otherwise simply read it
            var extension = Path.GetExtension(poem);
            string poemTextVersion;

            if (extension != null && extension.Contains("htm"))
            {
                //Extract text from html and write it into a .txt file with same name.
                poemTextVersion = htmlToText.ConvertHtml2TxtFile(poem);
                //var poemTextVersion = _htmlToText.ConvertHtml2TxtFileSpecial(poem);

                //Remove all \r\n
                poemTextVersion = poemTextVersion.Replace("\r\n", " ");
                poemTextVersion = poemTextVersion.Replace("--", " ");
                poemTextVersion = poemTextVersion.Trim();
            }
            else
            {
                // Read the file as one string.
                var myFile = new StreamReader(poem);
                poemTextVersion = myFile.ReadToEnd();
                myFile.Close();

                //Remove all \r\n
                poemTextVersion = poemTextVersion.Replace("\r\n", " ");
                poemTextVersion = poemTextVersion.Replace("--", " ");
                poemTextVersion = poemTextVersion.Trim();
            }

            switch (sentenceSetUp)
            {
                case SentenceSetUp.NoSplit:
                    return SentenceSplitter.ProcessPoemTextVersionAsOneBigSentence(poemTextVersion);

                case SentenceSetUp.Split:
                    return SentenceSplitter.ProcessPoemTextVersionUsingOrbit(poemTextVersion);

                default:
                    return SentenceSplitter.ProcessPoemTextVersionUsingOrbit(poemTextVersion);
            }
        }

        #region Initializing and resetting variables - should get rid of
        public void ResetVariables()
        {
            _stemWordMatrix = null;
            Helper.OrderedVocabularyDictionary = null;
            Helper.StemWordsCoOccurenceMatrixIntValue = null;
            Helper.ResetReadingFrames();
            Helper.TotalNumberOfPairsWrtFrames = 0;
            Helper.LowesTotalNumberOfWindows = 0;
            TopFrequentWords = new List<string>();
            _vocabularyDictionary = new Dictionary<string, StemWord>();
            StemWords = new List<StemWord>();
            //_documentsFileNames = Directory.GetFiles(_folderPath);
            _frameCounterDictionary = new Dictionary<int, int>();
            _vocabularyDictionary = new Dictionary<string, StemWord>();
        }
        //TODO fix reset variables function
        //public void ResetVariablesAnatFromFile(string poet)
        //{
        //    _stemWordMatrix = null;
        //    //_uniqueWords = 0;
        //    Helper.OrderedVocabularyDictionary = null;
        //    Helper.StemWordsCoOccurenceMatrixIntValue = null;
        //    Helper.ResetReadingFrames();
        //    Helper.TotalNumberOfPairsWrtFrames = 0;
        //    Helper.LowesTotalNumberOfWindows = 0;
        //    TopFrequentWords = new List<string>();
        //    _vocabularyDictionary = new Dictionary<string, StemWord>();
        //    StemWords = new List<StemWord>();
        //    _folderPath = poet;
        //    _documentsFileNames = GetAllTheDocumentsFromTheDocumentsFile(_folderPath);
        //    _frameCounterDictionary = new Dictionary<int, int>();
        //    _vocabularyDictionary = new Dictionary<string, StemWord>();
        //}

        //public void ResetVariablesAnat(string poet, IEnumerable<string> poemsStringArray)
        //{
        //    _stemWordMatrix = null;
        //    _vocabularyDictionary = new Dictionary<string, StemWord>();
        //    TopFrequentWords = new List<string>();
        //    StemWords = new List<StemWord>();
        //    Helper.OrderedVocabularyDictionary = null;
        //    Helper.StemWordsCoOccurenceMatrixIntValue = null;
        //    Helper.ResetReadingFrames();
        //    Helper.TotalNumberOfPairsWrtFrames = 0;
        //    Helper.LowesTotalNumberOfWindows = 0;
        //    _folderPath = poet;
        //    DataSourceName = TextHelper.GetSourceNameFromFile(poet);//should be commented out ? 8-25-2018
        //    _documentsFileNames = poemsStringArray.ToArray();

        //    _frameCounterDictionary = new Dictionary<int, int>();
        //    _vocabularyDictionary = new Dictionary<string, StemWord>();
        //}
        #endregion

        public static string[] GetAllTheDocumentsFromTheDocumentsFile(string fileName)
        {
            var documentsLinks = SDParser.CustomFileParsers.LineByLineParsingFunctions.ReadLines(fileName);
            return documentsLinks.ToArray();
        }

        //TODO: fix this
        private void WriteTheVocabularyWithoutHeaders(Dictionary<string, StemWord> vocabularyDictionary)
        {
            #region Write the vocabulary (regular) without headers

            using (var streamWriter
                = new StreamWriter(FolderPath + Constants.PoetAllPoemsVocabularyStemWordsOldFileName))
            {
                //Using LINQ alphabetically
                foreach (var entry in _vocabularyDictionary)
                {
                    //get the word's version with the highest score  ( get TopVariantWord )
                    var mostPopularStemVersion = entry.Value.TopVariantWord;

                    //streamWriter.WriteLine(entry.Key.Root + " " + entry.Value + " " + string.Format(Constants.Precision, (decimal)entry.Key.MarginalProbabilityUsingRFCount));
                    streamWriter.WriteLine(mostPopularStemVersion + " " + entry.Value + " " +
                                           string.Format(Constants.Precision, entry.Value.MarginalProbability));

                    var newStemWord = entry.Key;

                }
                streamWriter.Close();
            }

            #endregion
        }
        public void ReplaceStemWithTopVariantAndWriteOutVocabularyWithStatisticsToFilesAfterReduction()
        {
            //Write out simple statistics about the corpus 
            using (
                var file = new StreamWriter(FolderPath + Constants.PoetAllPoemsGeneralStatisticsFileNameAfterReduction))
            {
                //the +1,-1 is to fix the display and so for not including the '/' in the dataSource's name
                file.WriteLine("General statistics for " +
                               FolderPath.Substring(FolderPath.LastIndexOf('\\') + 1,
                                   FolderPath.Length - FolderPath.LastIndexOf('\\') - 1));
                file.WriteLine("");
                file.WriteLine("The total number of reading frames is : " + Helper.GetReadingFrames());
                file.WriteLine("The total number of (nonstop) word pairs wrt to reading frames is : " + Helper.TotalNumberOfPairsWrtFrames);
                file.WriteLine("The total number of (stemmed) words is : " + _vocabularyDictionary.Sum(x => x.Value.Frequency));
                file.WriteLine("The total number of (stemmed) vocabulary words is : " + _vocabularyDictionary.Count);
            }
            
            //_vocabularyDictionary.PrintMarginalProbability(FolderPath + @"\VocabularyStemmedWords_Step4_Reduced.txt");

            #region Write the vocabulary (regular) without headers
            //      public static string PoetAllPoemsVocabularyStemWordsAfterReduction = @"/VocabularyStemmedWords_Reduced_step4.txt";
            //using (var streamWriter =
            //        new StreamWriter(FolderPath + Constants.PoetAllPoemsVocabularyStemWordsAfterReduction))
            //{
            //    //Using LINQ alphabetically entry value
            //    _vocabularyDictionary = (from entry in _vocabularyDictionary
            //                                         orderby entry.Value.Frequency descending
            //                                         select entry).
            //        ToDictionary(pair => pair.Key, pair => pair.Value);

            //    _topVariantDictionaryOrderedByWeight = new Dictionary<string, StemWord>();

            //    //Using LINQ alphabetically
            //    foreach (var entry in _vocabularyDictionary)
            //    {
            //        var mostPopularStemVersion = (entry.Value).TopVariantWord;
            //        streamWriter.WriteLine(mostPopularStemVersion + " " + entry.Value.Frequency + " " +
            //                               string.Format(Constants.Precision, entry.Value.MarginalProbability));

            //        var newStemWord = entry.Key;
            //        _topVariantDictionaryOrderedByWeight.Add(newStemWord, entry.Value);
            //        //_stemWordsTopVariantString.Add(mostPopularStemVersion);
            //    }
            //    streamWriter.Close();
            //}

            #endregion
        }

        private IEnumerable<StemWord> NormalizeIONAClusteredStemWordsUsingAlanDictionary(
            IEnumerable<StemWord> stemWordsFromIona)
        {
            StemWords = stemWordsFromIona.Where(stemWord => _vocabularyDictionary.ContainsKey(stemWord.Root)).ToList();
            return StemWords.ToArray();
        }
        
        private Dictionary<string, StemWord> _topVariantDictionaryOrderedByWeight;
        private List<string> _stemWordsTopVariantString;
        private void Populate_TopVariantDictionary_StemWordsTopVariantString()
        {
            _vocabularyDictionary = (from entry in _vocabularyDictionary
                                     orderby entry.Value.Frequency descending
                                     select entry).
                ToDictionary(pair => pair.Key, pair => pair.Value);

            _topVariantDictionaryOrderedByWeight = new Dictionary<string, StemWord>();

            _stemWordsTopVariantString = new List<string>();

            //Using LINQ alphabetically
            foreach (var entry in _vocabularyDictionary)
            {
                var mostPopularStemVersion = (entry.Value).TopVariantWord;
                var newStemWord = entry.Key;
                _topVariantDictionaryOrderedByWeight.Add(newStemWord, entry.Value);
                _stemWordsTopVariantString.Add(mostPopularStemVersion);
            }
        }
        
        public void WriteBasicResultsToFiles(int[][] stemmedWordMatrix)
        {
            //write words' frequencies 
            #region write words' frequencies
            //Helper.write_sparse_int_wordsFrequencies(_vocabularyDictionary.Count, stemmedWordMatrix,
            //    _folderPath + Constants.PoetJointFrequencyMatrix);
            #endregion

            var x = WritePoetJointFrequencyMatrixBeforeReductionForGephi.GoAsync(
               _vocabularyDictionary.Count,
               stemmedWordMatrix,
               FolderPath,
               _vocabularyDictionary.Keys.ToList());
            //_stemWordsTopVariantString);

            //write words' joint probability 
            //TODO FIX
            #region write words' joint probability  
            //_vocabularyDictionary = Helper.write_sparse_decimal_gephi(
            //    StemWordDictionaryOrderedByWeight.Count,
            //    stemmedWordMatrix,
            //    FolderPath + Constants.PoetJointProbabilityMatrixGephi,
            //    FolderPath + Constants.PoetJointProbabilityMatrix,
            //    FolderPath + Constants.PoetJointProbabilityMatrixDistributionPlot,
            //    _stemWordsTopVariantString, _vocabularyDictionary); //_StemWordsString
            #endregion

            //Update the ordered Vocabulary 
            Helper.OrderedVocabularyDictionary = _vocabularyDictionary;
            _vocabularyDictionary.PrintMarginalProbability(FolderPath + @"\VocabularyStemmedWords_Reduced_step4.txt");

            //TODO the next bug location
            //Write the vocabulary (detailed) without headers  
            #region Write the vocabulary (detailed) without headers  
            using (var streamWriter2 = new StreamWriter(FolderPath + Constants.PoetAllPoemsVocabularyStemWordsDetailedFileName))
            {
                //sort dictionary based in (total) entry value//Using LINQ alphabetically entry value
                _vocabularyDictionary = (from entry in _vocabularyDictionary orderby entry.Value.Frequency descending select entry).
                    ToDictionary(pair => pair.Key, pair => pair.Value);

                streamWriter2.WriteLine("[{0} {1}]", "Most common word", "Frequency");
                foreach (var entry in _vocabularyDictionary)
                {
                    streamWriter2.WriteLine(entry.Value.GetRootAndVariantsAndAllFrequenciesStringRepresentation());
                }
                streamWriter2.Close();//in MSN example it is missing.     
            }
            #endregion

        }
        public void WriteBasicResultsToFilesWithClusteredStemWords_AndUpdateJointFrequencyAndJointProbabilityClusteredStemWords(
            IEnumerable<StemWord> clusteredStemWords)
        {
            //write words' frequencies 
            #region write words' frequencies
            //Helper.write_sparse_int_wordsFrequencies(_vocabularyDictionary.Count, _stemWordMatrix, _folderPath + Constants.PoetJointFrequencyMatrix);
            #endregion

            #region WriteClusteredValuesJointFrequency
            var stemWords = clusteredStemWords as StemWord[] ?? clusteredStemWords.ToArray();
            JointFrequencyClusteredAgentsMatrixValues = Helper.WriteJointFrequencyForClusteredAgents(
                _stemWordMatrix, FolderPath + Constants.ClusteredAgentsJointFrequencyMatrix,
                stemWords);
            #endregion

            //write words' frequencies for Gephi
            #region write words' frequencies for Gephi
            //var x = WritePoetJointFrequencyMatrixBeforeReductionForGephi.GoAsync(_vocabularyDictionary.Count, _stemWordMatrix, FolderPath, _stemWordsTopVariantString);
            var x = WritePoetJointFrequencyMatrixBeforeReductionForGephi.GoAsync(_vocabularyDictionary.Count, _stemWordMatrix, FolderPath, _vocabularyDictionary.Keys.ToList());
            #endregion

            //write words' joint probability 
            #region write words' joint probability
            //_vocabularyDictionary = Helper.write_sparse_decimal_gephi(
            //    StemWordDictionaryOrderedByWeight.Count, _stemWordMatrix,
            //    FolderPath + Constants.PoetJointProbabilityMatrixGephi,
            //    FolderPath + Constants.PoetJointProbabilityMatrix,
            //    FolderPath + Constants.PoetJointProbabilityMatrixDistributionPlot,
            //    _stemWordsTopVariantString, _vocabularyDictionary);
            #endregion

            #region WriteClusteredValuesJointProbability
            JointProbabilityClusteredAgentsMatrixValues = Helper.WriteReducedMatrix(_stemWordMatrix,
                   FolderPath + Constants.ClusteredAgentsJointProbabilityMatrix, stemWords);
            #endregion

            //Update the ordered Vocabulary 
            Helper.OrderedVocabularyDictionary = _vocabularyDictionary;

            #region Write the vocabulary (regular) without headers
            using (var streamWriter = new StreamWriter(FolderPath + @"\VocabularyStemmedWords.txt"))
            {
                //sort dictionary based in (total) entry value
                //Using LINQ alphabetically entry value
                _vocabularyDictionary = (from entry in _vocabularyDictionary orderby entry.Value.Frequency descending select entry).
                    ToDictionary(pair => pair.Key, pair => pair.Value);

                //Using LINQ alphabetically
                foreach (var entry in _vocabularyDictionary)
                {
                    //get the word's version with the highest score  ( get TopVariantWord )
                    var mostPopularStemVersion = (entry.Value).TopVariantWord;

                    streamWriter.WriteLine(mostPopularStemVersion + " " + entry.Value + " " +
                        string.Format(Constants.Precision, entry.Value.MarginalProbability));

                    //To export to the network statistics analysis
                    ExportDictionaryToNodeXL.Add(new Tuple<string, string, string>(mostPopularStemVersion, entry.Value.ToString(),
                        string.Format(Constants.Precision, entry.Value.MarginalProbability)));


                    //Create new dictionary that has the most popular variant word as the Name (or main name) 
                    var newStemWord = entry.Key;
                    //newStemWord.Root = mostPopularStemVersion;
                }
                streamWriter.Close();
                streamWriter.Dispose();
            }
            #endregion
            //TODO the next bug location
            //Write the vocabulary (detailed) without headers  
            #region Write the vocabulary (detailed) without headers  
            using (var streamWriter2 = new StreamWriter(FolderPath + Constants.PoetAllPoemsVocabularyStemWordsDetailedFileName))
            {
                //sort dictionary based in (total) entry value//Using LINQ alphabetically entry value
                _vocabularyDictionary = (from entry in _vocabularyDictionary orderby entry.Value.Frequency descending select entry).
                    ToDictionary(pair => pair.Key, pair => pair.Value);

                //Using LINQ alphabetically
                //var sortedVocab = (from entry in vocabulary_dictionary orderby entry.Key ascending select entry).
                //    ToDictionary(pair => pair.Key, pair => pair.Value);
                streamWriter2.WriteLine("[{0} {1}]", "Most common word", "Frequency");
                foreach (var entry in _vocabularyDictionary)
                {
                    streamWriter2.WriteLine(entry.Value.GetRootAndVariantsAndAllFrequenciesStringRepresentation());
                }
                streamWriter2.Close();//in MSN example it is missing.     
            }
            #endregion
        }

        public Dictionary<string, StemWord> ReduceRegularDictionary(Dictionary<string, StemWord> dictionary, List<string> removedWordsIdList)
        {
            var counter = -1;
            var tmpvocabularyDictionary = new Dictionary<string, StemWord>();

            foreach (var pair in dictionary)
            {
                if (!removedWordsIdList.Contains(pair.Key))
                {
                    counter++;
                    StemWord temp = pair.Value;
                    temp.Id = counter;
                    //_temp.MarginalProbabilityUsingRfCount = (decimal)pair.Key.Frequency / _totalNumberOfWords;
                    temp.MarginalProbability = -1;//pair.Key.Frequency / denominator;
                    temp.MarginalProbabilityUsingRfCount = -1;
                    //tmpvocabularyDictionary.Add(temp, pair.Value);
                    tmpvocabularyDictionary.Add(pair.Key, temp);
                }
                //else
                //{
                //   numberOfWordsToRemoveFromTotalWords += pair.Value.Frequency;
                //}
            }
            //dictionary = new Dictionary<string, StemWord>(); //new Dictionary<StemWord, int>();
            //dictionary = tmpvocabularyDictionary;
            return tmpvocabularyDictionary;
            //tmpvocabularyDictionary = null;
        }
        public List<string> RecursiveTextProcessingAlgorithm(int horizonSize, bool testSimulation,
            List<string> allSentencesWithStemWords, string allPoemsHorizonAlgorithmSentencesFileName)
        {
            int debugCounter = 0;
            StreamWriter file = null;
            if (!testSimulation)
            {
                try
                {
                    file = new StreamWriter(FolderPath + allPoemsHorizonAlgorithmSentencesFileName);
                }
                catch (Exception e)
                {
                    throw new Exception("Could not write file: " + FolderPath + allPoemsHorizonAlgorithmSentencesFileName
                        + ". " + e.Message);
                }
            }

            foreach (var sentenceLineString in allSentencesWithStemWords)
            {
                var start = 0;
                debugCounter++;
                string[] stemWords = sentenceLineString.Split(' ');

                if (stemWords.Length <= 0) //=> empty sentence
                    continue; //The continue statement passes control to the next iteration of the enclosing while, do, for, or foreach statement in which it appears.

                var end = start + stemWords.Length;
                while (start <= end)
                {
                    var horizonWordList = new List<StemWord>();

                    //The code divides the sentences into 2 types. Those where the length of the sentence is > the horizon and those that are smaller or equal.
                    //While sentence is still larger than the horizon
                    HorizonAlgorithm horizon;
                    if (horizonSize + start <= end)
                    {
                        for (var i = start; i < start + horizonSize; i++)
                        {
                            if (stemWords[i] != "")
                                horizonWordList.Add(StemWordsManager.ReturnStemWord(stemWords[i]));
                        }

                        //print out the value of the horizonWordList which is the horizon sentence 
                        if (!testSimulation)
                            LowesHelpers.PrintWordListToFileAsOneLine(horizonWordList, file);

                        //add to _frameDictionary
                        var frameSize = horizonWordList.Count;
                        if (_frameCounterDictionary.ContainsKey(frameSize))
                            _frameCounterDictionary[frameSize] = _frameCounterDictionary[frameSize] + 1;
                        else
                            _frameCounterDictionary.Add(frameSize, 1);

                        //here is a potential bug
                        if (horizonWordList.Count < 1)
                        {
                            var debugCounterInt = debugCounter;
                            throw new Exception($"{debugCounterInt} Is the location of the bug.");
                        }

                        horizon = new HorizonAlgorithm(horizonSize, horizonWordList);
                        List<Tuple<int, int>> matrixUpdate = horizon.UpdateMatrix();

                        Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
                                                             horizonWordList.Count - 1;

                        foreach (Tuple<int, int> t in matrixUpdate)
                        {
                            _stemWordMatrix[t.Item1][t.Item2] = _stemWordMatrix[t.Item1][t.Item2] + 1;
                            _stemWordMatrix[t.Item2][t.Item1] = _stemWordMatrix[t.Item1][t.Item2]; //added
                        }
                    }
                    //When sentence is smaller than the  horizon
                    else
                    {
                        int newHorizonSize = end - start;
                        for (int i = start; i < end; i++)
                            horizonWordList.Add(StemWordsManager.ReturnStemWord(stemWords[i]));

                        //The horizonWordList is the list of remaining words. If Count > 1, means last word not to be counted in pairs (but frames are still there?)
                        if (horizonWordList.Count > 1) //In case you DON'T want the final word to be a pair on its own!
                                                       //if (horizonWordList.Count > 0)  //In case you DO want the final word to be a pair on its own!
                        {
                            Helper.TotalNumberOfPairsWrtFrames = Helper.TotalNumberOfPairsWrtFrames +
                                                                 horizonWordList.Count - 1;

                            //print out the value of the horizonWordList which is the horizon sentence 
                            if (!testSimulation)
                                LowesHelpers.PrintWordListToFileAsOneLine(horizonWordList, file);

                            //add to _frameDictionary
                            var frameSize = horizonWordList.Count;
                            if (_frameCounterDictionary.ContainsKey(frameSize))
                                _frameCounterDictionary[frameSize] = _frameCounterDictionary[frameSize] + 1;
                            else
                                _frameCounterDictionary.Add(frameSize, 1);

                            horizon = new HorizonAlgorithm(newHorizonSize, horizonWordList);
                            List<Tuple<int, int>> matrixUpdate = horizon.UpdateMatrix();

                            foreach (Tuple<int, int> t in matrixUpdate)
                            {
                                _stemWordMatrix[t.Item1][t.Item2] = _stemWordMatrix[t.Item1][t.Item2] + 1;
                            }
                        }
                    }
                    start++;
                }
            }
            file?.Close();
            return allSentencesWithStemWords;
        }

        public void UpdateStemWordsMarginalProbabilityFromJointProbabilityMatrix(
                   decimal denominator, string fileName, int[][] stemWordMatrix)
        {
            //Update the stem words with the new marginal probability based on the RFs
            //The marginal probability is the total number of times that word appeared in a pair, over the total number of possible pairs.

            TextWriter tw = new StreamWriter(FolderPath + fileName);
            try
            {
                tw.WriteLine("{0}: {1} / {2} = {3}.", "Word", "Sum of Word-Pairs", "Total Number of Pairs", "Value");
                tw.WriteLine();
                foreach (var pair in _vocabularyDictionary)
                {
                    var stemWord = pair.Value;

                    //Get the total sum of all the occurrences of this word with every other word 
                    var sum = stemWordMatrix[pair.Value.Id].Sum();
                    //totalValue += sum;
                    var result = (decimal)sum / denominator;// totalNumberOfPairsWRTFrames;
                    stemWord.MarginalProbability = result;
                    tw.WriteLine("{0}: {1}/{2} = {3,0:0.####}",
                        pair.Value.Root.PadRight(5),
                        sum.ToString().PadLeft(5),
                        denominator.ToString(CultureInfo.InvariantCulture).PadRight(5),
                        result);
                    //_tempvocabularyDictionary.Add(pair.Key, stemWord);
                }
                tw.Close();
            }
            finally
            {
                tw.Dispose();
            }
            Helper.StemWordsCoOccurenceMatrixIntValue = stemWordMatrix;
        }

        public decimal[,] CalculateAndWriteOutTheConditionalProbabilityForClusteredAgents(StemWord[] stemWords)
        {
            return Helper.WriteConditionalProbabilityForClusteredAgents(
                FolderPath + Constants.ClusteredAgentsConditionalProbabilityMatrix,
                _vocabularyDictionary.Keys.ToList(),
                stemWords);
        }
        public void CalculateAndWriteOutTheConditionalProbability()
        {
            Helper.write_sparse_decimal_conditionalProbability_gephi(
                _vocabularyDictionary.Count,
                _stemWordMatrix,
                FolderPath + Constants.PoetConditionalProbabilityMatrixGephi,
                FolderPath + Constants.PoetConditionalProbabilityMatrix,
                FolderPath + Constants.PoetConditionalProbabilityMatrixDistributionPlot,
               _vocabularyDictionary.Keys.ToList());
        }
        public void CalculateAndWriteOutTheCorrelationCoefficient()
        {
            Helper.write_sparse_decimal_correlationcoefficient_gephi(
                _vocabularyDictionary.Count,
                _stemWordMatrix,
                FolderPath + Constants.PoetCorrelationCoefficientMatrixGephi,
                FolderPath + Constants.PoetCorrelationCoefficientMatrix,
                FolderPath + Constants.PoetCorrelationCoefficientMatrixDistributionPlot,
               _vocabularyDictionary.Keys.ToList());
        }
        public void CalculateAndWriteOutThePointWiseMutualInformation()
        {
            Helper.write_sparse_decimal_pointwisemutualinformation_gephi(
                _vocabularyDictionary.Count,
                _stemWordMatrix,
                FolderPath + Constants.PoetPMIMatrixGephi,
                FolderPath + Constants.PoetPMIMatrix,
                FolderPath + Constants.PoetPMIMatrixDistributionPlot,
               _vocabularyDictionary.Keys.ToList());
        }
        public void CalculateAndWriteOutTheLogOddsRatio(int horizon)
        {
            Helper.write_sparse_decimal_logoddsratio_gephi(
                _vocabularyDictionary.Count,
                _stemWordMatrix,
                FolderPath + Constants.PoetLogOddsRatioMatrixGephi,
                FolderPath + Constants.PoetLogOddsRatioMatrix,
                FolderPath + Constants.PoetLogOddsRatioMatrixDistributionPlot,
                _vocabularyDictionary.Keys.ToList(), horizon);
        }
        public void MoveResultFilesToCorrespondingFoldersAndCleanUp()
        {
            //final clean up: put all .txt and .html files into a folder called corpus
            var corpusFolder = FolderPath + @"\corpus";
            if (!Directory.Exists(corpusFolder))
                Directory.CreateDirectory(corpusFolder);

            var allFiles = Directory.GetFiles(FolderPath, Constants.FileExtensionTxt)
                .Where(w => !w.Contains("DocumentsFile.txt")).ToArray();

            string[] txtFiles;
            //remove the files that make up the corpus from txtFiles: _documentsFileNames
            if (_documentsFileNames != null && _documentsFileNames.Length > 0)
                txtFiles = allFiles.Except(_documentsFileNames).ToArray();
            else
                txtFiles = allFiles;
            try
            {
                foreach (var item in txtFiles)
                {
                    File.Move(item, Path.Combine(corpusFolder, Path.GetFileName(item)));
                }
            }
            catch (IOException e)
            {
                //try again after waiting..
                if (_attemptsToMoveFolder == 5)
                    ShowMessage.ShowErrorMessageAndExit("There is an error in DataSourceTextFormat (foreach (var item in txtFiles)). ", new Exception(e.Message));

                Thread.Sleep(1000);
                _attemptsToMoveFolder++;
                MoveResultFilesToCorrespondingFoldersAndCleanUp();
            }

            //put all the .csv and .txt files into a folder called statistics
            var statisticsFolder = FolderPath + @"\statistics_" + DataSourceName;
            const string fileExtensionCsv = "*.csv";
            const string fileExtensionDat = "*_stat_*.txt";

            if (!Directory.Exists(statisticsFolder))
                Directory.CreateDirectory(statisticsFolder);

            txtFiles = Directory.GetFiles(FolderPath, fileExtensionCsv);
            try
            {
                foreach (var item in txtFiles)
                {
                    File.Move(item, Path.Combine(statisticsFolder, Path.GetFileName(item)));
                }
            }
            catch (IOException excep)
            {
                if (_attemptsToMoveFolder == 5)

                    ShowMessage.ShowErrorMessageAndExit("There is an error in DataSourceTextFormat (foreach (var item in txtFiles)). " + excep.Message, excep);

                Thread.Sleep(1000);
                _attemptsToMoveFolder++;
                MoveResultFilesToCorrespondingFoldersAndCleanUp();

            }

            txtFiles = Directory.GetFiles(FolderPath, fileExtensionDat);
            try
            {
                foreach (var item in txtFiles)
                {
                    File.Move(item, Path.Combine(statisticsFolder, Path.GetFileName(item)));
                }
            }
            catch (Exception excep)
            {
                ShowMessage.ShowErrorMessageAndExit("There is an error in DataSourceTextFormat outermost loop.",
                    excep.InnerException != null
                        ? new Exception(excep.InnerException.ToString())
                        : new Exception(excep.Message));
            }
        }
    }
}

/*
//To find the difference between two dictionaries:
var diff = _stemWordDictionaryOrderedByWeight.Except(_vocabularyDictionary).Concat(_vocabularyDictionary.Except(_stemWordDictionaryOrderedByWeight));

Use the overload of the Equals method that specifies a StringComparison. 
Ordinal is the fastest way to compare two strings. 
bool result = root.Equals(root2, StringComparison.Ordinal);

Interfaces can't have static methods. A class that implements an interface needs to implement them all as instance methods. 
Static classes can't have instance methods.
QED.Aug 12, 2009
*/
//public async Task WriteAllandExtractedandMinusStopWordsandWithStemWordsandDictionaryResults(
//    string folderPath,
//    List<string> extractedSentences,
//    List<string> allSentencesMinusStopWords,
//    List<string> allSentencesWithStemWords,
//    Dictionary<string, StemWord> stemWordDictionaryOrderedByWeight)
////Dictionary<StemWord, int> stemWordDictionaryOrderedByWeight)
//{
//   await WritePoetAllPoemsSentencesFile.GoAsync(folderPath, extractedSentences)
//       .Tea(x => WritePoetAllPoemsSentencesNoStopWordsFile.GoAsync(folderPath, allSentencesMinusStopWords))
//       .Tea(x => WritePoetAllPoemsSentencesStemmedFile.GoAsync(folderPath, allSentencesWithStemWords))
//       .Tea(x => WritePoetAllPoemsVocabularyStemWordsDetailedFile.GoAsync(folderPath, stemWordDictionaryOrderedByWeight));
//}

//public static void CalculateTheMarginalProbabilityOfStemWordsInDictionary(
//    Dictionary<string, StemWord> dictionary, 
//    int[][] stemWordMatrix,
//    int totalNumberOfPairsInFrames)
//{
//    foreach (var pair in dictionary)
//    {
//        //Get the total sum of all the occurrences of this word with every other word 
//        pair.Value.MarginalProbability = Convert.ToDecimal(
//            stemWordMatrix[pair.Value.Id].Sum() / totalNumberOfPairsInFrames);
//    }
//}
//////////////////////

//public bool CheckIfWordIsLowFrequencyWord(string word)
//{
//    return LowWordFrequencyList.Contains(word);
//}
//public List<string> RemoveLowFrequencyWordsFromSentences(List<string> allSentencesWithStemWords)
//{
//    var regex = new Regex(@"[ ]{2,}", RegexOptions.None);
//    var allSentencesMinusStopWords = new List<string>();

//    foreach (var sentenceRawString in allSentencesWithStemWords)
//    {
//        var tempWordSentenceList = new List<string>();

//        var sentenceString = regex.Replace(sentenceRawString, @" ");
//        //replace multiple spaces with a single space
//        var inputStringArray = sentenceString.Split(" ".ToCharArray()).ToList();
//        inputStringArray = inputStringArray.ConvertAll(d => d.ToLower());

//        //instead compare word by word
//        foreach (var word in inputStringArray)
//        {
//            if (!LowWordFrequencyList.Contains(word))
//                tempWordSentenceList.Add(word);
//        }

//        string results = string.Join(" ", tempWordSentenceList);
//        allSentencesMinusStopWords.Add(results);
//    }
//    return allSentencesMinusStopWords;
//}

///// <summary>
///// This should happen after stemming has happened (if it was an option) because single frequency
///// words can combine into a higher frequency stem word. This only removes the words that have a low
///// occurrence. It does not affect the edge it might have shared with another word. 
///// </summary>
///// <param name="frequency"></param>
//public void RemoveLowFrequencyWordOccurrencesAndUpdateWordStatistics(int frequency)
//{
//    VocabularyLowWordFrequencyDictionary = new Dictionary<string, StemWord>();
//    LowWordFrequencyList = new List<string>();
//    foreach (var item in _vocabularyDictionary.Where(kvp => kvp.Value.Frequency <= frequency).ToList())
//    {
//        VocabularyLowWordFrequencyDictionary.Add(item.Key, item.Value);
//        LowWordFrequencyList.Add(item.Value.Root);
//        _vocabularyDictionary.Remove(item.Key);
//    }
//}
//public void WriteGeneralStatisticsFile()
//{
//    //Write out simple statistics about the corpus 
//    using (var file = new StreamWriter(_folderPath + Constants.PoetAllPoemsGeneralStatisticsFileName))
//    {
//        //the +1,-1 is to fix the display and so for not including the '/' in the dataSource's name
//        file.WriteLine("General statistics for " +
//                       _folderPath.Substring(_folderPath.LastIndexOf('\\') + 1,
//                           _folderPath.Length - _folderPath.LastIndexOf('\\') - 1));
//        file.WriteLine("");
//        file.WriteLine("The total number of reading frames is : " + Helper.GetReadingFrames());
//        file.WriteLine("The total number of (nonstop) word pairs wrt to reading frames is : " + Helper.TotalNumberOfPairsWrtFrames);
//        file.WriteLine("The total number of (stemmed) words is : " + _vocabularyDictionary.Sum(x => x.Value.Frequency));
//        file.WriteLine("The total number of (stemmed) vocabulary words is : " + _vocabularyDictionary.Count);
//    }
//}

/////////////////////////////////////
/*********************************/
#region temporarily commented out
//public DataSourceTextFormat(
//    string poetFolderPath,
//    int horizonSize,
//    TextReaderAlgorithmEnum algorithm,
//    SentenceSetUp sentenceSetup,
//    bool useStemming,
//    bool calculateAllAvailableStatistics,
//    bool directAnatVersion,
//    IEnumerable<string> poemsStringArray,
//    string documentsFilePath,
//    IEnumerable<StemWord> clusteredStemWords,
//    DataFileFormat dataFileFormat,
//    bool useLowWordFrequencyReduction,
//    int lowWordFrequencyValue,
//    bool useNetworkReduction,
//    int networkReductionValue)
//{
//    var allSentences = new List<string>();
//    //SpecialSentenceProcessor.StopWords = StopWordsHelper.GetStopWordsFromEmbeddedResource();
//    //UseStemming = useStemming;
//    /* Initialization */
//    ExportDictionaryToNodeXL = new List<Tuple<string, string, string>>();
//    //InitializeVariables();
//    //Horizon = horizonSize;
//    var _frameCounter = 0;
//    var stringArray = poemsStringArray as string[] ?? poemsStringArray.ToArray();

//    #region region 1: code related to the merging using ANAT
//    if (directAnatVersion)
//    {
//        poetFolderPath = ExtractTextFromStrings.GetFolderPath(documentsFilePath);
//        _frameCounter = 0;
//        //ResetVariablesAnat(poetFolderPath, stringArray);
//    }
//    else
//    {
//        //ResetVariablesAnatFromFile(poetFolderPath);
//    }
//    #endregion

//    //CurrentAlgorithm = algorithm;

//    #region region 2: code related to the merging using ANAT
//    //if (dataFileFormat == DataFileFormat.AllSupportedText)
//    //{
//    //    /* Extract text */
//    //    foreach (var dataFilePath in _documentsFileNames)
//    //    {
//    //        try
//    //        {
//    //            //HERE
//    //            //Add the title to see the effect here
//    //            var start = dataFilePath.LastIndexOf(@"\", StringComparison.Ordinal);
//    //            var finish = dataFilePath.LastIndexOf(@"[", StringComparison.Ordinal);
//    //            var fileTitle = dataFilePath.Substring(start + 1, finish - start - 1);

//    //            allSentences.Add(fileTitle);
//    //            allSentences.AddRange(
//    //                ProcessHtmlSourceFileAndWriteResultToTxtFile(dataFilePath, sentenceSetup));
//    //        }
//    //        catch (Exception e)
//    //        {
//    //            ShowMessage.ShowErrorMessageAndExit("There is an error in DataSourceTextFormat (if (dataFileFormat == DataFileFormat.AllSupportedText)). ", e);
//    //        }
//    //    }
//    //}
//    //else if (dataFileFormat == DataFileFormat.TextFormat)
//    #endregion
//    #region region 3: code covered in new version (new version should handle any request to run statistics including IONA's
//    {
//        /* Extract text */
//        foreach (var dataFilePath in _documentsFileNames)
//        {
//            try
//            {
//                //Add the title to see the effect here
//                var start = dataFilePath.LastIndexOf(@"\", StringComparison.Ordinal);
//                var finish = dataFilePath.LastIndexOf(@"[", StringComparison.Ordinal);
//                var fileTitle = dataFilePath.Substring(start + 1, finish - start - 1);
//                allSentences.Add(fileTitle);
//                allSentences.AddRange(TextToSentences.ProcessTextSourceFileAndWriteResultToTxtFile(dataFilePath, sentenceSetup));
//            }
//            catch (Exception e)
//            {
//                ShowMessage.ShowErrorMessageAndExit("There is an error in DataSourceTextFormat (if (dataFileFormat == DataFileFormat.AllSupportedText)). ", e);
//            }
//        }
//    }

//    /* Remove stop words and numerals*/
//    //var extractedSentences = allSentences.RemoveStopWordsAndNumeralsFromSentences();

//    /* CalculateWordStatisticsAndReplaceWordWithStemRoot */
//    //CalculateWordStatisticsAndReplaceWordWithStemRoot(useStemming, extractedSentences);

//    //if (useLowWordFrequencyReduction)
//    //{
//    //    RemoveLowFrequencyWordOccurrencesAndUpdateWordStatistics(lowWordFrequencyValue);
//    //    _allSentencesWithStemWords = RemoveLowFrequencyWordsFromSentences(_allSentencesWithStemWords);
//    //}

//    //StemWordDictionaryOrderedByWeight = (from entry in _vocabularyDictionary
//    //                                     orderby entry.Value descending
//    //                                     select entry).ToDictionary(pair => pair.Key, pair => pair.Value);

//    //_vocabularyDictionary = StemWordDictionaryOrderedByWeight;

//    //_totalNumberOfWords = _vocabularyDictionary.Sum(x => x.Value.Frequency);

//    //_allSentencesWithStemWords = algorithm == TextReaderAlgorithmEnum.LoweSingleWindowAlgorithm
//    //    ? RecursiveTextProcessingAlgorithmLowesSingleWindow.Go(horizonSize, false, _allSentencesWithStemWords, @"/GlobalPoemsFileHorizonSentences.txt", poetFolderPath, LowesHelpers.CreateStemWordsMatrix(_vocabularyDictionary))
//    //    : RecursiveTextProcessingAlgorithmLowesDoubleWindow.Go(horizonSize, false, _allSentencesWithStemWords, @"/GlobalPoemsFileHorizonSentences.txt", poetFolderPath, LowesHelpers.CreateStemWordsMatrix(_vocabularyDictionary));

//    Helper.TotalNumberOfPairsWrtFrames = Helper.LowesTotalNumberOfWindows;

//    //UpdateStemWordsMarginalProbabilityFromJointProbabilityMatrix(Helper.LowesTotalNumberOfWindows, Constants.PoetMarginalProbability,
//    //   algorithm == TextReaderAlgorithmEnum.LoweSingleWindowAlgorithm
//    //      ? RecursiveTextProcessingAlgorithmLowesSingleWindow.StemWordMatrix
//    //      : RecursiveTextProcessingAlgorithmLowesDoubleWindow.StemWordMatrix);

//    //old version and removed with the new one
//    //ReplaceStemWithTopVariantAndWriteOutVocabularyWithStatisticsToFiles();
//    //WriteTheVocabularyWithoutHeaders(StemWordDictionaryOrderedByWeight);
//    #endregion
//    #region region 4: Using network reduction

//    if (useNetworkReduction)
//    {
//        ////write words' frequencies for Gephi before reduction
//        //WritePoetJointFrequencyMatrixBeforeReductionForGephi.GoAsync(StemWordDictionaryOrderedByWeight.Count,
//        //    _stemWordMatrix, FolderPath, _stemWordsTopVariantString);

//        ////write out the original matrix file before the edge value reduction
//        //SDParser.FileFunctions.FileWriter.WriteSparseIntArrayToFile(_stemWordMatrix,
//        //    FolderPath + Constants.PoetJointFrequencyMatrixBeforeReduction);

//        ////remove certain edge values 
//        //var resultMatrix = Helper.write_sparse_int_replace_edgevalue(StemWordDictionaryOrderedByWeight.Count,
//        //    _stemWordMatrix, FolderPath + Constants.PoetJointFrequencyMatrixZero, networkReductionValue, 0);

//        //_stemWordMatrix =
//        //    Helper.RemoveZeroEntriesFromMatrixThenRemoveZeroColumnsAndRows(StemWordDictionaryOrderedByWeight.Count,
//        //        resultMatrix, FolderPath + Constants.PoetJointFrequencyMatrixZeroRemoved);


//        //ReduceRegularDictionary(Helper.ListOfWordsToRemove);
//        //ReduceSizeOfVariousDictionariesAfterReduction();

//        ////Rebuild the _StemWordMatrix !!!
//        //UpdateStemWordsMarginalProbabilityFromJointProbabilityMatrix(Helper.TotalNumberOfPairsWrtFrames, Constants.PoetMarginalProbabilityReduced, _stemWordMatrix);

//        ////Have to calculate _stemWordDictionaryOrderedByWeight. Generate new ordered_dictionary
//        //ReplaceStemWithTopVariantAndWriteOutVocabularyWithStatisticsToFilesAfterReduction();
//    }
//    #endregion
//    #region 5: WriteBasicResultsToFiles
//    //WriteBasicResultsToFiles();
//    try //updated version
//    {
//        WriteBasicResultsToFiles(_stemWordMatrix);
//    }
//    catch (Exception e)
//    {
//        ShowMessage.ShowErrorMessageAndExit("Error in WriteBasicResultsToFiles().", e);
//    }
//    #endregion

//    //Update the Ids of the clusteredStemWords with the indices of the probablility matrix,
//    //so that later I can correlate between values of the the values between the 
//    clusteredStemWords = Helper.UpdateClusteredStemWordsWithIdsCorrespondingToNewProbabilityMatrixIndexWeird(
//        clusteredStemWords, _topVariantDictionaryOrderedByWeight);//_vocabularyDictionary);


//    var stemWords = clusteredStemWords as StemWord[] ?? clusteredStemWords.ToArray();
//    WriteBasicResultsToFilesWithClusteredStemWords_AndUpdateJointFrequencyAndJointProbabilityClusteredStemWords(stemWords);

//    Top10StemWordsFromMatrix = _stemWordsTopVariantString.Take(10).ToList();
//    SyncedIdsStemWords = stemWords.ToList();

//    Helper.SetupProbabilityCalculationsEngine();//Needed before doing any probability calculations.
//                                                //CalculateAndWriteOutTheConditionalProbability();//iffy here .. TODO: fix this issue
//    ConditionalProbabilityClusteredAgentsMatrixValues = CalculateAndWriteOutTheConditionalProbabilityForClusteredAgents(stemWords);

//    //Update MatrixValue with the updated StemWord matrix
//    Helper.StemWordsCoOccurenceMatrixIntValue = _stemWordMatrix;

//    //CalculateAndWriteOutTheConditionalProbability();
//    //if (calculateAllAvailableStatistics)
//    //{
//    CalculateAndWriteOutTheConditionalProbability();
//    CalculateAndWriteOutTheCorrelationCoefficient();
//    CalculateAndWriteOutThePointWiseMutualInformation();
//    CalculateAndWriteOutTheLogOddsRatio(horizonSize);
//    //}
//    try
//    {
//        MoveResultFilesToCorrespondingFoldersAndCleanUp();
//    }
//    catch (Exception e)
//    {
//        Console.WriteLine(e);
//        throw;
//    }
//}
#endregion
