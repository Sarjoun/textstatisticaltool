﻿using System;
using System.Collections.Generic;
using System.IO;
using Iveonik.Stemmers;
using SDParser.Enums;
using SDParser.Extensions;
using SDParser.Primitives;
using SDParser.StopWords;
using SDParser.Structures;
using SDParser.TextProcessingCentral;

namespace SDStatistics.TextCorpus.DataSources
{
   public static class ProcessFileNamesAndReturnDictionary
   {
      public static Dictionary<string, StemWord>
         MakeWordStreamAndVariantWordsReturnDictionaryOrderedAlphabeticallyFromFile(
            DocumentsLinksFileName documentsLinksFileName,
            WordStreamFileName wordStreamFileName,
            IncludeTitleInSetup includeTitleInSetup)
      {
         SentenceProcessor.Stemmer = new EnglishStemmer();
         SentenceProcessor.StopWords = StopWordsHelper.GetStopWordsFromEmbeddedResource();
         SentenceProcessor.NewWords = new List<string>();
         SentenceProcessor.UniqueWordsAndTheirVariants = new Dictionary<string, StemWord>();
         SentenceProcessor.WordStreamWordTypeEnum = WordStreamWordTypeEnum.StemmedWord;

         StreamReader file = null;

         try
         {
            file = new StreamReader(documentsLinksFileName.Value);
            string line;
            while ((line = file.ReadLine()) != null)
            {
               var newWords = StoryProcessor.MakeWordStreamFromSingleStory(
                  line.RemoveDocIdFromStartOfPathIfItExists(),
                  includeTitleInSetup);

               if (newWords.Count <= 0) continue;

               //fileWriter.WriteLine("##SOD##" + line);
               //foreach (var word in newWords)
               //    fileWriter.WriteLine(word);
               //fileWriter.WriteLine("##EOD##");
            }
         }
         finally
         {
            file?.Close();
            //fileWriter.Close();
         }
         //return UniqueWordsAndTheirVariants = SentenceProcessor.UpdatedUniqueWordsAndTheirVariants();
         return SentenceProcessor.UpdatedUniqueWordsAndTheirVariants();
      }

      public static Dictionary<string, StemWord> MakeWordStreamAndVariantWordsReturnDictionaryOrderedAlphabeticallyFromListOfFileNames(
            string[] documentsFileNames,
            WordStreamFileName wordStreamFileName,
            bool createFile,
            IncludeTitleInSetup includeTitleInSetup)
      {
         //Setup static class SentenceProcessor
         SentenceProcessor.Stemmer = new EnglishStemmer();
         SentenceProcessor.StopWords = StopWordsHelper.GetStopWordsFromEmbeddedResource();
         SentenceProcessor.NewWords = new List<string>();
         SentenceProcessor.UniqueWordsAndTheirVariants = new Dictionary<string, StemWord>();
         SentenceProcessor.WordStreamWordTypeEnum = WordStreamWordTypeEnum.StemmedWord;

            if (createFile)
         {
            TextWriter fileWriter = new StreamWriter(wordStreamFileName.Value);
            try
            {
               foreach (var documentFileLink in documentsFileNames)
               {
                  try
                  {
                     StoryProcessor.MakeWordStreamFromSingleStory(
                        documentFileLink.RemoveDocIdFromStartOfPathIfItExists(), includeTitleInSetup);
                     var newWords = StoryProcessor.MakeWordStreamFromSingleStory(
                        documentFileLink.RemoveDocIdFromStartOfPathIfItExists(),
                        includeTitleInSetup);

                     fileWriter.WriteLine("##SOD##" + documentFileLink);
                     foreach (var word in newWords)
                        fileWriter.WriteLine(word);
                     fileWriter.WriteLine("##EOD##");

                     if (newWords.Count <= 0)
                        continue;
                  }
                  catch (Exception e)
                  {
                     Console.WriteLine(e);
                     throw;
                  }
               }
            }
            finally
            {
               fileWriter.Close();
            }
            return SentenceProcessor.UpdatedUniqueWordsAndTheirVariants();
         }
         else
         {
            foreach (var documentFileLink in documentsFileNames)
            {
               try
               {
                  StoryProcessor.MakeWordStreamFromSingleStory(
                     documentFileLink.RemoveDocIdFromStartOfPathIfItExists(),
                     includeTitleInSetup);
               }
               catch (Exception e)
               {
                  Console.WriteLine(e);
                  throw;
               }

            }
            //return UniqueWordsAndTheirVariants = SentenceProcessor.UpdatedUniqueWordsAndTheirVariants();
            return SentenceProcessor.UpdatedUniqueWordsAndTheirVariants();
         }
      }
   }
}
