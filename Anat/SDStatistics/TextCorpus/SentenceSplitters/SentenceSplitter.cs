using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SDStatistics.TextCorpus.OldCode;

namespace SDStatistics.TextCorpus.SentenceSplitters
{
   public static class SentenceSplitter
   {
      private const string SentenceExtractionPattern = @"\.|\?|\!";
      public static List<string> ProcessPoemTextVersionAsOneBigSentence(string assignedText)
      {
         var sentencesList = new List<string>();
         var sentenceString = new string(assignedText.Where(c => !char.IsPunctuation(c)).ToArray()).Trim();
         if (sentenceString.Length > 2)
            sentencesList.Add(sentenceString);
         return sentencesList;
      }

      public static List<string> ProcessPoemTextVersionUsingOrbit(string assignedText)
      {
         return SplitOnRegex(assignedText, RegexExtractOptions.FullStopQuestionExclamation)
            .Select(stringSentence => new string(stringSentence.Where(c => !char.IsPunctuation(c)).ToArray()).Trim())
            .Where(sentenceString => sentenceString.Length > 2)
            .ToList();
      }
      
      private static IEnumerable<string> SplitOnRegex(string text, RegexExtractOptions option)
      {
         switch (option)
         {
            case RegexExtractOptions.FullStopQuestionExclamation:
               return Regex.Split(text, SentenceExtractionPattern, RegexOptions.IgnoreCase);
            case RegexExtractOptions.Point:
               return Regex.Split(text, SentenceExtractionPattern, RegexOptions.IgnoreCase);
            //case regexExtractOptions.QuestionExclamation:
            //    return Regex.Split(text, questionExclamation, RegexOptions.IgnoreCase);
            //case regexExtractOptions.Semicolumn:
            //    return Regex.Split(text, semicolumn, RegexOptions.IgnoreCase);
            //case regexExtractOptions.Comma:
            //    return Regex.Split(text, comma, RegexOptions.IgnoreCase);
            default:
               return null;
         }
      }
   }
}