﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SDStatistics.TextCorpus.OldCode;

namespace SDStatistics.TextCorpus.SentenceSplitters
{
    //This class is only for extracting further the source file especially when the source file is of type HTML.
    //It looks like an overkill and complicated.
    //Instead I can use create 2 types of SentenceSplitter (HTML and text) each with their own rules, or even
    //try another approach to get the functionality that I require.
    internal class SentenceSplitterHTML
    {
        private const string SentenceExtractionPattern = @"\.|\?|\!|(<p>)|(<br>)";
        private List<string> _sentencesList;

        public SentenceSplitterHTML(string text)
        {
            AssignedText = text;
            _sentencesList = new List<string>(); //reset
        }
        public string AssignedText { get; set; }
        public List<string> ResultingSentencesList => _sentencesList;

        public void ProcessPoemTextVersionAsOneBigSentence()
        {
            _sentencesList = new List<string>();
            var sentenceString = new string(AssignedText.Where(c => !char.IsPunctuation(c)).ToArray());
            sentenceString = sentenceString.Replace("<p>", " ");
            sentenceString = sentenceString.Trim();

            if (sentenceString.Length > 2)
                _sentencesList.Add(sentenceString);
        }

        public void ProcessPoemTextVersionUsingOrbit()
        {
            _sentencesList = new List<string>();

            //Now we're splitting only on the point.
            var resultStringsArray = SplitOnRegex(AssignedText, RegexExtractOptions.FullStopQuestionExclamation);

            foreach (var stringSentence in resultStringsArray)
            {
                var sentenceString = new string(stringSentence.Where(c => !char.IsPunctuation(c)).ToArray());
                sentenceString = sentenceString.Trim();

                if ((sentenceString.Length <= 2) || (sentenceString == "<p>"))
                    continue;

                _sentencesList.Add(sentenceString);
            }
        }

        protected IEnumerable<string> SplitOnRegex(string text, RegexExtractOptions option)
        {
            switch (option)
            {
                case RegexExtractOptions.FullStopQuestionExclamation:
                    return Regex.Split(text, SentenceExtractionPattern, RegexOptions.IgnoreCase);
                case RegexExtractOptions.Point:
                    return Regex.Split(text, SentenceExtractionPattern, RegexOptions.IgnoreCase);
                //case regexExtractOptions.QuestionExclamation:
                //    return Regex.Split(text, questionExclamation, RegexOptions.IgnoreCase);
                //case regexExtractOptions.Semicolumn:
                //    return Regex.Split(text, semicolumn, RegexOptions.IgnoreCase);
                //case regexExtractOptions.Comma:
                //    return Regex.Split(text, comma, RegexOptions.IgnoreCase);
                default:
                    return null;
            }
        }
    }

}
