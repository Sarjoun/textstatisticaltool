﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using SDParser.Structures;
using SDStatistics.Helpers;
using SDStatistics.MathLibrary;
using SDStatistics.Network;

namespace SDStatistics.TextCorpus.Helpers
{
    public static class Helper
    {
        private const string Precision = "{0:0.00000000}";//"{0:0.00000}";

        //old implementation, consider revising
        private static int _readingFrames; //total number of reading frames//Initializing static field is redundant
        public static int Horizon; //needed to calculate the log Odds ration (from Lowe's table)

        //new implementation 4.16.2015
        public static decimal TotalNumberOfPairsWrtFrames;//to represent the value WxN (4.2.2016)
                                                          //public static Decimal _totalNumberOfPairsWRTFrames;

        //After 4/2/2016 - Meeting, _totalNumberOfPairsWRTFrames is now representative of WxN
        public static decimal LowesTotalNumberOfWindows; //to represent the value WxN,(not anymore!), Updated from DataSource! 

        public static Dictionary<string, StemWord> OrderedVocabularyDictionary { get; set; }

        public static int[][] StemWordsCoOccurenceMatrixIntValue { get; set; }

        public static void SetupProbabilityCalculationsEngine()
        {
            ProbabilityCalculations.StemWordsCoOccurenceMatrixIntValue = StemWordsCoOccurenceMatrixIntValue;
            ProbabilityCalculations.TotalNumberOfPairsWrtFrames = TotalNumberOfPairsWrtFrames;
            ProbabilityCalculations.LowesTotalNumberOfWindows = LowesTotalNumberOfWindows;
            ProbabilityCalculations.OrderedVocabularyDictionary = OrderedVocabularyDictionary;
        }

        public static List<string> ListOfWordsToRemove { get; set; }

        public static void AddReadingFrames(int addedRFs)
        {
            _readingFrames = _readingFrames + addedRFs;
        }
        public static int GetReadingFrames()
        {
            return _readingFrames;
        }
        public static void SetReadingFrames(int readingFrames)
        {
            _readingFrames = readingFrames;
        }
        public static void ResetReadingFrames()
        {
            _readingFrames = 0;
            //_frameCounter = 0;
            TotalNumberOfPairsWrtFrames = 0;
            LowesTotalNumberOfWindows = 0;
        }

        /// <summary>
        /// Every time an edge is removed, the total count of pairs that constitutes the denominator value that is
        /// used to compute the marginal probabilities and joint probabilities should be reduced too. The logical
        /// way to reduce the denominator each time an edge is reduced to zero, by the value of the edge.
        /// i.e. if replacing edge 1 with 0, then reduce denominator by 1,
        ///      if replacing edge 2 with 0, then reduce denominator by 2 etc..
        /// </summary>
        /// <param name="totalWordsCount"></param>
        /// <param name="matrixValue"></param>
        /// <param name="fileName"></param>
        /// <param name="edgeValueToRemove"></param>
        /// <param name="edgeValueReplacement"></param>
        /// <returns></returns>
        public static int[][] write_sparse_int_replace_edgevalue(int totalWordsCount, int[][] matrixValue,
            string fileName, int edgeValueToRemove, int edgeValueReplacement)
        {
            var target = edgeValueToRemove;
            var replace = edgeValueReplacement;
            var _matrixValue = matrixValue;
            TextWriter tw = new StreamWriter(fileName);
            try
            {
                for (var i = 0; i < totalWordsCount; i++)
                {
                    for (var j = 0; j < totalWordsCount; j++)
                    {
                        if (_matrixValue[i][j] <= target)
                        {
                            TotalNumberOfPairsWrtFrames -= _matrixValue[i][j];//reduce the number of pairs by the same value
                            _matrixValue[i][j] = replace;
                            //_totalNumberOfPairsWRTFrames -= target;//reduce the number of pairs by the same value
                        }
                        tw.Write(Convert.ToString(_matrixValue[i][j]) + " ");
                    }
                    tw.WriteLine();
                }
                tw.Close();
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }

            return _matrixValue; //RemoveZeroEntriesFromMatrixThenRemoveZeroColumnsAndRows(totalWords, matrixValue, fname2);
        }


        ///// <summary>
        ///// Checks if the word index is in the ListOfWordsToRemove.
        ///// Yes means it belongs to that list.
        ///// </summary>
        ///// <param name="wordIndex"></param>
        ///// <returns></returns>
        ////public static bool CheckIfWordIsNotInRemoveList(int wordIndex)
        ////{
        ////   return ListOfWordsToRemove.Contains(wordIndex);
        ////}
        public static bool CheckIfWordIsNotInRemoveList(string word)
        {
            return ListOfWordsToRemove.Contains(word);
        }


        /////// <summary>
        /////// Only use for debugging purposes to make sure that the reduction is correct.
        /////// Will take a lot of time in a regular corpus, better have a small reduced corpus for testing.
        /////// </summary>
        /////// <param name="vocabularyDictionary">The vocabulary-dictionary of (Stemmed) words</param>
        /////// <param name="fname"></param>
        ////public static void PrintContentOfStemmedVocabularyDictionaryForValidation(Dictionary<StemWord, int> vocabularyDictionary, string fname)
        ////{
        ////    TextWriter tw = new StreamWriter(fname);
        ////    try
        ////    {
        ////        foreach (KeyValuePair<StemWord, int> entry in vocabularyDictionary)
        ////        {
        ////            // do something with entry.Value or entry.Key
        ////            //((StemmedWord)entry.Key).
        ////        }
        ////    }
        ////    catch (Exception)
        ////    {
        ////        throw;
        ////    }
        ////    finally
        ////    {
        ////        tw.Close();
        ////        tw.Dispose();
        ////    }
        ////}

        public static int[][] RemoveZeroEntriesFromMatrixThenRemoveZeroColumnsAndRows(int totalNumberOfWords, int[][] matrixValue,
           string fname, Dictionary<string, StemWord> dictionary)
        {
            //ListOfWordsToRemove = new List<int>();
            ListOfWordsToRemove = new List<string>();
            for (var i = 0; i < totalNumberOfWords; i++)
            {
                var count = 0;
                for (var j = 0; j < totalNumberOfWords; j++)
                {
                    if (i != j)
                        count += matrixValue[i][j];
                }
                if (count == 0)
                    ListOfWordsToRemove.Add(dictionary.ElementAt(i).Key);
            }

            int dim = totalNumberOfWords - ListOfWordsToRemove.Count;// + 2;
            int[][] newmatrixValue = MatrixOperations.CreateAndInitializeIntegerMatrix(dim, dim);

            TextWriter tw = new StreamWriter(fname);
            try
            {
                var newRow = -1;
                for (var k = 0; k < totalNumberOfWords; k++)
                {
                    var newColumn = -1;
                    //if (!CheckIfWordIsNotInRemoveList(dictionary.ElementAt(k).Key))

                    //var result = ListOfWordsToRemove.Contains(dictionary.ElementAt(k).Key);

                    if (ListOfWordsToRemove.Contains(dictionary.ElementAt(k).Key))
                        continue;
                    newRow++;
                    for (var j = 0; j < totalNumberOfWords; j++)
                    {
                        //if (!CheckIfWordIsNotInRemoveList(dictionary.ElementAt(j).Key))

                        //var result2 = ListOfWordsToRemove.Contains(dictionary.ElementAt(j).Key);
                        if (ListOfWordsToRemove.Contains(dictionary.ElementAt(j).Key))
                            continue;

                        newColumn++;
                        newmatrixValue[newRow][newColumn] = matrixValue[k][j];
                        tw.Write(matrixValue[k][j] + " ");
                    }
                    tw.WriteLine();
                }
                tw.Close();
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }

            return newmatrixValue;
        }

        public static void write_sparse_int_wordsFrequencies(int totalWords, int[][] matrixValue, string fname) //
        {
            TextWriter tw = new StreamWriter(fname);
            try
            {
                for (var i = 1; i <= totalWords; i++)
                {
                    for (var j = 1; j <= totalWords; j++)
                        tw.Write(matrixValue[i][j] + " ");
                    //tw.Write(string.Format(precision, matrixValue[i][j]) + " ");
                    tw.WriteLine();
                }
                tw.Close();
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }
        }

        public static decimal[,] WriteJointFrequencyForClusteredAgents(int[][] matrixValue,
            string fname, IEnumerable<StemWord> clusteredStemWords)
        {
            var stemWordsList = clusteredStemWords as IList<StemWord> ?? clusteredStemWords.ToList();
            var newMatrixValue = new decimal[stemWordsList.Count, stemWordsList.Count];
            var globalCounterX = -1;

            TextWriter tw = new StreamWriter(fname);
            try
            {
                foreach (var stemWord1 in stemWordsList)
                {
                    globalCounterX++;
                    var globalCounterY = -1;
                    foreach (var stemWord2 in stemWordsList)
                    {
                        //try
                        //{
                            globalCounterY++;
                            var result = matrixValue[stemWord1.Id][stemWord2.Id];
                            tw.Write(result + " ");
                            
                            //try
                            //{
                                newMatrixValue[globalCounterX, globalCounterY] = result;
                            //}
                            //catch (Exception e)
                            //{
                            //    Console.WriteLine(e);
                            //    throw;
                            //}

//                        }
                        //catch (Exception e)
                        //{
                        //    Console.WriteLine(e);
                        //    throw;
                        //}
                        
                    }
                    tw.WriteLine();
                }
                //tw.Close();
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }
            return newMatrixValue;
        }

        public static void write_sparse_decimal(int totalWords, decimal[][] matrixValue, string fname)
        {
            TextWriter tw = new StreamWriter(fname);
            try
            {
                for (var i = 1; i <= totalWords; i++)
                {
                    for (var j = 1; j <= totalWords; j++)
                        tw.Write(string.Format(Precision, matrixValue[i][j] / TotalNumberOfPairsWrtFrames) + " ");
                    //twGephi.Write(matrixValue[i][j] + " ");
                    tw.WriteLine();
                }
                //twGephi.WriteLine("{0} {1} {2}", i + 1, j + 1, matrixValue[i][j]);
                tw.Close();
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }
        }

        //This is a hack code
        //this dictionary is to account 


        //In this function as well, as I am writing the joint probability matrix, 
        //I can easily calculate the sum of all the probabilities of every row 
        //(which is the same value as the column too), that is the new marginal probability 
        //which is the sum of all the probabilities of this word.
        public static Dictionary<string, StemWord> write_sparse_decimal_gephi(int totalWords,
            int[][] matrixValue, string fnameGephi, string fname, string fnameDistributionPlot,
            List<string> stemmedWords, Dictionary<string, StemWord> vocabularyDictionary)
        {
            Dictionary<decimal, int> data = new Dictionary<decimal, int>();

            var fileName3 = fnameDistributionPlot.Replace("DistributionPlot", "DistributionPlotOriginalFormat");


            TextWriter twPlotSimple = new StreamWriter(fileName3);
            TextWriter twGephi = new StreamWriter(fnameGephi);
            TextWriter tw = new StreamWriter(fname);

            decimal totalMarginal = 0;//should add up to 1
            try
            {
                string stringHeader = stemmedWords.Aggregate(";", (current, pair) => current + (pair + ";"));
                twGephi.WriteLine(stringHeader);

                decimal marginalValueForWord_i;
                for (int i = 1; i <= totalWords; i++)
                {
                    marginalValueForWord_i = 0;

                    twGephi.Write(stemmedWords[i - 1] + ";");
                    for (int j = 1; j <= totalWords; j++)
                    {
                        decimal key = (decimal)matrixValue[i][j] / TotalNumberOfPairsWrtFrames;
                        twGephi.Write(string.Format(Precision, key) + ";");
                        tw.Write(string.Format(Precision, key) + " ");

                        marginalValueForWord_i += key;
                        //dataSimple.Add(key);
                        twPlotSimple.Write(string.Format(Precision, key) + " ");
                        if (!data.ContainsKey(key))
                        {
                            data.Add(key, 1);
                        }
                        else
                        {
                            data[key] = data[key] + 1;
                        }
                    }
                    twGephi.WriteLine();
                    tw.WriteLine();

                    //Get the word[i] from the stemmedWord dictionary and update its value at the end with the sum of all the j-values
                    vocabularyDictionary.FirstOrDefault(x => x.Value.Id == i).Value.MarginalProbability =
                        marginalValueForWord_i;
                    totalMarginal += marginalValueForWord_i;
                }
                twGephi.Close();
                tw.Close();
            }
            finally
            {
                twGephi.Close();
                twGephi.Dispose();
                tw.Close();
                tw.Dispose();
                twPlotSimple.Close();
                twPlotSimple.Dispose();
            }

            //HERE BUG!
            CalculateFrequencyDistribution(data, fnameDistributionPlot);

            //CalculateRankPlot(data, fnameDistributionPlot);

            ////CalculateFrequencyRankSimple(dataSimple, fnameDistributionPlot);
            //decimal result = totalMarginal;
            return vocabularyDictionary;
        }

        ////Might remove
        //public static Dictionary<StemWord, int> WriteClusteredAgentsJointProbabilityMatrix(int totalWords,
        //   int[][] matrixValue, string fnameGephi, string fname, string fnameDistributionPlot,
        //   List<string> stemmedWords, Dictionary<StemWord, int> vocabularyDictionary)
        //{
        //    Dictionary<decimal, int> data = new Dictionary<decimal, int>();

        //    var fileName3 = fnameDistributionPlot.Replace("DistributionPlot", "DistributionPlotOriginalFormat");


        //    TextWriter twPlotSimple = new StreamWriter(fileName3);
        //    TextWriter twGephi = new StreamWriter(fnameGephi);
        //    TextWriter tw = new StreamWriter(fname);

        //    decimal totalMarginal = 0;//should add up to 1
        //    try
        //    {
        //        string stringHeader = stemmedWords.Aggregate(";", (current, pair) => current + (pair + ";"));
        //        twGephi.WriteLine(stringHeader);

        //        Decimal marginalValueForWord_i;
        //        for (int i = 1; i <= totalWords; i++)
        //        {
        //            marginalValueForWord_i = 0;

        //            twGephi.Write(stemmedWords[i - 1] + ";");
        //            for (int j = 1; j <= totalWords; j++)
        //            {
        //                decimal key = (decimal)matrixValue[i][j] / TotalNumberOfPairsWrtFrames;
        //                twGephi.Write(string.Format(Precision, key) + ";");
        //                tw.Write(string.Format(Precision, key) + " ");

        //                marginalValueForWord_i += key;
        //                //dataSimple.Add(key);
        //                twPlotSimple.Write(string.Format(Precision, key) + " ");
        //                if (!data.ContainsKey(key))
        //                {
        //                    data.Add(key, 1);
        //                }
        //                else
        //                {
        //                    data[key] = data[key] + 1;
        //                }
        //            }
        //            twGephi.WriteLine();
        //            tw.WriteLine();

        //            //Get the word[i] from the stemmedWord dictionary and update its value at the end with the sum of all the j-values
        //            vocabularyDictionary.FirstOrDefault(x => x.Key.Id == i).Key.MarginalProbability =
        //               marginalValueForWord_i;
        //            totalMarginal += marginalValueForWord_i;
        //        }
        //        twGephi.Close();
        //        tw.Close();
        //    }
        //    finally
        //    {
        //        twGephi.Close();
        //        twGephi.Dispose();
        //        tw.Close();
        //        tw.Dispose();
        //        twPlotSimple.Close();
        //        twPlotSimple.Dispose();
        //    }

        //    //HERE BUG!
        //    CalculateFrequencyDistribution(data, fnameDistributionPlot);

        //    //CalculateRankPlot(data, fnameDistributionPlot);

        //    ////CalculateFrequencyRankSimple(dataSimple, fnameDistributionPlot);
        //    //decimal result = totalMarginal;
        //    return vocabularyDictionary;
        //}

        public static IEnumerable<StemWord> UpdateClusteredStemWordsWithIdsCorrespondingToNewProbabilityMatrixIndexWeird(
           IEnumerable<StemWord> stemWords, Dictionary<string, StemWord> vocabularyDictionary)
        {
            var newMatrixCounter = 0;
            ClusteringAgentModifier.VocabularyDictionary = vocabularyDictionary;
            var stemWordsList = stemWords as IList<StemWord> ?? stemWords.ToList();
            foreach (var stemWord in stemWordsList)
            {
                try
                {
                    stemWord.Id = ClusteringAgentModifier.GetClusteredStemWordId(stemWord);
                    stemWord.StatsMatrixId = newMatrixCounter;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                newMatrixCounter++;

            }
            return stemWordsList;
        }

        public static decimal[,] WriteReducedMatrix(
           int[][] matrixValue,
           string fname,
           IEnumerable<StemWord> stemWords)
        {
            var stemWord1S = stemWords as IList<StemWord> ?? stemWords.ToList();
            var totalWords = stemWord1S.ToList().Count;
            var data = new Dictionary<decimal, int>();
            var newMatrixValue = new decimal[totalWords, totalWords];
            var globalCounterX = -1;
            TextWriter tw = new StreamWriter(fname);

            try
            {
                foreach (var stemWord1 in stemWord1S)
                {
                    globalCounterX++;
                    var globalCounterY = -1;
                    foreach (var stemWord2 in stemWord1S)
                    {
                        decimal key;
                        if ((stemWord1.Id == -1) || (stemWord2.Id == -1))
                            key = 0;
                        else
                            key = matrixValue[stemWord1.Id][stemWord2.Id] / TotalNumberOfPairsWrtFrames;
                        tw.Write(string.Format(Precision, key) + " ");

                        globalCounterY++;
                        newMatrixValue[globalCounterX, globalCounterY] = key;

                        if (!data.ContainsKey(key))
                            data.Add(key, 1);
                        else
                            data[key] = data[key] + 1;
                    }
                    tw.WriteLine();
                }
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }
            return newMatrixValue;
        }

        public static Dictionary<StemWord, int> write_new_joint_probability_matrix_with_replacement_edgevalue_zero(
              int minimumEdge, int totalWords, int[][] matrixValue, string fnameGephi,
              string fname, string fnameDistributionPlot, List<string> stemmedWords,
              Dictionary<StemWord, int> vocabularyDictionary)
        {
            Dictionary<decimal, int> data = new Dictionary<decimal, int>();
            var fileName3 = fnameDistributionPlot.Replace("DistributionPlot", "DistributionPlot3Simple");
            TextWriter twPlotSimple = new StreamWriter(fileName3);
            TextWriter twGephi = new StreamWriter(fnameGephi);
            TextWriter tw = new StreamWriter(fname);

            decimal totalMarginal = 0;//should add up to 1
            try
            {
                var stringHeader = stemmedWords.Aggregate(";", (current, pair) => current + (pair + ";"));
                twGephi.WriteLine(stringHeader);

                decimal marginalValueForWord_i;
                for (int i = 1; i <= totalWords; i++)
                {
                    marginalValueForWord_i = 0;

                    twGephi.Write(stemmedWords[i - 1] + ";");
                    for (int j = 1; j <= totalWords; j++)
                    {
                        decimal key = (decimal)matrixValue[i][j] / TotalNumberOfPairsWrtFrames;
                        twGephi.Write(string.Format(Precision, key) + ";");
                        tw.Write(string.Format(Precision, key) + " ");

                        marginalValueForWord_i += key;
                        //dataSimple.Add(key);
                        twPlotSimple.Write(string.Format(Precision, key) + " ");
                        if (!data.ContainsKey(key))
                        {
                            data.Add(key, 1);
                        }
                        else
                        {
                            data[key] = data[key] + 1;
                        }
                    }
                    twGephi.WriteLine();
                    tw.WriteLine();

                    //Get the word[i] from the stemmedWord dictionary and update its value at the end with the sum of all the j-values

                    vocabularyDictionary.FirstOrDefault(x => x.Key.Id == i).Key.MarginalProbability =
                        marginalValueForWord_i;
                    totalMarginal += marginalValueForWord_i;
                }
                twGephi.Close();
                tw.Close();
            }
            finally
            {
                twGephi.Close();
                twGephi.Dispose();
                tw.Close();
                tw.Dispose();
                twPlotSimple.Close();
                twPlotSimple.Dispose();
            }

            CalculateFrequencyDistribution(data, fnameDistributionPlot);
            //decimal result = totalMarginal;
            return vocabularyDictionary;
        }

        public static decimal[,] WriteConditionalProbabilityForClusteredAgents(
            string fname, List<string> stemmedWords, IEnumerable<StemWord> clusteredStemWords)
        {
            var stemWordsList = clusteredStemWords as IList<StemWord> ?? clusteredStemWords.ToList();
            var newMatrixValue = new decimal[stemWordsList.Count, stemWordsList.Count];
            var globalCounterX = -1;

            TextWriter tw = new StreamWriter(fname);
            try
            {
                foreach (var stemWord1 in stemWordsList)
                {
                    globalCounterX++;
                    var globalCounterY = -1;
                    foreach (var stemWord2 in stemWordsList)
                    {
                        decimal printOut = ProbabilityCalculations.GetConditionalProbability(stemWord1.Id, stemWord2.Id);
                        tw.Write(string.Format(Precision, printOut) + " ");
                        globalCounterY++;
                        newMatrixValue[globalCounterX, globalCounterY] = printOut;
                    }
                    tw.WriteLine();
                }
                tw.Close();
            }
            finally
            {
                //tw.Close();
                tw.Dispose();
            }
            return newMatrixValue;
        }

        public static void write_sparse_decimal_conditionalProbability_gephi(int totalWords, int[][] matrixValue,
            string fname, string fname2, string fnameDistributionPlot, List<string> stemmedWords)
        {
            TextWriter tw = new StreamWriter(fname);
            TextWriter tw2 = new StreamWriter(fname2);
            Dictionary<decimal, int> data = new Dictionary<decimal, int>();
            string fileName3 = fnameDistributionPlot.Replace("DistributionPlot", "DistributionPlotOriginalFormat");
            TextWriter twPlotSimple = new StreamWriter(fileName3);
            try
            {
                string stringHeader = stemmedWords.Aggregate(";", (current, pair) => current + (pair + ";"));
                tw.WriteLine(stringHeader);

                for (var i = 1; i <= totalWords; i++)
                {
                    tw.Write(stemmedWords[i - 1] + ";");
                    for (var j = 1; j <= totalWords; j++)
                    {
                        decimal printOut = ProbabilityCalculations.GetConditionalProbability(i, j);
                        tw.Write(string.Format(Precision, printOut) + ";");
                        tw2.Write(string.Format(Precision, printOut) + " ");

                        //dataSimple.Add(printOut);
                        twPlotSimple.Write(string.Format(Precision, printOut) + " ");
                        if (data.ContainsKey(printOut))
                        {
                            data[printOut] = data[printOut] + 1;
                        }
                        else
                        {
                            data.Add(printOut, 1);
                        }
                    }
                    tw.WriteLine();
                    tw2.WriteLine();
                }
                tw.Close();
                tw2.Close();
            }
            finally
            {
                tw.Close();
                tw2.Close();
                tw.Dispose();
                tw2.Dispose();
                twPlotSimple.Close();
                twPlotSimple.Dispose();
            }
            CalculateFrequencyDistribution(data, fnameDistributionPlot);
        }

        public static void write_sparse_decimal_correlationcoefficient_gephi(int totalWords, int[][] matrixValue,
            string fname, string fname2, string fnameDistributionPlot, List<string> stemmedWords)
        {
            TextWriter tw = new StreamWriter(fname);
            TextWriter tw2 = new StreamWriter(fname2);
            var correlationCoefficientDict = new Dictionary<decimal, int>();
            var fileName3 = fnameDistributionPlot.Replace("DistributionPlot", "DistributionPlotOriginalFormat");
            TextWriter twPlotSimple = new StreamWriter(fileName3);

            try
            {
                var stringHeader = stemmedWords.Aggregate(";", (current, pair) => current + (pair + ";"));
                tw.WriteLine(stringHeader);

                //The reason the for loop goes from 1 and not from 0, is because I have words from index[0] - index[totalWords-1]
                for (var i = 1; i <= totalWords; i++)
                {
                    //var word1 = stemmedWords[i - 1];
                    tw.Write(stemmedWords[i - 1] + ";");
                    for (var j = 1; j <= totalWords; j++)
                    {
                        //var word2 = stemmedWords[j - 1];
                        var correlationCoefficientDecimal = ProbabilityCalculations.GetCorrelationCoefficient(i, j);
                        tw.Write(string.Format(Precision, correlationCoefficientDecimal) + ";");
                        tw2.Write(string.Format(Precision, correlationCoefficientDecimal) + " ");

                        twPlotSimple.Write(string.Format(Precision, correlationCoefficientDecimal) + " ");
                        if (correlationCoefficientDict.ContainsKey(correlationCoefficientDecimal))
                        {
                            correlationCoefficientDict[correlationCoefficientDecimal] = correlationCoefficientDict[correlationCoefficientDecimal] + 1;
                        }
                        else
                        {
                            correlationCoefficientDict.Add(correlationCoefficientDecimal, 1);
                        }
                    }
                    tw.WriteLine();
                    tw2.WriteLine();
                }
                tw.Close();
                tw2.Close();
            }
            finally
            {
                tw.Close();
                tw2.Close();
                tw.Dispose();
                tw2.Dispose();
                twPlotSimple.Close();
                twPlotSimple.Dispose();
            }
            CalculateFrequencyDistribution(correlationCoefficientDict, fnameDistributionPlot);
        }

        public static void write_sparse_decimal_pointwisemutualinformation_gephi(int totalWords, int[][] matrixValue,
            string fname, string fname2, string fnameDistributionPlot, List<string> stemmedWords)
        {
            TextWriter tw = new StreamWriter(fname);
            TextWriter tw2 = new StreamWriter(fname2);
            var data = new Dictionary<decimal, int>();
            string fileName3 = fnameDistributionPlot.Replace("DistributionPlot", "DistributionPlotOriginalFormat");
            TextWriter twPlotSimple = new StreamWriter(fileName3);

            try
            {
                var stringHeader = stemmedWords.Aggregate(";", (current, pair) => current + (pair + ";"));
                tw.WriteLine(stringHeader);

                for (var i = 1; i <= totalWords; i++)
                {
                    tw.Write(stemmedWords[i - 1] + ";");
                    for (var j = 1; j <= totalWords; j++)
                    {
                        var printOutDecimalValue = ProbabilityCalculations.GetPointWiseMutualInformation(i, j);
                        //if (printOut == "-Infinity") -Infinity
                        tw.Write(string.Format(Precision, printOutDecimalValue) + ";");
                        tw2.Write(string.Format(Precision, printOutDecimalValue) + " ");

                        var printOutDecimal = 0m;
                        printOutDecimal = double.IsInfinity(printOutDecimalValue) 
                            ? 999999999m 
                            : Convert.ToDecimal(printOutDecimalValue);

                        //dataSimple.Add(printOut);
                        twPlotSimple.Write(string.Format(Precision, printOutDecimalValue) + " ");
                        if (data.ContainsKey(printOutDecimal))
                        {
                            data[printOutDecimal] = data[printOutDecimal] + 1;
                        }
                        else
                        {
                            data.Add(printOutDecimal, 1);
                        }
                    }
                    tw.WriteLine();
                    tw2.WriteLine();
                }
                tw.Close();
                tw2.Close();
            }
            finally
            {
                tw.Close();
                tw2.Close();
                tw.Dispose();
                tw2.Dispose();
                twPlotSimple.Close();
                twPlotSimple.Dispose();
            }
            CalculateFrequencyDistribution(data, fnameDistributionPlot);
            ////CalculateFrequencyRankSimple(dataSimple, fnameDistributionPlot);
        }

        public static void write_sparse_decimal_logoddsratio_gephi(int totalWords, int[][] matrixValue,
            string fname, string fname2, string fnameDistributionPlot, List<string> stemmedWords, int horizon)
        {
            Horizon = horizon;
            TextWriter tw = new StreamWriter(fname);
            TextWriter tw2 = new StreamWriter(fname2);
            Dictionary<decimal, int> data = new Dictionary<decimal, int>();
            string fileNameDistributionPlot = fnameDistributionPlot.Replace("DistributionPlot", "DistributionPlotLogOddsRatioOriginalFormat");
            TextWriter twPlotSimple = new StreamWriter(fileNameDistributionPlot);

            try
            {
                var stringHeader = stemmedWords.Aggregate(";", (current, pair) => current + (pair + ";"));
                tw.WriteLine(stringHeader);

                for (var i = 1; i <= totalWords; i++)
                {
                    tw.Write(stemmedWords[i - 1] + ";");
                    for (var j = 1; j <= totalWords; j++)
                    {
                        double printOut = ProbabilityCalculations.GetLogOddsRatio(i, j, Horizon);
                        //if (printOut == "-Infinity") -Infinity
                        tw.Write(string.Format(Precision, printOut) + ";");
                        tw2.Write(string.Format(Precision, printOut) + " ");

                        decimal printOutDecimal = 0m;
                        printOutDecimal = double.IsInfinity(printOut) 
                            ? 999999999m 
                            : Convert.ToDecimal(printOut);

                        //dataSimple.Add(printOut);
                        twPlotSimple.Write(string.Format(Precision, printOut) + " ");
                        if (data.ContainsKey(printOutDecimal))
                        {
                            data[printOutDecimal] = data[printOutDecimal] + 1;
                        }
                        else
                        {
                            data.Add(printOutDecimal, 1);
                        }
                    }
                    tw.WriteLine();
                    tw2.WriteLine();
                }
                tw.Close();
                tw2.Close();
            }
            finally
            {
                tw.Close();
                tw2.Close();
                tw.Dispose();
                tw2.Dispose();
                twPlotSimple.Close();
                twPlotSimple.Dispose();
            }
            CalculateFrequencyDistribution(data, fnameDistributionPlot);
            ////CalculateFrequencyRankSimple(dataSimple, fnameDistributionPlot);
        }

        public static void CalculateFrequencyDistribution(Dictionary<decimal, int> data, string fileName)
        {
            //template:
            //Dictionary<decimal, int> sortedDict = (from entry in data orderby entry.Key descending select entry).
            //        ToDictionary(pair => pair.Key, pair => pair.Value);

            //Remove the 0 edge values 
            data.Remove(0);

            //sorts based on the value of the edge..
            Dictionary<decimal, int> firstSortedDict = (data.OrderByDescending(entry => entry.Key)).
                ToDictionary(pair => pair.Key, pair => pair.Value);

            decimal highestEdgeValue = firstSortedDict.FirstOrDefault().Key;

            Dictionary<decimal, int> sortedDict = (data.OrderByDescending(entry => entry.Key)).
                ToDictionary(pair => pair.Key, pair => pair.Value);


            //sorts based on the count (instances) of the edge..
            //Dictionary<decimal, int> sortedDict = (data.OrderByDescending(entry => entry.Value)).
            //    ToDictionary(pair => pair.Key, pair => pair.Value);

            var fileNameNormalized = fileName.Replace("DistributionPlot", "DistributionPlotNormalized");
            TextWriter textWriterNormalized = new StreamWriter(fileNameNormalized);

            var fileName3 = fileName.Replace("DistributionPlot", "DistributionPlotNotNormalized");
            //var fileName3 = fileName.Replace("DistributionPlotNormalized", "DistributionPlotRaw");
            //var fileName3 = fileName.Replace("DistributionPlot", "DistributionPlotRaw");
            TextWriter textWriterNotNormalized = new StreamWriter(fileName3);

            try
            {
                foreach (KeyValuePair<decimal, int> kvp in sortedDict)
                {
                    textWriterNormalized.WriteLine(string.Format(Precision, kvp.Key / highestEdgeValue) + "\t" + kvp.Value);
                    textWriterNotNormalized.WriteLine(string.Format(Precision, kvp.Key) + "\t" + kvp.Value);
                }
            }
            finally
            {
                textWriterNormalized.Close();
                textWriterNormalized.Dispose();
                textWriterNotNormalized.Close();
                textWriterNotNormalized.Dispose();
            }
        }

        public static void CalculateRankPlot(Dictionary<decimal, int> data, string fileName)
        {
            //template:
            //Dictionary<decimal, int> sortedDict = (from entry in data orderby entry.Key descending select entry).
            //        ToDictionary(pair => pair.Key, pair => pair.Value);

            //Remove the 0 edge values 
            data.Remove(0);

            //var itemList = from item in this.ObjectContext.TreeMembers
            //               where item.UserId == userId
            //               let age = EntityFunctions.DiffYears
            //                              (item.Birthdate, DateTime.Now) ?? 0
            //               group item by age / 10 into ageGroup
            //               select new
            //               {
            //                   AgeInCompletedDecades = ageGroup.Key,
            //                   Count = ageGroup.Count()
            //               };


            //sorts based on the value of the edge..
            Dictionary<decimal, int> firstSortedDict = (data.OrderByDescending(entry => entry.Key)).
                ToDictionary(pair => pair.Key, pair => pair.Value);

            decimal highestEdgeValue = firstSortedDict.FirstOrDefault().Key;

            Dictionary<decimal, int> sortedDict = (data.OrderByDescending(entry => entry.Key)).
                ToDictionary(pair => pair.Key, pair => pair.Value);



            ////////////////////////////

            //var los = new List<ListObject>();
            //string sumField = "Value1";

            var result = firstSortedDict
                .GroupBy(lo => lo.Key).ToList();


            //var los = new List<ListObject>();
            //string sumField = "Value1";

            //var result = los
            //    .Where(lo => lo.string2 == "June")
            //    .GroupBy(lo => lo.string1)
            //    .Select(g => new {
            //        Key = g.Key,
            //        Total = g
            //            .Where(x => x.Values.ContainsKey(sumField))
            //            .Sum(s => s.Values[sumField]),
            //    })
            //    .OrderByDescending(t => t.Total)
            //    .Take(5);
            ////////////////////////////



            //sorts based on the count (instances) of the edge..
            //Dictionary<decimal, int> sortedDict = (data.OrderByDescending(entry => entry.Value)).
            //    ToDictionary(pair => pair.Key, pair => pair.Value);

            var fileNameNormalized = fileName.Replace("DistributionPlot", "RealRankPlotNormalized");
            TextWriter textWriterNormalized = new StreamWriter(fileNameNormalized);

            var fileNameNotNormalized = fileName.Replace("DistributionPlot", "RealRankPlotNotNormalized");
            TextWriter textWriterNotNormalized = new StreamWriter(fileNameNotNormalized);

            try
            {
                foreach (KeyValuePair<decimal, int> kvp in sortedDict)
                {
                    textWriterNormalized.WriteLine(string.Format(Precision, kvp.Key / highestEdgeValue) + "\t" + kvp.Value);
                    textWriterNotNormalized.WriteLine(string.Format(Precision, kvp.Key) + "\t" + kvp.Value);
                }
            }
            finally
            {
                textWriterNormalized.Close();
                textWriterNormalized.Dispose();
                textWriterNotNormalized.Close();
                textWriterNotNormalized.Dispose();
            }
        }

        /// <summary>
        /// Takes a string containing decimal numbers and multiplies the numbers with the given number and 
        /// leaves the number of decimal places after the point. Negative values are turned into 0.
        /// </summary>
        /// <param name="line">String with decimals</param>
        /// <param name="placesAfterDecimal">Number of decimal to leave</param>
        /// <param name="numberToMultiplyWith">Number to multiply with</param>
        /// <returns></returns>
        public static string ConvertStringWithDecimals(string line, int placesAfterDecimal,
            int numberToMultiplyWith)
        {
            //Extract all numbers from string and put in a list of integers.
            // NOTES: about the LINQ:
            // .Where() == filters the IEnumerable (which the array is)
            //     (c=>...) is the lambda for dealing with each element of the array
            //     where c is an array element.
            // .Trim()  == trims all blank spaces at the start and end of the string
            //var doubleArray = Regex.Split(sentence, @"[^0-9\.]+")
            //  .Where(c => c != "." && c.Trim() != "");


            var doubleArray = Regex.Split(line, @"[^0-9\.]+")
                .Where(c => c != "." && c.Trim() != "");

            //You could also use Linq's Aggregate extension method to do the same thing:
            //using System.Linq; // put with other using directives
            //int prod = numbers.Aggregate(1, (a, b) => a * b);
            // in case I wanted the result...

            var enumerable = doubleArray as IList<string> ?? doubleArray.ToList();
            if (!enumerable.Any())
                return line;

            //extract the first word (the rest are results)
            var word = line.Split(';')[0];

            //convert to double and multiply by the number
            var multiplyDoubleArray = enumerable.Select(x => Convert.ToDouble(x) * numberToMultiplyWith).ToArray();


            //replace all -ve with zero
            //var doubleArrayNoNegative = doubleArray.Select(x => Convert.ToDouble(x)*numberToMultiplyWith).ToArray();
            //var size = 0;
            //if (placesAfterDecimal == 1)
            //    size = 10;
            //if (placesAfterDecimal == 2)
            //    size = 100;
            //if (placesAfterDecimal == 3)
            //    size = 1000;

            var doubleArrayNoNegative = new double[100];
            var counter = 0;
            foreach (var val in multiplyDoubleArray)
            {
                if (val < 0)
                    doubleArrayNoNegative[counter] = 0;
                else
                {
                    doubleArrayNoNegative[counter] = val;
                }
                counter++;
            }
            //strip the extra decimals
            //decimal dec = decimal.Parse(decimalAsString);
            //string s = dec.ToString("0.#");

            var parameter = Convert.ToString(placesAfterDecimal) + ".#";

            var resultDoubleArray = placesAfterDecimal != 0
                ? doubleArrayNoNegative.Select(x => x.ToString(parameter)).ToArray()
                : doubleArrayNoNegative.Select(x => x.ToString(CultureInfo.InvariantCulture).Split('.')[0]).ToArray();

            //convert it back to a string that is separated by semi-columns (I know it's weird how a comma does it)
            var result = word + ";" + string.Join(";", resultDoubleArray.Select(p => p.ToString(CultureInfo.InvariantCulture)).ToArray());

            return result;
        }
    }
}