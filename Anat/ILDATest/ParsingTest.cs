﻿//using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System.Collections.Generic;

//namespace ILDA.Tests
//{
//   [TestClass]
//   public class ParsingTest
//   {
//      private const string File = @"C:\Users\sdoumit\Desktop\LDARunSettings.txt";
//      private const int RandomSeed = 77;
//      private readonly DateTime _startDate = new DateTime(2010, 12, 5);

//      public IEnumerable<string> GetLines()
//      {
//         return System.IO.File.ReadAllLines(File);
//      }

//        [TestMethod]
//        public void CheckThatRandomSeedIsCorrectlyProcessed()
//        {
//            Assert.AreEqual(
//               ILDA.Primitives.RandomSeed.FromValue(RandomSeed),
//               SimulationRunSettingsFileParser.ProcessLDASettingsValues(File).RandomSeed);
//        }

//        [TestMethod]
//        public void CheckThatStartDateIsCorrectlyProcessed()
//        {
//            Assert.AreEqual(
//               ILDA.Primitives.StartDate.FromValue(_startDate),
//               Parser.SimulationRunSettingsFileParser.ProcessLDASettingsValues(File).StartDate);
//        }

//        [TestMethod]
//        public void CheckThatFinishDateDoesNotHaveTheStartValue()
//        {
//            Assert.AreNotEqual(
//               ILDA.Primitives.FinishDate.FromValue(_startDate),
//               Parser.SimulationRunSettingsFileParser.ProcessLDASettingsValues(File).FinishDate);
//        }
//    }
//}
