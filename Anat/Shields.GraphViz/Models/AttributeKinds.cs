﻿namespace Shields.GraphViz.Models
{
    /// <summary>
    /// Attributes
    /// </summary>
    public enum AttributeKinds
    {
        /// <summary>
        /// Graph attribute
        /// </summary>
        Graph,
        /// <summary>
        /// Node attribute
        /// </summary>
        Node,
        /// <summary>
        /// Edge attribute
        /// </summary>
        Edge
    }
}
