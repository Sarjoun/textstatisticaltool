﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shields.GraphViz.Models
{
    /// <summary>
    /// 
    /// </summary>
    public enum GraphKinds
    {
        Undirected,
        Directed
    }
}
