﻿using System;
using Iveonik.Stemmers;

namespace StemmerDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Test();
        }

        private static void OriginalTest()
        {
            TestStemmer(new RussianStemmer(), "краcОта", "красоту", "красоте", "КрАсОтОй");
            TestStemmer(new EnglishStemmer(), "jump", "jumping", "jumps", "jumped");
            TestStemmer(new GermanStemmer(), "mochte", "mochtest", "mochten", "mochtet");
            Console.ReadKey();
        }

        private static void Test()
        {
            //TestStemmer(new EnglishStemmer(), "attack", "attacks", "attacked", "attacker", "attackers", 
            //    "attacking", "attackable", "release", "released", "region", "regions", "regional");
            TestStemmer(new EnglishStemmer(), "presidency", "president", "presidential", "print", "printing", "printer");


            Console.WriteLine();
            Console.Write("Please enter any key to proceed: ");
            Console.ReadKey();
        }

        private static void TestStemmer(IStemmer stemmer, params string[] words)
        {
            Console.WriteLine("Stemmer Test: (stemmer type " + stemmer + ")\r\n");
            var counter = 1;
            foreach (var word in words)
            {
                Console.WriteLine(counter++ + ") " + word + " --> " + stemmer.Stem(word));
            }
        }
    }
}
