﻿using System;
using System.Linq;
using System.Diagnostics;
using SDParser.Primitives;
using SDParser.FileFunctions;
using SDParser.Notifications;
using SDParser.ClusteringTypes;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using LeagueOfMonads;
using SDParser.CustomFileParsers;

namespace BasicClustering
{
    public class BasicClusteringAlgorithm
    {
        public string ClusteringName;
        private static List<Agent> _agents;
        private const string RFileName = @"C:\Program Files\R\R-3.4.4\bin\x64\Rscript.exe";
        private const string RWorkingDirectory = @"C:\Program Files\R\R-3.4.4\bin\x64";

        public BasicClusteringAlgorithm(string clusteringName)
        {
            ClusteringName = clusteringName;
        }

        public string CreateFileHeaderFromNGrams(List<Agent> agentsPoints)
        {
            return agentsPoints.Aggregate(
                "FileId,", (current, agentPoint) => current +
                agentPoint.NameForCorrelationAnalysis + ",").TrimEnd(',');
        }

        public static void ExecuteRScript(string rScriptPath)
        {
            try
            {
                using (var proc = new Process())
                {
                    proc.StartInfo.FileName = RFileName;
                    proc.StartInfo.WorkingDirectory = RWorkingDirectory;
                    proc.StartInfo.Arguments = rScriptPath;
                    proc.StartInfo.UseShellExecute = true;
                    proc.StartInfo.RedirectStandardOutput = false;
                    proc.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
                    proc.Start();
                    proc.WaitForExit(120000);
                }
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit("Exception in ExecuteRScript: " + e);
            }
        }

        public string UsingRforGettingCorrelatedNgramsInFileUsingRScript(WordStreamFileName wordStreamFileName)
        {
            var filePathForFileNgramMatrixOutput =
                PathNameGenerator.GetFilePathForFileNgramMatrixOutput(wordStreamFileName);

            var filePathForFileNgramMatrixRScript =
                PathNameGenerator.GetFilePathForFileNgramMatrixRscript(wordStreamFileName.Value);

            var filePathForFileNgramMatrixRScriptExecutionResult =
                PathNameGenerator.GetFilePathForFileNgramMatrixRscriptOutput(wordStreamFileName.Value);

            var filePathForFileNgramMatrixRScriptExecutionHaCoutputResult =
                PathNameGenerator.GetFilePathForFileNgramMatrixRscriptHACoutputGraph(wordStreamFileName.Value);

            //Write R-Script to file
            FileWriter.WriteStringToFile(
                RScriptGenerator.GenerateRScriptToCalculatePairsOfHighlyCorrelatedNgramPairs(
                    filePathForFileNgramMatrixOutput,
                    filePathForFileNgramMatrixRScriptExecutionResult,
                    filePathForFileNgramMatrixRScriptExecutionHaCoutputResult),
                filePathForFileNgramMatrixRScript);

            //Execute R-Script - output written to file
            ExecuteRScript(filePathForFileNgramMatrixRScript);

            return filePathForFileNgramMatrixRScriptExecutionResult;
        }

        private List<Agent> AssignTagFilesToAgentsAndWriteOutputToFile(
            List<Agent> agents, WordStreamFileName wordStreamFileName)
        {
            //Read the wordStreamFileName_StemmedWords
            var allFilesContentsUsingStemWords = ReadTheWordStreamFileWithStemWords(wordStreamFileName);

            //Need a function that takes an array of word grams and the available NGrams then determines
            //for every NGram if the percentage of matching: 0, 1/n, 2/n, ... n/n where n is the number of 
            //grams in the ngram and then writes it out to a file

            var analysisResult = new List<string> { CreateFileHeaderFromNGrams(agents) };

            var fileCounter = 1;
            if (agents.Count == 0)
                return agents;

            //var agentNgramThreshold = Math.Round(1 / ((double)agents.First().Grams.Count) / 2);
            var agentNgramThreshold = Math.Round((double)agents.First().Grams.Count / 2);
            //so that 0.6666 is 0.5

            foreach (var fileContentStemWord in allFilesContentsUsingStemWords)
            {
                var result = "File" + fileCounter++ + ",";
                //var result = Path.GetFileName(fileContent).Substring(0,6) + ",";

                foreach (var agent in agents)
                {
                    var tagValue = DetermineMatchValueForNGramAndFileNormalized(
                        ExtractNgramsFromAgent(agent),
                        ExtractNgramsFromFileLineEntry(fileContentStemWord));
                    if (tagValue >= agentNgramThreshold)
                        agent.TaggedDocumentsId.Add(fileCounter - 1);

                    result += tagValue + ",";
                }
                analysisResult.Add(result.TrimEnd(','));
            }

            FileWriter.WriteListToFile(analysisResult,
                PathNameGenerator.GetFilePathForFileNgramMatrixOutput(wordStreamFileName));

            //TODO: ADD a routine that would either read the file or while the file is created and add the tagged file value.
            return agents;
        }

        private static Agent MergeAgentsWithSameClusterIdIntoSameAgent(
                   IEnumerable<Agent> agentsBelongingToSameCluster, int clusterId)
        {
            var updatedListOfAgents = new List<Agent>();

            foreach (var agent in agentsBelongingToSameCluster)
            {
                agent.ClusterId = clusterId;
                updatedListOfAgents.Add(agent);
            }
            return MergeAgentsBelongingToSameCluster(updatedListOfAgents);
        }

        public static Agent MergeAgentsBelongingToSameCluster(List<Agent> agents)
        {
            if (agents.Count < 2)
            {
                ShowMessage.ShowWarningMessageAndExit("Error in StemWordNGramsManager.cs" +
                                                      " MergeAgentsBelongingToSameCluster." +
                                                      " The agents list has less than 2 elements");
            }

            var mergedStemWordNGram = TakeTwoAgentsAndMergeTheirClusterIdsAndGramsAndTaggedFileIds(
                agents[0], agents[1]);

            return agents.Skip(2).
                Aggregate(
                    mergedStemWordNGram,
                    TakeTwoAgentsAndMergeTheirClusterIdsAndGramsAndTaggedFileIds);
        }

        public static Agent TakeTwoAgentsAndMergeTheirClusterIdsAndGramsAndTaggedFileIds(Agent agent1, Agent agent2)
        {
            var ulist = agent1.Grams.Union(agent2.Grams).ToList();

            if (agent1.ClusterId != null)
                return new Agent(
                    agent1.Id < agent2.Id ? agent1.Id : agent2.Id,
                    agent1.ClusterId.Value,
                    NumberOfWordsInStartNGRAM.FromValue(ulist.Count),
                    ulist,
                    (agent1.Popularity + agent1.Popularity) / 2,
                     agent1.TaggedDocumentsId.Union(agent2.TaggedDocumentsId).ToList());

            throw new Exception("Agent\\StemWordNGram " + agent1.GramToStringUsingRoot() + " does not belong to any cluster.");
        }
        private Dictionary<int, List<Agent>> TransformDictionaryOfClusteredNgramStringsIntoListOfAgents(Dictionary<int, List<string>> clusteredDictionary)
        {
            return clusteredDictionary
                .ToDictionary(entry => entry.Key, entry => ConvertListOfNgramsToListOfAgents(entry.Value));
        }

        private List<Agent> ConvertListOfNgramsToListOfAgents(IEnumerable<string> ngrams)
        {
            return ngrams.Select(ConvertStringToNgram).ToList();
            //return ngrams.Select(ngram => ConvertStringToNgramWithClusterId(ngram, clusterId)).ToList();

            //List<Agent> result = new List<Agent>();
            //foreach (var ngram in ngrams)
            //{
            //    result.Add(ConvertStringToNgramWithClusterId(ngram, clusterId));
            //}
            //return result;
        }

        private static Agent ConvertStringToNgram(string ngram)
        {
            return _agents.Find(x => x.NameForCorrelationAnalysis == ngram);
        }

        static int _globalUniqueId = 1;
        public static int GetUniqueId()
        {
            return _globalUniqueId++;
        }

        public static Dictionary<int, List<string>> ClusterTheNGramsBasedOnHighestCorrelatedPairs(
            List<List<string>> targetList)
        {
            _globalUniqueId = 1;
            var processedList = new List<string>();
            var clusteringDictionary = new Dictionary<int, List<string>>();

            try
            {
                foreach (var list in targetList)
                {
                    var a = list.ElementAt(0);
                    var b = list.ElementAt(1);

                    int? aKey = null;
                    int? bKey = null;

                    if (processedList.Contains(a))
                    {
                        aKey = clusteringDictionary.FirstOrDefault(x => x.Value.Contains(a)).Key;
                    }

                    if (processedList.Contains(b))
                    {
                        bKey = clusteringDictionary.FirstOrDefault(x => x.Value.Contains(b)).Key;
                    }

                    if (aKey == null && bKey == null)
                    {
                        clusteringDictionary.Add(GetUniqueId(), list);
                        processedList.Add(a);
                        processedList.Add(b);
                    }
                    else if (aKey == null)
                    {
                        clusteringDictionary[bKey.Value].Add(a);
                        processedList.Add(a);
                    }
                    else if (bKey == null)
                    {
                        clusteringDictionary[aKey.Value].Add(b);
                        processedList.Add(b);
                    }
                    else
                    {
                        if (!clusteringDictionary[aKey.Value].Contains(b))
                            clusteringDictionary[aKey.Value].Add(b);
                        if (!clusteringDictionary[bKey.Value].Contains(a))
                            clusteringDictionary[bKey.Value].Add(a);
                    }
                }
                var newResult = AnalyzeAndClusterSecondCheck(clusteringDictionary);
                return newResult;
            }
            catch (Exception e)
            {
                ShowMessage.ShowWarningMessage("Exception in ClusterTheNGramsBasedOnHighestCorrelatedPairs: "
                    + e.Message);
                return clusteringDictionary;
            }
        }

        public static Dictionary<int, List<string>> AnalyzeAndClusterSecondCheck(
            Dictionary<int, List<string>> firstCluster)
        {
            //if an entry is a subset of another single entry then it should go away
            var clusteringDictionary = new Dictionary<int, List<string>>();
            var newCounter = 1;

            try
            {
                for (var i = 1; i <= firstCluster.Count; i++)
                {
                    var canBeAdded = true;
                    for (var j = 1; j <= firstCluster.Count; j++)
                    {
                        if (i == j) continue;
                        if (!firstCluster[i].Except(firstCluster[j]).Any())
                            canBeAdded = false;
                    }

                    if (canBeAdded)
                        clusteringDictionary.Add(newCounter++, firstCluster[i]);
                }
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit("Exception in AnalyzeAndClusterSecondCheck: " + e);
            }
            return clusteringDictionary;
        }

        public async Task<List<Agent>> ClusterTheAgentsAndReturnTheClusters(
            List<Agent> agents, WordStreamFileName wordStreamFileName, DocumentsLinksFileName originalDocumentsLinksFileName)
        {
            _agents = AssignTagFilesToAgentsAndWriteOutputToFile(
                agents.InitializeAndResetTheListOfTaggedDocumentsInTheAgents(),
                wordStreamFileName);

            var fileNameWithHighlyCorrelatedNgramCouples =
                UsingRforGettingCorrelatedNgramsInFileUsingRScript(wordStreamFileName);

            var tempListOfHighlyCorrelatedNgramCouples = FileReader
                .ReadFileAsync(fileNameWithHighlyCorrelatedNgramCouples).Result.ToList();
            var coupledNgrams = new List<List<string>>();

            foreach (var entry in tempListOfHighlyCorrelatedNgramCouples.Skip(1))
            {
                var entryList = new List<string>();
                var splitStringResults = entry.Split(new[] { '"' }).SelectMany((s, i) =>
                {
                    if (i % 2 == 1) return new[] { s.Replace(" ", "") };
                    return s.Split(new[] { ' ', ';' }, StringSplitOptions.RemoveEmptyEntries);
                }).ToList();

                entryList.Add(splitStringResults.ElementAt(1));
                entryList.Add(splitStringResults.ElementAt(2));
                coupledNgrams.Add(entryList);
            }

            var dictionaryOfClusteredNGramStringsRepresentations =
                ClusterTheNGramsBasedOnHighestCorrelatedPairs(coupledNgrams);

            var dictionaryOfClusteredAgents =
                TransformDictionaryOfClusteredNgramStringsIntoListOfAgents(
                    dictionaryOfClusteredNGramStringsRepresentations);

            WriteContentsOfClusteredAgentsDictionaryToFile(
                dictionaryOfClusteredAgents,
                PathNameGenerator.GetRinitialClusteringFileName(wordStreamFileName));

            var clusteredAgents = dictionaryOfClusteredAgents
                     .Select(entry => MergeAgentsWithSameClusterIdIntoSameAgent(entry.Value, entry.Key))
                     .ToList();

            foreach (var cluster in clusteredAgents)
            {
                var storyTitle = cluster.GramToStringUsingMostPopular().Split(Agent.GramSeparatorBOWSymbol).Take(3).Aggregate((i, j) => i + " " + j);

                //create folder and populate it with its stuff
                var clusterPath = FolderFunctions.CreateClusterStoryGraphFolderWithStoryTitle(
                    PathNameGenerator.ExtractDirectoryPathForStoryCluster(wordStreamFileName),
                    storyTitle);

                //create document file for this cluster from its tagged files
                await SetUpDocumentsFileForClusterAsync(
                    originalDocumentsLinksFileName,
                    cluster.TaggedDocumentsId,
                    PathNameGenerator.GetDocumentLinksFilePathForStory(clusterPath, storyTitle));

                //pull file path from (original) main documents file based on the id of the file
            }
            return clusteredAgents;
        }
        
        private static async Task<IEnumerable<string>> SetUpDocumentsFileForClusterAsync(
            DocumentsLinksFileName originalSourceDocumentsFile,
            List<int> taggedDocumentsIds,
            string documentsFilePath)
        {
            try
            {
                return await CreateTaggedDocumentsFileFromOriginalForStoryCluster.GetResultingTaggedDocumentsFileAsync(
                    originalSourceDocumentsFile, taggedDocumentsIds)
                    .Tea(x => SDParser.FileFunctions.FileWriter.WriteListToFileAsync(x.ToList(), documentsFilePath));
            }
            catch (Exception e)
            {
                throw new Exception("Could not create the base file for the TextStatistics part. " + e.Message);
            }
        }


        private static void WriteContentsOfClusteredAgentsDictionaryToFile(
            Dictionary<int, List<Agent>> clusteringDictionary,
            string fileName)
        {
            //print out the outcome of the clustering of the Ngrams from R
            var clustersPrintout = "";
            var divider = "\r\n---------------------------------------\r\n";
            var clusterCounter = 1;
            foreach (var cluster in clusteringDictionary)
            {
                List<int> unionOfTaggedDocuments = new List<int>();
                var clusterMembers = "";
                foreach (var item in cluster.Value)
                {
                    clusterMembers += item.Name + "\t:" + "(" + item.TaggedDocumentsId.Count + ")" + item.TaggedDocumentsIdForDisplay + "\r\n";
                    unionOfTaggedDocuments = unionOfTaggedDocuments.Union(item.TaggedDocumentsId).ToList();
                }
                var totalFiles = "(" + unionOfTaggedDocuments.Count + ")" + unionOfTaggedDocuments
                    .OrderBy(v => v)
                    .Aggregate("", (current, entry) => current + (entry + ",")).TrimEnd(',');

                clustersPrintout += "Cluster " + clusterCounter++ + " : " + totalFiles + "\r\n"
                    + clusterMembers.Trim() + divider;
            }

            FileWriter.WriteStringToFile(clustersPrintout, fileName);
        }

        private static string[] ExtractNgramsFromAgent(Agent agent)
        {
            return agent.Name.Split('+');
        }
        private static string[] ExtractNgramsFromFileLineEntry(string fileEntry)
        {
            return fileEntry.Split('\t');
        }
        private static string[] ReadTheWordStreamFileWithStemWords(WordStreamFileName wordStreamFileName)
        {
            return FileReader.ReadFileAsync(wordStreamFileName.Value.Replace(".txt", "_StemmedWords.txt")).Result.ToArray();
        }
        private static float DetermineMatchValueForNGramAndFileNormalized(
            string[] stemWordsInNgram, string[] fileStemWordContents)
        {
            return (float)stemWordsInNgram.Intersect(fileStemWordContents, StringComparer.OrdinalIgnoreCase).ToList().Count / stemWordsInNgram.Length;
        }


    }
}
