﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SDParser.CustomFileParsers;
using SDParser.Extensions;
using SDParser.Primitives;

namespace BasicClustering
{
    public static class CreateTaggedDocumentsFileFromOriginalForStoryCluster
    {
        public static async Task<IEnumerable<string>> GetResultingTaggedDocumentsFileAsync(
            DocumentsLinksFileName originalDocumentsLinksFileName,
            List<int> taggedDocumentsIds)
        {
            var originalDocumentsLinks = await LineByLineParsingFunctions.ReadLinesAsync(originalDocumentsLinksFileName.Value);
            var originalDocumentsLinksListWithoutIndexAtStart = originalDocumentsLinks.Select(x => x.RemoveInitialIndexTag()).ToList();
            return taggedDocumentsIds.Select(id => originalDocumentsLinksListWithoutIndexAtStart[id]).ToList();
        }

        public static IEnumerable<string> GetResultingTaggedDocumentsFile(
            DocumentsLinksFileName originalDocumentsLinksFileName,
            List<int> taggedDocumentsIds)
        {
            var originalDocumentsLinks = LineByLineParsingFunctions.ReadLines(originalDocumentsLinksFileName.Value);
            var originalDocumentsLinksListWithoutIndexAtStart = originalDocumentsLinks.Select(x => x.RemoveInitialIndexTag()).ToList();
            return taggedDocumentsIds.Select(id => originalDocumentsLinksListWithoutIndexAtStart[id]).ToList();
        }

    }
}


