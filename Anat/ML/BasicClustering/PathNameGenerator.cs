﻿using System;
using System.IO;
using SDParser.Primitives;

namespace BasicClustering
{
    public static class PathNameGenerator
    {
        //Using WordStreamFileName as a basis to extract the path
        public static string GetRinitialClusteringFileName(WordStreamFileName wordStreamFileName)
        {
            var temp = wordStreamFileName.Value.Replace("WordStream", "ClusteringAnalysis4");
            return temp.Replace(".txt", "_R_InitialClusteringResultsFromRscript.txt");
        }

        public static string GetFilePathForFileNgramMatrixOutput(WordStreamFileName wordStreamFileName)
        {
            var temp = wordStreamFileName.Value.Replace("WordStream", "ClusteringAnalysis1");
            return temp.Replace(".txt", "_FileNgramMatrix.txt");
        }

        public static string GetFilePathForFileNgramMatrixRscript(string wordStreamFileNameNgramMatrix)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            var fileName = Path.GetFileNameWithoutExtension(wordStreamFileNameNgramMatrix);
            var filePath = fileName + "_RScript.r";
            return path + "\\" + filePath.Replace("WordStream-", "NGramAnalysis-");
        }

        public static string GetFilePathForFileNgramMatrixRscriptOutput(string wordStreamFileNameNgramMatrix)
        {
            var filePath = wordStreamFileNameNgramMatrix.Replace(".txt", "_RScript_CorrelatedPairsResult.txt");
            return filePath.Replace("WordStream-", "ClusteringAnalysis3-");
        }

        // ReSharper disable once InconsistentNaming
        public static string GetFilePathForFileNgramMatrixRscriptHACoutputGraph(string wordStreamFileNameNgramMatrix)
        {
            var filePath = wordStreamFileNameNgramMatrix.Replace(".txt", "_RScript_CorrelatedMatrixHAC.jpeg");
            filePath = filePath.Replace("WordStream-", "ClusteringAnalysis2-");
            return filePath;
        }
        
        public static string ExtractDirectoryPathForStoryCluster(WordStreamFileName wordStreamFileName)
        {
            return Path.GetDirectoryName(wordStreamFileName.Value) + Path.DirectorySeparatorChar;
        }

        public static string GetDocumentLinksFilePathForStory(string clusterPath, string storyName)
        {
            return clusterPath + "\\" + "DocumentsLinks - " + storyName + ".txt";
        }

    }
}