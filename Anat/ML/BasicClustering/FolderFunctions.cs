﻿using System.IO;

namespace BasicClustering
{
    public static class FolderFunctions
    {
        public static string CreateClusterStoryGraphFolderWithClusterIdAndStoryTitle(string currentCorpusTierFolderPath, 
            int clusterId, string storyTitle)
        {
            //create the folder name
            var folderName = currentCorpusTierFolderPath + "\\" + "Cluster" + clusterId + "-" + storyTitle;

            //create the folder
            if (!Directory.Exists(folderName))
                Directory.CreateDirectory(folderName);
            return folderName;
        }

        public static string CreateClusterStoryGraphFolderWithStoryTitle(string currentCorpusTierFolderPath, 
            string storyTitle)
        {
            //create the folder name
            var folderName = currentCorpusTierFolderPath + "\\[Story] " + storyTitle;

            //create the folder
            if (!Directory.Exists(folderName))
                Directory.CreateDirectory(folderName);
            return folderName;
        }

    }
}
