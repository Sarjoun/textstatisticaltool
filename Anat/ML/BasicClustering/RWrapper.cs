﻿using System;
using RDotNet;

namespace BasicClustering
{
    public static class RWrapper
    {
        private static REngine _engine;
        public static void MyCorrelation(string destinationFilePath, REngine engine)
        {
            _engine = engine;

            _engine.Evaluate("library(\"Hmisc\")");
            _engine.Evaluate("library(\"reshape2\")");

            var filePath = destinationFilePath;
            var myData = _engine.Evaluate("myData <- read.csv(" + filePath + ", row.names = 1, header = TRUE);");

            var res = _engine.Evaluate("res <- cor(myData)");
            var col = _engine.Evaluate("col <- c('#2F2C62', '#42399B', '#4A52A7', '#59AFEA', '#7BCEB8', '#A7DA64','#EFF121', '#F5952D', '#E93131', '#D70131', '#D70131')");
            _engine.Evaluate("res[res == 1] <- NA");
            //_engine.Evaluate("res[res < 0.75] <- NA");
            _engine.Evaluate("res[res < 0.3] <- NA");
            _engine.Evaluate("res <- na.omit(melt(res))");
            _engine.Evaluate("res[order(-res$value),]");

            Random r = new Random();
            var newValue = r.Next(1, 1000);
            var newFilePath = ("\"C://R//SAVEHERE//allOutput" + newValue + ".txt\"");
            //"## capture all the output to a file.
            _engine.Evaluate("sink(" + newFilePath + ")");
            var output = _engine.Evaluate("print(res)");//#needed to write to file
            _engine.Evaluate("sink()");// #stop writing
        }
    }
}
