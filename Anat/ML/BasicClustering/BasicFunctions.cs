﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace BasicClustering
{
    public static class BasicFunctions
    {
        public const float Tolerance = 0.0000000001f;
        public const float Threshold = 0.65f;
        private static readonly string[] EdgeSeparators = {","};

        public static float[] ConvertoNumeral(this string documentList)
        {
            return documentList.Split(EdgeSeparators, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => float.Parse(x, CultureInfo.InvariantCulture.NumberFormat)).ToArray();
        }

        public static float CompareTwoCollectionsOfEqualLength(List<float> collection1, List<float> collection2)
        {
            if (collection1.Count == collection2.Count)
            {
                var x = collection1.Intersect(collection2).ToList().Count / collection1.Count;
                return x; //(float) collection1.Intersect(collection2).ToList().Count / collection1.Count;
            }
            throw new Exception("Error: Comparing 2 same-size collections that are not equal.");
        }

        public static float CompareTwoCollectionsOfAnyLength(List<float> collection1, List<float> collection2)
        {
            if (collection1.Count == collection2.Count)
                return CompareTwoCollectionsOfEqualLength(collection1, collection2);

            var denominator = collection1.Count < collection2.Count ? collection2.Count : collection1.Count;

            return (float) collection1.Intersect(collection2).ToList().Count / denominator;
        }

        public static bool AreTwoCollectionsIdentical(List<float> collection1, List<float> collection2)
            => Math.Abs(1 - CompareTwoCollectionsOfAnyLength(collection1, collection2)) < Tolerance;

        public static bool AreTwoCollectionsMergeable(List<float> collection1, List<float> collection2)
            => Math.Abs(CompareTwoCollectionsOfAnyLength(collection1, collection2)) > Threshold;

        public static bool IsOneCollectionCompletelyContainedInTheOther(List<float> collection1,
            List<float> collection2)
        {
            if (collection1.Count == collection2.Count)
            {
                return collection1.Except(collection2).ToList().Count == 0;
            }
            if (collection1.Count < collection2.Count)
            {
                return collection1.Except(collection2).ToList().Count == 0;
            }
            if (collection2.Count < collection1.Count)
            {
                return collection2.Except(collection1).ToList().Count == 0;
            }
            return false;
        }

    }
}