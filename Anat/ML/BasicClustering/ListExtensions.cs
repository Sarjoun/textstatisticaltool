﻿using System.Collections.Generic;
using System.Linq;
using SDParser.ClusteringTypes;

namespace BasicClustering
{
    public static class ListExtensions
    {
        public static List<Agent> InitializeAndResetTheListOfTaggedDocumentsInTheAgents
            (this List<Agent> agentList)
        {
            var newList = new List<Agent>();
            foreach (var agent in agentList)
            {
                var newAgent = agent;
                newAgent.TaggedDocumentsId = new List<int>();
                newList.Add(newAgent);
            }
            return newList;
        }

        public static List<Agent> CreateFoldersForEveryAgentCluster(
            this List<Agent> clustersList,
            string currentCorpusTierFolderPath)
        {
            var clusterCounter = 1;
            foreach (var cluster in clustersList)
            {
                //Create the folder
                FolderFunctions.CreateClusterStoryGraphFolderWithClusterIdAndStoryTitle(currentCorpusTierFolderPath, clusterCounter++,
                    cluster.Name.Split('+').Take(3).Aggregate((i, j) => i + " " + j));

                //Create the local DocumentFile that contains the list of tagged files for each cluster

                //Create file of NGrams involved 


                //List<int> unionOfTaggedDocuments = new List<int>();
                //var clusterMembers = "";
                //foreach (var item in cluster.Value)
                //{
                //    clusterMembers += item.Name + "\t:" + "(" + item.TaggedDocumentsId.Count + ")" + item.TaggedDocumentsIdForDisplay + "\r\n";
                //    unionOfTaggedDocuments = unionOfTaggedDocuments.Union(item.TaggedDocumentsId).ToList();
                //}
                //var totalFiles = "(" + unionOfTaggedDocuments.Count + ")" + unionOfTaggedDocuments
                //                     .OrderBy(v => v)
                //                     .Aggregate("", (current, entry) => current + (entry + ",")).TrimEnd(',');

                //clustersPrintout += "Cluster " + clusterCounter++ + " : " + totalFiles + "\r\n"
                //                    + clusterMembers.Trim() + divider;
            }

            //FileWriter.WriteStringToFile(clustersPrintout, fileName);
            return clustersList;
        }

    }
}
