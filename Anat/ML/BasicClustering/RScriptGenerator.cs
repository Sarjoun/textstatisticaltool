﻿namespace BasicClustering
{
    public static class RScriptGenerator
    {
        public static string GenerateRScriptToCalculatePairsOfHighlyCorrelatedNgramPairs(string nGramMatrixOutputFilePath, string rScriptResultFilePath, string rScriptHaCoutputGraphFilePath)
        {
            var rScript = "#This is a generated R-Script from C#\r\n";
            rScript += "\r\n";
            //if (!require("gplots")) { install.packages("gplots", dependencies = TRUE)   library(gplots)   }
            rScript += "library(\"Hmisc\") \r\n";
            rScript += "library(\"reshape\") \r\n";
            rScript += "library(\"reshape2\") \r\n";
            rScript += "library(\"gplots\") \r\n";

            rScript += "myData <- read.csv(\"" + nGramMatrixOutputFilePath.Replace(@"\", @"\\") + "\", row.names = 1, header = TRUE) \r\n";
            rScript += "\r\n";
            rScript += "res <- cor(myData)\r\n";
            rScript += "\r\n";


            rScript += "pngFilePath <- \"" + rScriptHaCoutputGraphFilePath.Replace(@"\", @"\\") + "\" \r\n";
            rScript += "png(pngFilePath,    # create PNG for the heat map \r\n";
            rScript += "width = 10 * 300,        # 5 x 300 pixels \r\n";
            rScript += "height = 10 * 300, \r\n";
            rScript += "res = 600,            # 300 pixels per inch \r\n";
            rScript += "pointsize = 6)        # smaller font size \r\n";

            rScript += "col <-c('#2F2C62', '#42399B', '#4A52A7', '#59AFEA', '#7BCEB8', '#A7DA64', '#EFF121', '#F5952D', '#E93131', '#D70131', '#D70131') \r\n";
            rScript += "heatmap.2(\r\n";
            rScript += "x = res,\r\n";
            rScript += "col = col,\r\n";
            rScript += "margins = c(12, 9),     # widens margins around plot \r\n";
            rScript += "main = \"NGram-to-NGram Correlation-Based HAC\",\r\n";
            rScript += "density.info = \"none\",\r\n";
            rScript += "trace = \"none\",\r\n";
            rScript += "dendrogram = \"row\",     # only draw a row dendrogram\r\n";
            rScript += "Colv = \"NA\",            # turn off column clustering\r\n";
            rScript += "labCol = TRUE, \r\n";

            rScript += "symm = TRUE, #True when matrix is square\r\n";
            rScript += "symkey = FALSE, #True if the data has negative value - I set it to false so that the legend looks good\r\n";
            rScript += "symbreaks = FALSE,\r\n";
            rScript += "scale = \"none\")\r\n";
            rScript += "dev.off()\r\n";

            rScript += "\r\n";
            rScript += "res[res == 1] <- NA\r\n";
            rScript += "res[res < 0.75] <- NA\r\n";
            rScript += "res <- na.omit(melt(res))\r\n";
            rScript += "res[order(-res$value),]\r\n";

            rScript += "sink(\"" + rScriptResultFilePath.Replace(@"\", @"\\") + "\") ## capture all output to file.\r\n";
            rScript += "print(res) #needed to write to file \r\n";
            rScript += "sink() #stop writing \r\n";

            return rScript;
        }
    }

}