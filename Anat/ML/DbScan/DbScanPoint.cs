namespace DbScan
{
    public class DbScanPoint<T>
    {
        public bool IsVisited;
        public T ClusterPoint;
        public int ClusterId;

        public DbScanPoint(T x)
        {
            ClusterPoint = x;
            IsVisited = false;
            ClusterId = (int)ClusterIds.Unclassified;
        }
    }
}