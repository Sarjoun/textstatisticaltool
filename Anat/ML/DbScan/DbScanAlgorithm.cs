using System;
using System.Collections.Generic;
using System.Linq;

namespace DbScan
{
    /// <summary>
    /// DBSCAN algorithm class
    /// </summary>
    /// <typeparam name="T">Takes dataset item row (features, preferences, vector) type</typeparam>
    public class DbScanAlgorithm<T> where T : DataSetItemBase
    {
        private readonly Func<T, T, double> _metricFunc;

        /// <summary>
        /// Takes metric function to compute distances between dataset items T
        /// </summary>
        /// <param name="metricFunc"></param>
        public DbScanAlgorithm(Func<T, T, double> metricFunc)
        {
            _metricFunc = metricFunc;
        }

        /// <summary>
        /// Performs the DBSCAN clustering algorithm.
        /// </summary>
        /// <param name="allPoints">Dataset</param>
        /// <param name="epsilon">Desired region ball radius</param>
        /// <param name="minimumPointsInRegion">Minimum number of points to be in a region</param>
        /// <param name="clusters">returns sets of clusters, renew the parameter</param>
        public void ComputeClusterDbscan(
            T[] allPoints, 
            double epsilon, 
            int minimumPointsInRegion, 
            out List<T[]> clusters)
        {
            DbScanPoint<T>[] allPointsDbScan = allPoints
                .Select(x => new DbScanPoint<T>(x))
                .ToArray();
            var clusterId = 0;

            for (var i = 0; i < allPointsDbScan.Length; i++)
            {
                var dbScanPoint = allPointsDbScan[i];

                if (dbScanPoint.IsVisited)
                    continue;

                dbScanPoint.IsVisited = true;

                DbScanPoint<T>[] neighborPts;

                RegionQuery(
                    allPointsDbScan, 
                    dbScanPoint.ClusterPoint, 
                    epsilon, 
                    out neighborPts);

                if (neighborPts.Length < minimumPointsInRegion)
                    dbScanPoint.ClusterId = (int)ClusterIds.Noise;
                else
                {
                    clusterId++;
                    ExpandCluster(
                        allPointsDbScan, 
                        dbScanPoint, 
                        neighborPts, 
                        clusterId, 
                        epsilon, 
                        minimumPointsInRegion);
                }
            }
            clusters = new List<T[]>(
                allPointsDbScan
                    .Where(x => x.ClusterId > 0)
                    .GroupBy(x => x.ClusterId)
                    .Select(x => x.Select(y => y.ClusterPoint).ToArray())
                );
        }
        
        public List<T[]> ClusterTheAgentsAndReturnTheClusters(
            T[] agentsPoints,
            double radius,
            int minimumPointsInCluster)
        {
            DbScanPoint<T>[] allPointsDbScan = agentsPoints.
                Select(x => new DbScanPoint<T>(x)).ToArray();
            var clusterId = 0;
            for (var i = 0; i < allPointsDbScan.Length; i++)
            {
                var dbScanPoint = allPointsDbScan[i];

                if (dbScanPoint.IsVisited)
                    continue;

                dbScanPoint.IsVisited = true;

                DbScanPoint<T>[] neighborPts;

                RegionQuery(
                    allPointsDbScan,
                    dbScanPoint.ClusterPoint,
                    radius,
                    out neighborPts);

                if (neighborPts.Length < minimumPointsInCluster)
                    dbScanPoint.ClusterId = (int)ClusterIds.Noise;
                else
                {
                    clusterId++;
                    ExpandCluster(
                        allPointsDbScan,
                        dbScanPoint,
                        neighborPts,
                        clusterId,
                        radius,
                        minimumPointsInCluster);
                }
            }
            return new List<T[]>(
                allPointsDbScan
                    .Where(x => x.ClusterId > 0)
                    .GroupBy(x => x.ClusterId)
                    .Select(x => x.Select(y => y.ClusterPoint).ToArray())
            );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="allPoints">Dataset</param>
        /// <param name="point">point to be in a cluster</param>
        /// <param name="neighborPoints">other points in same region with point parameter</param>
        /// <param name="clusterId">given clusterId</param>
        /// <param name="epsilon">Desired region ball range</param>
        /// <param name="minPts">Minimum number of points to be in a region</param>
        private void ExpandCluster(
            DbScanPoint<T>[] allPoints, 
            DbScanPoint<T> point, 
            DbScanPoint<T>[] neighborPoints, 
            int clusterId, 
            double epsilon, 
            int minPts)
        {
            point.ClusterId = clusterId;
            for (var i = 0; i < neighborPoints.Length; i++)
            {
                DbScanPoint<T> neighborPoint = neighborPoints[i];
                if (!neighborPoint.IsVisited)
                {
                    neighborPoint.IsVisited = true;
                    DbScanPoint<T>[] neighborPoints2;
                    RegionQuery(allPoints, neighborPoint.ClusterPoint, epsilon, out neighborPoints2);
                    if (neighborPoints2.Length >= minPts)
                    {
                        neighborPoints = neighborPoints.Union(neighborPoints2).ToArray();
                    }
                }
                if (neighborPoint.ClusterId == (int)ClusterIds.Unclassified)
                    neighborPoint.ClusterId = clusterId;
            }
        }

        /// <summary>
        /// Checks and searchs neighbor points for given point
        /// </summary>
        /// <param name="allPoints">Dataset</param>
        /// <param name="point">centered point to be searched neighbors</param>
        /// <param name="epsilon">radius of center point</param>
        /// <param name="neighborPts">result neighbors</param>
        private void RegionQuery(DbScanPoint<T>[] allPoints, T point, double epsilon, out DbScanPoint<T>[] neighborPts)
        {
            neighborPts = allPoints.
                Where(x => _metricFunc(point, x.ClusterPoint) <= epsilon)
                .ToArray();
        }
    }
}