namespace DbScan
{
    public class DataSetItem : DataSetItemBase
    {
        public double X;
        public double Y;
        public double Z;

        public DataSetItem(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
}