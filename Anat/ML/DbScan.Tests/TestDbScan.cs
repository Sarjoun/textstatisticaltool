﻿using System;
using System.Collections.Generic;
using System.Linq;
using IONA.Algorithm.Algorithm.Clustering.Core;
using IONA.Algorithm.Algorithm.Helpers;
using MathLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SDParser.ClusteringTypes;
using SDParser.ComplexTypes;
using SDParser.Primitives;
using SDParser.Structures;

namespace DbScan.Tests
{
    [TestClass]
    public class TestDbScan
    {

        [TestMethod]
        public void CalculateTheDistanceBetween2PointsIn3DSpace()
        {
            var idCounter = 1;
            var testPoints = new List<NGramSetItem>
            {
                new NGramSetItem(idCounter++, "point 1", new float[] {1, 1, 1}),
                new NGramSetItem(idCounter, "point 2", new float[] {2, 2, 2})
            };

            Assert.AreEqual(Math.Sqrt(3), DistanceBetween2Points.Calculate(testPoints[0].Dimensions, testPoints[1].Dimensions), 0.001);
        }

        [TestMethod]
        public void CalculateTheDistanceBetween2PointsIn4DSpace()
        {
            var idCounter = 1;
            var testPoints = new List<NGramSetItem>
            {
                new NGramSetItem(idCounter++, "point 1", new float[] {1, 1, 1, 1}),
                new NGramSetItem(idCounter, "point 2", new float[] {2, 2, 2, 2})
            };

            Assert.AreEqual(Math.Sqrt(4), 
                DistanceBetween2Points.Calculate(testPoints[0].Dimensions, testPoints[1].Dimensions), 0.001);
        }

        [TestMethod]
        public void CalculateTheDistanceBetween2PointsIn10DSpace()
        {
            var idCounter = 1;
            var testPoints = new List<NGramSetItem>
            {
                new NGramSetItem(idCounter++, "point 1", new float[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}),
                new NGramSetItem(idCounter, "point 2", new float[] {2, 2, 2, 2, 2, 2, 2, 2, 2, 2})
            };

            Assert.AreEqual(Math.Sqrt(10),
                DistanceBetween2Points.Calculate(testPoints[0].Dimensions, testPoints[1].Dimensions), 0.001);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void CalculateTheDistanceBetween2PointsInNxMSpaceWillBlowUpBecauseOfUnequalDimensions()
        {
            var idCounter = 1;
            var testPoints = new List<NGramSetItem>
            {
                new NGramSetItem(idCounter++, "point 1", new float[] {1, 1, 1, 1, 1}),
                new NGramSetItem(idCounter, "point 2", new float[] {2, 2, 2})
            };

            var result = DistanceBetween2Points.Calculate(testPoints[0].Dimensions, testPoints[1].Dimensions);
        }

        [TestMethod]
        public void ClusteringDataToMakeTwoClustersWithNoOutliersTestingRealDataNGram()
        {
            var testPoints = new List<NGramSetItem>();
            var counter = 1;
            //for (var i = 0; i < 1; i++)
            {
                //points around (1,1) with most 1 distance
                //testPoints.Add(new NGramSetItem(counter++, "", new float[] { 931, 1565, 1514 }));
                //testPoints.Add(new NGramSetItem(counter++, "", new float[] { 931, 1565, 1672 }));
                //testPoints.Add(new NGramSetItem(counter, "", new float[] { 931, 1565, 1622 }));

                //testPoints.Add(new NGramSetItem(counter++, "", new float[] { 931, 1565, 672 }));
                //testPoints.Add(new NGramSetItem(counter++, "", new float[] { 1565, 672, 1514 }));
                //testPoints.Add(new NGramSetItem(counter++, "", new float[] { 931, 672, 1514 }));
                //testPoints.Add(new NGramSetItem(counter, "", new float[] { 2010, 2066, 538 }));

                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 6, 5, 5 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 5, 5, 25 }));
                testPoints.Add(new NGramSetItem(counter, "", new float[] { 5, 8, 25 }));
            }

            var featureData = testPoints.ToArray();
            List<NGramSetItem[]> clusters;

            var dbs = new DbScanAlgorithm<NGramSetItem>((x, y) =>
                  DistanceBetween2Points.Calculate(x.Dimensions, y.Dimensions)
            );

            dbs.ComputeClusterDbscan(
               allPoints: featureData,
               epsilon: 5,
               minimumPointsInRegion: 2,
               clusters: out clusters);

            const int expectedNumberOfClusters = 1;

            Assert.AreEqual(expectedNumberOfClusters, clusters.Count);
        }

        [TestMethod]
        public void ClusteringDataToMakeTwoClustersWithNoOutliers2()
        {
            var testPoints = new List<NGramSetItem>();
            var counter = 1;
            //for (var i = 0; i < 1; i++)
            {
                //points around (1,1) with most 1 distance
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 1, 4, 0 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 1, 8, 0 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 1, 5, 0 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 1, 2, 5 }));

                //points around (5,5) with most 1 distance
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 5, 6, 5 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 6, 5, 5 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 5, 5, 25 }));
                testPoints.Add(new NGramSetItem(counter, "", new float[] { 5, 8, 25 }));
            }

            const int expectedNumberOfClusters = 3;

            var featureData = testPoints.ToArray();
            List<NGramSetItem[]> clusters;

            var dbs = new DbScanAlgorithm<NGramSetItem>((x, y) =>
            DistanceBetween2Points.Calculate(x.Dimensions, y.Dimensions)
                          //Math.Sqrt((x.X - y.X) * (x.X - y.X) +
                          //          (x.Y - y.Y) * (x.Y - y.Y))
                          );

            //var dbs = new DbScanAlgorithm<DataSetItem>((x, y) => 
            //Math.Sqrt( (x.X - y.X) * (x.X - y.X) + 
            //           (x.Y - y.Y) * (x.Y - y.Y)) );

            ////dbs.ComputeClusterDbscan(allPoints: featureData, epsilon: .01, minimumPointsInRegion: 10, clusters: out clusters);
            ////HashSet<DataSetItem[]> clusters;

            dbs.ComputeClusterDbscan(
                allPoints: featureData,
                epsilon: 3,
                minimumPointsInRegion: 2,
                clusters: out clusters);

            Assert.AreEqual(expectedNumberOfClusters, clusters.Count);
        }

        [TestMethod]
        public void ClusteringDataToMakeTwoClustersWithNoOutliers3()
        {
            var counter = 1;
            var testPoints = new List<NGramSetItem>();
            for (var i = 0; i < 1; i++)
            {
                //points around (1,1) with most 1 distance
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 1, 4, 0 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 1, 8, 0 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 1, 5, 0 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 1, 2, 5 }));

                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 5, 6, 5 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 6, 5, 5 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 5, 5, 5 }));
                testPoints.Add(new NGramSetItem(counter, "", new float[] { 5, 8, 5 }));
            }

            const int expectedNumberOfClusters = 2;

            var featureData = testPoints.ToArray();
            List<NGramSetItem[]> clusters;


            var dbs = new DbScanAlgorithm<NGramSetItem>((x, y) =>
                    DistanceBetween2Points.Calculate(x.Dimensions, y.Dimensions)
            //Math.Sqrt((x.X - y.X) * (x.X - y.X) +
            //          (x.Y - y.Y) * (x.Y - y.Y))
            );

            //var dbs = new DbScanAlgorithm<DataSetItem>((x, y) => 
            //Math.Sqrt( (x.X - y.X) * (x.X - y.X) + 
            //           (x.Y - y.Y) * (x.Y - y.Y)) );

            ////dbs.ComputeClusterDbscan(allPoints: featureData, epsilon: .01, minimumPointsInRegion: 10, clusters: out clusters);
            ////HashSet<DataSetItem[]> clusters;

            dbs.ComputeClusterDbscan(
                allPoints: featureData,
                epsilon: 3,
                minimumPointsInRegion: 2,
                clusters: out clusters);

            Assert.AreEqual(expectedNumberOfClusters, clusters.Count);
        }

        [TestMethod]
        public void ClusteringDataToMakeTwoClustersWithNoOutliers4()
        {
            var counter = 1;
            var testPoints = new List<NGramSetItem>();
            for (var i = 0; i < 1; i++)
            {
                //points around (1,1) with most 1 distance
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 1, 4, 0, 4 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 1, 8, 0, 8 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 1, 5, 0, 5 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 1, 2, 5, 2 }));

                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 5, 6, 5, 120 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 6, 5, 5, 111 }));
                testPoints.Add(new NGramSetItem(counter++, "", new float[] { 5, 5, 5, 400 }));
                testPoints.Add(new NGramSetItem(counter, "", new float[] { 5, 8, 5, 401 }));
            }

            //const int expectedNumberOfClusters = 2;

            var featureData = testPoints.ToArray();
            List<NGramSetItem[]> clusters;


            var dbs = new DbScanAlgorithm<NGramSetItem>((x, y) =>
                    DistanceBetween2Points.Calculate(x.Dimensions, y.Dimensions)
            //Math.Sqrt((x.X - y.X) * (x.X - y.X) +
            //          (x.Y - y.Y) * (x.Y - y.Y))
            );

            //var dbs = new DbScanAlgorithm<DataSetItem>((x, y) => 
            //Math.Sqrt( (x.X - y.X) * (x.X - y.X) + 
            //           (x.Y - y.Y) * (x.Y - y.Y)) );

            ////dbs.ComputeClusterDbscan(allPoints: featureData, epsilon: .01, minimumPointsInRegion: 10, clusters: out clusters);
            ////HashSet<DataSetItem[]> clusters;

            dbs.ComputeClusterDbscan(
                allPoints: featureData,
                epsilon: 8,
                minimumPointsInRegion: 2,
                clusters: out clusters);

            Assert.IsTrue(clusters.Count > 1);
        }


        private List<Agent> CreateListOfIonaAgents()
        {
            IEnumerable<VariantWord> variants = new VariantWord[0];
            var stemWordList1 =
                new List<StemWord>
                {
                    new StemWord("cat", variants, 1, 1),
                    new StemWord("cow", variants, 1, 2),
                    new StemWord("dog", variants, 1, 3)
                };

            var stemWordList2 =
                new List<StemWord>
                {
                    new StemWord("orange", variants, 1, 400),
                    new StemWord("blue", variants, 1, 500),
                    new StemWord("green", variants, 1, 600)
                };

            var stemWordList3 =
                new List<StemWord>
                {
                    new StemWord("cow", variants, 1, 2),
                    new StemWord("goat", variants, 1, 9),
                    new StemWord("Owl", variants, 1, 70)
                };

            return new List<Agent>
            {
                new Agent(1, new NumberOfWordsInStartNGRAM(3), stemWordList1, 1),
                new Agent(2, new NumberOfWordsInStartNGRAM(3), stemWordList2, 1),
                new Agent(3, new NumberOfWordsInStartNGRAM(3), stemWordList3, 1)
            };
        }

        [TestMethod]
        public void TestIonaConvertingNgramAgentsIntoPointsInDBScan()
        {
            List<NGramSetItem> result = DbScanNGramSetItemsManager.TransformStemWordNGramsIntoNGramSetItemsForDbScan(
                CreateListOfIonaAgents());
        }

        [TestMethod]
        public void TestIonaClusteringAgents()
        {
            var generalSettingsForIONA = new AllSettings
            {
                DbScanBallRadius = new DbScanBallRadius(100),
                DbScanMinPointsInRegion = new DbScanMinPointsInRegion(2)
            };

            List<NGramSetItem[]> result = DbScanAlgorithmWrapper.ClusterTheAgents(
                CreateListOfIonaAgents(), generalSettingsForIONA);

            Assert.IsTrue(result.Count < 4);

            //var newClusterDimensions =(CreateListOfIonaAgents()[0].Grams.Count + CreateListOfIonaAgents()[2].Grams.Count);

            if (result.Count > 0)
            {
                Assert.IsTrue(result[0].First().Id == 1);
                //Assert.IsTrue(result[0].First().Dimensions.Length <= newClusterDimensions);
            }
        }

        [Ignore]
        [TestMethod]
        public void ClusteringDataToMakeTwoClustersWithNoOutliers_NeedsToBeReviewed()
        {
            var testPoints = new List<DataSetItem>();
            for (var i = 0; i < 1; i++)
            {
                //points around (1,1) with most 1 distance
                testPoints.Add(new DataSetItem(1, 4, 0));
                testPoints.Add(new DataSetItem(1, 8, 0));
                testPoints.Add(new DataSetItem(1, 5, 0));
                testPoints.Add(new DataSetItem(1, 2, 5));

                //points around (5,5) with most 1 distance
                testPoints.Add(new DataSetItem(5, 6, 5));
                testPoints.Add(new DataSetItem(6, 5, 5));
                testPoints.Add(new DataSetItem(5, 5, 25));
                testPoints.Add(new DataSetItem(5, 8, 25));
            }

            const int expectedNumberOfClusters = 2;

            var featureData = testPoints.ToArray();
            List<DataSetItem[]> clusters;


            var dbs = new DbScanAlgorithm<DataSetItem>((x, y) =>
                Math.Sqrt((x.X - y.X) * (x.X - y.X) +
                          (x.Y - y.Y) * (x.Y - y.Y)));

            //var dbs = new DbScanAlgorithm<DataSetItem>((x, y) => 
            //Math.Sqrt( (x.X - y.X) * (x.X - y.X) + 
            //           (x.Y - y.Y) * (x.Y - y.Y)) );

            ////dbs.ComputeClusterDbscan(allPoints: featureData, epsilon: .01, minimumPointsInRegion: 10, clusters: out clusters);
            ////HashSet<DataSetItem[]> clusters;

            dbs.ComputeClusterDbscan(
                allPoints: featureData,
                epsilon: 3,
                minimumPointsInRegion: 2,
                clusters: out clusters);

            Assert.AreEqual(expectedNumberOfClusters, clusters.Count);
        }
    }
}
