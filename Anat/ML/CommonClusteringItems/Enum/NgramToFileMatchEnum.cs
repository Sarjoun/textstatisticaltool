﻿namespace CommonClusteringItems.Enum
{
    public enum NgramToFileMatchEnum
    {
        NoMatch = 0,
        PartialMatch = 1,
        FullMatch = 2    
    }
}
