﻿using System.Collections.Generic;
using System.Linq;

namespace CommonClusteringItems
{
    public class BasicNGramSetItem
    {
        public float[] Dimensions;
        public int? Id;
        public string Name;
        public string NameForCorrelationAnalysis => Name.Replace("+", "_");

        public BasicNGramSetItem(int? id, string name, float[] dimensions, List<int> clusterIds)
        {
            Id = id;
            Name = name;
            Dimensions = dimensions;
        }

        public string DimensionsToStringTabDelimited =>
            Dimensions
                .Aggregate("", (current, entry) => current + (entry + "\t"))
                .TrimEnd();
    }
}


