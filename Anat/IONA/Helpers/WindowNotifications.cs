﻿using System;
using System.Diagnostics;
using System.Windows;

namespace IONA.Helpers
{
    public static class WindowNotifications
    {
        public static void DisplayErrorMessage(string message, Exception e)
        {
            var boxResult =
                MessageBox.Show(message + e.Message,
                    " The simulation will exit now.\r\n Press OK when ready",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            if (boxResult == MessageBoxResult.OK)
            {
                Application.Current.Shutdown();
            }
            Debug.WriteLine(message + e.Message);
            Console.WriteLine(e);
        }
    }
}
