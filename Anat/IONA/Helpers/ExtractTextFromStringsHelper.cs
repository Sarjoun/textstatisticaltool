﻿using System;
using IONA.Extensions;
using SDParser.Primitives;

namespace IONA.Helpers
{
    public static class ExtractTextFromStringsHelper
    {
        public static string GetTargetDocumentsFileName(string folderPath, string newFileName)
        {
            return folderPath.CreateIfFolderDoesNotExistAndReturnPath() + "\\" + newFileName;
        }

        public static string GetTargetFolderPath(string folderName, UnTaggedDocumentsSubCorporaFileName unTaggedDocumentsSubCorporaFileName)
        {
            return ExtractCurrentCorpusTierFolderPathFromUnTaggedDocumentsSubCorporaFileName(unTaggedDocumentsSubCorporaFileName) + folderName;
        }

        public static string ExtractCurrentCorpusTierFolderPathFromUnTaggedDocumentsSubCorporaFileName(
            UnTaggedDocumentsSubCorporaFileName fileName)
        {
           return fileName.Value.Substring(0, fileName.Value.LastIndexOf("corpusTier", StringComparison.Ordinal));
        }

    }
}
