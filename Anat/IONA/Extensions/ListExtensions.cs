﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IONA.Algorithm.Algorithm.Clustering.Core;
using IONA.Algorithm.Algorithm.Helpers;
using IONA.Enums;
using SDParser.ClusteringTypes;
using SDParser.ComplexTypes;

namespace IONA.Extensions
{
    public static class ListExtensions
    {
        public static List<Agent> MergeStemWordNGramsBasedOnTheDocsTimeSeries(
            this List<Agent> stemWordNGrams)
        {
            //Get the number of clusters which is the largest number of all clusterIds
            var numberOfClusters = stemWordNGrams.OrderByDescending(item => item.ClusterId).First().ClusterId;

            var newStemWordNGram = new List<Agent>();
            for (var i = 1; i <= numberOfClusters; i++)
            {
                newStemWordNGram.Add(AgentsManager.
                    MergeAgentsBelongingToSameCluster(
                        stemWordNGrams.FindAll(x => x.ClusterId == i)));
            }

            newStemWordNGram.AddRange(stemWordNGrams.FindAll(x => x.ClusterId == 0));
            return newStemWordNGram;
        }

        public static List<Agent> MergeAgentsBelongingToSameCluster(
          this List<Agent> stemWordNGrams)
        {
            //Get the number of clusters which is the largest number of all clusterIds
            var numberOfClusters = stemWordNGrams.OrderByDescending(item => item.ClusterId).First().ClusterId;

            var newStemWordNGram = new List<Agent>();
            for (var i = 1; i <= numberOfClusters; i++)
            {
                newStemWordNGram.Add(AgentsManager.
                    MergeAgentsBelongingToSameCluster(
                        stemWordNGrams.FindAll(x => x.ClusterId == i)));
            }

            newStemWordNGram.AddRange(stemWordNGrams.FindAll(x => x.ClusterId == 0));
            return newStemWordNGram;
        }


        public static List<Agent> DeleteAgentsWithNoTaggedDocs(
            this List<Agent> stemWordNGrams)
        {
            return stemWordNGrams.
                Where(stemWordNGram => stemWordNGram.TaggedDocumentsId != null).ToList();
        }

        public static List<int> ExtractTagVectorValuesOrdered(
            this List<Agent> stemWordNGrams)
        {
            var result = new List<int>();
            result = stemWordNGrams.
                Aggregate(
                result,
                (current, stemWordNGram) => stemWordNGram.
                                                AlphabeticalOrderIdTagVectorToList.
                                                Union(current).
                                                ToList());
            return result.OrderBy(x => x).ToList();
        }


        public static List<List<int>> TransformAgentsDocsIntoMatrix(
           this List<Agent> stemWordNGrams)
        {
            return stemWordNGrams.
               Select(stemWordNGram => stemWordNGram.TaggedDocumentsId).
               ToList();
        }


        public static List<Agent> OrderAgents(
            this List<Agent> originalList,
            AgentDisplayOrderType agentDisplayOrderType)
        {
            switch (agentDisplayOrderType)
            {
                case AgentDisplayOrderType.TaggingVectorSize:
                    return originalList.OrderBy(item => item.TaggedDocumentsId.Count).ToList();

                case AgentDisplayOrderType.StemWordGramSize:
                    return originalList.OrderBy(item => item.Grams.Count).ToList();

                default:
                    return originalList;
            }
        }

        public static async Task<List<Agent>> WriteOutInitialClusteringResultsToFileAsync(
           this List<Agent> clusteredAgents,
           AllSettings generalSettingsForIONA)
        {
            await FileWriter.WriteToFileSpecificResultTypeForAgentsAsync(
               clusteredAgents,
               generalSettingsForIONA.FirstMergeOfAgentsFileName,
               AgentDisplayOrderType.None);

            await FileWriter.WriteStemWordNGramsToGraphFileAsync(
               clusteredAgents,
               generalSettingsForIONA.FirstMergeOfAgentsGraphFileName,
               AgentDisplayOrderType.None);

            await FileWriter.WriteOutMatrixToFileAsync(
               clusteredAgents.OrderAgents(AgentDisplayOrderType.TaggingVectorSize).TransformAgentsDocsIntoMatrix(),
               generalSettingsForIONA.FirstMergeTaggedDocIDsFileName);

            return clusteredAgents;
        }

        public static async Task<List<Agent>> WriteOutSecondaryClusteringOperationsResultsToFileAsync(
           this List<Agent> clusteredAgents,
           AllSettings generalSettingsForIONA)
        {
            await FileWriter.WriteToFileSpecificResultTypeForAgentsAsync(
               clusteredAgents,
               generalSettingsForIONA.SecondMergeOfAgentsFileName,
               AgentDisplayOrderType.TaggingVectorSize);

            await FileWriter.WriteStemWordNGramsToGraphFileAsync(
            clusteredAgents,
            generalSettingsForIONA.SecondMergeOfAgentsGraphFileName,
            AgentDisplayOrderType.TaggingVectorSize);

            await FileWriter.WriteStemWordNGramsToGraphFileUsingMostPopularVariantAsync(
                clusteredAgents,
                generalSettingsForIONA.SecondMergeOfAgentsGraphFileName.Value.Replace(".txt", "MostPopular.txt"),
                AgentDisplayOrderType.TaggingVectorSize);

            await FileWriter.WriteOutMatrixToFileAsync(
             clusteredAgents.OrderAgents(AgentDisplayOrderType.TaggingVectorSize).TransformAgentsDocsIntoMatrix(),
             generalSettingsForIONA.SecondMergeTaggedDocIdsFileName);

            return clusteredAgents;
        }

        public static async Task<List<Agent>> WriteToFileListOfAgentsInfoAsync(
            this List<Agent> originalStemWordNGrams,
            string fileName)
        {
            var stemWordNGrams = originalStemWordNGrams.OrderAgents(AgentDisplayOrderType.None);
            var streamWriter = new StreamWriter(fileName);
            await streamWriter.WriteLineAsync(
                "NGrams" + "\t" +
                "Cluster Id" + "\t" +
                "Tagged File Ids");
            await streamWriter.WriteLineAsync(
                "------------------------------------------------------------------------------------------");
            try
            {
                foreach (var stemWordNGram in stemWordNGrams)
                {
                    await streamWriter.WriteLineAsync(
                        "[" + stemWordNGram.GramToStringUsingRoot() + "]\t[" +
                        stemWordNGram.ClusterId + "]\t[" +
                        stemWordNGram.AlphabeticalOrderIdTagVectorToStringCommaDelimited + "]");
                }
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
            return originalStemWordNGrams;
        }
    }
}

