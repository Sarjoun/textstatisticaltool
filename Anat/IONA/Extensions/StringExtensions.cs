﻿using System;
using System.Diagnostics;
using System.IO;

namespace IONA.Extensions
{
    public static class StringExtensions
    {
        public static string ExtractStemWordInfoFromFrequencyFileLine(this string line)
        {
            return line.Trim().Split('=')[1];
        }

        public static string RemoveLastEntryIfNewLine(this string line)
        {
            return line.TrimEnd('\r', '\n');
        }

        public static string RemoveInitialIndexTag(this string line)
        {
            return line.Substring(
                line.IndexOf(")", StringComparison.Ordinal) + 1,
                line.Length - line.IndexOf(")", StringComparison.Ordinal) - 1);
        }

        public static string CreateIfFolderDoesNotExistAndReturnPath(this string folderPath)
        {
            try
            {
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Catastrophic error in creating output folder " + folderPath + ". " + e.Message);
                return folderPath;
            }
            return folderPath;
        }
    }
}
