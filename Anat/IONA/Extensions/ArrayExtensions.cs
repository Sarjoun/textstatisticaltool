﻿using System.Collections.Generic;
using System.Linq;

namespace IONA.Extensions
{
    public static class ArrayExtensions
    {
        public static IEnumerable<object> GetEveryConsecutiveStrings(this string[] wordsWithValues)
        {
            return wordsWithValues.Zip(wordsWithValues.Skip(1).Concat(new[] {"null"}), (x, y) => new
            {
                First = x,
                Second = y
            }).Where((item, index) => index % 2 == 0);
        }
    }
}
