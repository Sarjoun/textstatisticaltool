﻿using SDParser;
using SDParser.Primitives;

namespace IONA.Extensions
{
    public static class GeneralSimulationSettingsExtensions
    {
        public static ParsedSimulationSettings UpdateTier(
            this ParsedSimulationSettings generalSimulationSettings)
        {
            generalSimulationSettings.Tier = Tier.FromValue(generalSimulationSettings.Tier.Value + 1);
            return generalSimulationSettings;
        }
        public static ParsedSimulationSettings UpdateSimulationSettingsFile(
            this ParsedSimulationSettings generalSimulationSettings)
        {
            generalSimulationSettings.SimulationSettingsFile = SimulationSettingsFile.
                FromValue(
                generalSimulationSettings.
                    SimulationSettingsFile.
                    Value.
                    Replace("IONA", "LDA").
                    Replace("Tier" + generalSimulationSettings.Tier.Value, 
                            "Tier" + (generalSimulationSettings.Tier.Value + 1)));
            return generalSimulationSettings;
        }
    }
}
