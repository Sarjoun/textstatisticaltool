﻿using IONA.Algorithm.Algorithm.Helpers;
using IONA.Algorithm.Algorithm.NGrams.Structs;

namespace IONA.Extensions
{
    public static class NGramExtensions
    {
        public static bool IsSimilar(this NGram current,
           NGram other,
           int wordToWordthreshold,
           double ngramThreshold)
        {
            var result = 0;
            var ngramLength = current.N;
            for (var i = 0; i < ngramLength; i++)
            {
                if (LevenshteinDistance.Compute(current.Grams[i], other.Grams[i]) >= wordToWordthreshold)
                    result--;
                else
                    result++;
            }

            return result / (double)ngramLength >= ngramThreshold;
        }
    }


}
