﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IONA.Algorithm.Algorithm.Helpers
{
    public static class CosineSimilarity
    {
        private const string ErrorMessage = @"Error. Vectors are of different lenths.";
        private const double Tolerance = 0.000000000000000001;
        private static bool CheckIfSameVector(IReadOnlyList<float> vector1, IReadOnlyList<float> vector2)
        {
            var length = vector1.Count;

            for (var i = 0; i < length; i++)
            {
                if (Math.Abs(vector1[i] - vector2[i]) > Tolerance)
                    return false;
            }
            return true;
        }

        public static float ComputeCosineSimilarity(float[] vector1, float[] vector2)
        {
            if (vector1.Length != vector2.Length)
                throw new Exception(ErrorMessage);

            if (CheckIfSameVector(vector1, vector2))
                return 1;

            var denom = VectorLength(vector1) * VectorLength(vector2);
            if (Math.Abs(denom) < Tolerance)
                return 0F;
            return InnerProduct(vector1, vector2) / denom;
        }

        public static float InnerProduct(float[] vector1, float[] vector2)
        {
            if (vector1.Length != vector2.Length)
                throw new Exception(ErrorMessage);

            return vector1.Select((t, i) => t * vector2[i]).Sum();
            //translation
            //float result = 0F;
            //for (int i = 0; i < vector1.Length; i++)
            //    result += vector1[i] * vector2[i];
            //return result;
        }

        public static float VectorLength(float[] vector)
        {
            return (float)Math.Sqrt(vector.Aggregate(0.0F, (current, t) => current + (t * t)));

            // translation:
            //var sum = 0.0F;
            //for (var i = 0; i < vector.Length; i++)
            //    sum = sum + (vector[i] * vector[i]);
        }
    }

}
