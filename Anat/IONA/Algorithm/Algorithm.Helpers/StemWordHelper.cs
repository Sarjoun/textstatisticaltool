﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IONA.Algorithm.Algorithm.Clustering.Structs;
using SDParser;
using SDParser.CustomFileParsers;
using SDParser.Primitives;
using SDParser.Structures;

namespace IONA.Algorithm.Algorithm.Helpers
{
    public static class StemWordHelper
    {
        private static Dictionary<string, StemWord> _stemWordsDictionary;
        private static List<VocabularyWordStruct> _vocabularyWordStructs;

        public static StemWord GetStemWordFromString(string stemWordString)
        {
            var temp = _stemWordsDictionary[stemWordString];
            temp.DictionaryId = _vocabularyWordStructs.Find(x => x.Word == temp.Root).VocabularyId;
            return temp;
        }

        public static async Task<Dictionary<string, StemWord>> UploadStemWordsDictionaryFromFileAsync(
            WordFrequencyFileName wordFrequencyFileName,
            List<VocabularyWordStruct> vocabularyWordStructs)
        {
            _vocabularyWordStructs = vocabularyWordStructs;
            return _stemWordsDictionary =
                await WordFrequencyFileParser.
                        GetStemWordsDictionaryAsync(wordFrequencyFileName);
        }

    }
}
