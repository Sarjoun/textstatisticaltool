﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BasicClustering;
using SDParser.ClusteringTypes;
using SDParser.ComplexTypes;

namespace IONA.Algorithm.Algorithm.Helpers
{
    public static class BasicClusteringWrapper
    {
        public static Task<List<Agent>> ClusterTheAgentsUsingRAsync(
            List<Agent> agents,
            AllSettings generalSettingsForIONA)
        {
            return new BasicClusteringAlgorithm("test")
                .ClusterTheAgentsAndReturnTheClusters(agents, generalSettingsForIONA.WordStreamFileName, generalSettingsForIONA.DocumentsLinksFileName);
            //.CreateFoldersForEveryAgentCluster(GetFolderPathForCluster(generalSettingsForIONA));
        }

        //public static string GetFolderPathForCluster(AllSettings generalSettingsForIONA)
        //{
        //    return Path.GetDirectoryName(generalSettingsForIONA.WordStreamFileName.Value) + Path.DirectorySeparatorChar;
        //}

    }
}

