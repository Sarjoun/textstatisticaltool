﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IONA.Algorithm.Algorithm.Clustering.Structs;
using SDParser.Primitives;

namespace IONA.Algorithm.Algorithm.Helpers
{
    public static class FileReader
    {
        public static async Task<IEnumerable<string>> ReturnContentOfFileAsync(string fileName)
        {
            using (var reader = File.OpenText(fileName))
            {
                var fileText = await reader.ReadToEndAsync();
                return fileText.
                    Trim().
                    Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            }
        }

        public static async Task<string> GetSettingValueFromFileAsync(string fileName, string settingValueName)
        {
            using (var reader = File.OpenText(fileName))
            {
                var fileText = await reader.ReadToEndAsync();
                var contents = fileText.Trim().Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                var entry= contents.First(x => x.Contains(settingValueName));
                var start = entry.IndexOf('=') + 1;
                var length = entry.Length - entry.IndexOf('=') - 1;
                var result = entry.Substring(start, length);
                return result;
            }
        }

        public static async Task<List<VocabularyWordStruct>> 
            GetWordStructsFromVocabulary(VocabularyFileName vocabularyFileName)
        {
            var result = new List<VocabularyWordStruct>();
            using (var reader = File.OpenText(vocabularyFileName.Value))
            {
                var fileText = await reader.ReadToEndAsync();
                var content = fileText.
                    Trim().
                    Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                result.
                    AddRange(
                        content.Select(record => record.Split('\t')).
                            Select(res => new VocabularyWordStruct(
                                res[0], 
                                Convert.ToInt32(res[1]), 
                                Convert.ToInt32(res[2]))));
            }
            return result;
        }

    }
}