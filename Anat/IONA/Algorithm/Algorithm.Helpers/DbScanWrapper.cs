﻿using DbScan;
using MathLibrary;
using SDParser.ComplexTypes;
using System.Collections.Generic;
using IONA.Algorithm.Algorithm.Clustering.Core;
using SDParser.ClusteringTypes;


namespace IONA.Algorithm.Algorithm.Helpers
{
    public static class DbScanAlgorithmWrapper
    {
        public static List<NGramSetItem[]> ClusterTheAgents(
            List<Agent> agents,
            AllSettings generalSettingsForIONA)
        {
            return new DbScanAlgorithm<NGramSetItem>((x, y) =>
              DistanceBetween2Points
              .Calculate(x.Dimensions, y.Dimensions))
              .ClusterTheAgentsAndReturnTheClusters(
                    agentsPoints: DbScanNGramSetItemsManager.TransformStemWordNGramsIntoNGramSetItemsForDbScan(agents).ToArray(),
                    radius: generalSettingsForIONA.DbScanBallRadius,
                    minimumPointsInCluster: generalSettingsForIONA.DbScanMinPointsInRegion);
        }
    }
}
