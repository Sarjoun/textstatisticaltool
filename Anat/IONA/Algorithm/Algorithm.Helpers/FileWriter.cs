﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IONA.Enums;
using IONA.Extensions;
using IONA.Helpers;
using SDParser;
using SDParser.ClusteringTypes;
using SDParser.Primitives;

namespace IONA.Algorithm.Algorithm.Helpers
{
    public static class FileWriter
    {
        private const string LDARunSettingsFileSection = "-- Auto-generated LDA simulation settings for new Tier";

        public static async Task<SimulationSettingsFile> WriteLDARunSettingsToFileAndReturnFileNameForNextTierRunAsync(
            IONARunSettingsFileName ionaRunSettingsFileName,
            UnTaggedDocumentsSubCorporaFileName unTaggedDocumentsSubCorporaFileName)
        {
            var generalSimulationSettings = await MainParserForSettingsFile
                .GetLDASettingsFromFile(CurrentSimulationSettingsFileName.FromValue(ionaRunSettingsFileName.Value));

            generalSimulationSettings.UntaggedDocsFromPreviousTier = UntaggedDocsFromPreviousTier.FromValue(unTaggedDocumentsSubCorporaFileName.Value);

            //Update the new ILDA file
            generalSimulationSettings = generalSimulationSettings.UpdateSimulationSettingsFile().UpdateTier();

            //Update the StatisticsSources if RunStatistics = True and Tier = 1
            if (generalSimulationSettings.Tier.Value - 1 == 1 && generalSimulationSettings.RunStatistics.Value)
            {
                generalSimulationSettings.StatisticsSources =
                    StatisticsSources.FromValue(UpdateValueOfDestinationDocumentsLinksFileWhenRunStatisticsIsOn(unTaggedDocumentsSubCorporaFileName));
            }

            var ldaRunSettingsContents = generalSimulationSettings.GetTextVersionForFilePrintOut();

            var streamWriter = new StreamWriter(generalSimulationSettings.SimulationSettingsFile.Value);
            try
            {
                await streamWriter.WriteLineAsync(LDARunSettingsFileSection);
                foreach (var setting in ldaRunSettingsContents)
                {
                    await streamWriter.WriteLineAsync(setting);
                }
                streamWriter.Close();
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
            return generalSimulationSettings.SimulationSettingsFile;
        }

        //public static async Task WriteListToFileAsync(
        //    List<string> generalContent,
        //    string fileName)
        //{
        //    var streamWriter = new StreamWriter(fileName);
        //    try
        //    {
        //        foreach (var content in generalContent)
        //        {
        //            await streamWriter.WriteLineAsync(content);
        //        }
        //    }
        //    finally
        //    {
        //        streamWriter.Close();
        //        streamWriter.Dispose();
        //    }
        //}

        //public static async Task WriteStringToFileAsync(
        //    string generalContent,
        //    string fileName)
        //{
        //    var streamWriter = new StreamWriter(fileName);
        //    try
        //    {
        //        await streamWriter.WriteLineAsync(generalContent);
        //    }
        //    finally
        //    {
        //        streamWriter.Close();
        //        streamWriter.Dispose();
        //    }
        //}


        //Even if there is a previous value, overwrite it with new value. Any filePath will work, doesn't need to be UnTaggedDocumentsSubCorporaFileName
        private static string UpdateValueOfDestinationDocumentsLinksFileWhenRunStatisticsIsOn(UnTaggedDocumentsSubCorporaFileName unTaggedDocumentsSubCorporaFileName)
        {
            //return ExtractTextFromStringsHelper.GetTargetDocumentsFileName(
            //        ExtractTextFromStringsHelper.GetTargetFolderPath("Statistics", unTaggedDocumentsSubCorporaFileName), "DocumentsFile.txt");
            return ExtractTextFromStringsHelper.GetTargetDocumentsFileName(
                ExtractTextFromStringsHelper.GetTargetFolderPath("Statistics", unTaggedDocumentsSubCorporaFileName), "");
        }


        public static async Task WriteToFileSpecificResultTypeForAgentsAsync(
            List<Agent> originalStemWordNGrams,
            string fileName,
            AgentDisplayOrderType agentDisplayOrderType)
        {
            var stemWordNGrams = originalStemWordNGrams.OrderAgents(agentDisplayOrderType);
            var streamWriter = new StreamWriter(fileName);
            try
            {
                foreach (var stemWordNGram in stemWordNGrams)
                {
                    await streamWriter.WriteLineAsync(
                        stemWordNGram.GramToStringUsingRoot() + "\t" +
                        stemWordNGram.ClusterId + "\t" +
                        stemWordNGram.AlphabeticalOrderIdTagVectorToStringTabDelimited);
                }
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static async Task WriteToFileSpecificResultTypeForAgentsSpecialAsync(
            List<Agent> originalStemWordNGrams,
            string fileName,
            AgentDisplayOrderType agentDisplayOrderType)
        {
            var stemWordNGrams = originalStemWordNGrams.OrderAgents(agentDisplayOrderType);
            var streamWriter = new StreamWriter(fileName);
            try
            {
                foreach (var stemWordNGram in stemWordNGrams)
                {
                    await streamWriter.WriteLineAsync(
                        stemWordNGram.GramToStringUsingMostPopular() + "\t" +
                        stemWordNGram.ClusterId + "\t" +
                        stemWordNGram.AlphabeticalOrderIdTagVectorToStringTabDelimited);
                }
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static void WriteToFileSpecificResultTypeForAgents(
            List<Agent> originalStemWordNGrams,
            string fileName,
            AgentDisplayOrderType agentDisplayOrderType)
        {
            var stemWordNGrams = originalStemWordNGrams.OrderAgents(agentDisplayOrderType);
            var streamWriter = new StreamWriter(fileName);
            try
            {
                foreach (var stemWordNGram in stemWordNGrams)
                {
                    streamWriter.WriteLine(
                        stemWordNGram.GramToStringUsingRoot() + "\t" +
                        stemWordNGram.ClusterId + "\t" +
                        stemWordNGram.AlphabeticalOrderIdTagVectorToStringTabDelimited);
                }
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static void WriteToFileSpecificResultTypeForAgentsSpecial(
            List<Agent> originalStemWordNGrams,
            string fileName,
            AgentDisplayOrderType agentDisplayOrderType)
        {
            var stemWordNGrams = originalStemWordNGrams.OrderAgents(agentDisplayOrderType);
            var streamWriter = new StreamWriter(fileName);
            try
            {
                foreach (var stemWordNGram in stemWordNGrams)
                {
                    streamWriter.WriteLine(
                        stemWordNGram.GramToStringUsingMostPopular() + "\t" +
                        stemWordNGram.ClusterId + "\t" +
                        stemWordNGram.AlphabeticalOrderIdTagVectorToStringTabDelimited);
                }
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }


        public static async Task WriteOutMatrixToFileAsync(List<List<int>> matrix, string fileName)
        {
            var streamWriter = new StreamWriter(fileName);
            try
            {
                foreach (var row in matrix)
                {
                    await streamWriter.WriteLineAsync(row.Aggregate("", (current, column) => current + (column + ",")).TrimEnd(','));
                }
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static async Task WriteStemWordNGramsToGraphFileAsync(
            List<Agent> originalStemWordNGrams,
            string fileName,
            AgentDisplayOrderType agentDisplayOrderType)
        {
            var stemWordNGrams = originalStemWordNGrams.OrderAgents(agentDisplayOrderType);

            var streamWriter = new StreamWriter(fileName);
            try
            {
                foreach (var stemWordNGram in stemWordNGrams)
                {
                    await streamWriter.WriteLineAsync(stemWordNGram.GramToStringUsingRoot());
                    //await streamWriter.WriteLineAsync(stemWordNGram.GramToStringUsingRoot().Replace("-", "+"));
                }
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }


        public static async Task WriteStemWordNGramsToGraphFileUsingMostPopularVariantAsync(
            List<Agent> originalStemWordNGrams,
            string fileName,
            AgentDisplayOrderType agentDisplayOrderType)
        {
            var stemWordNGrams = originalStemWordNGrams.OrderAgents(agentDisplayOrderType);

            var streamWriter = new StreamWriter(fileName);
            try
            {
                foreach (var stemWordNGram in stemWordNGrams)
                {
                    await streamWriter.WriteLineAsync(stemWordNGram.GramToStringUsingMostPopular());
                    //await streamWriter.WriteLineAsync(stemWordNGram.GramToStringUsingRoot().Replace("-", "+"));
                }
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        //public static async Task WriteOutInitialClusteringResultsToFileAsync(
        //    List<Agent> clusteredAgents,
        //    FirstMergeOfAgentsFileName firstMergeOfAgentsFileName,
        //    FirstMergeOfAgentsGraphFileName firstMergeOfAgentsGraphFileName,
        //    FirstMergeTaggedDocIDsFileName firstMergeTaggedDocIDsFileName)
        //{
        //    await WriteToFileListOfAgentsInfoAsync(
        //        clusteredAgents,
        //        firstMergeOfAgentsFileName,
        //        AgentDisplayOrderType.None);

        //    await WriteStemWordNGramsToGraphFileAsync(
        //        clusteredAgents,
        //        firstMergeOfAgentsGraphFileName,
        //        AgentDisplayOrderType.None);

        //    await FileWriter.WriteOutMatrixToFileAsync(
        //        clusteredAgents.OrderAgents(AgentDisplayOrderType.TaggingVectorSize).TransformAgentsDocsIntoMatrix(),
        //        firstMergeTaggedDocIDsFileName);
        //}

        //public static async Task WriteOutClusteringOperationsResultsToFileAsync(
        //    List<Agent> clusteredAgents,
        //    SecondMergeOfAgentsFileName secondMergeOfAgentsFileName,
        //    SecondMergeOfAgentsGraphFileName secondMergeOfAgentsGraphFileName,
        //    SecondMergeTaggedDocIdsFileName secondMergeTaggedDocIdsFileName)
        //{
        //    await WriteToFileListOfAgentsInfoAsync(
        //        clusteredAgents,
        //        secondMergeOfAgentsFileName,
        //        AgentDisplayOrderType.TaggingVectorSize);

        //    await WriteStemWordNGramsToGraphFileAsync(
        //        clusteredAgents,
        //        secondMergeOfAgentsGraphFileName,
        //        AgentDisplayOrderType.TaggingVectorSize);

        //    await WriteOutMatrixToFileAsync(
        //        clusteredAgents.OrderAgents(AgentDisplayOrderType.TaggingVectorSize).TransformAgentsDocsIntoMatrix(),
        //        secondMergeTaggedDocIdsFileName);

        //}
    }
}
