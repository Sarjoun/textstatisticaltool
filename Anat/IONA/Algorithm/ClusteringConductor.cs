﻿using System;
using System.Linq;
using IONA.Helpers;
using IONA.Extensions;
using SDParser.Primitives;
using SDParser.ComplexTypes;
using System.Threading.Tasks;
using System.Collections.Generic;
using SDParser.CustomFileParsers;
using IONA.Algorithm.Algorithm.Helpers;
using IONA.Algorithm.Algorithm.NGrams.Structs;
using IONA.Algorithm.Algorithm.Clustering.Core;
using IONA.Algorithm.Algorithm.Clustering.Structs;
using LeagueOfMonads;
using RDotNet;
using SDParser.ClusteringTypes;
using SDParser.Extensions;

namespace IONA.Algorithm
{
    public static class ClusteringConductor
    {
        public static List<string> TaggedDocuments;
        public static List<string> UnTaggedDocuments;
        public static List<Agent> ClusteredAgents { get; set; }

        public static async Task<List<Agent>> TurnSuperNGramsIntoAgentsByClusteringThenMergingThenTaggingThemAsync(
          List<VocabularyWordStruct> vocabularyWordStructs, List<SuperNGram> superNGrams,
          AllSettings settingsForIONA, NGramIdList newNGramIdList)//, REngine rEngine)
        {
            try
            {
                await StemWordHelper.UploadStemWordsDictionaryFromFileAsync(settingsForIONA.WordFrequencyFileName, vocabularyWordStructs);

                var initialAgents = AgentsManager.ConvertSuperNGramsToDefaultAgents(superNGrams);

                if (false)//use DBScan for clustering the Ngrams
                {
                    ClusteredAgents = await AgentsManager
                                            .UpdateAgentsClusterIdsFromCorrespondingDbScanNgramSetItem(
                                                         initialAgents,
                                                         DbScanAlgorithmWrapper
                                                         .ClusterTheAgents(initialAgents, settingsForIONA))
                                            .WriteToFileListOfAgentsInfoAsync(settingsForIONA.ClusteredNGramMergeResultFileName);
                }
                //ClusteredAgents = await AgentsManager
                //                   .UpdateAgentsClusterIdsFromCorrespondingBasicClusteringSetItem(
                //                        initialAgents,
                //                        BasicClusteringWrapper.ClusterTheAgentsUsingR(initialAgents, settingsForIONA))
                //                    .WriteToFileListOfAgentsInfoAsync( 
                //                        settingsForIONA.ClusteredNGramMergeResultFileName);

                ClusteredAgents = await BasicClusteringWrapper
                    .ClusterTheAgentsUsingRAsync(initialAgents, settingsForIONA);

                await ClusteredAgents.WriteToFileListOfAgentsInfoAsync(settingsForIONA.ClusteredNGramMergeResultFileName);

                //ClusteredAgents = await ClusterDocumentsUsingAgents
                //        .UseClusteredAgentsToTagDatabaseDocsAndUpdateAgentsWithTaggedDocResults(
                //           ClusteredAgents.MergeAgentsBelongingToSameCluster(),
                //           await DocWordFileParser.GetDocWordsDictionaryAsync(settingsForIONA.DocWordFileName),
                //           CalculateDocumentTaggingThreshold(settingsForIONA.DocNGramTaggingThreshold))
                //        .DeleteAgentsWithNoTaggedDocs()
                //        .WriteOutInitialClusteringResultsToFileAsync(settingsForIONA);

                await ClusteredAgents.WriteOutInitialClusteringResultsToFileAsync(settingsForIONA);



                return ClusteredAgents;
                //return ClusteredAgents = await ClusterAgentsUsingTaggedDocs
                //        .MergeAgentsGramsUsingSuite(ClusteredAgents, settingsForIONA.InterNGramMergingThreshold,
                //           settingsForIONA.SecondMergeOfAgentsFileName)
                //        .WriteOutSecondaryClusteringOperationsResultsToFileAsync(settingsForIONA);
            }
            catch (Exception e)
            {
                WindowNotifications.DisplayErrorMessage($"Bug in ClusteringConductor at \r\n" 
                    +$"TurnSuperNGramsIntoAgentsByClusteringThenMergingThenTaggingThemAsync.\r\n", e);
                return null;
                //var newStemWordNGramsPoints = DbScanNGramSetItemsManager.TransformStemWordNGramsIntoNGramSetItemsWith2DPointsForDbScan(clusteredStemWordNGrams);
            }
        }

        private static int CalculateDocumentTaggingThreshold(DocNGramTaggingThreshold docNGramTaggingThreshold)
        {
            /* Determining the minimum threshold value for the StemGramNGrams (SGN) to tag a doc
             * is based on the smallest SGN's half size value divided by 2 and rounded up:
             * i.e. an SGN with 3 StemWords would have a threshold of 2 i.e. 2 words matching
             * This way documents can merge multiple SGNs.
            */  // minWordPresenceTaggingThreshold
            return (int)Math.Round((double)ClusteredAgents.
                OrderBy(s => s.NumberOfWordsInStartNGRAM).
                First().
                NumberOfWordsInStartNGRAM * docNGramTaggingThreshold);
        }

        public static async Task<TaggedAndUntaggedFileLinks> GetFinalTaggedAndUnTaggedDocumentsAsync(DocumentsLinksFileName documentsLinksFileName)
        {
            TaggedDocuments = new List<string>();
            UnTaggedDocuments = new List<string>();
            var taggedDocuments = new List<int>();
            foreach (var stemWordNGram in ClusteredAgents)
            {
                foreach (var taggedDocumentId in stemWordNGram.TaggedDocumentsId)
                {
                    if (!taggedDocuments.Contains(taggedDocumentId))
                        taggedDocuments.Add(taggedDocumentId);
                }
            }
            var documentsLinks = await LineByLineParsingFunctions.ReadLinesAsync(documentsLinksFileName);
            var documentsLinksList = documentsLinks.ToList();

            foreach (var docId in taggedDocuments)
            {
                if (!TaggedDocuments.Contains(documentsLinksList[docId - 1]))
                    TaggedDocuments.Add(documentsLinksList[docId - 1]);
            }

            foreach (var documentLinkList in documentsLinksList)
            {
                if (!TaggedDocuments.Contains(documentLinkList))
                    UnTaggedDocuments.Add(documentLinkList.RemoveDocIdFromStartOfPathIfItExists());
            }

            return new TaggedAndUntaggedFileLinks(TaggedDocuments, UnTaggedDocuments);


            //TODO Try it again using LINQ!
            //TaggedDocuments = documentsLinksList.
            //    FindAll(x => taggedDocuments.Contains(documentsLinksList.IndexOf(x-1)));

            //UnTaggedDocuments = documentsLinksList.
            //    FindAll(x => !taggedDocuments.Contains(documentsLinksList.IndexOf(x-1)));

            //return new TaggedAndUntaggedFileLinks(TaggedDocuments, UnTaggedDocuments);
        }
    }
}