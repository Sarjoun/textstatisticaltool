namespace IONA.Algorithm.Algorithm.NGrams.Structs
{
    public struct NGramIdStruct
    {
        public int NGramId { get; set; }
        public NGram NGram { get; set; }
        public SuperNGram SuperNGram { get; set; }

        public NGramIdStruct(int id, NGram ngram) : this()
        {
            NGramId = id;
            NGram = ngram;
            SuperNGram = null;
        }

        public NGramIdStruct(int id, SuperNGram superNGram)
            : this()
        {
            NGramId = id;
            NGram = superNGram;
            SuperNGram = superNGram;
        }
    }
}