using System;
using System.Collections;
using System.Collections.Generic;
using SDParser.Primitives;

namespace IONA.Algorithm.Algorithm.NGrams.Structs
{
    public class NGram
    {
        public int N { get; set; }//max number of grams
        public List<string> Grams { get; set; }
        public int Popularity { get; set; }

        public NGram(NumberOfWordsInStartNGRAM n, List<string> grams, int popularity)
        {
            N = n;
            Grams = grams;
            Popularity = popularity;
        }

        public NGram()
        {
            N = 0;
            Grams = new List<string>();
            Popularity = 0;
        }

        //Can compare different sized NGrams
        private double OldSimilarity(NGram otherNGram)
        {
            int totalTrue = Grams.Count;
            int actualTrue = 0;
            foreach (string thisString in Grams)
            {
                foreach (string otherString in otherNGram.Grams)
                {
                    if (thisString == otherString)
                        actualTrue++;
                }
            }

            if (totalTrue == actualTrue)
                return 1;
            else
                return actualTrue / totalTrue;
        }

        public bool Equivalent(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != GetType()) return false;

            var otherNGram = (NGram)obj;

            if (N == otherNGram.N)
            {
                var totalTrueCount = Grams.Count;
                var actualTrueCount = 0;
                if (Grams.Count == otherNGram.Grams.Count)
                {
                    foreach (var thisString in Grams)
                    {
                        foreach (var otherString in otherNGram.Grams)
                        {
                            if (thisString == otherString)
                                actualTrueCount++;
                            else
                            {
                                if (Compare2WordsByLetterNGram(thisString, otherString))
                                    actualTrueCount++;
                            }
                        }
                    }

                    return (totalTrueCount == actualTrueCount) ||
                           ((Convert.ToDouble(actualTrueCount) / Convert.ToDouble(totalTrueCount) * 10) > 5);
                }
                return false;
            }
            return false;
        }

        /// <summary>
        /// The algorithm is the following. Get all the ngrams for each of the 2 words where n is the length
        /// of the corresponding word. Then compare all the ngrams that come out of the 2 lists, where the 
        /// goal is to be able to find the % of both words in common. 
        ///    1.1 If the ratio in both words of identical ngrams is >= 0.5 in both words then they are 
        ///        similar.
        ///    1.2 Pick the largest ngram in size that is identical to both words, if the ratio of the ngram to
        ///        the word's size is >= 0.5 then they are equal.
        ///    1.3 If the ratio of of the identical ngram in one word is >= 0.8, irrespective what it is in the
        ///        the other word, then they are equal. This covers the case of one word being very short.
        /// </summary>
        /// <param name="word1"></param>
        /// <param name="word2"></param>
        /// <returns></returns>
        private static bool Compare2WordsByLetterNGram(string word1, string word2)
        {
            return CompareNGrams(word1, GetWordNGrams(word1), word2, GetWordNGrams(word2), 0.5);
        }

        public static List<string> GetWordNGrams(string word)
        {
            //string[] NGrams = NGram.GenerateNGrams(word, (int)Math.Ceiling((double)word.Length));
            string[] NGrams = GenerateNGrams(word, word.Length);
            List<string> results = new List<string>();
            if (NGrams != null)
                foreach (string st in NGrams)
                {
                    results.Add(st);
                }
            return results;
        }


        internal static string[] GenerateNGrams(string text, int gramLength)
        {
            if (text == null || text.Length == 0)
                return null;

            ArrayList grams = new ArrayList();
            int length = text.Length;
            if (length < gramLength)
            {
                string gram;
                for (int i = 1; i <= length; i++)
                {
                    gram = text.Substring(0, (i) - (0));
                    if (grams.IndexOf(gram) == -1)
                        grams.Add(gram);
                }

                gram = text.Substring(length - 1, (length) - (length - 1));
                if (grams.IndexOf(gram) == -1)
                    grams.Add(gram);

            }
            else
            {
                for (int i = 1; i <= gramLength - 1; i++)
                {
                    string gram = text.Substring(0, (i) - (0));
                    if (grams.IndexOf(gram) == -1)
                        grams.Add(gram);

                }

                for (int i = 0; i < (length - gramLength) + 1; i++)
                {
                    string gram = text.Substring(i, (i + gramLength) - (i));
                    if (grams.IndexOf(gram) == -1)
                        grams.Add(gram);
                }

                for (int i = (length - gramLength) + 1; i < length; i++)
                {
                    string gram = text.Substring(i, (length) - (i));
                    if (grams.IndexOf(gram) == -1)
                        grams.Add(gram);
                }
            }
            //return Tokenizer.ArrayListToArray(grams);
            return null;
        }

        /// <summary>
        ///    1.1 If the ratio in both words of identical ngrams is >= 0.5 in both words then they are 
        ///        similar.
        ///    1.2 Pick the largest ngram in size that is identical to both words, if the ratio of the ngram to
        ///        the word's size is >= 0.5 then they are equal.
        ///    1.3 If the ratio of of the identical ngram in one word is >= 0.8, irrespective what it is in the
        ///        the other word, then they are equal. This covers the case of one word being very short.
        /// </summary>
        /// <param name="list1"></param>
        /// <param name="list2"></param>
        /// <param name="ratio"></param>
        /// <returns></returns>
        private static bool CompareNGrams(string word1, List<string> list1, string word2, List<string> list2, double ratio)
        {
            int size1 = list1.Count;
            int size2 = list2.Count;
            int counter1 = 0;
            int counter2 = 0;

            List<string> count1 = new List<string>();
            List<string> count2 = new List<string>();

            foreach (string st1 in list1)
            foreach (string st2 in list2)
                if (st1 == st2)
                {
                    counter1++;
                    count1.Add(st1);
                }

            foreach (string st2 in list2)
            foreach (string st1 in list1)
                if (st2 == st1)
                {
                    counter2++;
                    count2.Add(st2);
                }

            string largestNgram1 = "";
            //Get the largest ngram in list1
            foreach (string st1 in count1)
            {
                if (st1.Length > largestNgram1.Length)
                    largestNgram1 = st1;
            }

            string largestNgram2 = "";
            //Get the largest ngram in list2
            foreach (string st2 in count2)
            {
                if (st2.Length > largestNgram2.Length)
                    largestNgram2 = st2;
            }

            if (((Convert.ToDouble(largestNgram1.Length) / Convert.ToDouble(word1.Length)) >= ratio) &
                ((Convert.ToDouble(largestNgram2.Length) / Convert.ToDouble(word2.Length)) >= ratio))
            {
                return true;
            }
            else
            {
                if (((Convert.ToDouble(largestNgram1.Length) / Convert.ToDouble(word1.Length)) >= 0.7) ||
                    ((Convert.ToDouble(largestNgram2.Length) / Convert.ToDouble(word2.Length)) >= 0.7))
                    return true;
                else
                    return false;
            }
        }

        public static double CompareNGramsValue(string word1, List<string> list1, string word2, List<string> list2, double ratio)
        {
            double result = 0;
            int size1 = list1.Count;
            int size2 = list2.Count;
            int counter1 = 0;
            int counter2 = 0;

            List<string> count1 = new List<string>();
            List<string> count2 = new List<string>();

            foreach (string st1 in list1)
            foreach (string st2 in list2)
                if (st1 == st2)
                {
                    counter1++;
                    count1.Add(st1);
                }

            foreach (string st2 in list2)
            foreach (string st1 in list1)
                if (st2 == st1)
                {
                    counter2++;
                    count2.Add(st2);
                }

            string largestNgram1 = "";
            //Get the largest ngram in list1
            foreach (string st1 in count1)
            {
                if (st1.Length > largestNgram1.Length)
                    largestNgram1 = st1;
            }

            string largestNgram2 = "";
            //Get the largest ngram in list2
            foreach (string st2 in count2)
            {
                if (st2.Length > largestNgram2.Length)
                    largestNgram2 = st2;
            }

            if (((Convert.ToDouble(largestNgram1.Length) / Convert.ToDouble(word1.Length)) >= ratio) &
                ((Convert.ToDouble(largestNgram2.Length) / Convert.ToDouble(word2.Length)) >= ratio))
            {
                result = ((Convert.ToDouble(largestNgram1.Length) / Convert.ToDouble(word1.Length))) + ((Convert.ToDouble(largestNgram2.Length) / Convert.ToDouble(word2.Length))) / 2;
                // return true;
            }
            else
            {
                if (((Convert.ToDouble(largestNgram1.Length) / Convert.ToDouble(word1.Length)) >= 0.7) ||
                    ((Convert.ToDouble(largestNgram2.Length) / Convert.ToDouble(word2.Length)) >= 0.7))
                {
                    result = ((Convert.ToDouble(largestNgram1.Length) / Convert.ToDouble(word1.Length))) + ((Convert.ToDouble(largestNgram2.Length) / Convert.ToDouble(word2.Length))) / 2;
                }
                else
                    return 0;
            }
            return result;
        }

        
        //use the same
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;


            //if (obj.GetType() != this.GetType()) return false;
            if ((obj.GetType() != typeof(NGram)) && (obj.GetType() != typeof(SuperNGram)))
                return false;

            NGram otherNGram = (NGram)obj;

            if (N == otherNGram.N)
            {
                int totalTrue = Grams.Count;
                int actualTrue = 0;
                if (Grams.Count == otherNGram.Grams.Count)
                {
                    foreach (string thisString in Grams)
                    {
                        foreach (string otherString in otherNGram.Grams)
                        {
                            if (thisString == otherString)
                                actualTrue++;
                            //else
                            //{
                            //    if(Compare2WordsByLetterNGram(thisString, otherString))
                            //        actualTrue++;
                            //}
                        }
                    }

                    //if ((totalTrue == actualTrue) || (Math.Ceiling(Convert.ToDouble(actualTrue) / Convert.ToDouble(totalTrue) * 10) > 65))
                    if ((totalTrue == actualTrue))// || (Math.Ceiling(Convert.ToDouble(actualTrue) / Convert.ToDouble(totalTrue) * 10) > 65))
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public override string ToString()
        {
            string returnString = "";

            //only print if there's something to print                
            if (Grams.Count != 0)
            {
                foreach (string st in Grams)
                    returnString += st + "+";
                returnString = returnString.Substring(0, returnString.Length - 1);
            }
            return returnString.Trim();
        }

        public string ToStringCSV_Gephi()
        {
            string returnString = "";

            //only print if there's something to print                
            if (Grams.Count != 0)
            {
                foreach (string st in Grams)
                    returnString += st + ";";
                //returnString = returnString.Substring(0, returnString.Length - 1);
            }
            return returnString.Trim();
        }



        public string ToStringExport()
        {
            string returnString = "";

            //only print if there's something to print                
            if (Grams.Count != 0)
            {
                foreach (string st in Grams)
                    returnString += st + " ";
                returnString = returnString.Substring(0, returnString.Length - 1);
            }
            return returnString.Trim();
        }

        /// <summary>
        /// By definition an empty Ngram has list = 0, or N = 0;
        /// </summary>
        /// <returns></returns>
        public bool Empty()
        {
            return (Grams.Count == 0);
        }

    }
}
