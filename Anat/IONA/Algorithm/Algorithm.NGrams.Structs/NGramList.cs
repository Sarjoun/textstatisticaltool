using System;
using System.Collections.Generic;
using System.Linq;
using SDParser.Primitives;

namespace IONA.Algorithm.Algorithm.NGrams.Structs
{
    public class NGramList
    {
        public List<NGram> NGramsList { get; set; }
        public int HighestNGramPopularityValue { get; set; }
        public int LowestNGramPopularityValue { get; set; }
        public int PopularityCount()
        {
            return NGramsList.Sum(ng => ng.Popularity);
        }
        public double AverageNGramPopularityValue()
        {
            UpdatePopularityValues();
            return (HighestNGramPopularityValue + (double)LowestNGramPopularityValue) / 2;
        }
        public NGramList()
        {
            NGramsList = new List<NGram>();
            HighestNGramPopularityValue = 1;
            LowestNGramPopularityValue = 1;
        }

        public void AddNGram(NGram newNGRAM)
        {
            var canAdd = true;
            var currentNGram = new NGram();

            //check if ngram is in the list.
            foreach (var ngram in NGramsList)
            {
                if (!ngram.Equals(newNGRAM)) continue;
                canAdd = false;
                currentNGram = ngram;
            }

            //if the nGram is not in the list => add it
            //else update it 
            if (canAdd)
            {
                NGramsList.Add(newNGRAM);
            }
            else
            {
                NGramsList.Remove(currentNGram);
                NGramsList.Add(
                    new NGram(
                        NumberOfWordsInStartNGRAM.FromValue(currentNGram.N), 
                        currentNGram.Grams, 
                        currentNGram.Popularity + 1));
            }
        }

        public void RemoveNGram(NGram ngram)
        {
            try
            {
                NGramsList.Remove(ngram);
                UpdatePopularityValues();
            }
            catch (Exception)
            {
                //do nothing
            }
        }

        //especially useful after an ngram has been removed.
        private void UpdatePopularityValues()
        {
            HighestNGramPopularityValue = 1;
            LowestNGramPopularityValue = 1;
            foreach (var ng in NGramsList)
            {
                if (ng.Popularity > HighestNGramPopularityValue)
                    HighestNGramPopularityValue = ng.Popularity;
                else if (ng.Popularity < LowestNGramPopularityValue)
                    LowestNGramPopularityValue = ng.Popularity;
            }
        }
    }
}
