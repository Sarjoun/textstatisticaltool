using System;
using System.Collections.Generic;
using System.Linq;
using SDParser.Structures;

namespace IONA.Algorithm.Algorithm.NGrams.Structs
{
    //This class inherits from NGram by adding the list of word variants of each
    //word in the N-Gram words.
    public class SuperNGram : NGram
    {
        //This struct is internal to SuperNGram
        public struct SuperNGramWordVariantList
        {
            public int N { get; set; }//max number of grams
            public List<string> Grams { get; set; }
            public int Popularity { get; set; }
            public string Word { get; set; }
            public List<VariantWord> List { get; set; }

            public SuperNGramWordVariantList(string word, List<VariantWord> list)
                : this()
            {
                Word = word;
                List = list;
            }

            public bool CheckIfWordContainedInList(string word)
            {
                return List.Any(variantWordStruct => variantWordStruct.Variant == word);
            }

        }

        public List<SuperNGramWordVariantList> WordVariantList { get; set; }
        public List<List<VariantWord>> VariantLists { get; set; }

        //This is the ordered version of the NGrams. I don't want the SuperNGram class to 
        //automatically order the NGram when it applies CheckIfSuperGramInText because
        //I might find reasons not to order. For now I'll keep a copy of the OrderedGrams
        //and allow the structure to update its base.Grams with its OrderedGrams if asked to.
        public List<string> OrderedGrams { get; set; }

        public bool UpdateOriginalGramsWithOrderedGrams()
        {
            if (OrderedGrams.Count != 0)
            {
                Grams = OrderedGrams;
                return true;
            }
            return false;
        }

        //The superNGram constructor takes the base ngram class and the list of variantsWordList
        //and assigns by order the possible variant values of the ngram words
        public SuperNGram(NGram ngram, List<List<VariantWord>> variantsList)
        {
            VariantLists = variantsList;
            Grams = ngram.Grams;
            N = ngram.N;
            Popularity = ngram.Popularity;
            WordVariantList = new List<SuperNGramWordVariantList>();
            BuildVariationNGramLists();
        }

        public void BuildVariationNGramLists()
        {
            for (var i = 1; i <= N; i++)//My NGram WordVariantList counter starts at 1
            {
                WordVariantList.Add(new SuperNGramWordVariantList(base.Grams[i - 1], VariantLists[i - 1]));
            }
        }

        /// <summary>
        /// There are N lists in the superNgram, one list for each word that makes up the NGram.
        /// If I can find at least one matching word from every list in the text, then I consider
        /// it a success.            
        /// 
        /// If order is set to True then the local OrderedGrams are updated with the correct order
        /// of the NGram words. If not needed I would be saving processing time.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="fileContentsMinusTitle"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public bool CheckIfSuperGramInText(string text, string fileContentsMinusTitle, bool order)
        {
            //string[] words = fileContentsMinusTitle.Split(' '); 
            var wordsArray = text.Split(' ');//used for the ordering (additional functionality can                
                                             //be added here to give the words more value (like using NLP or capital letter to determine Nouns etc...)       
            var generalCounter = 0;
            foreach (var variantList in WordVariantList)
            {
                foreach (var variantWordStruct in variantList.List)
                {
                    if (text.Contains(variantWordStruct.Variant))
                    {
                        generalCounter++;
                        break;
                    }
                }
            }

            #region order the superNgram
            if (order)
            {
                OrderedGrams = new List<string>();
                if (generalCounter > 1)// or greater than 1 (same thing)
                {
                    //do the ordering here...
                    foreach (var wordString in wordsArray)
                    {
                        foreach (SuperNGramWordVariantList wordVariantList in WordVariantList)
                        {
                            foreach (VariantWord wordVariant in wordVariantList.List)
                            {
                                if (wordString.Contains(wordVariant.Variant))
                                {
                                    //tempOrderedNGram.Add(w_vl.Word);
                                    if (!OrderedGrams.Contains(wordVariantList.Word))
                                        OrderedGrams.Add(wordVariantList.Word);
                                    break;
                                }
                            }
                        }
                    }

                    /* If not all the original SuperNGram was ordered i.e. original = Ivory Coast Elections 
                       and only Ivory and Coast were found then only Ivory-Coast will be added and Elections
                       will dropped out. 
                       So I need to add the missing N-Gram components. The rule now would be to add them 
                       AFTER the ordered N-Gram because the idea is that it should have a lower importance
                       then the preceding ordered grams which were found in the text. The other words such
                       as Elections might be found in other news stories texts and would be ordered properly
                       eventually when the TaggedDocs are aggregated. */

                    //Find out the remaining words/NGrams and add them (without order) to the end of the list
                    //List<string> remainingWords = new List<string>();
                    //OrderedGrams = new List<string>();

                    foreach (var strng in Grams)
                    {
                        if (!OrderedGrams.Contains(strng))
                            OrderedGrams.Add(strng);
                    }

                    //OrderedGrams = tempOrderedNGram;
                    //sanity check, both grams lists should be equal in size
                    Grams = OrderedGrams;
                }
            }
            #endregion
            return (Math.Abs(generalCounter - N) < 2); //KNOB  //relaxed the check //knob as in a controller
        }
    }
}
