﻿namespace IONA.Algorithm.Algorithm.NGrams.Structs
{
   public class TotalAndPopularNGramsContainer
    {
        public NGramList AllNGramList { get; set; }//All NGrams
        public NGramList PopularNGramList { get; set; } //Popular NGrams
        public NGramIdList PopularNGramIdList { get; set; }

        public TotalAndPopularNGramsContainer(NGramList allNGramList, NGramList popularNGramList)
        {
            AllNGramList = allNGramList;
            PopularNGramList = popularNGramList;
            PopularNGramIdList = new NGramIdList(PopularNGramList);
        }
    }
}
