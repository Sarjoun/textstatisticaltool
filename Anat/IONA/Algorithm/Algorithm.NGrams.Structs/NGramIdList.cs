using System.Collections.Generic;

namespace IONA.Algorithm.Algorithm.NGrams.Structs
{
    public class NGramIdList
    {
        public NGramList OriginalList { get; set; }
        public List<NGramIdStruct> IdList { get; set; }

        public NGramIdList(NGramList list)
        {
            OriginalList = list;
            IdList = new List<NGramIdStruct>();

            for (var i = 0; i < list.NGramsList.Count; i++)
            {
                IdList.Add(new NGramIdStruct(i, list.NGramsList[i]));
            }
        }

    }
}
