﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IONA.Algorithm.Algorithm.NGrams.Structs;
using IONA.Extensions;
using SDParser.Primitives;

namespace IONA.Algorithm.Algorithm.NGrams.Core
{
    public static class PopularNGramsExtractor
    {
        public static NGram GetNGramFromPopularNGramFileLine(
            string popularNGramFileLine,
            NumberOfWordsInStartNGRAM numberOfWordsInStartNGRAM)
        {
            char[] bufferChars = { ' ' };
            var test = popularNGramFileLine.Split(bufferChars, StringSplitOptions.None)
                .Where(x => x != string.Empty).ToArray();
            return new NGram(
                numberOfWordsInStartNGRAM,
                GetNGramList(test[2]),
                Convert.ToInt32(test[1]));
        }

        private static List<string> GetNGramList(this string ngramString)
        {
            return ngramString.Split('+').ToList();
        }

        public static async Task<List<NGram>> GetListOfPopularNGramsFromFileAsync(
            PopularNGramsFileName popularNGramsFileName,
            NumberOfWordsInStartNGRAM numberOfWordsInStartNGRAM)
        {
            var returnPopularNGramsList = new List<NGram>();
            using (var reader = File.OpenText(popularNGramsFileName))
            {
                var fileText = await reader.ReadToEndAsync();

                returnPopularNGramsList.
                        AddRange(
                        fileText.
                        RemoveLastEntryIfNewLine().
                        Split(
                            new[] { Environment.NewLine },
                            StringSplitOptions.None).
                            ToArray().
                            Select(line => GetNGramFromPopularNGramFileLine(
                                line,
                                numberOfWordsInStartNGRAM)));
            }
            return returnPopularNGramsList;
        }
    }
}
