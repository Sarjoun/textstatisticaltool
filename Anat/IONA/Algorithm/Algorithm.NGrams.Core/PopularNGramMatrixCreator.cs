﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
//using ILDA.Algorithm.Algorithm.Helpers;
using IONA.Algorithm.Algorithm.NGrams.Structs;
using IONA.Extensions;
using SDParser.Primitives;

namespace IONA.Algorithm.Algorithm.NGrams.Core
{
   public static class PopularNGramMatrixCreator
   {
      public static async Task<double[][]> WritePopularNGramsSimilarityMatrixFileAsync(
         NGramList popularNGramList,
         PopularNGramsSimilarityMatrixFileName popularNGramsSimilarityMatrixFileName)
      {
         //Create NGram matrix for all the popular NGrams
         var size = popularNGramList.NGramsList.Count;
          double[][] nGramMatrix = null;//MathHelper.ReturnZeroValueDoubleMatrix(size, size);
         List<NGram> nGramlist = popularNGramList.NGramsList;
         var matrixShiftCounter = -1;

         for (var i = 0; i < size; i++)
         {
            matrixShiftCounter++;
            for (var j = matrixShiftCounter; j < size; j++)
            {
               if (nGramlist[i].IsSimilar(nGramlist[j], 4, 0.5))
                  nGramMatrix[i][j] = 1;
            }
         }
         TextWriter tw = new StreamWriter(popularNGramsSimilarityMatrixFileName.Value);

         for (var i = 0; i < size; i++)
         {
            for (var j = 0; j < size; j++)
            {
               await tw.WriteAsync(nGramMatrix[i][j] + " ");
            }
            await tw.WriteAsync("\n");
         }

         tw.Close();
         tw.Dispose();
         return nGramMatrix;
      }

   }
}
