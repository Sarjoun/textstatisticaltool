using System;
using System.IO;
using System.Linq;
using SDParser.Primitives;
using SDParser.ComplexTypes;
using SDParser.Combinatorics;
using System.Threading.Tasks;
using System.Collections.Generic;
using IONA.Algorithm.Algorithm.NGrams.Structs;
using SDParser.FileFunctions;
using SDParser.Notifications;

namespace IONA.Algorithm.Algorithm.NGrams.Core
{
    public static class PopularNGramsCreator
    {
        /// <summary>
        /// This method is given the bag-of-words topics and then creates all the possible combinations based
        /// on the value of N which represents the Gram value. The popularity value is calculated based on a
        /// function we devised that keeps the number of passing ngrams the same irrespective of the value of N
        /// </summary>
        public static async Task<TotalAndPopularNGramsContainer> CreateNGramsAndWriteThemToFileAsync(
           AllSettings generalSettingsForIONA)
        {
            var numberOfWordsNGRAM = generalSettingsForIONA.NumberOfWordsInStartNGRAM;
            var ngramThreshold = generalSettingsForIONA.NGramsThresholdValue;
            var topicsFileName = generalSettingsForIONA.TopicsBagOfWordsFileName;
            var allNGramsFileName = generalSettingsForIONA.AllNGramsFileName;
            var popularNGramsFileName = generalSettingsForIONA.PopularNGramsFileName;

            var allNGramList = new NGramList();
            var popNGramList = new NGramList();
            var topicsTemp = await FileProcessingFunctions.GetTopicsStringArrayFromFileAsync(topicsFileName.Value);
            var topics = topicsTemp.Select(x => x.Substring(x.IndexOf(')') + 1).Trim());

            foreach (var t in topics)
            {
                var combinations = new Combinations<string>(t.Split(' '), numberOfWordsNGRAM);
                foreach (var grams in combinations)
                {
                    allNGramList.AddNGram(new NGram(numberOfWordsNGRAM, grams.ToList(), 1));
                }
            }

            //In the 2 following procedures: 1-Write all N-Grams  2-Write important N-Grams
            //Write out the result of all NGrams to a file.
            try
            {
                var streamWriterAllNGramsFileName = new StreamWriter(allNGramsFileName.Value);
                var allNgramFileCounter = 1;

                foreach (var nGram in allNGramList.NGramsList)
                {
                    await streamWriterAllNGramsFileName.WriteLineAsync(
                        allNgramFileCounter + " " + nGram.Popularity + " " + nGram);
                    allNgramFileCounter++;
                }

                streamWriterAllNGramsFileName.Close();
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit("Error in PopularNGramsCreator.", e);
            }

            if (ngramThreshold == 0)
                ngramThreshold = NGramsThresholdValue
                    .FromValue(Convert.ToInt32(Math.Ceiling(allNGramList.AverageNGramPopularityValue())));

            try
            {
                var sw = new StreamWriter(popularNGramsFileName);
                popNGramList.NGramsList = new List<NGram>();
                var popularNgramFileCounter = 1;

                foreach (var ng in allNGramList.NGramsList)
                {
                    if (ng.Popularity < ngramThreshold.Value) continue;

                    //found a popular ngram.
                    popNGramList.NGramsList.Add(ng);
                    await sw.WriteLineAsync(popularNgramFileCounter + " " + ng.Popularity + " " + ng);
                    popularNgramFileCounter++;
                    //returnResult += ng + "\r\n";
                }
                //trim the last newline off returnResult
                //returnResult = returnResult.TrimEnd(new char[] { '\r', '\n' });

                sw.Close();
                sw.Dispose();
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit("Error in CreateNGramsAndWriteThemToFileAsync.", e);
            }

            return new TotalAndPopularNGramsContainer(allNGramList, popNGramList);
        }
    }
}