﻿using System;

namespace IONA.Algorithm.Algorithm.NGrams.Core
{
    public static class NGramThresholdManager
    {
        /// <summary>
        /// We used a function \theta_n = R.e ^{ 1 - n /\alpha}
        /// where the bestalpha was found to be number 5.
        /// I also calculated the values in Matlab and found out the best values
        ///for e ^{ (1 - n) / alpha}
        /// </summary>
        /// <param name="numberOfRuns"></param>
        /// <param name="numberOfNGrams"></param>
        /// <returns></returns>
        public static double Threshold(int numberOfRuns, int numberOfNGrams)
        {
            double eValue;
            switch (numberOfNGrams)
            {
                case 1:
                    eValue = 1;
                    return Math.Ceiling(numberOfRuns * eValue);
                case 2:
                    eValue = 0.8187;
                    return Math.Ceiling(numberOfRuns * eValue);
                case 3:
                    eValue = 0.6703;
                    return Math.Ceiling(numberOfRuns * eValue);
                case 4:
                    eValue = 0.5488;
                    return Math.Ceiling(numberOfRuns * eValue);
                case 5:
                    eValue = 0.4493;
                    return Math.Ceiling(numberOfRuns * eValue);
                case 6:
                    eValue = 0.3679;
                    return Math.Ceiling(numberOfRuns * eValue);
                case 7:
                    eValue = 0.3012;
                    return Math.Ceiling(numberOfRuns * eValue);
                case 8:
                    eValue = 0.2466;
                    return Math.Ceiling(numberOfRuns * eValue);
                case 9:
                    eValue = 0.2019;
                    return Math.Ceiling(numberOfRuns * eValue);
                case 10:
                    eValue = 0.1653;
                    return Math.Ceiling(numberOfRuns * eValue);
                default: return 0;
            }
        }
    }
}
