﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IONA.Algorithm.Algorithm.Clustering.Structs;
using IONA.Algorithm.Algorithm.NGrams.Structs;
using SDParser;
using SDParser.ComplexTypes;
using SDParser.CustomFileParsers;
using SDParser.Primitives;
using SDParser.Structures;

namespace IONA.Algorithm.Algorithm.NGrams.Core
{
    public static class SuperNGramsInitializer
    {
        private static Dictionary<string, StemWord> _stemWordsAndVariantsDictionary;
        public static async Task<List<SuperNGram>> BuildInitialSuperNGramsFromFilesAsync(
            List<VocabularyWordStruct> vocabularyWordStructs, AllSettings generalSettingsForIONA)
        {
            var popularNGramsFileName = generalSettingsForIONA.PopularNGramsFileName;
            var numberOfWordsInStartNGRAM = generalSettingsForIONA.NumberOfWordsInStartNGRAM;
            var wordFrequencyFileName = generalSettingsForIONA.WordFrequencyFileName;

            var popularNGrams = await PopularNGramsExtractor.
                GetListOfPopularNGramsFromFileAsync( popularNGramsFileName, numberOfWordsInStartNGRAM);

            _stemWordsAndVariantsDictionary = await WordFrequencyFileParser.
                    GetStemWordsDictionaryAsync(wordFrequencyFileName);

            //Update the stemWords with their Vocabulary Id and Frequency value
            foreach (var entry in _stemWordsAndVariantsDictionary)
            {
                entry.Value.DictionaryId = vocabularyWordStructs.Find(x => x.Word == entry.Key).VocabularyId;
            }

            return popularNGrams.Select(
                nGram => new SuperNGram(nGram, GetVariantWordListOldDesign(nGram))).ToList();
        }

        public static List<List<VariantWord>> GetVariantWordListOldDesign(NGram nGram)
        {
            return nGram.Grams.Select(word => _stemWordsAndVariantsDictionary[word].Variants).ToList();
        }
    }
}
