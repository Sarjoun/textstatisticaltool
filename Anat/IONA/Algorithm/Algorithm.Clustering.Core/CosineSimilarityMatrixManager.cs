﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IONA.Algorithm.Algorithm.Helpers;
using SDParser.ClusteringTypes;
using SDParser.Primitives;

namespace IONA.Algorithm.Algorithm.Clustering.Core
{
    public static class CosineSimilarityMatrixManager
    {
        public static async Task CreateInitialStemWordNGramCosineSimilarityMatrixStemWordNGrams(
            InitialAgentCosineSimilarityMatrixFileName initialAgentCosineSimilarityMatrixFileName,
            List<Agent> stemWordNGrams)
        {
            var streamWriter = new StreamWriter(initialAgentCosineSimilarityMatrixFileName);
            try
            {
                foreach (var stemWordNGram in stemWordNGrams)
                {
                    await streamWriter.WriteLineAsync(
                        TransformSimilarityArrayToString(
                            stemWordNGram.SimilarityArrayWithOtherNGramsBasedOnAlphabet(stemWordNGrams)));
                    //agent.SimilarityArrayWithOtherNGramsBasedOnFrequency(stemWordNGrams)));
                }
                streamWriter.Close();
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static float ndistance(float[] a, float[] b)
        {
            var total = a.Select((t, i) => b[i] - t).Sum(diff => diff * diff);
            return (float)Math.Sqrt(total);
        }
        public static string TransformSimilarityArrayToString(double[] similarityArray)
        {
            return similarityArray.Aggregate("", (current, similar) => current + (similar + "\t")).TrimEnd();
        }

        public static double[] SimilarityArrayWithOtherNGramsBasedOnFrequency(this Agent agent,
            List<Agent> stemWordNGrams)
        {
            return stemWordNGrams.Select(
                    otherNGram => CosineSimilarity.
                        ComputeCosineSimilarity(
                            agent.StemWordFrequencyOrderIdTagVector, 
                            otherNGram.StemWordFrequencyOrderIdTagVector))
                            .Select(dummy => (double) dummy).ToArray();
        }

        public static double[] SimilarityArrayWithOtherNGramsBasedOnAlphabet(this Agent agent,
            List<Agent> stemWordNGrams)
        {
            return stemWordNGrams.Select(
                    otherNGram => CosineSimilarity.
                        ComputeCosineSimilarity(
                            agent.StemWordAlphabeticalOrderIdTagVector,
                            otherNGram.StemWordAlphabeticalOrderIdTagVector))
                .Select(dummy => (double)dummy).ToArray();
        }


    }
}
