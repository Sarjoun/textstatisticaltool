﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using IONA.Enums;
using IONA.Algorithm.Algorithm.Clustering.Structs;
using IONA.Algorithm.Algorithm.Helpers;
using LeagueOfMonads;
using LeagueOfMonads.NoLambda;
using SDParser.ClusteringTypes;
using SDParser.Primitives;

namespace IONA.Algorithm.Algorithm.Clustering.Core
{
    public static class ClusterAgentsUsingTaggedDocs
    {
        public static List<Agent> MergeAgentsGramsUsingSuite(List<Agent> stemWordNGrams,
            InterNGramMergingThreshold interNGramMergingThreshold,
            string fileName)
        {
            return Identity.Create(stemWordNGrams).
                Map(MergeAgentsWithSameCountAndIdenticalDocTagIds).
                Tee(PrintInterimClusteringResults, fileName.Replace(".txt", "_Interim1.txt")).
                Map(MergeAgentsWithSameCountAndSimilarDocTagIds, interNGramMergingThreshold).
                Tee(PrintInterimClusteringResults, fileName.Replace(".txt", "_Interim2.txt")).
                Map(MergeAgentsWithUnequalDocTagIds, interNGramMergingThreshold).
                Tee(PrintInterimClusteringResults, fileName.Replace(".txt", "_Interim3.txt")).Value;

            //return Identity.Create(stemWordNGrams).
            //    Map(MergeAgentsWithSameCountAndIdenticalDocTagIds).
            //    Tee(PrintInterimClusteringResults, fileName.Replace(".txt", "_Interim1.txt")).
            //    Tee(PrintInterimClusteringResultsSpecial, fileName.Replace(".txt", "_Interim1_S.txt")).
            //    Map(MergeAgentsWithSameCountAndSimilarDocTagIds, interNGramMergingThreshold).
            //    Tee(PrintInterimClusteringResults, fileName.Replace(".txt", "_Interim2.txt")).
            //    Tee(PrintInterimClusteringResultsSpecial, fileName.Replace(".txt", "_Interim2_S.txt")).
            //    Map(MergeAgentsWithUnequalDocTagIds, interNGramMergingThreshold).
            //    Tee(PrintInterimClusteringResults, fileName.Replace(".txt", "_Interim3.txt")).
            //    Tee(PrintInterimClusteringResultsSpecial, fileName.Replace(".txt", "_Interim3_S.txt")).Value;

        }
        //var temp1 = Task.Run(() => MakeNodesStatements(nodeNames));

        public static void PrintInterimClusteringResults(this List<Agent> stemWordNGrams, string fileName)
        {
            FileWriter.WriteToFileSpecificResultTypeForAgents(stemWordNGrams, fileName, AgentDisplayOrderType.None);
        }

        //public static void PrintInterimClusteringResultsSpecial(this List<Agent> stemWordNGrams, string fileName)
        //{
        //    FileWriter.WriteToFileSpecificResultTypeForAgents(stemWordNGrams, fileName, AgentDisplayOrderType.None);
        //}

        public static async Task PrintInterimClusteringResultsAsync(this List<Agent> stemWordNGrams, string fileName)
        {
            await FileWriter.WriteToFileSpecificResultTypeForAgentsAsync(
                stemWordNGrams, fileName, AgentDisplayOrderType.None);
        }

        public static List<Agent> MergeAgentsWithUnequalDocTagIds(
          this List<Agent> stemWordNGrams, InterNGramMergingThreshold interNGramMergingThreshold)
        {
            //sort the list
            var sortedList = stemWordNGrams.OrderBy(x => x.TaggedDocumentsId.Count).ToList();
            List<MergingAgentResult> mergingAgentResults;
            do
            {
                mergingAgentResults = new List<MergingAgentResult>();
                for (var firstAgentId = 0; firstAgentId < sortedList.Count; firstAgentId++)
                {
                    for (var secondAgentId = firstAgentId + 1; secondAgentId < sortedList.Count; secondAgentId++)
                    {
                        mergingAgentResults.Add(
                            MergeAgentsIfOneAgentIsContainedInOtherAgentAndReturnMergeResult(
                                sortedList[firstAgentId],
                                firstAgentId,
                                sortedList[secondAgentId],
                                secondAgentId,
                                interNGramMergingThreshold));
                    }
                }

                //get the list of the agents that never merged
                var returnResult = sortedList.Where((t, i) =>
                            !mergingAgentResults.Any(
                                x => x.AssimilatedAgentId == i &&
                                x.AgentsToBeMerged == AgentsToBeMerged.Yes)
                                &&
                            !mergingAgentResults.Any(
                                x => x.AssimilatorAgentId == i &&
                                x.AgentsToBeMerged == AgentsToBeMerged.Yes)
                                ).ToList();

                //get the merger of the agents that merged
                mergingAgentResults = mergingAgentResults.
                                            Where(x => x.AgentsToBeMerged == AgentsToBeMerged.Yes).ToList();

                //group the agents by the assimilator agent
                for (var i = 0; i < sortedList.Count; i++)
                {
                    var temp = mergingAgentResults.Where(x => x.AssimilatorAgentId == i).ToList();
                    var listToMerge = temp.Select(x => x.AssimilatedAgent).ToList();

                    if (temp.Any())
                        returnResult.Add(
                        temp
                        .First()
                        .AssimilatorAgent
                        .MergeAgentsHavingSmallerOrEqualTaggedDocsIds(listToMerge));
                }

                sortedList = returnResult;
            } while (mergingAgentResults.Count > 0);

            return sortedList;
        }

        public static MergingAgentResult MergeAgentsIfOneAgentIsContainedInOtherAgentAndReturnMergeResult(
            Agent agent, int stemWordNGramId,
            Agent nextAgent, int nextStemWordNGramId, float threshold)
        {
            if (agent.IsContainedIn(nextAgent, threshold))
                return new MergingAgentResult(agent, stemWordNGramId, nextAgent, nextStemWordNGramId, AgentsToBeMerged.Yes);

            if (nextAgent.IsContainedIn(agent, threshold))
                return new MergingAgentResult(nextAgent, nextStemWordNGramId, agent, stemWordNGramId, AgentsToBeMerged.Yes);

            return new MergingAgentResult(agent, stemWordNGramId, nextAgent, nextStemWordNGramId, AgentsToBeMerged.No);
        }

        public static bool IsContainedIn(this Agent current, Agent other, float threshold)
        {
            if (current.TaggedDocumentsId.Count > other.TaggedDocumentsId.Count)
                return false;

            var intersect = current.TaggedDocumentsId.Intersect(other.TaggedDocumentsId).ToList();

           if (intersect.Count == current.TaggedDocumentsId.Count)
              return true;

            return ((float)intersect.Count / (float)current.TaggedDocumentsId.Count) >= threshold;
        }

        public static List<Agent> MergeAgentsWithSameCountAndIdenticalDocTagIds(
            this List<Agent> stemWordNGrams)
        {
            var stemWordNGramsRemaining = stemWordNGrams.ToList();
            var mergedStemWordNGrams = new List<Agent>();

            try
            {
                var stemWordClusterPairs = CreateListOfTaggedDocumentsListsFromAllAgents(stemWordNGrams).
                    CreateListOfPairsByGroupingAgentsUsingTheirTaggedDocsCount().
                    ToList();

                foreach (var stemWordClusterPair in stemWordClusterPairs)
                {
                    var result = stemWordNGrams.
                        GetAllAgentsWithTaggedDocumentIdsOfCount(stemWordClusterPair.Key);

                    stemWordNGramsRemaining = stemWordNGramsRemaining.Except(result).ToList();

                    mergedStemWordNGrams.AddRange(
                        AgentsManager.
                            MergeListOfAgentsHavingSameCountAndIdenticalTaggedDocsIds(result));
                }
                mergedStemWordNGrams.AddRange(stemWordNGramsRemaining);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return mergedStemWordNGrams;
        }

        public static List<Agent> MergeAgentsWithSameCountAndSimilarDocTagIds(
            this List<Agent> agents, InterNGramMergingThreshold interNGramMergingThreshold)
        {
            var agentsRemaining = agents.ToList();
            var mergedAgents = new List<Agent>();

            var stemWordClusterPairs = CreateListOfTaggedDocumentsListsFromAllAgents(agents).
                CreateListOfPairsByGroupingAgentsUsingTheirTaggedDocsCount().
                ToList();

            foreach (var stemWordClusterPair in stemWordClusterPairs)
            {
                var result = agents.GetAllAgentsWithTaggedDocumentIdsOfCount(stemWordClusterPair.Key);
                agentsRemaining = agentsRemaining.Except(result).ToList();
                mergedAgents.AddRange(AgentsManager.MergeListOfAgentsHavingSameCountAndSimilarTaggedDocsIds(result, interNGramMergingThreshold));
            }
            mergedAgents.AddRange(agentsRemaining);
            return mergedAgents;
        }

        public static List<List<float>> CreateListOfTaggedDocumentsListsFromAllAgents
            (this List<Agent> agents)
        {
            return agents.Select(agent => agent.TaggedDocumentsIdFloat.ToList()).ToList();
        }

        public static IOrderedEnumerable<KeyValuePair<int, int>> CreateListOfPairsByGroupingAgentsUsingTheirTaggedDocsCount
            (this List<List<float>> allTaggedDocumentsFromAllStemWordNGrams)
        {
            return allTaggedDocumentsFromAllStemWordNGrams.GroupBy(x => x.Count)
                        .Where(g => g.Count() > 1)
                        .ToDictionary(x => x.Key, y => y.Count())
                        .OrderBy(x => x.Key);
        }

        public static IOrderedEnumerable<KeyValuePair<int, int>> CreateListOfSinglesBasedOnTheCountOfTheirTaggedDocsCount
            (this List<List<float>> allTaggedDocumentsFromAllStemWordNGrams)
        {
            return allTaggedDocumentsFromAllStemWordNGrams.GroupBy(x => x.Count)
                    .Where(g => g.Count() == 1)
                    .ToDictionary(x => x.Key, y => y.Count())
                    .OrderBy(x => x.Key);
        }

        public static List<Agent> GetAllAgentsWithTaggedDocumentIdsOfCount
            (this List<Agent> stemWordNGrams, int documentsIdsCount)
        {
            return stemWordNGrams.Where(swn => swn.TaggedDocumentsId.Count == documentsIdsCount).ToList();
        }
    }
}