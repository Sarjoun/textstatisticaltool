﻿//using System.Collections.Generic;
//using System.IO;
//using ILDA.Algorithm.Algorithm.Helpers;
//using IONA.Algorithm.Algorithm.Clustering.Structs;
//using IONA.Algorithm.Algorithm.NGrams.Structs;
//using SDParser.Primitives;

//namespace IONA.Algorithm.Algorithm.Clustering.Core
//{
//    public static class Tagger
//    {
//        //Create NGram matrix for all the popular NGrams
//        public static double[][] CreateTaggedDocSimilarityMatrix(
//           List<TaggedDoc> taggedDocList,
//           TaggedDocumentsSimilarityMatrixFileName taggedDocumentsSimilarityMatrixFileName)
//        {
//            //int size = Globals.TaggedDocuments.Count;
//            var size = taggedDocList.Count;
//            double[][] taggedDocMatrix = MathHelper.ReturnZeroValueDoubleMatrix(size, size);
            
//            //List<TaggedDoc> taggedDocList = Globals.TaggedDocuments;

//            int matrixShiftCounter = -1;

//            for (int i = 0; i < size; i++)
//            {
//                matrixShiftCounter++;
//                for (int j = matrixShiftCounter; j < size; j++)
//                {
//                    if (CompareTwoTaggedDocs(taggedDocList[i], taggedDocList[j]) > 1)
//                        taggedDocMatrix[i][j] = 1;
//                }
//            }
//            //Globals.TaggedDocsMatrix = taggedDocMatrix;
//            TextWriter tw = new StreamWriter(taggedDocumentsSimilarityMatrixFileName);

//            for (int i = 0; i < size; i++)
//            {
//                for (int j = 0; j < size; j++)
//                {
//                    tw.Write(taggedDocMatrix[i][j] + " ");
//                }
//                tw.Write("\n");
//            }

//            tw.Close();
//            tw.Dispose();

//            return taggedDocMatrix;
//        }

//        /// <summary>
//        /// Comparison function to compare 2 tagged docs based on their tag vectors.
//        /// The rule is to find the most common tags between the tag vectors, this
//        /// won't give any result if the taggedDocs have unique ngrams.
//        /// Algorithm: Compare number of common numbers and locations.
//        /// i.e.  TaggedDoc1 = 1 0 0 1 0 0 
//        ///       TaggedDoc2 = 1 0 0 0 0 1
//        ///       TaggedDoc3 = 0 0 0 0 1 0
//        ///      TD1 & TD2 = 1/6 similar
//        ///      TD1 & TD3 = 0 similar
//        /// </summary>
//        /// <param name="tagd1"></param>
//        /// <param name="tagd2"></param>
//        /// <returns></returns>
//        public static double CompareTwoTaggedDocs(TaggedDoc tagd1, TaggedDoc tagd2)
//        {
//            // 1. Get all the vector's indices where the value is 1 into a signatureVector
//            // 2. Compare all the resultant signatureVector
//            return Compare2WordsByLetterNGramValue(
//                    tagd1.TagVector.GetPositiveStringVector(), 
//                    tagd2.TagVector.GetPositiveStringVector());
//        }

//        /// <summary>
//        /// Compare2WordsByLetterNGramValue
//        /// </summary>
//        /// <param name="word1">string 1</param>
//        /// <param name="word2">string 2</param>
//        /// <returns>double result</returns>
//        public static double Compare2WordsByLetterNGramValue(string word1, string word2)
//        {
//            return NGram.CompareNGramsValue(word1, NGram.GetWordNGrams(word1), word2, NGram.GetWordNGrams(word2), 0.5);
//        }
//    }
//}
