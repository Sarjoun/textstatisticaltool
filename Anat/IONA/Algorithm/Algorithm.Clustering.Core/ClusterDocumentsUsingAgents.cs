﻿using System.Collections.Generic;
using System.Linq;
using SDParser.ClusteringTypes;
using SDParser.Structures;

namespace IONA.Algorithm.Algorithm.Clustering.Core
{
    public static class ClusterDocumentsUsingAgents
    {
        private static Dictionary<int, DocWord> _docWordsDictionary;
        private static int _minThreshold;

        public static List<Agent> UseClusteredAgentsToTagDatabaseDocsAndUpdateAgentsWithTaggedDocResults(
            List<Agent> agents,
            Dictionary<int, DocWord> docWordsDictionary,
            int minThreshold)
        {
            _docWordsDictionary = docWordsDictionary;
            _minThreshold = minThreshold;

            foreach (var agent in agents)
            {
                agent.TaggedDocumentsId =
                    GetMatchingDocIds(agent.AlphabeticalOrderIdTagVectorToList);
            }
            return agents;
        }
        
        private static List<int> GetMatchingDocIds(List<int> agentSignature)
        {
            return (from docWord in _docWordsDictionary.Values
                    where agentSignature.Intersect(docWord.GetListOfVocabWordIds())
                              .ToList()
                              .Count >= _minThreshold
                    select docWord.DocId).ToList();
        }
    }
}
