﻿using System.Collections.Generic;
using System.Linq;
using CommonClusteringItems;
using SDParser.ClusteringTypes;

namespace IONA.Algorithm.Algorithm.Clustering.Core
{
    public static class BasicClusteringNGramSetItemsManager
    {
        public static List<BasicNGramSetItem> TransformStemWordNGramsIntoNGramSetItemsForBasicAlgorithm(
            List<Agent> stemWordNGrams)
        {
            return stemWordNGrams.Select(
                    stemWordNGram => new BasicNGramSetItem(
                        stemWordNGram.Id,
                        stemWordNGram.GramToStringUsingRoot(),
                        stemWordNGram.StemWordAlphabeticalOrderIdTagVector,
                        new List<int>()))
                .ToList();
        }
    }
}
