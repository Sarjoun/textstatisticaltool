﻿using System;
using DbScan;
using System.Linq;
using SDParser.Primitives;
using System.Collections.Generic;
using CommonClusteringItems;
using IONA.Algorithm.Algorithm.Helpers;
using IONA.Algorithm.Algorithm.NGrams.Structs;
using SDParser.ClusteringTypes;
using SDParser.Notifications;

namespace IONA.Algorithm.Algorithm.Clustering.Core
{
    public static class AgentsManager
    {
        public static List<Agent> ConvertSuperNGramsToDefaultAgents(List<SuperNGram> listSuperNGrams)
        {
            var counter = 1;
            return listSuperNGrams.Select(superNGram =>
                    ConvertSuperNGramToAgent(superNGram, counter++)).ToList();
        }

        public static Agent MergeAgentsHavingSmallerOrEqualTaggedDocsIds(
            this Agent currentAgent,
            List<Agent> smallerStemWordNGrams)
        {
            return smallerStemWordNGrams.Aggregate(
                currentAgent,
                (current, smallerStemWordNGram) => current.
                MergeAgentHavingSmallerOrEqualTaggedDocsIds(smallerStemWordNGram));
        }

        public static Agent MergeAgentHavingSmallerOrEqualTaggedDocsIds(
            this Agent currentAgent,
            Agent smallerAgent)
        {
            var gramlist = currentAgent.Grams.Union(smallerAgent.Grams).ToList();
            var taggedDocList = currentAgent.TaggedDocumentsId.Union(smallerAgent.TaggedDocumentsId).ToList();
            return new Agent(
                currentAgent.Id,
                -1,
                NumberOfWordsInStartNGRAM.FromValue(gramlist.Count),
                gramlist,
                (currentAgent.Popularity + smallerAgent.Popularity) / 2,
                taggedDocList
            );
        }

        private static Agent ConvertSuperNGramToAgent(SuperNGram superNGram, int id)
        {
            return new Agent(
                id,
                NumberOfWordsInStartNGRAM.FromValue(superNGram.N),
                superNGram.Grams.Select(StemWordHelper.GetStemWordFromString).ToList(),
                superNGram.Popularity);
        }

        public static List<Agent> UpdateAgentsClusterIdsFromCorrespondingBasicClusteringSetItem(
            List<Agent> agents,
            List<NGramSetItem[]> clusters)
        {
            var clusterCounter = 1;
            foreach (var cluster in clusters)
            {
                foreach (var nGramSetItem in cluster)
                {
                    agents.Single(x => x.Id == nGramSetItem.Id).ClusterId = clusterCounter;
                }
                clusterCounter++;
            }

            return agents.OrderBy(w => w.ClusterId).ToList();
        }

        public static List<Agent> UpdateAgentsClusterIdsFromCorrespondingBasicClusteringSetItem(
            List<Agent> agents,
            List<BasicNGramSetItem[]> clusters)
        {
            var clusterCounter = 1;
            foreach (var cluster in clusters)
            {
                foreach (var nGramSetItem in cluster)
                {
                    agents.Single(x => x.Id == nGramSetItem.Id).ClusterId = clusterCounter;
                }
                clusterCounter++;
            }
            return agents;//agents.OrderBy(w => w.ClusterId).ToList();
        }

        public static List<Agent> UpdateAgentsClusterIdsFromCorrespondingDbScanNgramSetItem(
            List<Agent> agents,
            List<NGramSetItem[]> clusters)
        {
            var clusterCounter = 1;
            foreach (var cluster in clusters)
            {
                foreach (var nGramSetItem in cluster)
                {
                    agents.Single(x => x.Id == nGramSetItem.Id).ClusterId = clusterCounter;
                }
                clusterCounter++;
            }

            return agents.OrderBy(w => w.ClusterId).ToList();
        }

        public static Agent MergeAgentsBelongingToSameCluster(List<Agent> agents)
        {
            if (agents.Count < 2)
            {
                ShowMessage.ShowWarningMessageAndExit("Error in StemWordNGramsManager.cs" +
                                                      " MergeAgentsBelongingToSameCluster." +
                                                      " The agents list has less than 2 elements");
            }

            var mergedStemWordNGram = TakeTwoAgentsAndMergeTheirClusterIdsAndGrams(
                agents[0], agents[1]);

            return agents.Skip(2).
                Aggregate(
                    mergedStemWordNGram,
                    TakeTwoAgentsAndMergeTheirClusterIdsAndGrams);
        }
        public static Agent TakeTwoAgentsAndMergeTheirClusterIdsAndGrams(Agent agent1, Agent agent2)
        {
            var ulist = agent1.Grams.Union(agent2.Grams).ToList();

            if (agent1.ClusterId != null)
                return new Agent(
                    agent1.Id < agent2.Id ? agent1.Id : agent2.Id,
                    agent1.ClusterId.Value,
                    NumberOfWordsInStartNGRAM.FromValue(ulist.Count),
                    ulist,
                    (agent1.Popularity + agent1.Popularity) / 2);

            throw new Exception("Agent\\StemWordNGram " + agent1.GramToStringUsingRoot() + " does not belong to any cluster.");
        }

        public static List<Agent> MergeListOfAgentsHavingSameCountAndIdenticalTaggedDocsIds(List<Agent> tempAgentsWithEqualLengthDocIdTag)
        {
            var tempList = new List<Agent>();
            var newList = tempAgentsWithEqualLengthDocIdTag.ToList();
            var tempNewList = tempAgentsWithEqualLengthDocIdTag.ToList();
            var counter = 0;
            while (counter < newList.Count && tempNewList.Count != 0) //TODO check if the code makes sense here... replace || with &&
            {
                try
                {
                    var temp = new HashSet<float>(newList[counter].TaggedDocumentsIdFloat);

                    var listToMerge = MergeClusteredAgentsHavingIdenticalTaggedDocsIds(
                        tempNewList.Where(x => temp.SetEquals(x.TaggedDocumentsIdFloat)).ToList());

                    if (listToMerge == null)
                    {
                        counter = newList.Count;
                        tempNewList = new List<Agent>();
                        continue;
                    }

                    tempList.Add(listToMerge);
                    counter += tempNewList.RemoveAll(x => temp.SetEquals(x.TaggedDocumentsIdFloat));

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
            return tempList;
        }
        public static Agent MergeClusteredAgentsHavingIdenticalTaggedDocsIds(List<Agent> agents)
        {
            if (agents.Count <= 1)
            {
                return agents.FirstOrDefault();
            }

            var mergedStemWordNGram = MergeTwoAgentsHavingIdenticalTaggedDocsIds(
                agents[0], agents[1]);

            return agents.Skip(2).
            Aggregate(
                mergedStemWordNGram,
                MergeTwoAgentsHavingIdenticalTaggedDocsIds);
        }
        private static Agent MergeTwoAgentsHavingIdenticalTaggedDocsIds(Agent agent1, Agent agent2)
        {
            var ulist = agent1.Grams.Union(agent2.Grams).ToList();
            return new Agent(
                agent1.Id < agent2.Id ? agent1.Id : agent2.Id,
                -1,
                NumberOfWordsInStartNGRAM.FromValue(ulist.Count),
                ulist,
                (agent1.Popularity + agent1.Popularity) / 2,
                agent1.TaggedDocumentsId
            );
        }
        public static List<Agent> MergeListOfAgentsHavingSameCountAndSimilarTaggedDocsIds(List<Agent> agentsWithEqualLengthDocIdTag, double thresholdPercentage)
        {
            var resultList = new List<Agent>();
            var threshold = Math.Ceiling((1 - thresholdPercentage) * agentsWithEqualLengthDocIdTag.First().TaggedDocumentsId.Count);

            var lastItemAndNeverMerged = true;
            for (var i = 0; i < agentsWithEqualLengthDocIdTag.Count; i++)
            {
                var neverMerged = true;
                for (var j = i + 1; j < agentsWithEqualLengthDocIdTag.Count; j++)
                {
                    if (agentsWithEqualLengthDocIdTag[i].TaggedDocumentsId.Except(
                            agentsWithEqualLengthDocIdTag[j].TaggedDocumentsId).
                            ToList().Count <= threshold)
                    {
                        resultList.Add(MergeTwoAgentsHavingSimilarlTaggedDocsIds(
                            agentsWithEqualLengthDocIdTag[i],
                            agentsWithEqualLengthDocIdTag[j]));
                        neverMerged = false;

                        if (j == agentsWithEqualLengthDocIdTag.Count - 1)
                            lastItemAndNeverMerged = false;
                    }
                }

                if (neverMerged && lastItemAndNeverMerged)
                    resultList.Add(agentsWithEqualLengthDocIdTag[i]);
            }
            return resultList;
        }
        private static Agent MergeTwoAgentsHavingSimilarlTaggedDocsIds(Agent agent1, Agent agent2)
        {
            var ulist = agent1.Grams.Union(agent2.Grams).ToList();
            return new Agent(
                agent1.Id < agent2.Id ? agent1.Id : agent2.Id,
                -1,
                NumberOfWordsInStartNGRAM.FromValue(ulist.Count),
                ulist,
                (agent1.Popularity + agent1.Popularity) / 2,
                agent1.TaggedDocumentsId.Union(agent2.TaggedDocumentsId).ToList()
            );
        }

        public static List<List<float>> MergeListOfListsOfFloatsHavingSameCountOfListValues(List<List<float>> listOfLists)
        {
            var tempList = new List<List<float>>();
            var newList = listOfLists.ToList();
            var tempNewList = listOfLists.ToList();
            int counter = 0;
            while (counter < newList.Count)
            {
                var temp = new HashSet<float>(newList[counter]);
                var listToMerge = MergeListOfListsOfFloatsHavingIdenticalValues(
                    tempNewList.Where(x => temp.SetEquals(x)).ToList());
                tempList.Add(listToMerge);
                counter += tempNewList.RemoveAll(x => temp.SetEquals(x));
            }
            return tempList;
        }
        private static List<float> MergeListOfListsOfFloatsHavingIdenticalValues(List<List<float>> listOfLists)
        {
            if (listOfLists.Count <= 1)
            {
                return listOfLists.FirstOrDefault();
            }

            var mergedListFloat = MergeTwoListsOfFloatHavingIdenticalTaggedDocsIds(
                listOfLists[0], listOfLists[1]);

            return listOfLists.Skip(2).
                Aggregate(
                    mergedListFloat,
                    MergeTwoListsOfFloatHavingIdenticalTaggedDocsIds);
        }
        private static List<float> MergeTwoListsOfFloatHavingIdenticalTaggedDocsIds(List<float> list1, List<float> list2)
        {
            return list1.Union(list2).ToList();
        }
    }
}
