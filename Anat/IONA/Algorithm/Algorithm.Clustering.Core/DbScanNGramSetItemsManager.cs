﻿using System.Collections.Generic;
using System.Linq;
using DbScan;
using SDParser.ClusteringTypes;

namespace IONA.Algorithm.Algorithm.Clustering.Core
{
    public static class DbScanNGramSetItemsManager
    {
        public static List<NGramSetItem> TransformStemWordNGramsIntoNGramSetItemsForDbScan(
            List<Agent> stemWordNGrams)
        {
            return stemWordNGrams.Select(
               stemWordNGram => new NGramSetItem(
                  stemWordNGram.Id,
                  stemWordNGram.GramToStringUsingRoot(),
                  stemWordNGram.StemWordAlphabeticalOrderIdTagVector))
                  .ToList();
        }
    }
}
