namespace IONA.Algorithm.Algorithm.Clustering.Structs
{
    /// <summary>
    /// Each document when tagged by NGrams is represented by a TaggedDoc class.
    /// This class has the address of the file, the numeric ID  and the TagVector.
    /// The TagVector is the set of NGrams that are present in this file.
    /// A corpus of files is a group of TaggedDocs.
    /// I can add later on a criteria or threshold to decide whether an NGram can 
    /// belong to a file's tagVector.
    /// </summary>
    public class TaggedDoc
    {
        public string Address { get; set; }
        public int OriginalId { get; set; }
        public int Id { get; set; }

        public TagVector TagVector { get; set; }
        public string ResultantStringVector { get; private set; }

        public TaggedDoc(string address, int originalId, int id, TagVector tagVector)
        {
            Address = address;
            OriginalId = originalId;
            Id = id;
            TagVector = tagVector;
            ResultantStringVector = tagVector.GetPositiveStringVector();
        }
    }
}
