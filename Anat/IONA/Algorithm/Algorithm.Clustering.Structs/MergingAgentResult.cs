﻿using SDParser.ClusteringTypes;

namespace IONA.Algorithm.Algorithm.Clustering.Structs
{
    public class MergingAgentResult
    {
        public Agent AssimilatedAgent { get; set; }
        public int AssimilatedAgentId { get; set; }
        public Agent AssimilatorAgent { get; set; }
        public int AssimilatorAgentId { get; set; }
        public AgentsToBeMerged AgentsToBeMerged { get; set; }

        public MergingAgentResult(Agent assimilatedAgent,
            int assimilatedAgentId, Agent assimilatorAgent,
            int assimilatorAgentId, AgentsToBeMerged agentsToBeMerged)
        {
            AssimilatedAgent = assimilatedAgent;
            AssimilatedAgentId = assimilatedAgentId;
            AssimilatorAgent = assimilatorAgent;
            AssimilatorAgentId = assimilatorAgentId;
            AgentsToBeMerged = agentsToBeMerged;
        }

    }
}
