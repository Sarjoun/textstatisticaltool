﻿using System.Collections.Generic;

namespace IONA.Algorithm.Algorithm.Clustering.Structs
{
    public class TaggedAndUntaggedFileLinks
    {
        public List<string> TaggedDocs { get; set; }
        public List<string> UnTaggedDocs { get; set; }

        public TaggedAndUntaggedFileLinks(List<string> taggedDocs, List<string> unTaggedDocs)
        {
            TaggedDocs = taggedDocs;
            UnTaggedDocs = unTaggedDocs;
        }

    }
}
