using System.Linq;
using System.Collections.Generic;
using IONA.Algorithm.Algorithm.NGrams.Structs;

namespace IONA.Algorithm.Algorithm.Clustering.Structs
{
    public class TagVector
    {
        public int Size { get; set; }
        public List<int> Signature { get; set; }
        public List<NGram> Content { get; set; }
        public TagVector(){ }

        public TagVector(int size, List<NGram> content, List<int> signature)
        {
            Size = size;
            Content = content;
            Signature = signature;
        }

        public TagVector(int size)
        {
            Size = size;
            Content = new List<NGram>();
            for (var i = 0; i < size; i++)
                Content.Add(new NGram());
            Signature = new List<int>();
            for (var i = 0; i < size; i++)
                Signature.Add(0);
        }

        public string GetPositiveStringVector()
        {
            var result = "";
            for (var i = 0; i < Signature.Count; i++)
            {
                if (Signature[i] != 0)
                    result += i + " ";
            }
            return result;
        }

        public int GetCountOfFullSlots()
        {
            return Content.Count(n => n.N != 0);
        }

        //Can compare different sized NGrams
        public double Similarity(TagVector otherVector)
        {
            var actualTrue = (
                from ngram in Content
                from ngram2 in otherVector.Content
                where ngram.N != 0
                where ngram.Equivalent(ngram2)
                select ngram).Count();
            return (double)actualTrue / otherVector.GetCountOfFullSlots();
        }

        public string ToPrintContent()
        {
            var result = Content.Aggregate("", (current, ng) => current + (ng + "-"));

            result = result.Substring(0, result.Length - 1);
            return result;
        }

        public string ToPrintContentCSV_Gephi()
        {
            string result = "";
            foreach (NGram ng in Content)
                result += ng.ToStringCSV_Gephi();
            result = result.Substring(0, result.Length - 1);
            return result;
        }

        /// <summary>
        /// Get the filled part of the Content 
        /// Added for the data export for students and 2nd party API
        /// </summary>
        /// <returns></returns>
        public List<NGram> FullContent()
        {
            List<NGram> returnedList = new List<NGram>();
            foreach (NGram ngram in Content)
            {
                if (!ngram.Empty())
                    returnedList.Add(ngram);
            }
            return returnedList;
        }
        
        /// <summary>
        /// for createMatrixOfClustersToDocumentsForNewsSpread
        /// (search for this for more info.)
        /// 
        /// 1 for existing NGram
        /// 0 for non-existing NGram
        /// </summary>
        /// <returns></returns>
        public string GetPatternSignature()
        {
            var returnedResult = "";
            foreach (var ngram in Content)
            {
                if (ngram.Empty())
                    returnedResult += "0 ";
                else
                    returnedResult += "1 ";
            }
            return returnedResult.TrimEnd(' ');
        }
    }
}
