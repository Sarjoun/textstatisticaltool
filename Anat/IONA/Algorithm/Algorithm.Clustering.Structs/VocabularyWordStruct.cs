﻿namespace IONA.Algorithm.Algorithm.Clustering.Structs
{
    public class VocabularyWordStruct
    {
        public string Word { get; set; }
        public int VocabularyId { get; set; }
        public int FrequencyId { get; set; }

        public VocabularyWordStruct(string word, int vocabularyId, int frequencyId)
        {
            Word = word;
            VocabularyId = vocabularyId;
            FrequencyId = frequencyId;
        }
    }
}
