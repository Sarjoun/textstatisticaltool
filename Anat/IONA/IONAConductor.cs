using System.Collections.Generic;
using IONA.Algorithm.Algorithm.NGrams.Structs;
using SDParser.ClusteringTypes;

namespace IONA
{
    public static class IONAConductor
    {
        public static List<Agent> ClusteredAgents { get; set; }

        public static NGramIdList CreateNewNGramIdList(List<SuperNGram> superNGrams, NGramIdList popularNGramIdList)
        {
            var tempNGramIdList = new NGramIdList(new NGramList())
            {
                OriginalList = popularNGramIdList.OriginalList
            };

            foreach (var ngramIDs in popularNGramIdList.IdList)
            {
                foreach (var superNGram in superNGrams)
                {
                    if (ngramIDs.NGram.Equals(superNGram))
                        tempNGramIdList.IdList.Add(
                           new NGramIdStruct(ngramIDs.NGramId, superNGram));
                }
            }
            return tempNGramIdList;
        }
    }
}
