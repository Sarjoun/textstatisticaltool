﻿namespace IONA.Enums
{
    public enum AgentDisplayOrderType
    {
        TaggingVectorSize,
        StemWordGramSize,
        PopularityValue,
        ClusterId,
        None
    }
}

