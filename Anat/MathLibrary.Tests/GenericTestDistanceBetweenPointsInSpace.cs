﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathLibrary.Tests
{
    [TestClass]
    public class GenericTestDistanceBetweenPointsInSpace
    {
        private float[] _pointOne = new float[] { };
        private float[] _pointTwo = new float[] { };

        [TestMethod]
        public void TestDistanceBetween2PointsIn1D()
        {
            _pointOne = new float[] { 1 };
            _pointTwo = new float[] { 3 };
            Assert.AreEqual(2, DistanceBetween2Points.Calculate(_pointOne, _pointTwo));
        }
        [TestMethod]
        public void TestDistanceBetween2PointsIn2D()
        {
            _pointOne = new float[] { 1, 1 };
            _pointTwo = new float[] { 1, 1 };
            Assert.AreEqual(0, DistanceBetween2Points.Calculate(_pointOne, _pointTwo));

            _pointOne = new float[] { 1, 2 };
            _pointTwo = new float[] { 3, 4 };
            Assert.AreEqual(2.828427f, DistanceBetween2Points.Calculate(_pointOne, _pointTwo));
        }
        [TestMethod]
        public void TestDistanceBetween2PointsIn4DPositiveWholeNumbers()
        {
            _pointOne = new float[] { 1, 2, 3, 4 };
            _pointTwo = new float[] { 5, 6, 7, 8 };
            Assert.AreEqual(8, DistanceBetween2Points.Calculate(_pointOne, _pointTwo));
        }
        [TestMethod]
        public void TestDistanceBetween2PointsIn4DNegativeAndPositiveDecimalNumers()
        {
            _pointOne = new[] { 1.5f, 2, -5 };
            _pointTwo = new[] { -3.45f, -13, 145 };
            Assert.AreEqual(150.829376f, DistanceBetween2Points.Calculate(_pointOne, _pointTwo));

            _pointOne = new[] { 13.37f, 2, 6, -7 };
            _pointTwo = new[] { 1.2f, 3.4f, -5.6f, 7.89f };
            var result = DistanceBetween2Points.Calculate(_pointOne, _pointTwo);
            Assert.AreEqual(22.5020218f, result);
        }
    }
}
