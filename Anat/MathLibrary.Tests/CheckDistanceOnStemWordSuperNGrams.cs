﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathLibrary.Tests
{
    [TestClass]
    public class CheckDistanceOnStemWordSuperNGrams
    {
        private float[] _pointOne = new float[] { };
        private float[] _pointTwo = new float[] { };

        [TestMethod]
        public void DistanceBetweenStemWordNGramsFromFile()
        {
            _pointOne = new[] { 931f, 1565f, 1514f};
            _pointTwo = new[] { 931f, 1565f, 672f };
            var result = DistanceBetween2Points.Calculate(_pointOne, _pointTwo);
            Assert.AreEqual(842f, result);

            _pointOne = new[] { 931f, 1565f, 1514f };
            _pointTwo = new[] { 1565f, 672f, 1514f };
            result = DistanceBetween2Points.Calculate(_pointOne, _pointTwo);
            Assert.AreEqual(1095.17346f, result);

            //haiti - polic - peopl   931 1565    1514
            //haiti - polic - elect   931 1565    672
            //polic - elect - peopl   1565    672 1514

            _pointOne = new float[] { 1881, 185, 880 };
            _pointTwo = new float[] { 1881, 131, 880 };
            result = DistanceBetween2Points.Calculate(_pointOne, _pointTwo);
            Assert.AreEqual(54f, result);

            //shark - been - german   1881    185 880
            //shark-attack-german	1881	131	880
        }
    }
}
