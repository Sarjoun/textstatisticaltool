﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathLibrary.Tests
{
    [TestClass]
    public class ClusteringKMeansUnitTest
    {
        private double[][] _rawData;

        [Ignore]
        [TestMethod]
        public void TestThatClusteringWorks()
        {
            const int numClusters = 3;
            LoadDataSet(2);
            var clustering = ClusteringKMeans.Cluster(_rawData, numClusters);

            Assert.AreEqual(20, clustering.Length);
        }

        private void LoadDataSet(int id)
        {
            if (id == 1)
            {
                // real data likely to come from a text file or SQL
                _rawData = new double[20][];
                _rawData[0] = new[] { 65.0, 220.0 };
                _rawData[1] = new[] { 73.0, 160.0 };
                _rawData[2] = new[] { 59.0, 110.0 };
                _rawData[3] = new[] { 61.0, 120.0 };
                _rawData[4] = new[] { 75.0, 150.0 };
                _rawData[5] = new[] { 67.0, 240.0 };
                _rawData[6] = new[] { 68.0, 230.0 };
                _rawData[7] = new[] { 70.0, 220.0 };
                _rawData[8] = new[] { 62.0, 130.0 };
                _rawData[9] = new[] { 66.0, 210.0 };
                _rawData[10] = new[] { 77.0, 190.0 };
                _rawData[11] = new[] { 75.0, 180.0 };
                _rawData[12] = new[] { 74.0, 170.0 };
                _rawData[13] = new[] { 70.0, 210.0 };
                _rawData[14] = new[] { 61.0, 110.0 };
                _rawData[15] = new[] { 58.0, 100.0 };
                _rawData[16] = new[] { 66.0, 230.0 };
                _rawData[17] = new[] { 59.0, 120.0 };
                _rawData[18] = new[] { 68.0, 210.0 };
                _rawData[19] = new[] { 61.0, 130.0 };
            }
            if (id == 2)
            {
                _rawData = new double[20][];
                _rawData[0] = new[] { 65.0, 220.0, 2 };
                _rawData[1] = new[] { 73.0, 160.0, 11 };
                _rawData[2] = new[] { 59.0, 110.0, 1 };
                _rawData[3] = new[] { 61.0, 120.0, 10 };
                _rawData[4] = new[] { 75.0, 150.0, 1 };
                _rawData[5] = new[] { 67.0, 240.0, 23 };
                _rawData[6] = new[] { 68.0, 230.0, 2 };
                _rawData[7] = new[] { 70.0, 220.0, 24 };
                _rawData[8] = new[] { 62.0, 130.0, 1 };
                _rawData[9] = new[] { 66.0, 210.0, 21 };
                _rawData[10] = new[] { 77.0, 190.0, 11 };
                _rawData[11] = new[] { 75.0, 180.0, 1 };
                _rawData[12] = new[] { 74.0, 170.0, 1 };
                _rawData[13] = new[] { 70.0, 210.0, 21 };
                _rawData[14] = new[] { 61.0, 110.0, 1 };
                _rawData[15] = new[] { 58.0, 100.0, 11 };
                _rawData[16] = new[] { 66.0, 230.0, 21 };
                _rawData[17] = new[] { 59.0, 120.0, 1 };
                _rawData[18] = new[] { 68.0, 210.0, 2 };
                _rawData[19] = new[] { 61.0, 130.0, 1 };
            }
        }
    }
}
