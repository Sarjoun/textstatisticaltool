﻿namespace SDParser.Enums
{
    public enum StatisticsTypeEnum
    {
        ConditionalProbability,
        CorrelationCoefficient,
        JointFrequency,
        JointProbability,
        LogOddsRatio,
        PointWiseMutualInformation
    }
}
