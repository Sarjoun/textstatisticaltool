﻿namespace SDParser.Enums
{
    public enum NetworkPropertyEnum
    {
        Word,
        Frequency,
        MarginalProbability,
        Degree,
        EigenvectorCentrality,
        BetweennessCentrality,
        ClosenessCentrality,
        ClusteringCoefficient
    }
}
