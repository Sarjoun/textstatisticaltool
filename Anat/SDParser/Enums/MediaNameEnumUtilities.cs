﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SDParser.Enums
{
    public static class MediaNameEnumUtilities
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static IEnumerable<string> GetNames<T>()
        {
            return Enum.GetNames(typeof(T));
        }

        public static string GetRssMediaName(MediaNameEnum feedNameEnum)
        {
            switch (feedNameEnum)
            {
                case MediaNameEnum.Reuters_TopNews: return "feeds.reuters.com_reuters_topNews";
                case MediaNameEnum.Reuters_WorldNews: return "feeds.reuters.com_reuters_worldNews";
                case MediaNameEnum.NewYorkTimes_HomePage: return "www.nytimes.com_services_xml_rss_nyt_HomePage";
                case MediaNameEnum.NewYorkTimes_International: return "www.nytimes.com_services_xml_rss_nyt_International";
                case MediaNameEnum.Google_Output: return "news.google.com_output";
                case MediaNameEnum.CNN_Local: return "rss.cnn.com_rss_edition";
                case MediaNameEnum.CNN_World: return "rss.cnn.com_rss_edition_world";
                case MediaNameEnum.Reuters_Events: return "feeds.reuters.com_reuters_blogs_events";

                case MediaNameEnum.Salon_News: return "feeds.salon.com_salon_news";
                case MediaNameEnum.FoxNews_1: return "www.foxnews.com_xmlfeed_rss_04313000";
                case MediaNameEnum.FoxNews_2: return "www.foxnews.com_xmlfeed_rss_043138000";
                case MediaNameEnum.FoxNews_3: return "www.foxnews.com_xmlfeed_rss_043138100";
                case MediaNameEnum.USAToday_TopStories: return "rssfeeds.usatoday.com_UsatodaycomWorld-TopStories";
                case MediaNameEnum.CBSNews_Main: return "www.cbsnews.com_feeds_rss_main";
                case MediaNameEnum.CBSNews_World: return "www.cbsnews.com_feeds_rss_world";
                case MediaNameEnum.WallStreetJournal_RSS: return "online.wsj.com_xml_rss_3_7085";
                case MediaNameEnum.BBC_International: return "feeds.bbci.co.uk_news_rssedition=int";
                case MediaNameEnum.ChristianScienceMonitor_TopNews: return "rss.csmonitor.com_feeds_top";
                case MediaNameEnum.WashingtonPost_World: return "feeds.washingtonpost.com_wp-dyn_rss_world_index_xml";
                case MediaNameEnum.YahooNews_World: return "rss.news.yahoo.com_rss_world";
                case MediaNameEnum.Naharnet_Lebanon: return "www.naharnet.com_domino_tn_newsdesk.nsf_rssnewsopenagent&category=Lebanon";
                case MediaNameEnum.Naharnet_MiddleEast: return "www.naharnet.com_domino_tn_newsdesk.nsf_rssnewsopenagent&category=MiddleEast";
                case MediaNameEnum.Naharnet_TheWorld: return "www.naharnet.com_domino_tn_newsdesk.nsf_rssnewsopenagent&category=TheWorld";
                case MediaNameEnum.Haaretz_Headlines: return "www.haaretz.com_cmlink_haaretz-com-headlines-rss-1.263335localLinksEnabled=false";
                case MediaNameEnum.Haaretz_International: return "www.haaretz.com_cmlink_international-rss-1.208898localLinksEnabled=false";
                case MediaNameEnum.JerusalemPost_FrontPage: return "www.jpost.com_Rss_RssFeedsFrontPage.aspx";
                case MediaNameEnum.JerusalemPost_MiddleEast: return "www.jpost.com_Rss_RssFeedsMiddleEastNews.aspx";
                case MediaNameEnum.YediothAhronoth_HomePage: return "www.ynet.co.il_Integration_StoryRss3254";
                case MediaNameEnum.PakistanNews_RSS: return "www.pakistannews.net_rss.php";
                case MediaNameEnum.Rianovosti_WorldIndex: return "rss.feedsportal.com_c_860_fe.ed_en.rian.ru_export_rss2_world_index";
                case MediaNameEnum.NewsAustralia_WorldBreakingNews: return "feeds.news.com.au_public_rss_2.0_news_bknews_world_614";
                case MediaNameEnum.NewsAustralia_WorldNews: return "feeds.news.com.au_public_rss_2.0_news_world_615";
                case MediaNameEnum.NewsAustralia_WorldMostPopular: return "feeds.news.com.au_public_rss_2.0_news_mostpopular_world_407";
                case MediaNameEnum.IndiaTimes_TopStories: return "timesofindia.indiatimes.com_rssfeedstopstories.cms";
                case MediaNameEnum.IndiaTimes_Feeds: return "timesofindia.indiatimes.com_rssfeeds_296589292.cms";
                case MediaNameEnum.NewsFromSyria_News: return "newsfromsyria.com_feed";
                case MediaNameEnum.Petra_Jordan: return "www.petra.gov.jo_library_RSS_RSS_General_EnID=634269312564843750";
                case MediaNameEnum.AlMasryAlYoum_Egypt: return "www.almasryalyoum.com_en_rss_feed_term_193_rss";
                case MediaNameEnum.CypruseDirectory_RSS: return "www.cyprusedirectory.com_rss.aspx";
                case MediaNameEnum.Xinhuanet_EnglishWorldNews_China: return "rss.xinhuanet.com_rss_english_english_worldnews";
                case MediaNameEnum.Hurriyet_TurkishNews: return "www.hurriyetdailynews.com_rss.php";
                case MediaNameEnum.Al_Jazeera_English: return "english.aljazeera.net_Services_Rss_PostingId=2007731105943979989";
                case MediaNameEnum.IribNews_English_Iran: return "english.iribnews.ir_rss_en.aspx";
                case MediaNameEnum.Tayyar_English_Lebanon: return "www.tayyar.org_Tayyar_News_newsrss.aspxlang=null&international=null";
                case MediaNameEnum.Zaman_Turkey: return "www.todayszaman.com_rsssectionId=100";
                case MediaNameEnum.SaudiArabiaOfficial_English: return "www.spa.gov.sa_English_rss9";
                case MediaNameEnum.KhaleejTimes_TopStories_Dubai: return "www.khaleejtimes.com_services_rss_topstories_rss";
                case MediaNameEnum.KhaleejTimes_International_Dubai: return "www.khaleejtimes.com_services_rss_international_rss";
                case MediaNameEnum.France24_World: return "www.france24.com_en_monde_rss";
                case MediaNameEnum.BuenosAiresHerald_Argentina: return "www.buenosairesherald.com_articles_rss.aspx";
                case MediaNameEnum.Al_Arabiya_MiddleEast_UAE: return "www.alarabiya.net_rss_en_meast";
                case MediaNameEnum.Al_Arabiya_International_UAE: return "www.alarabiya.net_rss_en_intr";
                case MediaNameEnum.ShanghaiDaily_China: return "www.shanghaidaily.com_rss_world";
                case MediaNameEnum.Spiegel_Germany: return "www.spiegel.de_international_index";
                default: return "Unspecified";
            }
        }

    }
}
