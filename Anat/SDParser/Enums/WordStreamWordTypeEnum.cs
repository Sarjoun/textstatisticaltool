﻿namespace SDParser.Enums
{
    public enum WordStreamWordTypeEnum
    {
        OriginalWord,
        StemmedWord
    }
}