﻿namespace SDParser.Enums
{
    public enum TextReaderAlgorithmEnum
    {
        DoubleHorizonAlgorithm,
        HorizonAlgorithm,
        LoweSingleWindowAlgorithm,
        LoweDoubleWindowAlgorithm
    }
}