﻿using System.Collections.Generic;
using System.Linq;
using Iveonik.Stemmers;
using SDParser.Enums;
using SDParser.Extensions;
using SDParser.Structures;

namespace SDParser.TextProcessingCentral
{
    public static class SentenceProcessor
    {
        public static Dictionary<string, StemWord> UniqueWordsAndTheirVariants { get; set; }
        public static List<string> NewWords;
        public static List<string> StopWords;
        public static IStemmer Stemmer;
        public static WordStreamWordTypeEnum WordStreamWordTypeEnum;

        public static string ExtractStemVersionFromWord(string word)
        {
            return !word.Trim().IsNumeric()
                   && word.Trim().IsMinLength()
                   && !StopWords.Contains(word.Trim()
                       .StripPossessiveApostropheAndS()
                       .RemoveExPrefixFollowedByHyphen()
                       .StripExHyphen()
                       .StripPunctuation()
                       .ToSingular()
                       .ToLower())
                ? Stemmer.Stem(word
                    .ToLower()
                    .Trim()
                    .StripPossessiveApostropheAndS()
                    .StripPunctuation())
                : null;
        }

        public static IEnumerable<string> ProcessSentence(IEnumerable<string> words)
        {
            return (
                from word
                in words
                where !word.Trim().IsNumeric()
                      && word.Trim().IsMinLength()
                      && !StopWords.Contains(word.Trim()
                          .StripPossessiveApostropheAndS()
                          .RemoveExPrefixFollowedByHyphen()
                          .StripExHyphen()
                          .StripPunctuation()
                          .ToSingular()
                          .ToLower())
                select ProcessWordUsingStemming(word
                    .Trim()
                    .StripPossessiveApostropheAndS()
                    .StripPunctuation())).ToList();
        }

        public static IEnumerable<string> SpecialProcessSentence(IEnumerable<string> words)
        {
            return (
                from word
                in words
                where !word.Trim().IsNumeric()
                      && word.Trim().IsMinLength()
                      && !StopWords.Contains(word.Trim()
                          .StripPossessiveApostropheAndS()
                          .RemoveExPrefixFollowedByHyphen()
                          .StripExHyphen()
                          .StripPunctuation()
                          .ToSingular()
                          .ToLower())
                select SpecialProcessWordUsingStemming(word
                    .Trim()
                    .StripPossessiveApostropheAndS()
                    .StripPunctuation())).ToList();
        }

        public static IEnumerable<string> StripStopWords(this IEnumerable<string> words)
        {
            return words.Where(
                word => !word.Trim().IsNumeric() &&
                        word.Trim().IsMinLength() &&
                        !StopWords.Contains(word.Trim())).ToArray();
        }//public because of the news file title

        //To order the entries in the dictionary alphabetically
        public static Dictionary<string, StemWord> UpdatedUniqueWordsAndTheirVariants()
        {
            //order the entries in the dictionary alphabetically
            var orderedVariantWords = (from entry in UniqueWordsAndTheirVariants
                    orderby entry.Key
                    ascending
                    select entry).
                ToDictionary(pair => pair.Key, pair => pair.Value);
            UniqueWordsAndTheirVariants = orderedVariantWords;
            UniqueWordsAndTheirVariants.Remove(string.Empty);
            return UniqueWordsAndTheirVariants;
        }

        private static string ProcessWordUsingStemming(string originalWord)
        {
            return AddToNewWordsAndVariantWords(originalWord, Stemmer.Stem(originalWord.ToLower()));
        }

        private static string AddToNewWordsAndVariantWords(string originalWord, string normalizedWord)
        {
            //Check to see if it is new
            if (NewWords.Contains(normalizedWord))
            {
                AddToVariantWords(originalWord, normalizedWord);
            }
            else
            {
                NewWords.Add(normalizedWord);
                AddToVariantWords(originalWord, normalizedWord);
            }
            //if (WordStreamWordTypeEnum == WordStreamWordTypeEnum.OriginalWord)
            //    return originalWord;
            //return normalizedWord;
            return originalWord;
        }

        private static string SpecialProcessWordUsingStemming(string originalWord)
        {
            return SpecialReturnNormalizedWord(originalWord, Stemmer.Stem(originalWord.ToLower()));
        }

        private static string SpecialReturnNormalizedWord(string originalWord, string normalizedWord)
        {
            if (WordStreamWordTypeEnum == WordStreamWordTypeEnum.OriginalWord)
                return originalWord;
            return normalizedWord;
        }


        private static void AddToVariantWords(string originalWord, string normalizedWord)
        {
            if (!UniqueWordsAndTheirVariants.ContainsKey(normalizedWord))
            {
                UniqueWordsAndTheirVariants.Add(normalizedWord,
                    new StemWord(normalizedWord,
                        new List<VariantWord> {
                            new VariantWord(originalWord, 1) },
                        1,
                        null));
            }
            else
            {
                UniqueWordsAndTheirVariants[normalizedWord].AddVariantWord(originalWord);
            }
        }

    }
}
