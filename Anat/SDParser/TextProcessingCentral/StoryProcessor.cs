﻿using System;
using System.Collections.Generic;
using System.IO;
using SDParser.Extensions;
using SDParser.Primitives;
using SDParser.Structures;

namespace SDParser.TextProcessingCentral
{
    public static class StoryProcessor
    {
        public static bool IsTypeOfFileIsAnatFormat(string storyFileName)
        {
            //Check if it is an ANAT type file with the format: <title><[date]><(c?)> or not (free-form)
            return storyFileName.Contains("[") && storyFileName.Contains("]") && storyFileName.Contains("(") && storyFileName.Contains(")");
        }

        public static string GetStoryFileTitleFromGenericFormatFile(string storyFileName)
        {
            var start = storyFileName.LastIndexOf(@"\", StringComparison.Ordinal);
            var finish = storyFileName.LastIndexOf(@".", StringComparison.Ordinal);
            return storyFileName.Substring(start + 1, finish - start - 1);
        }

        public static string GetStoryFileTitleFromANATFormatFile(string storyFileName)
        {
            var start = storyFileName.LastIndexOf(@"\", StringComparison.Ordinal);
            var finish = storyFileName.LastIndexOf(@"[", StringComparison.Ordinal);
            return storyFileName.Substring(start + 1, finish - start - 1);
        }

        public static string GetFileTitle(string storyFileName)
        {
            return IsTypeOfFileIsAnatFormat(storyFileName)
                ? GetStoryFileTitleFromANATFormatFile(storyFileName)
                : GetStoryFileTitleFromGenericFormatFile(storyFileName);
        }

        public static Dictionary<string, StemWord> SingleStoryUniqueWordsAndTheirVariants { get; set; }

        public static List<string> MakeWordStreamFromSingleStory(string storyFileName, IncludeTitleInSetup includeTitleInSetup)
        {
            StreamReader file = null;
            SingleStoryUniqueWordsAndTheirVariants = null;
            var newWords = new List<string>();
            try
            {
                if (File.Exists(storyFileName))
                {
                    #region document's title (fileName)
                    if (includeTitleInSetup.Value)
                    {
                        var fileTitle = GetFileTitle(storyFileName);
                        newWords.AddRange(
                            SentenceProcessor
                            .ProcessSentence(fileTitle
                            .Trim()
                            .DestroyArtificatOfNewsTitleTrimmingWithTwoPoints()
                            .Trim()
                            .ReplaceBracketsAndPunctuationsWithSpace()
                            .Trim()
                            .Split(' ', '-')
                            .StripStopWords()));
                    }
                    #endregion

                    file = new StreamReader(storyFileName);
                    string line;
                    while ((line = file.ReadLine()) != null)
                    {
                        newWords.AddRange(
                            SentenceProcessor
                            .ProcessSentence(line
                            .Trim()
                            .ReplaceBracketsAndPunctuationsWithSpace()
                            .Trim()
                            .Split(' ', '-')
                            .StripStopWords()));
                    }
                }
            }
            finally
            {
                file?.Close();
            }
            SingleStoryUniqueWordsAndTheirVariants = SentenceProcessor.UniqueWordsAndTheirVariants;
            return newWords;
        }

        public static List<string> SpecialMakeWordStreamFromSingleStory(string storyFileName, IncludeTitleInSetup includeTitleInSetup)
        {
            StreamReader file = null;
            SingleStoryUniqueWordsAndTheirVariants = null;
            var newWords = new List<string>();
            try
            {
                if (File.Exists(storyFileName))
                {
                    #region document's title (fileName)
                    if (includeTitleInSetup.Value)
                    {
                        var fileTitle = GetFileTitle(storyFileName);
                        newWords.AddRange(
                            SentenceProcessor
                                .SpecialProcessSentence(fileTitle
                                    .Trim()
                                    .DestroyArtificatOfNewsTitleTrimmingWithTwoPoints()
                                    .Trim()
                                    .ReplaceBracketsAndPunctuationsWithSpace()
                                    .Trim()
                                    .Split(' ', '-')
                                    .StripStopWords()));
                    }
                    #endregion

                    file = new StreamReader(storyFileName);
                    string line;
                    while ((line = file.ReadLine()) != null)
                    {
                        newWords.AddRange(
                            SentenceProcessor
                                .SpecialProcessSentence(line
                                    .Trim()
                                    .ReplaceBracketsAndPunctuationsWithSpace()
                                    .Trim()
                                    .Split(' ', '-')
                                    .StripStopWords()));
                    }
                }
            }
            finally
            {
                file?.Close();
            }
            //SingleStoryUniqueWordsAndTheirVariants = SentenceProcessor.UniqueWordsAndTheirVariants;
            return newWords;
        }
    }
}
