﻿using System.Collections.Generic;
using System.Linq;

namespace SDParser.Structures
{
    public class DocWord
    {
        public int DocId { get; set; }
        public List<WordFrequency> WordFrequencies { get; set; }//orderd naturally

        //Get a vector of values representing the wordId in the Vocabulary sorted alphabetically
        public int[] GetArrayOfVocabWordIds()
        {
            return WordFrequencies.
                    OrderBy(w => w.WordId).
                    Select(wordFrequency => wordFrequency.WordId).ToArray();
        }

        public List<int> GetListOfVocabWordIds()
        {
            return WordFrequencies.
                OrderBy(w => w.WordId).
                Select(wordFrequency => wordFrequency.WordId).ToList();
        }
        //(Maybe later get a vector of values representing the frequencyId sorted)

        public DocWord(int docId, List<WordFrequency> wordFrequencies)
        {
            DocId = docId;
            WordFrequencies = wordFrequencies;
        }

        public DocWord(int docId, WordFrequency wordFrequency)
        {
            DocId = docId;
            WordFrequencies = new List<WordFrequency> {wordFrequency};
        }

        public DocWord(int docId)
        {
            DocId = docId;
            WordFrequencies = new List<WordFrequency>();
        }
    }
}
