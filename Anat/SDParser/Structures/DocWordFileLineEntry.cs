﻿namespace SDParser.Structures
{
    public class DocWordFileLineEntry
    {
        public int DocId { get; set; }
        public int WordId { get; set; }
        public int WordFrequency { get; set; }
        public WordFrequency GetWordFrequency => new WordFrequency(WordId, WordFrequency);
        
        public DocWordFileLineEntry(int docId, int wordId, int wordFrequency)
        {
            DocId = docId;
            WordId = wordId;
            WordFrequency = wordFrequency;
        }
    }
}