namespace SDParser.Structures
{
    public struct WordFrequency
    {
        public string Word { get; set; }
        public int WordId { get; set; }
        public int Frequency { get; set; }

        public WordFrequency(string word, int frequency)
            : this()
        {
            Word = word;
            Frequency = frequency;
        }

        public WordFrequency(int wordId, int frequency)
            : this()
        {
            WordId = wordId;
            Frequency = frequency;
        }
    }
}