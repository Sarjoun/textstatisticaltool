using System;
using System.Collections.Generic;
using System.Linq;
using SDParser.Notifications;

namespace SDParser.Structures
{
    public class StemWord
    {
        public string Root { get; }
        public List<VariantWord> Variants { get; private set; }
        public int Frequency { get; private set; }
        public int? DictionaryId { get; set; }
        //public int? FrequencyId { get; set; }

        /********************/
        public int Id { get; set; }
        public int RankId { get; set; }
        public int StatsMatrixId { get; set; }
        public decimal MarginalProbabilityUsingRfCount { get; set; } //Rf: reading frame
        public decimal MarginalProbability { get; set; }
        public Dictionary<string, int> WordVariations { get; set; }
        /********************/


        public StemWord(
            string root,
            IEnumerable<VariantWord> variants,
            int frequency,
            int? dictionaryId)
        {
            Root = root;
            Variants = variants.ToList();
            Frequency = frequency;
            DictionaryId = dictionaryId;
        }

        public void AddVariantWord(string variantWord)
        {
            if (Variants.All(v => v.Variant != variantWord))
                Variants.Add(new VariantWord(variantWord, 1));
            else
            {
                Variants = Variants
                    .Where(l => l.Variant != variantWord)
                    .Union(Variants
                          .Where(l => l.Variant == variantWord)
                          .Select(
                            l => new VariantWord
                            {
                                Variant = variantWord,
                                Frequency = l.Frequency + 1
                            })).ToList();
            }
            Frequency++;
        }
        
        public void MergeStemWords(StemWord stemWord)
        {
            if(Root != stemWord.Root)
                ShowMessage.ShowErrorMessageAndExit("Trying to merge stemwords that are not the same in StemWord line 61." +
                                                    "The local word is " + Root + " and the incoming is " + stemWord.Root, 
                                                    new NotSupportedException());
            foreach (var stemWordVariant in stemWord.Variants)
            {
                AddVariantWord(stemWordVariant.Variant);
            }
        }

        //temp to make stats project compile
        public StemWord(string temp, int itemp)
        {

        }

        //public string OriginalName { get; set; } //This was added later because I created 
        //// a stemWord dictionary that replaced the rootStem with the most popular word Variant 
        //// and this is to keep a record.

        //To incorporate the OriginalName changes I will make a function for that.

        //public string OriginalName => Variants.Max(v => v.Frequency).ToString();
        //public string TopVariantWord => Variants.First(x => x.Frequency == Variants.Max(i => i.Frequency)).ToString();
        public string TopVariantWord => Variants.Aggregate((i1,i2) => i1.Frequency > i2.Frequency ? i1 : i2).Variant;
        

        public void IncreaseFrequency()
        {
            Frequency++;
        }

        public string GetRootWordAndItsFrequency()
        {
            return $"{Frequency,5} {Root,10}";
        }

       public string PrintRootItsFrequencyAndItsMarginalProbability()
       {
          return $"{Frequency,5} {Root,10} {MarginalProbability,5}";
       }

      private void OrderVariantsBasedOnFrequencyValue()
        {
            Variants = Variants.OrderByDescending(item => item.Frequency).ToList();
        }

        public string GetRootAndVariantsAndAllFrequenciesStringRepresentation()
        {
            OrderVariantsBasedOnFrequencyValue();
            return GetRootWordAndItsFrequency() + $"\t :" +
                   Variants.Aggregate("", (current, variant) => $"{current,1} {variant.PrintOutFrequencyNameTabDelimited(),1}");
        }

       public string PrintRootAndVariantsAndAllMarginalProbabilities()
       {
          OrderVariantsBasedOnFrequencyValue();
          return PrintRootItsFrequencyAndItsMarginalProbability() + $"\t :" +
                 Variants.Aggregate("", (current, variant) => $"{current,1} {variant.PrintOutFrequencyNameTabDelimited(),1}");
       }

      public string PrintRootAndItsFrequencyForDataAnalysisCommaDelimited()
        {
            return $"{Frequency},{Root}";
        }

        public string PrintRootAndVariantsAndAllFrequenciesForDataAnalysisCommaDelimited()
        {
            OrderVariantsBasedOnFrequencyValue();
            return PrintRootAndItsFrequencyForDataAnalysisCommaDelimited() +
                   Variants
                       .Aggregate("",
                           (current, variant) => $"{current},{variant.GetFrequencyAndVariantNameCommaDelimited()}");
        }
        public bool DoesWordExistInVariants(string word)
        {
            return Variants != null && Variants.Any(variant => variant.Variant == word);
        }

    }
}