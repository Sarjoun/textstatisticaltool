﻿namespace SDParser.Structures
{
    public struct VariantWord
    {
        public string Variant { get; set; }
        public int Frequency { get; set; }

        public VariantWord(string variant, int frequency)
        {
            Variant = variant;
            Frequency = frequency;
        }

        public void IncreaseFrequency()
        {
            Frequency = Frequency + 1;
        }
        public string PrintOutFrequencyNameTabDelimited()
        {
            //§ ♀☻♥♦♣♠•◘○◙♂♀♪♫☼►◄↕‼¶§▬↔↑↓→←∟↔▲▼ !""#$%8H╓ ╫ ╪ ┘ ┌ █  µ τΦΘΩδ⌡÷≤±♀╒ ╘ ╙ ╥ ╟ ╞ ┼(alt-214)
            //return $"{Frequency,4} {Variant,13} -";
            return $"{Frequency}\t{Variant}";
        }
        public string GetFrequencyNameColonDelimited()
        {
            return $"{Frequency}:{Variant}";
        }

        public string GetFrequencyAndVariantNameCommaDelimited()
        {
            return $"{Frequency},{Variant}";
        }
    }
}
