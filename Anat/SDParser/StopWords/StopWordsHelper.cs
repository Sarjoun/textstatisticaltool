﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SDParser.Primitives;

namespace SDParser.StopWords
{
    public static class StopWordsHelper
    {
        public static List<string> GetStopWordsFromEmbeddedResource()
        {
            return Properties.Resources.StopWords2.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            //note: dangerous to use Environment.NewLine, it doesn't work
        }

        public static List<string> GetStopWordsFromLocalFile(StopWordsFilePath stopWordsFile)
        {
            var stopWords = new List<string>();
            var file = new StreamReader(stopWordsFile.Value);
            string line;
            while ((line = file.ReadLine()) != null)
            {
                line = line.Trim();
                var words = line.Split(' ');
                stopWords.AddRange(words);
            }
            file.Close();
            return stopWords;
        }
    }
}
