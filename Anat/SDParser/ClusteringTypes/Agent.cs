﻿using System;
using System.Collections.Generic;
using System.Linq;
using SDParser.Primitives;
using SDParser.Structures;

namespace SDParser.ClusteringTypes
{
    public class Agent
    {
        public const char GramSeparatorBOWSymbol = '+';
        public int Id { get; set; }
        public int? ClusterId { get; set; }
        public int NumberOfWordsInStartNGRAM { get; set; }
        public List<StemWord> Grams { get; set; }
        public float Popularity { get; set; }
        public float[] StemWordFrequencyOrderIdTagVector { get; set; }
        public float[] StemWordAlphabeticalOrderIdTagVector { get; set; }//float needed for the clustering
        public string FrequencyOrderIdTagVectorToStringTabDelimited => StemWordFrequencyOrderIdTagVector.Aggregate("", (current, entry) => current + (entry + "\t")).TrimEnd();

        public string AlphabeticalOrderIdTagVectorToStringTabDelimited => 
            StemWordAlphabeticalOrderIdTagVector.Aggregate("", (current, entry) => current + (entry + "\t")).TrimEnd();

        public string AlphabeticalOrderIdTagVectorToStringCommaDelimited =>
            StemWordAlphabeticalOrderIdTagVector.Aggregate("", (current, entry) => current + (entry + ",")).TrimEnd(',');

        public List<int> AlphabeticalOrderIdTagVectorToList 
            => Array.ConvertAll(StemWordAlphabeticalOrderIdTagVector, f => (int)f).ToList();

        public List<int> TaggedDocumentsId { get; set; }

        public string TaggedDocumentsIdForDisplay => TaggedDocumentsId.Aggregate("", (current, entry) =>
            current + (entry + ",")).TrimEnd(',');


        public float[] TaggedDocumentsIdFloat => TaggedDocumentsId.Select(x => (float)x).ToArray();

        public string Name => GramToStringUsingRoot();
        public string NameForCorrelationAnalysis => Name.Replace("+", "_");

        public Agent(
            int id,
            NumberOfWordsInStartNGRAM numberOfStemWords,
            List<StemWord> grams,
            float popularity)
        {
            Id = id;
            ClusterId = 0;
            NumberOfWordsInStartNGRAM = numberOfStemWords;
            Grams = grams;
            Popularity = popularity;

            var alphabeticallyOrdered = grams.OrderBy(g => g.Root);
            StemWordAlphabeticalOrderIdTagVector = alphabeticallyOrdered
                .Select(stemWord => (float)stemWord.DictionaryId.Value)
                .ToArray();

            //StemWordAlphabeticalOrderIdTagVector = grams.Select(stemWord =>
            //{
            //    Debug.Assert(stemWord.DictionaryId != null, "stemWord.DictionaryId != null");
            //    return (float) stemWord.DictionaryId.Value;
            //}).ToArray();

            var frequencyOrdered = grams.OrderBy(g => g.Frequency);

            StemWordFrequencyOrderIdTagVector = frequencyOrdered.Select(stemWord => (float)stemWord.Frequency).ToArray();
        }

        public Agent(
            int id,
            int clusterId,
            NumberOfWordsInStartNGRAM numberOfStemWords,
            List<StemWord> grams,
            float popularity)
        {
            Id = id;
            ClusterId = clusterId;
            NumberOfWordsInStartNGRAM = numberOfStemWords;
            Grams = grams;
            Popularity = popularity;

            var alphabeticallyOrdered = grams.OrderBy(g => g.Root);
            StemWordAlphabeticalOrderIdTagVector = alphabeticallyOrdered.Select(stemWord => (float)stemWord.DictionaryId.Value).ToArray();
            //StemWordAlphabeticalOrderIdTagVector = grams.Select(stemWord =>
            //{
            //    Debug.Assert(stemWord.DictionaryId != null, "stemWord.DictionaryId != null");
            //    return (float)stemWord.DictionaryId.Value;
            //}).ToArray();

            var frequencyOrdered = grams.OrderBy(g => g.Frequency);
            StemWordFrequencyOrderIdTagVector = frequencyOrdered.Select(stemWord => (float)stemWord.Frequency).ToArray();
        }

        public Agent(
            int id,
            int clusterId,
            NumberOfWordsInStartNGRAM numberOfStemWords,
            List<StemWord> grams,
            float popularity,
            List<int> taggedDocumentsId)
        {
            Id = id;
            ClusterId = clusterId;
            NumberOfWordsInStartNGRAM = numberOfStemWords;
            Grams = grams;
            Popularity = popularity;
            TaggedDocumentsId = taggedDocumentsId;

            var alphabeticallyOrdered = grams.OrderBy(g => g.Root);
            StemWordAlphabeticalOrderIdTagVector = alphabeticallyOrdered.Select(stemWord => (float)stemWord.DictionaryId.Value).ToArray();
            var frequencyOrdered = grams.OrderBy(g => g.Frequency);
            StemWordFrequencyOrderIdTagVector = frequencyOrdered.Select(stemWord => (float)stemWord.Frequency).ToArray();
        }
        public string GramToStringUsingRoot()
        {
            var name = "";
            for (var i = 0; i < Grams.Count; i++)
            {
                if (i == Grams.Count - 1)
                    name += Grams[i].Root;
                else
                {
                    name += Grams[i].Root + GramSeparatorBOWSymbol;//"-";
                }
            }
            return name;
        }

        public string GramToStringUsingMostPopular()
        {
            var name = "";
            for (var i = 0; i < Grams.Count; i++)
            {
                if (i == Grams.Count - 1)
                    name += Grams[i].TopVariantWord;
                else
                {
                    name += Grams[i].TopVariantWord + GramSeparatorBOWSymbol;//"-";
                }
            }
            return name;
        }

        public string GramToStringUsingMostPopularUsingNewFormat()
        {
            var name = "";
            for (var i = 0; i < Grams.Count; i++)
            {
                if (i == Grams.Count - 1)
                    name += Grams[i].TopVariantWord;
                else
                {
                    name += Grams[i].TopVariantWord + " ";
                }
            }
            return name;
        }

        //for getting the value of the reverse edge in non-symmetric matrices
        public string GramToStringUsingMostPopularUsingNewFormatInReverseOrder()
        {
            var name = "";
            var gramsReverse = Grams.AsEnumerable().Reverse().ToList();
            for (var i = 0; i < gramsReverse.Count; i++)
            {
                if (i == gramsReverse.Count - 1)
                    name += gramsReverse[i].TopVariantWord;
                else
                {
                    name += gramsReverse[i].TopVariantWord + " ";
                }
            }
            return name;
        }

        //The advantage of using the new Dictionary type where I use the stemmedVersion of the word (root) as key
        //is that it simplifies the required functionalities which makes working with them optimal
        //(because the changes allowed us to assume the word is the root).
        private bool CheckIfWordExistsInAgent(string word)
        {
            return Grams.Any(stemWord => stemWord.Root == word);
        }

        //private StemWord GetStemWordFromGrams(string word)
        //{
        //    return Grams.Find(stemWord => stemWord.Root == word);
        //}

        //private void CheckIfStemWordInDiscontinuedGramsAndRemoveIfFound(string word)
        //{
        //    if (!CheckIfWordExistsInAgent(word)) return;

        //    Grams.Remove(GetStemWordFromGrams(word));
        //    //should I reduce popularity by the word's frequency or by number 1?
        //    //It doesn't matter because ALAN is providing the statistics so I don't have to worry about it.
        //    Popularity--;//just for fun.
        //}

        public void SyncAgentWithListOfCurrentStemmedWordsToRemoveDiscontinuedOnes(List<string> stemmedWords)
        {
            //Make sure that all the root words in the Gram exist in stemmedWords
            //var _grams = (List < StemWord >)Grams;

            var _grams = new List<StemWord>();

            foreach (var stemWord in Grams)
            {
                if (stemmedWords.Contains(stemWord.Root))
                {
                    _grams.Add(stemWord);
                }

                //Grams.Remove(stemWord);
                //Popularity--;
            }

            Popularity = Popularity - (Grams.Count - _grams.Count);
            Grams = _grams;
            //foreach (var stemmedWord in stemmedWords)
            //{
            //    CheckIfStemWordInGramsAndRemoveIfFound(stemmedWord);
            //}
            //return this;
        }

    }
}
