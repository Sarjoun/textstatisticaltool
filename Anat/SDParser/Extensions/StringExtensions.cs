﻿using System;
using System.Collections.Generic;
using System.IO;
using SDParser.Structures;

namespace SDParser.Extensions
{
    public static class StringExtensions
    {
        public static string CleanUpFilePathBackSlashes(this string fileName)
        {
            return fileName.Replace(@"\\", @"\");
        }

        public static string CreateEmptyFile(this string fileName)
        {
            File.Create(fileName).Dispose();
            return fileName;
        }

        public static string ExtractLDATokenValue(this string line)
        {
            return line.Trim().Split('=')[1];
        }

        public static string ExtractLDAToken(this string line)
        {
            return line.Trim().Split('=')[0];
        }

        public static string ExtractUntilKeyWord(this string fileName, string keyWord)
        {
            return fileName.Substring(0,
                fileName.IndexOf(keyWord, StringComparison.Ordinal));
        }

        public static KeyValuePair<string, StemWord> GetDictionaryKeyValuePairFromWordFrequencyFileLine(
            this string line)
        {
            return line.SplitWordFrequencyFileLineIntoBasicParts().GetStemWordDictionaryKeyValuePairFromLine();
        }

        public static string[] SplitWordFrequencyFileLineIntoBasicParts(this string line)
        {
            return line.Split(new[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static DocWordFileLineEntry DocWordFileLineEntryFromLine(this string line)
        {
            return line.SplitDocWordFileLineIntoBasicParts().GetDocWordFileLineEntryFromLine();
        }
        public static string[] SplitDocWordFileLineIntoBasicParts(this string line)
        {
            return line.Split(new[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string RemoveInitialIndexTag(this string line)
        {
            return line.Substring(
                line.IndexOf(")", StringComparison.Ordinal) + 1,
                line.Length - line.IndexOf(")", StringComparison.Ordinal) - 1);
        }
    }
}
