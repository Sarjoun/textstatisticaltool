﻿using System;
using System.Collections.Generic;
using System.Linq;
using SDParser.Structures;

namespace SDParser.Extensions
{
    public static class ArrayExtensions
    {
        //The format is <TotalFrequency> <StemWord> : (<Frequency> <VariantWord>) x n
        // e.g.   170     attack	 :     4 attacker     2 attacking    72 attack     8 attacked    12 attackers    72 attacks
        public static KeyValuePair<string, StemWord> GetStemWordDictionaryKeyValuePairFromLine(
            this string[] arrayOfWords)
        {
            var test1 = arrayOfWords[1];
            var test2 = new StemWord(
                arrayOfWords[1],
                arrayOfWords.Skip(3).ToArray().GetVariantWordStructs(),
                Convert.ToInt32(arrayOfWords[0]),
                null);

            return new KeyValuePair<string, StemWord>(
                test1,
                test2);
        }

        public static VariantWord[] GetVariantWordStructs(this string[] onlyVariantWordsThatHaveValues)
        {
            var result = onlyVariantWordsThatHaveValues.Zip(
                onlyVariantWordsThatHaveValues.Skip(1).Concat(new[] { "0" }), (x, y) => new
                {
                    First = y,
                    Second = x,
                }).Where((item, index) => index % 2 == 0);

            return result.Select(
                item => new VariantWord(
                    item.First,
                    Convert.ToInt32(item.Second))).ToArray();

            //var result2 = new List<VariantWord>();
            //foreach (var item in result)
            //{
            //    result2.Add(new VariantWord(item.First, Convert.ToInt32(item.Second)));
            //}
            //return result2.ToArray();
        }

        public static DocWordFileLineEntry GetDocWordFileLineEntryFromLine(
            this string[] arrayOfWords)
        {
            return new DocWordFileLineEntry
            (
                Convert.ToInt32(arrayOfWords[0]),
                Convert.ToInt32(arrayOfWords[1]),
                Convert.ToInt32(arrayOfWords[2]));
        }
    }
}
