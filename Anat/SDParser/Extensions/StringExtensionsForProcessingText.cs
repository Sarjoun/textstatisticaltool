﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SDParser.Extensions
{
    public static class StringExtensionsForProcessingText
    {
        //https://stackoverflow.com/questions/421616/how-can-i-strip-punctuation-from-a-string
        //public static string StripPunctuation(this string s)
        //{
        //    var sb = new StringBuilder();
        //    foreach (char c in s)
        //    {
        //        if (!char.IsPunctuation(c))
        //            sb.Append(c);
        //        else
        //            return sb.ToString();
        //    }
        //    return sb.ToString();
        //}

        public static string StripPossessiveApostropheAndS(this string word)
        {
            return word.Contains("'")
                ? word.ReturnFullWordOnlyIfItIsMeantAsA3AinLetter()
                : word;
        }

        public static string ReturnFullWordOnlyIfItIsMeantAsA3AinLetter(this string word)
        {
            //checks for shi'ite or chi'ite or 'abbas p=or 'ain

            //My rule is to check that there is more than 1 letter following the apostrophe, especially not an s
            return word.Substring(word.IndexOf("'", StringComparison.Ordinal)).Length > 2
                ? word
                : word.Substring(0, word.IndexOf("'", StringComparison.Ordinal));
        }


        public static string StripExHyphen(this string word)
        {
            return word.Contains("ex-") ? word.Substring(0, word.IndexOf("'", StringComparison.Ordinal)) : word;
        }

        public static string RemoveExPrefixFollowedByHyphen(this string word)
        {
            //For example, ex-president becomes president
            return word.StartsWith("ex-")
                ? word.Replace("ex-", "")
                : word;
        }

        private static readonly List<string> SymbolsToBeReplacedWithSpace = new List<string> { "...", "..", "++", "\"" };

        public static bool StringContainsMyDesignatedSymbols(this string word)
        {
            return SymbolsToBeReplacedWithSpace.Contains(word);
        }

        public static string ReplaceMyDesignatedSymbolsWithSpace(this string word)
        { //later this will be removed by the following function in the functional call
            return word
                .Replace("...", " ")
                .Replace("..", " ")
                .Replace("++", " ")
                .Replace("“", " ")
                .Replace("”", " ")
                .Replace("’", " ")
                .Replace(",", " ")
                .Replace("\"", " ");
        }

        public static string ReplaceSpecialSymbolsWithSpace(this string word)
        {
            return word.StringContainsMyDesignatedSymbols()
                ? word.ReplaceMyDesignatedSymbolsWithSpace()
                : word;
        }


        public static string ReplacePunctuationWithSpace(this string word)
        {
            return word.StringContainsMyDesignatedSymbols()
                ? word.ReplaceMyDesignatedSymbolsWithSpace()
                : word;
        }

        public static string ReplaceBracketsAndPunctuationsWithSpace(this string word)
        {
            var replaceBrackets = Regex.Replace(word,
                @"\((?'content'[^)]+)", match => $", {match.Groups["content"].Value}");

            return Regex.Replace(replaceBrackets, @"[^\w]+", " ");
        }

        public static string ReplaceThreePointsWithSpace(this string word)
        {
            //Real example, "Iran’s...maligned" should become "Iran maligned"
            return word.Contains("...")
                ? word.Replace("...", " ")
                : word;
        }

        //public static string[] ReplaceThreePointsWithSpace(this string word)
        //{
        //    return word.Contains("...") 
        //        ? word.Replace("...", " ").Split(' ') 
        //        : new[] {word};
        //}

        public static bool CheckIfContainsWordThatHasTwoPointsAtItsEnds(this string word)
        {
            return word.Split(' ').Any(x => x.EndsWith(".."));
        }

        public static string GetLastWordThatHasTwoPointsAtItsEnds(this string word)
        {
            return word.Split(' ').LastOrDefault(w => w.EndsWith(".."));
        }
        
        public static string DestroyArtificatOfNewsTitleTrimmingWithTwoPoints(this string word)
        {
            return word.CheckIfContainsWordThatHasTwoPointsAtItsEnds() 
                ? word.Replace(word.GetLastWordThatHasTwoPointsAtItsEnds(), "")
                : word;
        }


        public static string[] SplitOnDashes(this string word)
        {
            return word.Split('-');
        }

        public static string StripPunctuation(this string s)
        {
            return new string(s.Where(c => !char.IsPunctuation(c)).ToArray());
        }

        //public static string StripPunctuation(this string s)
        //{
        //    return new string(s.Where(c => !char.IsPunctuation(c)).ToArray());
        //}

        public static string StripPunctuationSpecial(this string word)
        {
            var sb = new StringBuilder();
            var once = true;
            foreach (var c in word)
            {
                if (!char.IsPunctuation(c))
                    sb.Append(c);
                else
                {
                    var r = word.IndexOfAny(new char[] { '.' }) + 3;

                    if (c == '.' && r <= word.Count())
                    {
                        if (once)
                        {
                            sb.Append(c);
                            once = false;
                        }
                        else
                            return sb.ToString();
                    }
                    else
                        return sb.ToString();
                }
            }
            return sb.ToString();
        }

        public static bool IsLower(this string word)
        {
            return word.ToLower() == word;
        }

        public static bool IsNumeric(this string word)
        {
            return Regex.IsMatch(word, @"([\.,])?\d([\.,][\d])?");
            //return Regex.IsMatch(word, @"([\.,-])?\d([\.,-][\d])?");
        }

        public static bool IsSingular(this string word)
        {
            return Singularizer.Singularize(word) == word;
        }

        public static string ToSingular(this string word)
        {
            return Singularizer.Singularize(word);
        }

        public static bool IsMinLength(this string word)
        {
            return word.Length >= 3;
        }


        public static bool CheckIfDocumentHasANATFormatWithDocIdWithParenthesisAtStart(this string word)
        {
            //Basically this should work for any file format it is presented.
            //It only cares for the case if this was an IONA/ILDA generated file that has the format <id><)><Title>
            if (!word.Contains(")"))
                return false;

            var checkIfInt = word.Substring(0, word.IndexOf(')'));
            int conversionValue;
            return int.TryParse(checkIfInt, out conversionValue);
        }
        
        public static string RemoveDocIdFromStartOfPathIfItExists(this string word)
        {
            return word.CheckIfDocumentHasANATFormatWithDocIdWithParenthesisAtStart() 
                ? word.Substring(word.IndexOf(')') + 1) 
                : word;
            //return word.Substring(word.IndexOf(')') + 1);
        }

        //public static string CleanUpFilePathBackSlashes(this string fileName)
        //{
        //    return fileName.Replace(@"\\",@"\");
        //}
    }
}
