﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace SDParser.Extensions
{
    public class Singularizer
    {
        //http://www.momswhothink.com/reading/list-of-nouns.html
        //uncountable nouns
        private static readonly IList<string> Unpluralizables =
            new List<string>
            {
                "equipment",
                "information",
                "rice",
                "money",
                "species",
                "series",
                "fish",
                "sheep",
                "deer",
                "homs",
                "police", "advice","air","alcohol","art,beef","blood","butter","cheese","chewing","gum","chocolate","coffee","confusion","cotton","education","electricity","entertainment","experience","fiction","flour","food","forgiveness","fresh","air","furniture","gold","grass","ground","happiness","history","homework","honey","hope","ice","information","jam","juice","knowledge","lamb","lightning","literature","love","luck","luggage","meat","milk","mist","money","music","news","noise","oil","oxygen","paper","patience","pay","peace","peanut","butter","pepper","petrol","plastic","pork","power","pressure","rain","rice","sadness","salt","sand","shopping","silver","snow","space","speed","steam","sugar","sunshine","tea","tennis","time","toothpaste","traffic","trousers","vinegar","washing","up","washing","up","liquid","water","weather","wine","wood","wool","work",
                "yves"
            };

        private static readonly IDictionary<string, string> Singularizations =
            new Dictionary<string, string>
            {
                // Start with the rarest cases, and move to the most common
                {"people", "person"},
                {"oxen", "ox"},
                {"children", "child"},
                {"feet", "foot"},
                {"teeth", "tooth"},
                {"geese", "goose"},
                // And now the more standard rules.
                {"(.*)ives$", "$1ife"},
                {"(.*)ves$", "$1f"},
                // ie, wolf, wife
                {"(.*)men$", "$1man"},
                {"(.+[aeiou])ys$", "$1y"},
                {"(.*[bcdfghjklmnpqrtvwxyz])s$", @"$1"},
                {"(.+[^aeiou])ies$", "$1y"},
                {"(.+)zes$", "$1"},
                {"([m|l])ice$", "$1ouse"},
                {"matrices", @"matrix"},
                {"indices", @"index"},
                {"(.+[^aeiou])ices$","$1ice"},
                {"(.*)ices", @"$1ex"},
                // ie, Matrix, Index
                {"(octop|vir)i$", "$1us"},
                {"(.+(s|x|sh|ch))es$", @"$1"},
                //{"(.+)s$", @"$1"} //fails on Paris - Pari
                {"ladens$", "$laden"} //special for the missing ' in laden's and laden
            };

        public static string Singularize(string word)
        {
            if (Unpluralizables.Contains(word.ToLowerInvariant()))
            {
                return word;
            }

            foreach (var singularization in Singularizations)
            {
                if (Regex.IsMatch(word, singularization.Key))
                {
                    return Regex.Replace(word, singularization.Key, singularization.Value);
                }
            }

            return word;
        }

        public static bool IsPlural(string word)
        {
            if (Unpluralizables.Contains(word.ToLowerInvariant()))
            {
                return true;
            }

            foreach (var singularization in Singularizations)
            {
                if (Regex.IsMatch(word, singularization.Key))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
