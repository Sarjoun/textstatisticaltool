﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SDParser.FileFunctions
{
    public static class FileReader
    {
        public static async Task<IEnumerable<string>> ReadFileAsync(string fileName)
        {
            using (var reader = File.OpenText(fileName))
            {
                var fileText = await reader.ReadToEndAsync();
                return fileText.
                    Trim().
                    Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            }
        }
    }
}
