﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SDParser.FileFunctions
{
    public static class FileWriter
    {
        public static async Task WriteListToFileAsync(
            List<string> generalContent,
            string fileName)
        {
            var streamWriter = new StreamWriter(fileName);
            try
            {
                foreach (var content in generalContent)
                {
                    await streamWriter.WriteLineAsync(content);
                }
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static void WriteListToFile(
            List<string> generalContent,
            string fileName)
        {
            var streamWriter = new StreamWriter(fileName);
            try
            {
                foreach (var content in generalContent)
                {
                    streamWriter.WriteLine(content);
                }
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static async Task WriteStringToFileAsync(
            string generalContent,
            string fileName)
        {
            var streamWriter = new StreamWriter(fileName);
            try
            {
                await streamWriter.WriteLineAsync(generalContent);
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static void WriteStringToFile(
            string generalContent,
            string fileName)
        {
            var streamWriter = new StreamWriter(fileName);
            try
            {
                streamWriter.WriteLine(generalContent);
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static void WriteSparseIntArrayToFile(int[][] sparseIntMatrix, string fname)
        {
            if (sparseIntMatrix == null) throw new ArgumentNullException(nameof(sparseIntMatrix));

            var totalWords = sparseIntMatrix.GetLength(0) - 1;
            TextWriter tw = new StreamWriter(fname);
            try
            {
                for (var i = 0; i < totalWords; i++)
                {
                    for (var j = 0; j < totalWords; j++)
                    {
                        tw.Write(Convert.ToString(sparseIntMatrix[i][j]) + " ");
                    }
                    tw.WriteLine();
                }
                tw.Close();
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }
        }

    }
}
