﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SDParser.FileFunctions
{
    public static class FileProcessingFunctions
    {
        public static async Task<IEnumerable<string>> GetTopicsStringArrayFromFileAsync(string fileName)
        {
            using (var reader = File.OpenText(fileName))
            {
                var fileText = await reader.ReadToEndAsync();
                return fileText.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public static async Task<string> GetTopicsAsStringFromFileAsync(string fileName)
        {
            using (var reader = File.OpenText(fileName))
            {
                return await reader.ReadToEndAsync();
            }
        }
    }
}
