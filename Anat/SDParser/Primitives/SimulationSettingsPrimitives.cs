﻿using System;
using SDParser.Enums;

namespace SDParser.Primitives
{
    public class Tier : OneWayTypeDef<int, Tier>
    {
        public Tier(int value) : base(value) { }
    }
    public class StartDate : OneWayTypeDef<DateTime, StartDate>
    {
        public StartDate(DateTime value) : base(value) { }
    }
    public class FinishDate : OneWayTypeDef<DateTime, FinishDate>
    {
        public FinishDate(DateTime value) : base(value) { }
    }
    public class MediaName : OneWayTypeDef<MediaNameEnum, MediaName>
    {
        public MediaName(MediaNameEnum value) : base(value) { }
    }
    public class RandomSeed : OneWayTypeDef<int, RandomSeed>
    {
        public RandomSeed(int value) : base(value) { }
    }
    public class NumberOfGibbsIterations : OneWayTypeDef<int, NumberOfGibbsIterations>
    {
        public NumberOfGibbsIterations(int value) : base(value) { }
    }
    public class NumberOfInitialTopicsLDA : OneWayTypeDef<int, NumberOfInitialTopicsLDA>
    {
        public NumberOfInitialTopicsLDA(int value) : base(value) { }
    }
    public class NumberOfTopWordsToUseFromBoWofTopic : OneWayTypeDef<int, NumberOfTopWordsToUseFromBoWofTopic>
    {
        public NumberOfTopWordsToUseFromBoWofTopic(int value) : base(value) { }
    }
    public class NumberOfWordsInStartNGRAM : OneWayTypeDef<int, NumberOfWordsInStartNGRAM>
    {
        public NumberOfWordsInStartNGRAM(int value) : base(value) { }
    }
    public class NumberOfLDAIterations : OneWayTypeDef<int, NumberOfLDAIterations>
    {
        public NumberOfLDAIterations(int value) : base(value) { }
    }
    public class NGramsThresholdValue : OneWayTypeDef<int, NGramsThresholdValue>
    {
        public NGramsThresholdValue(int value) : base(value) { }
    }
    public class MinimumWordOccurenceForVocabularyEntry : OneWayTypeDef<int, MinimumWordOccurenceForVocabularyEntry>
    {
        public MinimumWordOccurenceForVocabularyEntry(int value) : base(value) { }
    }
    public class IncludeTitleInSetup : OneWayTypeDef<bool, IncludeTitleInSetup>
    {
        public IncludeTitleInSetup(bool value) : base(value) { }
    }
    public class IncludeTitleInTagging : OneWayTypeDef<bool, IncludeTitleInTagging>
    {
        public IncludeTitleInTagging(bool value) : base(value) { }
    }
    public class OrderTopics : OneWayTypeDef<bool, OrderTopics>
    {
        public OrderTopics(bool value) : base(value) { }
    }
    public class WriteAllTopicsWordDistribution : OneWayTypeDef<bool, WriteAllTopicsWordDistribution>
    {
        public WriteAllTopicsWordDistribution(bool value) : base(value) { }
    }
    public class RunLDA : OneWayTypeDef<bool, RunLDA>
    {
        public RunLDA(bool value) : base(value) { }
    }
    public class RunNGramAnalysis : OneWayTypeDef<bool, RunNGramAnalysis>
    {
        public RunNGramAnalysis(bool value) : base(value) { }
    }
    public class UseLocalMediaVocabulary : OneWayTypeDef<bool, UseLocalMediaVocabulary>
    {
        public UseLocalMediaVocabulary(bool value) : base(value) { }
    }
    public class CreateCoOccurrenceMatrix : OneWayTypeDef<bool, CreateCoOccurrenceMatrix>
    {
        public CreateCoOccurrenceMatrix(bool value) : base(value) { }
    }
    public class OutputFolder : OneWayTypeDef<string, OutputFolder>
    {
        public OutputFolder(string value) : base(value) { }
    }
    //Used to make all tier folders be created in the original date time stamp as different tiers are created at different time intervals
    public class StudyNumber : OneWayTypeDef<string, StudyNumber>
    {
        public StudyNumber(string value) : base(value) { }
    }
}
