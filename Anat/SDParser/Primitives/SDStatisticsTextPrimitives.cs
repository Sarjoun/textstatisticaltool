﻿using SDParser.Enums;

namespace SDParser.Primitives
{
   public class RunStatistics : OneWayTypeDef<bool, RunStatistics>
   {
      public RunStatistics(bool value) : base(value) { }
   }

   public class StatisticsSources : OneWayTypeDef<string, StatisticsSources>
   {
      public StatisticsSources(string value) : base(value) { }
   }

   public class StatisticsTarget : OneWayTypeDef<string, StatisticsTarget>
   {
      public StatisticsTarget(string value) : base(value) { }
   }

   public class ReductionInNetworkEdgeValue : OneWayTypeDef<bool, ReductionInNetworkEdgeValue>
   {
      public ReductionInNetworkEdgeValue(bool value) : base(value) { }
   }

   public class RunNetworkStatistics : OneWayTypeDef<bool, RunNetworkStatistics>
   {
      public RunNetworkStatistics(bool value) : base(value) { }
   }

   public class ReductionInWordFrequency : OneWayTypeDef<bool, ReductionInWordFrequency>
   {
      public ReductionInWordFrequency(bool value) : base(value) { }
   }

   public class RemoveLowWordFrequencyValue : OneWayTypeDef<int, RemoveLowWordFrequencyValue>
   {
      public RemoveLowWordFrequencyValue(int value) : base(value) { }
   }

   public class ReductionOfNetworkEdgesWeight : OneWayTypeDef<int, ReductionOfNetworkEdgesWeight>
   {
      public ReductionOfNetworkEdgesWeight(int value) : base(value) { }
   }

   public class TextPartioningAlgorithm : OneWayTypeDef<TextReaderAlgorithmEnum, TextPartioningAlgorithm>
   {
      public TextPartioningAlgorithm(TextReaderAlgorithmEnum value) : base(value) { }
   }

   public class SplitTheSentences : OneWayTypeDef<bool, SplitTheSentences>
   {
      public SplitTheSentences(bool value) : base(value) { }
   }

   public class UseStemmingInTextStatistics : OneWayTypeDef<bool, UseStemmingInTextStatistics>
   {
      public UseStemmingInTextStatistics(bool value) : base(value) { }
   }

   public class HorizonSize : OneWayTypeDef<int, HorizonSize>
   {
      public HorizonSize(int value) : base(value) { }
   }

   public class CalculateConditionalProbability : OneWayTypeDef<bool, CalculateConditionalProbability>
   {
      public CalculateConditionalProbability(bool value) : base(value) { }
   }

   public class CalculateCorrelationCoefficient : OneWayTypeDef<bool, CalculateCorrelationCoefficient>
   {
      public CalculateCorrelationCoefficient(bool value) : base(value) { }
   }

   public class CalculatePointWiseMutualInformation : OneWayTypeDef<bool, CalculatePointWiseMutualInformation>
   {
      public CalculatePointWiseMutualInformation(bool value) : base(value) { }
   }
}
