﻿namespace SDParser.Primitives
{
    public class AllNGramsFileName : OneWayTypeDef<string, AllNGramsFileName>
    {
        public AllNGramsFileName(string value) : base(value) { }
    }

    public class PopularNGramsFileName : OneWayTypeDef<string, PopularNGramsFileName>
    {
        public PopularNGramsFileName(string value) : base(value) { }
    }

    public class PopularNGramsSimilarityMatrixFileName : OneWayTypeDef<string, PopularNGramsSimilarityMatrixFileName>
    {
        public PopularNGramsSimilarityMatrixFileName(string value) : base(value) { }
    }
    public class InitialAgentCosineSimilarityMatrixFileName : OneWayTypeDef<string, InitialAgentCosineSimilarityMatrixFileName>
    {
        public InitialAgentCosineSimilarityMatrixFileName(string value) : base(value) { }
    }
    public class ClusteredNGramMergeResultFileName : OneWayTypeDef<string, ClusteredNGramMergeResultFileName>
    {
        public ClusteredNGramMergeResultFileName(string value) : base(value) { }
    }
    public class FirstMergeOfAgentsFileName : OneWayTypeDef<string, FirstMergeOfAgentsFileName>
    {
        public FirstMergeOfAgentsFileName(string value) : base(value) { }
    }
    public class SecondMergeOfAgentsFileName : OneWayTypeDef<string, SecondMergeOfAgentsFileName>
    {
        public SecondMergeOfAgentsFileName(string value) : base(value) { }
    }
    public class FirstMergeTaggedDocIDsFileName : OneWayTypeDef<string, FirstMergeTaggedDocIDsFileName>
    {
        public FirstMergeTaggedDocIDsFileName(string value) : base(value) { }
    }
    public class FirstMergeOfAgentsGraphFileName : OneWayTypeDef<string, FirstMergeOfAgentsGraphFileName>
    {
        public FirstMergeOfAgentsGraphFileName(string value) : base(value) { }
    }
    public class SecondMergeOfAgentsGraphFileName : OneWayTypeDef<string, SecondMergeOfAgentsGraphFileName>
    {
        public SecondMergeOfAgentsGraphFileName(string value) : base(value) { }
    }
    public class SecondMergeTaggedDocIdsFileName : OneWayTypeDef<string, SecondMergeTaggedDocIdsFileName>
    {
        public SecondMergeTaggedDocIdsFileName(string value) : base(value) { }
    }
    //public class NGramTaggedFilesFileName : OneWayTypeDef<string, NGramTaggedFilesFileName>
    //{
    //    public NGramTaggedFilesFileName(string value) : base(value) { }
    //}
    //public class NGramTaggedFilesInfoFileName : OneWayTypeDef<string, NGramTaggedFilesInfoFileName>
    //{
    //    public NGramTaggedFilesInfoFileName(string value) : base(value) { }
    //}
    //public class TaggedDocumentsSimilarityMatrixFileName : OneWayTypeDef<string, TaggedDocumentsSimilarityMatrixFileName>
    //{
    //    public TaggedDocumentsSimilarityMatrixFileName(string value) : base(value) { }
    //}

    public class TaggedDocumentsSubCorporaFileName : OneWayTypeDef<string, TaggedDocumentsSubCorporaFileName>
    {
        public TaggedDocumentsSubCorporaFileName(string value) : base(value) { }
    }

    public class UnTaggedDocumentsSubCorporaFileName : OneWayTypeDef<string, UnTaggedDocumentsSubCorporaFileName>
    {
        public UnTaggedDocumentsSubCorporaFileName(string value) : base(value) { }
    }

}
