﻿namespace SDParser.Primitives
{
    public class Alpha : OneWayTypeDef<double, Alpha>
    {
        public Alpha(double value) : base(value) { }
    }

    public class Beta : OneWayTypeDef<double, Beta>
    {
        public Beta(double value) : base(value) { }
    }

    public class VocabularySize : OneWayTypeDef<int, VocabularySize>
    {
        public VocabularySize(int value) : base(value) { }
    }
    
    public class CountOfAllWordsInDocs : OneWayTypeDef<int, CountOfAllWordsInDocs>
    {
        public CountOfAllWordsInDocs(int value) : base(value) { }
    }

    public class CountOfAllDocs : OneWayTypeDef<int, CountOfAllDocs>
    {
        public CountOfAllDocs(int value) : base(value) { }
    }
    public class CountOfAllTaggedDocs : OneWayTypeDef<int, CountOfAllTaggedDocs>
    {
        public CountOfAllTaggedDocs(int value) : base(value) { }
    }

}
