﻿using System;

namespace SDParser.Primitives
{
    public abstract class OneWayTypeDef<T, TDerived>
        where TDerived : OneWayTypeDef<T, TDerived>
    {
        public readonly T Value;

        protected OneWayTypeDef(T value)
        {
            Value = value;

            if (Value == null)
                throw new ArgumentException($"Value cannot be null: {typeof(TDerived).Name}");
        }

        public static implicit operator T(OneWayTypeDef<T, TDerived> typeDef)
        {
            return typeDef == null
                ? default(T)
                : typeDef.Value;
        }

        public override bool Equals(object obj)
        {
            var other = obj as OneWayTypeDef<T, TDerived>;
            return (object)other != null
                   && Value.Equals(other.Value);
        }

        public bool Equals(OneWayTypeDef<T, TDerived> other)
        {
            return Value.Equals(other.Value);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public static bool operator ==(OneWayTypeDef<T, TDerived> a, object b)
        {
            if (object.ReferenceEquals(a, b))
                return true;

            // ReSharper disable once RedundantCast.0
            if ((object)a == null || (object)b == null)
                return false;

            return a.GetType() == b.GetType()
                   && a.Value.Equals(((OneWayTypeDef<T, TDerived>)b).Value);
        }

        public static bool operator !=(OneWayTypeDef<T, TDerived> a, object b)
        {
            return !(a == b);
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public static TDerived FromValue(T value)
        {
            return (TDerived)Activator
                .CreateInstance(
                    typeof(TDerived),
                    value);
        }

    }
}
