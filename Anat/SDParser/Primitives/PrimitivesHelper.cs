﻿using SDParser.Extensions;

namespace SDParser.Primitives
{
    public static class PrimitivesHelper
    {
        public static string PrintOutPrimitiveAndValue<T>(T fileNameType, string value)
        {
            return fileNameType.GetType().Name + "=" + value.CleanUpFilePathBackSlashes();
        }
    }
}
