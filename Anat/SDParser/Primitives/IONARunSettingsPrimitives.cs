﻿namespace SDParser.Primitives
{
    public class SimulationSettingsFile : OneWayTypeDef<string, SimulationSettingsFile>
    {
        public SimulationSettingsFile(string value) : base(value) { }
    }
    public class UntaggedDocsFromPreviousTier : OneWayTypeDef<string, UntaggedDocsFromPreviousTier>
    {
        public UntaggedDocsFromPreviousTier(string value) : base(value) { }
    }
    public class IONARunSettingsFileName : OneWayTypeDef<string, IONARunSettingsFileName>
    {
        public IONARunSettingsFileName(string value) : base(value) { }
    }
    //public NewLDARunSettingsFileName(string value) : base(value) { }
}
