﻿namespace SDParser.Primitives
{
    public class TotalFilesNumber : OneWayTypeDef<int, TotalFilesNumber>
    {
        public TotalFilesNumber(int value) : base(value) { }
    }

    public class TaggedFilesNumber : OneWayTypeDef<int, TaggedFilesNumber>
    {
        public TaggedFilesNumber(int value) : base(value) { }
    }

    public class DBCoverage : OneWayTypeDef<double, DBCoverage>
    {
        public DBCoverage(int value) : base(value) { }
    }

}
