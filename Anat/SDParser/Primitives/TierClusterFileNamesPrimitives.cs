﻿namespace SDParser.Primitives
{
    public class ClusteredTagsTemplateFileName : OneWayTypeDef<string, ClusteredTagsTemplateFileName>
    {
        public ClusteredTagsTemplateFileName(string value) : base(value) { }
    }

    public class CurrentTagId : OneWayTypeDef<int, CurrentTagId> 
    {
        public CurrentTagId(int value) : base(value) { }
    }
}
