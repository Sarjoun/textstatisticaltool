﻿using System;
using System.Globalization;
using System.Linq;
using SDParser.Enums;

namespace SDParser.Primitives
{
   static class PrimitivesExtractor
   {
      private const string DateFormat = "MMddyyyy";

      #region IONA & ILDA Settings
      public static Tier GetTier(string data)
      {
         return Tier.FromValue(Convert.ToInt32(data));
      }
      public static StartDate GetStartDate(string data)
      {
         return StartDate.FromValue(DateTime.ParseExact(
             data,
             DateFormat,
             CultureInfo.InvariantCulture));
      }
      public static FinishDate GetFinishDate(string data)
      {
         return FinishDate.FromValue(DateTime.ParseExact(
             data,
             DateFormat,
             CultureInfo.InvariantCulture));
      }
      public static MediaName GetClosestMediaNameFromSimulationMedia(string data)
      {
         var closestMatch =
             MediaNameEnumUtilities.GetNames<MediaNameEnum>()
                 .ToArray()
                 .First(x => x.Contains(data));

         return MediaName.FromValue((MediaNameEnum)Enum.Parse(typeof(MediaNameEnum), closestMatch));
      }
      public static RandomSeed GetRandomSeed(string data)
      {
         return RandomSeed.FromValue(Convert.ToInt32(data));
      }
      public static NumberOfGibbsIterations GetNumberOfGibbsIterations(string data)
      {
         return NumberOfGibbsIterations.FromValue(Convert.ToInt32(data));
      }
      public static NumberOfInitialTopicsLDA GetNumberOfInitialTopicsLDA(string data)
      {
         return NumberOfInitialTopicsLDA.FromValue(Convert.ToInt32(data));
      }
      public static NumberOfTopWordsToUseFromBoWofTopic GetNumberOfTopWordsToUseFromBOWOfTopic(string data)
      {
         return NumberOfTopWordsToUseFromBoWofTopic.FromValue(Convert.ToInt32(data));
      }
      public static NumberOfWordsInStartNGRAM GetNumberOfWordsInStartNGRAM(string data)
      {
         return NumberOfWordsInStartNGRAM.FromValue(Convert.ToInt32(data));
      }
      public static NumberOfLDAIterations GetNumberOfLDAIterations(string data)
      {
         return NumberOfLDAIterations.FromValue(Convert.ToInt32(data));
      }
      public static NGramsThresholdValue GetNGramsThresholdValue(string data)
      {
         //double n;
         //return NGramsThresholdValue.FromValue(double.TryParse(data, out n) ? Convert.ToDouble(data) : 0);
         int n;
         return NGramsThresholdValue.FromValue(int.TryParse(data, out n) ? Convert.ToInt32(data) : 4);
      }
      public static MinimumWordOccurenceForVocabularyEntry GetMinimumWordOccurenceForVocabularyEntry(string data)
      {
         return MinimumWordOccurenceForVocabularyEntry.FromValue(Convert.ToInt32(data));
      }
      public static IncludeTitleInSetup GetIncludeTitleInSetup(string data)
      {
         return IncludeTitleInSetup.FromValue(Convert.ToBoolean(data));
      }
      public static IncludeTitleInTagging GetIncludeTitleInTagging(string data)
      {
         return IncludeTitleInTagging.FromValue(Convert.ToBoolean(data));
      }
      public static OrderTopics GetOrderTopicsValue(string data)
      {
         return OrderTopics.FromValue(Convert.ToBoolean(data));
      }
      public static WriteAllTopicsWordDistribution GetWriteAllTopicsWordDistribution(string data)
      {
         return WriteAllTopicsWordDistribution.FromValue(Convert.ToBoolean(data));
      }
      public static RunLDA GetRunLDA(string data)
      {
         return RunLDA.FromValue(Convert.ToBoolean(data));
      }
      public static RunNGramAnalysis GetRunNGramAnalysis(string data)
      {
         return RunNGramAnalysis.FromValue(Convert.ToBoolean(data));
      }
      public static UseLocalMediaVocabulary GetUseLocalMediaVocabulary(string data)
      {
         return UseLocalMediaVocabulary.FromValue(Convert.ToBoolean(data));
      }
      public static CreateCoOccurrenceMatrix GetCreateCoOccurrenceMatrix(string data)
      {
         return CreateCoOccurrenceMatrix.FromValue(Convert.ToBoolean(data));
      }
      public static OutputFolder GetOutputFolder(string data) { return OutputFolder.FromValue(data); }
      public static RootFolderPath GetRootFolder(string data) { return RootFolderPath.FromValue(data); }
      public static DatabaseRootFolderPath GetDatabaseRootFolder(string data) { return DatabaseRootFolderPath.FromValue(data); }
      public static StudyNumber GetStudyNumber(string data)
      {
         return StudyNumber.FromValue(string.IsNullOrEmpty(data)
             ? $@"{DateTime.Now:HH.mm.ss [dd-MM-yy]}"
             : data);
      }
      public static UntaggedDocsFromPreviousTier GetUntaggedDocsFromPreviousTier(string data)
      {
         return UntaggedDocsFromPreviousTier.FromValue(string.IsNullOrEmpty(data)
             ? $@""
             : data);
      }
      public static Alpha GetAlpha(string data) { return Alpha.FromValue(Convert.ToDouble(data)); }
      public static Beta GetBeta(string data) { return Beta.FromValue(Convert.ToDouble(data)); }

      #endregion

      #region DbScanAlgorithm Settings
      public static DbScanBallRadius GetDbScanBallRadius(string data)
      {
         return DbScanBallRadius.FromValue(Convert.ToDouble(data));
      }
      public static DbScanMinPointsInRegion GetDbScanMinPointsInRegion(string data)
      {
         return DbScanMinPointsInRegion.FromValue(Convert.ToInt32(data));
      }
      public static InterNGramMergingThreshold GetInterNGramMergingThreshold(string data)
      {
         return InterNGramMergingThreshold.FromValue(float.Parse(data, CultureInfo.InvariantCulture.NumberFormat));
      }
      public static DocNGramTaggingThreshold GetDocNGramTaggingThreshold(string data)
      {
         return DocNGramTaggingThreshold.FromValue(float.Parse(data, CultureInfo.InvariantCulture.NumberFormat));
      }
      #endregion

      #region TextStatistics Settings   
      public static RunStatistics GetRunStatistics(string data)
      {
         return RunStatistics.FromValue(Convert.ToBoolean(data));
      }
      public static StatisticsSources GetStatisticsSources(string data)
      {
         return StatisticsSources.FromValue(data);
      }
      public static StatisticsTarget GetStatisticsTarget(string data)
      {
         return StatisticsTarget.FromValue(data);
      }
      public static ReductionInNetworkEdgeValue GetReductionInNetworkEdgeValue(string data)
      {
         return ReductionInNetworkEdgeValue.FromValue(Convert.ToBoolean(data));
      }
      public static RunNetworkStatistics GetRunNetworkStatistics(string data)
      {
         return RunNetworkStatistics.FromValue(Convert.ToBoolean(data));
      }
      public static ReductionInWordFrequency GetReductionInWordFrequency(string data)
      {
         return ReductionInWordFrequency.FromValue(Convert.ToBoolean(data));
      }
      public static RemoveLowWordFrequencyValue GetRemoveLowWordFrequencyValue(string data)
      {
         return RemoveLowWordFrequencyValue.FromValue(Convert.ToInt32(data));
      }
      public static ReductionOfNetworkEdgesWeight GetReductionOfNetworkEdgesWeight(string data)
      {
         return ReductionOfNetworkEdgesWeight.FromValue(Convert.ToInt32(data));
      }
      public static TextPartioningAlgorithm GetTextPartioningAlgorithm(string data)
      {
         return TextPartioningAlgorithm.FromValue((TextReaderAlgorithmEnum)Enum.Parse(typeof(TextReaderAlgorithmEnum), data));
      }
      public static SplitTheSentences GetSplitTheSentences(string data)
      {
         return SplitTheSentences.FromValue(Convert.ToBoolean(data));
      }
      public static HorizonSize GetHorizonSize(string data)
      {
         return HorizonSize.FromValue(Convert.ToInt32(data));
      }
      public static UseStemmingInTextStatistics GetUseStemmingInTextStatistics(string data)
      {
         return UseStemmingInTextStatistics.FromValue(Convert.ToBoolean(data));
      }
      public static CalculateConditionalProbability GetCalculateConditionalProbability(string data)
      {
         return CalculateConditionalProbability.FromValue(Convert.ToBoolean(data));
      }
      public static CalculatePointWiseMutualInformation GetCalculatePointWiseMutualInformation(string data)
      {
         return CalculatePointWiseMutualInformation.FromValue(Convert.ToBoolean(data));
      }
      public static CalculateCorrelationCoefficient GetCalculateCorrelationCoefficient(string data)
      {
         return CalculateCorrelationCoefficient.FromValue(Convert.ToBoolean(data));
      }
      #endregion

      #region IONA Output Files Settings
      public static CountOfAllDocs GetCountOfAllDocs(string data) { return CountOfAllDocs.FromValue(Convert.ToInt32(data)); }
      public static CountOfAllTaggedDocs GetCountOfAllTaggedDocs(string data) { return CountOfAllTaggedDocs.FromValue(Convert.ToInt32(data)); }
      public static CountOfAllWordsInDocs GetCountOfAllWordsInDocs(string data) { return CountOfAllWordsInDocs.FromValue(Convert.ToInt32(data)); }
      public static VocabularySize GetVocabularySize(string data) { return VocabularySize.FromValue(Convert.ToInt32(data)); }
      public static DocumentsLinksFileName GetDocumentsLinksFileName(string data) { return DocumentsLinksFileName.FromValue(data); }
      public static DocWordFileName GetDocWordFileName(string data) { return DocWordFileName.FromValue(data); }
      public static TopicDocumentArrayFileName GetTopicDocumentArrayFileName(string data) { return TopicDocumentArrayFileName.FromValue(data); }
      public static TaggedDocumentsSubCorporaFileName GetTaggedDocumentsSubCorporaFileName(string data) { return TaggedDocumentsSubCorporaFileName.FromValue(data); }
      public static UnTaggedDocumentsSubCorporaFileName GetUnTaggedDocumentsSubCorporaFileName(string data) { return UnTaggedDocumentsSubCorporaFileName.FromValue(data); }
      #endregion

      #region ILDA Output Files Settings
      public static AllNGramsFileName GetAllNGramsFileName(string data)
      {
         return AllNGramsFileName.FromValue(data);
      }
      public static WordFrequencyFileName GetWordFrequencyFileName(string data)
      {
         return WordFrequencyFileName.FromValue(data);
      }
      public static WordStreamFileName GetWordStreamFileName(string data)
      {
           return WordStreamFileName.FromValue(data);
      }
        public static VocabularyFileName GetVocabularyFileName(string data)
      {
         return VocabularyFileName.FromValue(data);
      }
      public static TopicsBagOfWordsFileName GetTopicsBagOfWordsFileName(string data)
      {
         return TopicsBagOfWordsFileName.FromValue(data);
      }
      public static PopularNGramsFileName GetPopularNGramsFileName(string data)
      {
         return PopularNGramsFileName.FromValue(data);
      }
      public static PopularNGramsSimilarityMatrixFileName GetPopularNGramsSimilarityMatrixFileName(string data)
      {
         return PopularNGramsSimilarityMatrixFileName.FromValue(data);
      }
      public static InitialAgentCosineSimilarityMatrixFileName GetInitialAgentCosineSimilarityMatrixFileName(string data)
      {
         return InitialAgentCosineSimilarityMatrixFileName.FromValue(data);
      }
      public static ClusteredNGramMergeResultFileName GetClusteredNGramMergeResultFileName(string data)
      {
         return ClusteredNGramMergeResultFileName.FromValue(data);
      }
      public static FirstMergeOfAgentsFileName GetFirstMergeOfAgentsFileName(string data)
      {
         return FirstMergeOfAgentsFileName.FromValue(data);
      }
      public static FirstMergeTaggedDocIDsFileName GetFirstMergeTaggedDocIDsFileName(string data)
      {
         return FirstMergeTaggedDocIDsFileName.FromValue(data);
      }
      public static FirstMergeOfAgentsGraphFileName GetFirstMergeOfAgentsGraphFileName(string data)
      {
         return FirstMergeOfAgentsGraphFileName.FromValue(data);
      }
      public static SecondMergeOfAgentsFileName GetSecondMergeOfAgentsFileName(string data)
      {
         return SecondMergeOfAgentsFileName.FromValue(data);
      }
      public static SecondMergeOfAgentsGraphFileName GetSecondMergeOfAgentsGraphFileName(string data)
      {
         return SecondMergeOfAgentsGraphFileName.FromValue(data);
      }
      public static SecondMergeTaggedDocIdsFileName GetSecondMergeTaggedDocIdsFileName(string data)
      {
         return SecondMergeTaggedDocIdsFileName.FromValue(data);
      }
      #endregion
   }
}
