﻿namespace SDParser.Primitives
{
    public class DbScanBallRadius : OneWayTypeDef<double, DbScanBallRadius>
    {
        public DbScanBallRadius(double value) : base(value) { }
    }
    public class DbScanMinPointsInRegion : OneWayTypeDef<int, DbScanMinPointsInRegion>
    {
        public DbScanMinPointsInRegion(int value) : base(value) { }
    }

    //Other primitives
    public class InterNGramMergingThreshold : OneWayTypeDef<float, InterNGramMergingThreshold>
    {
        public InterNGramMergingThreshold(float value) : base(value) { }
    }
    public class DocNGramTaggingThreshold : OneWayTypeDef<float, DocNGramTaggingThreshold>
    {
        public DocNGramTaggingThreshold(float value) : base(value) { }
    }
}


