﻿namespace SDParser.Primitives
{
    public class RootFolderPath : OneWayTypeDef<string, RootFolderPath>
    {
        public RootFolderPath(string value) : base(value) { }
    }
    public class DatabaseRootFolderPath : OneWayTypeDef<string, DatabaseRootFolderPath>
    {
        public DatabaseRootFolderPath(string value) : base(value) { }
    }
    public class MainDatabaseFolderPath : OneWayTypeDef<string, MainDatabaseFolderPath>
    {
        public MainDatabaseFolderPath(string value) : base(value) { }
    }
    public class SimulationDateFolderPath : OneWayTypeDef<string, SimulationDateFolderPath>
    {
        public SimulationDateFolderPath(string value) : base(value) { }
    }

    public class ExperimentFolderPath : OneWayTypeDef<string, ExperimentFolderPath>
    {
        public ExperimentFolderPath(string value) : base(value) { }
    }
    public class MediaFolderPath : OneWayTypeDef<string, MediaFolderPath>

    {
        public MediaFolderPath(string value) : base(value) { }
    }
    public class CurrentCorpusTierFolderPath : OneWayTypeDef<string, CurrentCorpusTierFolderPath>
    {
        public CurrentCorpusTierFolderPath(string value) : base(value) { }
    }
    public class StopWordsFilePath : OneWayTypeDef<string, StopWordsFilePath>
    {
        public StopWordsFilePath(string value) : base(value) { }
    }
    public class ExperimentCustomFolderPath : OneWayTypeDef<string, ExperimentCustomFolderPath>
    {
        public ExperimentCustomFolderPath(string value) : base(value) { }
    }
    public class CustomDbFolderSources : OneWayTypeDef<string, CustomDbFolderSources>
    {
        public CustomDbFolderSources(string value) : base(value) { }
    }

    ////////////////////////////////////////////////////////////////////////

    public class ResourcesFolderPath : OneWayTypeDef<string, ResourcesFolderPath>
    {
        public ResourcesFolderPath(string value) : base(value) { }
    }
    public class DesktopFolderPath : OneWayTypeDef<string, DesktopFolderPath>
    {
        public DesktopFolderPath(string value) : base(value) { }
    }


    //Globals.MainFolder = Globals.RootFolder + @":\TopicModels\";
    //Globals.ExperimentFolder = Globals.RootFolder + @":\TopicModels\Experiments\";
    //Globals.StopWordsFile = Globals.RootFolder + @":\TopicModels\stopwords.txt";
    //Globals.ExperimentCustomFolder = Globals.RootFolder + @":\TopicModels\ExperimentsCustom\";
    //Globals.CustomDBFolderSources = Globals.RootFolder + @":\TopicModels\indexes_Custom\Sources\";

}
