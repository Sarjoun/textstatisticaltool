﻿namespace SDParser.Primitives
{
    public class DocumentsLinksFileName : OneWayTypeDef<string, DocumentsLinksFileName>
    {
        public DocumentsLinksFileName(string value) : base(value) { }
    }

    public class DocWordFileName : OneWayTypeDef<string, DocWordFileName>
    {
        public DocWordFileName(string value) : base(value) { }
    }
    public class WordFrequencyFileName : OneWayTypeDef<string, WordFrequencyFileName>
    {
        public WordFrequencyFileName(string value) : base(value) { }
    }

    public class WordFrequencyForDataFileName : OneWayTypeDef<string, WordFrequencyForDataFileName>
    {
        public WordFrequencyForDataFileName(string value) : base(value) { }
    }
    public class VocabularyFileName : OneWayTypeDef<string, VocabularyFileName>
    {
        public VocabularyFileName(string value) : base(value) { }
    }

    public class WordStreamFileName : OneWayTypeDef<string, WordStreamFileName>
    {
        public WordStreamFileName(string value) : base(value) { }
    }

    //NWT
    public class WordTopicArrayFileName : OneWayTypeDef<string, WordTopicArrayFileName>
    {
        public WordTopicArrayFileName(string value) : base(value) { }
    }

    //NTD
    public class TopicDocumentArrayFileName : OneWayTypeDef<string, TopicDocumentArrayFileName>
    {
        public TopicDocumentArrayFileName(string value) : base(value) { }
    }
    //Z
    public class TopicsVectorFileName : OneWayTypeDef<string, TopicsVectorFileName>
    {
        public TopicsVectorFileName(string value) : base(value) { }
    }

    public class TopicsBagOfWordsFileName : OneWayTypeDef<string, TopicsBagOfWordsFileName>
    {
        public TopicsBagOfWordsFileName(string value) : base(value) { }
    }

    public class TopicsBagOfWordsDataFileName : OneWayTypeDef<string, TopicsBagOfWordsDataFileName>
    {
        public TopicsBagOfWordsDataFileName(string value) : base(value) { }
    }
}

