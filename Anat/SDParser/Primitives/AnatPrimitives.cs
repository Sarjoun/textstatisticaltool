﻿namespace SDParser.Primitives
{
    public class CurrentSimulationSettingsFileName : OneWayTypeDef<string, CurrentSimulationSettingsFileName>
    {
        public CurrentSimulationSettingsFileName(string value) : base(value) { }
    }
}
