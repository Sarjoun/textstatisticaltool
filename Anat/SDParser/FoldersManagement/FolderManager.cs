﻿using System;
using System.IO;
using SDParser.Primitives;

namespace SDParser.FoldersManagement
{
    public static class FolderManager
    {
        public static RootFolderPath GetRootFolderPath()
        {
            return RootFolderPath.FromValue(
                AppDomain.CurrentDomain.BaseDirectory.Substring(0, 3));
        }
        public static DesktopFolderPath GetDesktopFolderPath()
        {
            return DesktopFolderPath.FromValue(
                Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\");
        }
        public static bool CreateOrOverwriteLDASampleSettingsFileOnDesktop()
        {
            var ldaSettingsFile =
                StopWordsFilePath.FromValue(GetDesktopFolderPath().Value + @"\LDARunSettings.txt");

            try
            {
                File.WriteAllText(
                    ldaSettingsFile.Value,
                    SDParser.Properties.Resources.LDARunSettings);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
                //throw;
            }
            return true;
        }

    }
}
