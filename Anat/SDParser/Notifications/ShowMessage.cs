﻿using System;
using System.Threading.Tasks;
using System.Windows;
using SDParser.FileFunctions;

namespace SDParser.Notifications
{
   public static class ShowMessage
   {
      public static void ShowErrorMessageAndExit(string message, Exception e)
      {
         var result =
             MessageBox.Show(message + "\r\n" + e.Message,
                 "The simulation will exit now.\r\nPress OK when ready",
                 MessageBoxButton.OK,
                 MessageBoxImage.Error);
         var x = WriteErrorInFileOnDesktop(message + "\r\n" + e.Message);
         if (result == MessageBoxResult.OK)
         {
            Application.Current.Shutdown();
         }
         Console.WriteLine(e);
      }

      private static async Task WriteErrorInFileOnDesktop(string message)
      {
         var fileName = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\ErrorFromANAT " + DateTime.Now.ToFileTime() + ".txt";
         await FileWriter.WriteStringToFileAsync(message, fileName);
      }

      public static void ShowErrorMessageAndExit(string message)
      {
         var result =
             MessageBox.Show(message + "\r\n",
                 "The simulation will exit now.\r\nPress OK when ready",
                 MessageBoxButton.OK,
                 MessageBoxImage.Error);
         if (result == MessageBoxResult.OK)
         {
            Application.Current.Shutdown();
         }
      }

      public static void ShowWarningMessage(string message)
      {
         var result =
             MessageBox.Show(message + "\r\n",
                 "The simulation will proceed now.\r\nPress OK when ready",
                 MessageBoxButton.OK,
                 MessageBoxImage.Warning);
         if (result == MessageBoxResult.OK)
         {

         }
      }
      public static void ShowWarningMessageAndExit(string message)
      {
         var result =
             MessageBox.Show(message + "\r\n",
                 "The simulation will proceed now.\r\nPress OK when ready",
                 MessageBoxButton.OK,
                 MessageBoxImage.Warning);
         if (result == MessageBoxResult.OK)
         {
            Application.Current.Shutdown();
         }
         Application.Current.Shutdown();
      }
   }
}
