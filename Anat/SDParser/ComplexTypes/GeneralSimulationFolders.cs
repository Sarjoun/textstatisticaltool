﻿using LeagueOfMonads;
using SDParser.Primitives;

namespace SDParser.ComplexTypes
{
    public class GeneralSimulationFolders : Union<
        ExperimentFolderPath,
        ExperimentCustomFolderPath>
    {
        public GeneralSimulationFolders(ExperimentFolderPath item) : base(item) { }
        public GeneralSimulationFolders(ExperimentCustomFolderPath item) : base(item) { }

        public static implicit operator GeneralSimulationFolders(ExperimentFolderPath value)
        {
            return new GeneralSimulationFolders(value);
        }
        public static implicit operator GeneralSimulationFolders(ExperimentCustomFolderPath value)
        {
            return new GeneralSimulationFolders(value);
        }
    }
}
