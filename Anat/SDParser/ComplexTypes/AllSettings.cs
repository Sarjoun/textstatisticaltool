﻿using LeagueOfMonads;
using SDParser.Primitives;

namespace SDParser.ComplexTypes
{
    public class AllSettings
    {
        public SimulationSettingsFile SimulationSettingsFile { get; set; }
        public UntaggedDocsFromPreviousTier UntaggedDocsFromPreviousTier { get; set; }
        public Tier Tier { get; set; }
        public StartDate StartDate { get; set; }
        public FinishDate FinishDate { get; set; }
        public MediaName MediaName { get; set; }
        public RandomSeed RandomSeed { get; set; }
        public NumberOfGibbsIterations NumberOfGibbsIterations { get; set; }
        public NumberOfInitialTopicsLDA NumberOfInitialTopicsLDA { get; set; }
        public NumberOfTopWordsToUseFromBoWofTopic NumberOfTopWordsToUseFromBoWofTopic { get; set; }
        public NumberOfWordsInStartNGRAM NumberOfWordsInStartNGRAM { get; set; }
        public NumberOfLDAIterations NumberOfLDAIterations { get; set; }
        public NGramsThresholdValue NGramsThresholdValue { get; set; }
        public MinimumWordOccurenceForVocabularyEntry MinimumWordOccurenceForVocabularyEntry { get; set; }
        public IncludeTitleInSetup IncludeTitleInSetup { get; set; }
        public IncludeTitleInTagging IncludeTitleInTagging { get; set; }
        public OrderTopics OrderTopics { get; set; }
        public WriteAllTopicsWordDistribution WriteAllTopicsWordDistribution { get; set; }
        public RunLDA RunLDA { get; set; }
        public RunNGramAnalysis RunNGramAnalysis { get; set; }
        public UseLocalMediaVocabulary UseLocalMediaVocabulary { get; set; }
        public CreateCoOccurrenceMatrix CreateCoOccurrenceMatrix { get; set; }
        public OutputFolder OutputFolder { get; set; }
        public RootFolderPath RootFolderPath { get; set; }
        public DatabaseRootFolderPath DatabaseRootFolderPath { get; set; }
        public Option<StudyNumber> StudyNumber { get; set; }
        public DbScanBallRadius DbScanBallRadius { get; set; }
        public DbScanMinPointsInRegion DbScanMinPointsInRegion { get; set; }
        public InterNGramMergingThreshold InterNGramMergingThreshold { get; set; }
        public DocNGramTaggingThreshold DocNGramTaggingThreshold { get; set; }
        public CurrentCorpusTierFolderPath CurrentCorpusTierFolderPath { get; set; }
        public DocumentsLinksFileName DocumentsLinksFileName { get; set; }
        public DocWordFileName DocWordFileName { get; set; }
        public WordFrequencyFileName WordFrequencyFileName { get; set; }
        public WordFrequencyForDataFileName WordFrequencyForDataFileName { get; set; }
        public VocabularyFileName VocabularyFileName { get; set; }
        public WordStreamFileName WordStreamFileName { get; set; }
        public WordTopicArrayFileName WordTopicArrayFileName { get; set; }
        public TopicDocumentArrayFileName TopicDocumentArrayFileName { get; set; }
        public TopicsVectorFileName TopicsVectorFileName { get; set; }
        public TopicsBagOfWordsFileName TopicsBagOfWordsFileName { get; set; }
        public TopicsBagOfWordsDataFileName TopicsBagOfWordsDataFileName { get; set; }
        public IONARunSettingsFileName IONARunSettingsFileName { get; set; }
        public AllNGramsFileName AllNGramsFileName { get; set; }
        public PopularNGramsFileName PopularNGramsFileName { get; set; }
        public PopularNGramsSimilarityMatrixFileName PopularNGramsSimilarityMatrixFileName { get; set; }
        public InitialAgentCosineSimilarityMatrixFileName InitialAgentCosineSimilarityMatrixFileName { get; set; }
        public ClusteredNGramMergeResultFileName ClusteredNGramMergeResultFileName { get; set; }
        public FirstMergeTaggedDocIDsFileName FirstMergeTaggedDocIDsFileName { get; set; }
        public FirstMergeOfAgentsFileName FirstMergeOfAgentsFileName { get; set; }
        public FirstMergeOfAgentsGraphFileName FirstMergeOfAgentsGraphFileName { get; set; }
        public SecondMergeOfAgentsFileName SecondMergeOfAgentsFileName { get; set; }
        public SecondMergeOfAgentsGraphFileName SecondMergeOfAgentsGraphFileName { get; set; }
        public SecondMergeTaggedDocIdsFileName SecondMergeTaggedDocIdsFileName { get; set; }
        public TaggedDocumentsSubCorporaFileName TaggedDocumentsSubCorporaFileName { get; set; }
        public UnTaggedDocumentsSubCorporaFileName UnTaggedDocumentsSubCorporaFileName { get; set; }
        //
        public CurrentTagId CurrentTagId { get; set; }
        public ClusteredTagsTemplateFileName ClusteredTagsTemplateFileName { get; set; }
        public CountOfAllDocs CountOfAllDocs { get; set; }
        public CountOfAllTaggedDocs CountOfAllTaggedDocs { get; set; }
        public CountOfAllWordsInDocs CountOfAllWordsInDocs { get; set; }
        public VocabularySize VocabularySize { get; set; }
        public Alpha Alpha { get; set; }
        public Beta Beta { get; set; }
        //
        public RunStatistics RunStatistics { get; set; }
        public StatisticsSources StatisticsSources { get; set; }
        public StatisticsTarget StatisticsTarget { get; set; }
        public ReductionInNetworkEdgeValue ReductionInNetworkEdgeValue { get; set; }
        public RunNetworkStatistics RunNetworkStatistics { get; set; }
        public ReductionInWordFrequency ReductionInWordFrequency { get; set; }
        public RemoveLowWordFrequencyValue RemoveLowWordFrequencyValue { get; set; }
        public ReductionOfNetworkEdgesWeight ReductionOfNetworkEdgesWeight { get; set; }
        public TextPartioningAlgorithm TextPartioningAlgorithm { get; set; }
        public SplitTheSentences SplitTheSentences { get; set; }
        public HorizonSize HorizonSize { get; set; }
        public UseStemmingInTextStatistics UseStemmingInTextStatistics { get; set; }
        public CalculateConditionalProbability CalculateConditionalProbability { get; set; }
        public CalculatePointWiseMutualInformation CalculatePointWiseMutualInformation { get; set; }
        public CalculateCorrelationCoefficient CalculateCorrelationCoefficient { get; set; }

        public TextStatisticsSettings ExtractTextStatisticsSettings => new TextStatisticsSettings
        {
            RunStatistics = RunStatistics,
            StatisticsSources = StatisticsSources,
            StatisticsTarget = StatisticsTarget,
            ReductionInNetworkEdgeValue = ReductionInNetworkEdgeValue,
            RunNetworkStatistics = RunNetworkStatistics,
            ReductionInWordFrequency = ReductionInWordFrequency,
            RemoveLowWordFrequencyValue = RemoveLowWordFrequencyValue,
            ReductionOfNetworkEdgesWeight = ReductionOfNetworkEdgesWeight,
            TextPartioningAlgorithm = TextPartioningAlgorithm,
            SplitTheSentences = SplitTheSentences,
            HorizonSize = HorizonSize,
            IncludeTitleInSetup = IncludeTitleInSetup,
            UseStemmingInTextStatistics = UseStemmingInTextStatistics,
            CalculateConditionalProbability = CalculateConditionalProbability,
            CalculatePointWiseMutualInformation = CalculatePointWiseMutualInformation,
            CalculateCorrelationCoefficient = CalculateCorrelationCoefficient,
        };
    }
}

