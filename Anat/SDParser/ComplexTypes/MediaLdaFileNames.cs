﻿using System;
using SDParser.Extensions;
using SDParser.Primitives;
   
namespace SDParser.ComplexTypes
{
   public class MediaLDAFileNames : IComplexTypeILDA
   {
      public Tier Tier { get; set; }
      public CurrentCorpusTierFolderPath CurrentCorpusTierFolderPath { get; set; }
      public DocumentsLinksFileName DocumentsLinksFileName { get; set; }
      public DocWordFileName DocWordFileName { get; set; }
      public WordFrequencyFileName WordFrequencyFileName { get; set; }
      public WordFrequencyForDataFileName WordFrequencyForDataFileName { get; set; }
      public VocabularyFileName VocabularyFileName { get; set; }
      public WordStreamFileName WordStreamFileName { get; set; }
      public WordTopicArrayFileName WordTopicArrayFileName { get; set; }
      public TopicDocumentArrayFileName TopicDocumentArrayFileName { get; set; }
      public TopicsVectorFileName TopicsVectorFileName { get; set; }
      public TopicsBagOfWordsFileName TopicsBagOfWordsFileName { get; set; }
      public TopicsBagOfWordsDataFileName TopicsBagOfWordsDataFileName { get; set; }
      public IONARunSettingsFileName IONARunSettingsFileName { get; set; }

      public bool CreateEmptyFilesForAllLDAFileNames()
      {
         try
         {
            DocumentsLinksFileName.Value.CreateEmptyFile();
            DocWordFileName.Value.CreateEmptyFile();
            WordFrequencyFileName.Value.CreateEmptyFile();
            WordFrequencyForDataFileName.Value.CreateEmptyFile();
            VocabularyFileName.Value.CreateEmptyFile();
            WordStreamFileName.Value.CreateEmptyFile();
            WordTopicArrayFileName.Value.CreateEmptyFile();
            TopicDocumentArrayFileName.Value.CreateEmptyFile();
            TopicsVectorFileName.Value.CreateEmptyFile();
            TopicsBagOfWordsFileName.Value.CreateEmptyFile();
            TopicsBagOfWordsDataFileName.Value.CreateEmptyFile();
            IONARunSettingsFileName.Value.CreateEmptyFile();
         }
         catch (Exception e)
         {
            Console.WriteLine(e);
            return false;
         }
         return true;
      }

      public string GetAllFileNamesAndTheirPathsToPrint()
      {

         var result = PrimitivesHelper.PrintOutPrimitiveAndValue(Tier, Tier.Value.ToString());
         result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(DocumentsLinksFileName, DocumentsLinksFileName.Value);
         result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(DocWordFileName, DocWordFileName.Value);
         result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(WordFrequencyFileName, WordFrequencyFileName.Value);
         result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(WordFrequencyForDataFileName, WordFrequencyForDataFileName.Value);
         result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(VocabularyFileName, VocabularyFileName.Value);
         result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(WordStreamFileName, WordStreamFileName.Value);
         result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(WordTopicArrayFileName, WordTopicArrayFileName.Value);
         result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(TopicDocumentArrayFileName, TopicDocumentArrayFileName.Value);
         result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(TopicsVectorFileName, TopicsVectorFileName.Value);
         result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(TopicsBagOfWordsFileName, TopicsBagOfWordsFileName.Value);
         result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(TopicsBagOfWordsDataFileName, TopicsBagOfWordsDataFileName.Value);
         result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(IONARunSettingsFileName, IONARunSettingsFileName.Value);
         return result;
      }
   }
}
