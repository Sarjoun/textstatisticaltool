﻿using System.Collections.Generic;
using SDParser.Extensions;
using SDParser.Primitives;

namespace SDParser.ComplexTypes
{
    public class MediaSettings
    {
        public Tier Tier { get; set; }
        public ExperimentFolderPath ExperimentFolderPath { get; set; }
        public SimulationDateFolderPath SimulationDateFolderPath { get; }
        public MediaName MediaName { get; set; }
        public MediaFolderPath MediaFolderPath { get; set; }
        public CurrentCorpusTierFolderPath CurrentCorpusTierFolderPath { get; }
        public List<string> AllTierStoryFiles { get; set; }
        public MediaLDAFileNames MediaLdaFileNames { get; set; }
        public MediaAnalysisFileNames MediaAnalysisFileNames { get; set; }
        public MediaClusterTagsFileNames MediaClusterTagsFileNames { get; set; }

        public MediaSettings(
           ExperimentFolderPath experimentFolderPath, SimulationDateFolderPath simulationDateFolderPath,
            MediaName mediaName, MediaFolderPath mediaFolderPath, Tier tier,
            CurrentCorpusTierFolderPath currentCorpusTierFolderPath)
        {
            ExperimentFolderPath = experimentFolderPath;
            SimulationDateFolderPath = simulationDateFolderPath;
            MediaName = mediaName;
            MediaFolderPath = mediaFolderPath;
            Tier = tier;
            CurrentCorpusTierFolderPath = currentCorpusTierFolderPath;

            //For the LDA
            MediaLdaFileNames = GenerateNamesForRelevantMediaFileNames(
                Tier, CurrentCorpusTierFolderPath, MediaName);
            MediaLdaFileNames.CreateEmptyFilesForAllLDAFileNames();

            //For the NGram Analysis
            MediaAnalysisFileNames = SetUpMediaAnalysisFileNames(
                Tier, CurrentCorpusTierFolderPath, MediaName);
            MediaAnalysisFileNames.WriteAllFileNamesToDisk();
        }

        public MediaLDAFileNames GenerateNamesForRelevantMediaFileNames(//used to be called SetUpMediaLdaFileNames
            Tier tier, CurrentCorpusTierFolderPath currentCorpusTierFolderPath, MediaName mediaName)
        {
            return new MediaLDAFileNames
            {
                Tier = tier,
                CurrentCorpusTierFolderPath = currentCorpusTierFolderPath,
                DocumentsLinksFileName = DocumentsLinksFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<DocumentsLinksFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                DocWordFileName = DocWordFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<DocWordFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                WordFrequencyFileName = WordFrequencyFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<WordFrequencyFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                WordFrequencyForDataFileName = WordFrequencyForDataFileName.FromValue(ParsedSimulationSettings.GenerateDataFileName<WordFrequencyFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                VocabularyFileName = VocabularyFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<VocabularyFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                WordStreamFileName = WordStreamFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<WordStreamFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                WordTopicArrayFileName = WordTopicArrayFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<WordTopicArrayFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                TopicDocumentArrayFileName = TopicDocumentArrayFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<TopicDocumentArrayFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                TopicsVectorFileName = TopicsVectorFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<TopicsVectorFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                TopicsBagOfWordsFileName = TopicsBagOfWordsFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<TopicsBagOfWordsFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                TopicsBagOfWordsDataFileName = TopicsBagOfWordsDataFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<TopicsBagOfWordsDataFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                IONARunSettingsFileName = IONARunSettingsFileName.FromValue(ParsedSimulationSettings.
                                    GenerateFileNameFromParameterList<IONARunSettingsFileName>(
                                        new List<string> {
                                            currentCorpusTierFolderPath.Value.ExtractUntilKeyWord("corpusTier"),
                                            mediaName.Value.ToString(),
                                            "Tier" + tier.Value}))
            };
        }

        public MediaAnalysisFileNames SetUpMediaAnalysisFileNames(
            Tier tier, CurrentCorpusTierFolderPath currentCorpusTierFolderPath, MediaName mediaName)
        {
            return new MediaAnalysisFileNames
            {
                Tier = tier,
                CurrentCorpusTierFolderPath = currentCorpusTierFolderPath,
                AllNGramsFileName = AllNGramsFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<AllNGramsFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                PopularNGramsFileName = PopularNGramsFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<PopularNGramsFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                PopularNGramsSimilarityMatrixFileName = PopularNGramsSimilarityMatrixFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<PopularNGramsSimilarityMatrixFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                InitialAgentCosineSimilarityMatrixFileName = InitialAgentCosineSimilarityMatrixFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<InitialAgentCosineSimilarityMatrixFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                ClusteredNGramMergeResultFileName = ClusteredNGramMergeResultFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<ClusteredNGramMergeResultFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                FirstMergeOfAgentsFileName = FirstMergeOfAgentsFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<FirstMergeOfAgentsFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                FirstMergeTaggedDocIDsFileName = FirstMergeTaggedDocIDsFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<FirstMergeTaggedDocIDsFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                FirstMergeOfAgentsGraphFileName = FirstMergeOfAgentsGraphFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<FirstMergeOfAgentsGraphFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                SecondMergeOfAgentsFileName = SecondMergeOfAgentsFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<SecondMergeOfAgentsFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                SecondMergeOfAgentsGraphFileName = SecondMergeOfAgentsGraphFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<SecondMergeOfAgentsGraphFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                SecondMergeTaggedDocIdsFileName = SecondMergeTaggedDocIdsFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<SecondMergeTaggedDocIdsFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                TaggedDocumentsSubCorporaFileName = TaggedDocumentsSubCorporaFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<TaggedDocumentsSubCorporaFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() })),
                UnTaggedDocumentsSubCorporaFileName = UnTaggedDocumentsSubCorporaFileName.FromValue(ParsedSimulationSettings.GenerateFileNameFromParameterList<UnTaggedDocumentsSubCorporaFileName>(new List<string> { currentCorpusTierFolderPath.Value, mediaName.Value.ToString() }))
            };
        }
    }
}
