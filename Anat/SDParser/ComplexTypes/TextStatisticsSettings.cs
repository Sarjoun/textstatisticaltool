﻿using SDParser.Primitives;

namespace SDParser.ComplexTypes
{
    public class TextStatisticsSettings
    {
        public RunStatistics RunStatistics { get; set; }
        public IncludeTitleInSetup IncludeTitleInSetup { get; set; }
        public StatisticsSources StatisticsSources { get; set; }
        public StatisticsTarget StatisticsTarget { get; set; }
        public ReductionInNetworkEdgeValue ReductionInNetworkEdgeValue { get; set; }
        public RunNetworkStatistics RunNetworkStatistics { get; set; }
        public ReductionInWordFrequency ReductionInWordFrequency { get; set; }
        public RemoveLowWordFrequencyValue RemoveLowWordFrequencyValue { get; set; }
        public ReductionOfNetworkEdgesWeight ReductionOfNetworkEdgesWeight { get; set; }
        public TextPartioningAlgorithm TextPartioningAlgorithm { get; set; }
        public SplitTheSentences SplitTheSentences { get; set; }
        public HorizonSize HorizonSize { get; set; }
        public UseStemmingInTextStatistics UseStemmingInTextStatistics { get; set; }
        public CalculateConditionalProbability CalculateConditionalProbability { get; set; }
        public CalculatePointWiseMutualInformation CalculatePointWiseMutualInformation { get; set; }
        public CalculateCorrelationCoefficient CalculateCorrelationCoefficient { get; set; }
    }
}
