﻿using SDParser.Primitives;

namespace SDParser.ComplexTypes
{
    public class MediaClusterTagsFileNames : IComplexTypeILDA
    {
        public CurrentTagId CurrentTagId { get; set; }
        public ClusteredTagsTemplateFileName ClusteredTagsTemplateFileName { get; set; }

        public MediaClusterTagsFileNames(CurrentTagId currentTagId,
            ClusteredTagsTemplateFileName clusteredTagsTemplateFileName)
        {
            CurrentTagId = currentTagId;
            ClusteredTagsTemplateFileName = clusteredTagsTemplateFileName;
        }

        public string GetAllFileNamesAndTheirPathsToPrint()
        {
            var result = "";
            if (CurrentTagId != null)
            {
                result += PrimitivesHelper.PrintOutPrimitiveAndValue(CurrentTagId, CurrentTagId.Value.ToString());
            }
            else
            {
                result += "CurrentTagId=CurrentTagId is not set yet!";
            }

            if (CurrentTagId != null)
            {
                result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(ClusteredTagsTemplateFileName, ClusteredTagsTemplateFileName.Value);
            }
            else
            {
                result += "ClusteredTagsTemplateFileName=ClusteredTagsTemplateFileName is not set yet!";
            }
            return result;
        }
    }
}
