using System;
using SDParser.Extensions;
using SDParser.Notifications;
using SDParser.Primitives;

namespace SDParser.ComplexTypes
{
    public class MediaAnalysisFileNames : IComplexTypeILDA
    {
        public Tier Tier { get; set; }
        public CurrentCorpusTierFolderPath CurrentCorpusTierFolderPath { get; set; }
        public AllNGramsFileName AllNGramsFileName { get; set; }
        public PopularNGramsFileName PopularNGramsFileName { get; set; }
        public PopularNGramsSimilarityMatrixFileName PopularNGramsSimilarityMatrixFileName { get; set; }
        public InitialAgentCosineSimilarityMatrixFileName InitialAgentCosineSimilarityMatrixFileName { get; set; }
        public ClusteredNGramMergeResultFileName ClusteredNGramMergeResultFileName { get; set; }
        public FirstMergeTaggedDocIDsFileName FirstMergeTaggedDocIDsFileName { get; set; }
        public FirstMergeOfAgentsFileName FirstMergeOfAgentsFileName { get; set; }
        public FirstMergeOfAgentsGraphFileName FirstMergeOfAgentsGraphFileName { get; set; }
        public SecondMergeOfAgentsFileName SecondMergeOfAgentsFileName { get; set; }
        public SecondMergeOfAgentsGraphFileName SecondMergeOfAgentsGraphFileName { get; set; }
        public SecondMergeTaggedDocIdsFileName SecondMergeTaggedDocIdsFileName { get; set; }
        public TaggedDocumentsSubCorporaFileName TaggedDocumentsSubCorporaFileName { get; set; }
        public UnTaggedDocumentsSubCorporaFileName UnTaggedDocumentsSubCorporaFileName { get; set; }

        public bool WriteAllFileNamesToDisk()
        {
            try
            {
                AllNGramsFileName.Value.CreateEmptyFile();
                PopularNGramsFileName.Value.CreateEmptyFile();
                PopularNGramsSimilarityMatrixFileName.Value.CreateEmptyFile();
                TaggedDocumentsSubCorporaFileName.Value.CreateEmptyFile();
                UnTaggedDocumentsSubCorporaFileName.Value.CreateEmptyFile();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        public string GetAllFileNamesAndTheirPathsToPrint()
        {
            try
            {
                var result = PrimitivesHelper.PrintOutPrimitiveAndValue(CurrentCorpusTierFolderPath, CurrentCorpusTierFolderPath.Value);
                result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(AllNGramsFileName, AllNGramsFileName.Value);
                result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(PopularNGramsFileName, PopularNGramsFileName.Value);
                result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(PopularNGramsSimilarityMatrixFileName, PopularNGramsSimilarityMatrixFileName.Value);
                result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(InitialAgentCosineSimilarityMatrixFileName, InitialAgentCosineSimilarityMatrixFileName.Value);
                result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(ClusteredNGramMergeResultFileName, ClusteredNGramMergeResultFileName.Value);
                result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(FirstMergeOfAgentsFileName, FirstMergeOfAgentsFileName.Value);
                result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(FirstMergeTaggedDocIDsFileName, FirstMergeTaggedDocIDsFileName.Value);
                result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(FirstMergeOfAgentsGraphFileName, FirstMergeOfAgentsGraphFileName.Value);
                result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(SecondMergeOfAgentsFileName, SecondMergeOfAgentsFileName.Value); 
                result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(SecondMergeOfAgentsGraphFileName, SecondMergeOfAgentsGraphFileName.Value);
                result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(SecondMergeTaggedDocIdsFileName, SecondMergeTaggedDocIdsFileName.Value);
                result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(TaggedDocumentsSubCorporaFileName, TaggedDocumentsSubCorporaFileName.Value);
                result += "\r\n" + PrimitivesHelper.PrintOutPrimitiveAndValue(UnTaggedDocumentsSubCorporaFileName, UnTaggedDocumentsSubCorporaFileName.Value);
                return result;
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit("Error while writing file names in method "+
                    "GetAllFileNamesAndTheirPathsToPrint in the MediaAnalysisFileName", e);
                Console.WriteLine(e);
                return"";
            }
        }
    }
}