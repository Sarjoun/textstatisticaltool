using System.Collections.Generic;
using System.Linq;
using SDParser.Primitives;

namespace SDParser
{
    public class ParsedSimulationSettings
    {
        private const string DateFormat = "MMddyyyy";
        public SimulationSettingsFile SimulationSettingsFile { get; set; }
        public UntaggedDocsFromPreviousTier UntaggedDocsFromPreviousTier { get; set; }
        public Tier Tier { get; set; }
        public StartDate StartDate { get; set; }
        public FinishDate FinishDate { get; set; }
        public MediaName MediaName { get; set; }
        public RandomSeed RandomSeed { get; set; }
        public NumberOfGibbsIterations NumberOfGibbsIterations { get; set; }
        public NumberOfInitialTopicsLDA NumberOfInitialTopicsLDA { get; set; }
        public NumberOfTopWordsToUseFromBoWofTopic NumberOfTopWordsToUseFromBoWofTopic { get; set; }
        public NumberOfWordsInStartNGRAM NumberOfWordsInStartNGRAM { get; set; }
        public NumberOfLDAIterations NumberOfLDAIterations { get; set; }
        public NGramsThresholdValue NGramsThresholdValue { get; set; }
        public MinimumWordOccurenceForVocabularyEntry MinimumWordOccurenceForVocabularyEntry { get; set; }
        public IncludeTitleInSetup IncludeTitleInSetup { get; set; }
        public IncludeTitleInTagging IncludeTitleInTagging { get; set; }
        public OrderTopics OrderTopics { get; set; }
        public WriteAllTopicsWordDistribution WriteAllTopicsWordDistribution { get; set; }
        public RunLDA RunLDA { get; set; }
        public RunNGramAnalysis RunNGramAnalysis { get; set; }
        public UseLocalMediaVocabulary UseLocalMediaVocabulary { get; set; }
        public CreateCoOccurrenceMatrix CreateCoOccurrenceMatrix { get; set; }
        public OutputFolder OutputFolder { get; set; }
        public RootFolderPath RootFolderPath { get; set; }
        public DatabaseRootFolderPath DatabaseRootFolderPath { get; set; }
        public StudyNumber StudyNumber { get; set; }
        public DbScanBallRadius DbScanBallRadius { get; set; }
        public DbScanMinPointsInRegion DbScanMinPointsInRegion { get; set; }
        public InterNGramMergingThreshold InterNGramMergingThreshold { get; set; }
        public DocNGramTaggingThreshold DocNGramTaggingThreshold { get; set; }
        //Text statistics
        public RunStatistics RunStatistics { get; set; }
        public StatisticsSources StatisticsSources { get; set; }
        public StatisticsTarget StatisticsTarget { get; set; }
        public ReductionInNetworkEdgeValue ReductionInNetworkEdgeValue { get; set; }
        public RunNetworkStatistics RunNetworkStatistics { get; set; }
        public ReductionInWordFrequency ReductionInWordFrequency { get; set; }
        public RemoveLowWordFrequencyValue RemoveLowWordFrequencyValue { get; set; }
        public ReductionOfNetworkEdgesWeight ReductionOfNetworkEdgesWeight { get; set; }
        public TextPartioningAlgorithm TextPartioningAlgorithm { get; set; }
        public SplitTheSentences SplitTheSentences { get; set; }
        public HorizonSize HorizonSize { get; set; }
        public UseStemmingInTextStatistics UseStemmingInTextStatistics { get; set; }
        public CalculateConditionalProbability CalculateConditionalProbability { get; set; }
        public CalculatePointWiseMutualInformation CalculatePointWiseMutualInformation { get; set; }
        public CalculateCorrelationCoefficient CalculateCorrelationCoefficient { get; set; }

        public IEnumerable<string> FileContents { get; set; }
        public static string GenerateFileNameFromParameterList<T>(List<string> parameterList)
        {
            var result = parameterList[0] + typeof(T).Name.Replace("FileName", "");
            result = parameterList.Skip(1).Aggregate(result, (current, param) => current + ("-" + param));
            result += Return_DAT_or_TXT_forFileExtension<T>(result);
            return result;
        }

        public static string Return_DAT_or_TXT_forFileExtension<T>(string fileName)
        {
            string[] keywordsForFileExtensionDat = { "matrix", "someothertext", "_DAT" };
            return keywordsForFileExtensionDat.Any(typeof(T).Name.ToLower().Contains)
                ? @".dat"
                : @".txt";
        }

        public static string GenerateDataFileName<T>(List<string> parameters)
        {
            var result = parameters[0] + typeof(T).Name.Replace("FileName", "");
            result = parameters.Skip(1).Aggregate(result, (current, param) => current + ("-" + param));
            result += @".dat";
            return result;
        }

        public IEnumerable<string> GetTextVersionForFilePrintOut()
        {
            var result = new List<string>
            {
                "Tier=" + Tier.Value,
                "StartDate=" + StartDate.Value.ToString(DateFormat),
                "FinishDate=" + FinishDate.Value.ToString(DateFormat),
                "MediaName=" + MediaName.Value,
                "RandomSeed=" + RandomSeed.Value,
                "NumberOfGibbsIterations=" + NumberOfGibbsIterations.Value,
                "NumberOfInitialTopicsLDA=" + NumberOfInitialTopicsLDA.Value,
                "NumberOfTopWordsToUseFromBoWofTopic=" + NumberOfTopWordsToUseFromBoWofTopic.Value,
                "NumberOfWordsInStartNGRAM=" + NumberOfWordsInStartNGRAM.Value,
                "NumberOfLDAIterations=" + NumberOfLDAIterations.Value,
                "NGramsThresholdValue=" + NGramsThresholdValue.Value,
                "MinimumWordOccurenceForVocabularyEntry=" + MinimumWordOccurenceForVocabularyEntry.Value,
                "IncludeTitleInSetup=" + IncludeTitleInSetup.Value,
                "IncludeTitleInTagging=" + IncludeTitleInTagging.Value,
                "OrderTopics=" + OrderTopics.Value,
                "WriteAllTopicsWordDistribution=" + WriteAllTopicsWordDistribution.Value,
                "RunLDA=" + RunLDA.Value,
                "RunNGramAnalysis=" + RunNGramAnalysis.Value,
                "UseLocalMediaVocabulary=" + UseLocalMediaVocabulary.Value,
                "CreateCoOccurrenceMatrix=" + CreateCoOccurrenceMatrix.Value,
                "OutputFolder=" + OutputFolder.Value,
                "RootFolderPath=" + RootFolderPath.Value,
                "DatabaseRootFolderPath=" + DatabaseRootFolderPath.Value,
                "SimulationSettingsFile=" + SimulationSettingsFile.Value,
                "UntaggedDocsFromPreviousTier=" + UntaggedDocsFromPreviousTier.Value,
                "StudyNumber="+StudyNumber.Value,
                "DbScanBallRadius="+DbScanBallRadius.Value,
                "DbScanMinPointsInRegion="+DbScanMinPointsInRegion.Value,
                "InterNGramMergingThreshold="+InterNGramMergingThreshold.Value,
                "DocNGramTaggingThreshold="+DocNGramTaggingThreshold.Value,
                "RunStatistics="+RunStatistics.Value,
                "StatisticsSources="+StatisticsSources.Value,
                "StatisticsTarget="+StatisticsTarget.Value,
                "ReductionInNetworkEdgeValue=" +ReductionInNetworkEdgeValue.Value,
                "RunNetworkStatistics=" +RunNetworkStatistics.Value,
                "ReductionInWordFrequency=" +ReductionInWordFrequency.Value,
                "RemoveLowWordFrequencyValue=" +RemoveLowWordFrequencyValue.Value,
                "ReductionOfNetworkEdgesWeight=" +ReductionOfNetworkEdgesWeight.Value,
                "TextPartioningAlgorithm="+TextPartioningAlgorithm.Value,
                "SplitTheSentences="+SplitTheSentences.Value,
                "HorizonSize="+HorizonSize.Value,
                "UseStemmingInTextStatistics="+UseStemmingInTextStatistics.Value,
                "CalculateConditionalProbability="+CalculateConditionalProbability.Value,
                "CalculatePointWiseMutualInformation="+CalculatePointWiseMutualInformation.Value,
                "CalculateCorrelationCoefficient="+CalculateCorrelationCoefficient.Value
            };
            FileContents = result.ToArray();
            return FileContents;
        }

        public string MissingSettingName { get; set; }

        public bool CheckForMissingSettingInSimulationFile()
        {
            //make sure that no values are missing, i.e. have null values 
            //TODO: get list of all missing settings and add smart checks that validate the values
            if (Tier == null) { MissingSettingName = "Tier"; return false; }
            if (StartDate == null) { MissingSettingName = "StartDate"; return false; }
            if (FinishDate == null) { MissingSettingName = "FinishDate"; return false; }
            if (MediaName == null) { MissingSettingName = "MediaName"; return false; }
            if (RandomSeed == null) { MissingSettingName = "RandomSeed"; return false; }
            if (NumberOfGibbsIterations == null) { MissingSettingName = "NumberOfGibbsIterations"; return false; }
            if (NumberOfInitialTopicsLDA == null) { MissingSettingName = "NumberOfInitialTopicsLDA"; return false; }
            if (NumberOfTopWordsToUseFromBoWofTopic == null) { MissingSettingName = "NumberOfTopWordsToUseFromBoWofTopic"; return false; }
            if (NumberOfWordsInStartNGRAM == null) { MissingSettingName = "NumberOfWordsInStartNGRAM"; return false; }
            if (NumberOfLDAIterations == null) { MissingSettingName = "NumberOfLDAIterations"; return false; }
            if (NGramsThresholdValue == null) { MissingSettingName = "NGramsThresholdValue"; return false; }
            if (MinimumWordOccurenceForVocabularyEntry == null) { MissingSettingName = "MinimumWordOccurenceForVocabularyEntry"; return false; }
            if (IncludeTitleInSetup == null) { MissingSettingName = "IncludeTitleInSetup"; return false; }
            if (IncludeTitleInTagging == null) { MissingSettingName = "IncludeTitleInTagging"; return false; }
            if (OrderTopics == null) { MissingSettingName = "OrderTopics"; return false; }
            if (WriteAllTopicsWordDistribution == null) { MissingSettingName = "WriteAllTopicsWordDistribution"; return false; }
            if (RunLDA == null) { MissingSettingName = "RunLDA"; return false; }
            if (RunNGramAnalysis == null) { MissingSettingName = "RunNGramAnalysis"; return false; }
            if (UseLocalMediaVocabulary == null) { MissingSettingName = "UseLocalMediaVocabulary"; return false; }
            if (CreateCoOccurrenceMatrix == null) { MissingSettingName = "CreateCoOccurrenceMatrix"; return false; }
            if (OutputFolder == null) { MissingSettingName = "OutputFolder"; return false; }
            if (RootFolderPath == null) { MissingSettingName = "RootFolderPath"; return false; }
            if (DatabaseRootFolderPath == null) { MissingSettingName = "DatabaseRootFolderPath"; return false; }
            if (SimulationSettingsFile == null) { MissingSettingName = "SimulationSettingsFile"; return false; }
            if (UntaggedDocsFromPreviousTier == null) { MissingSettingName = "UntaggedDocsFromPreviousTier"; return false; }
            if (StudyNumber == null) { MissingSettingName = "StudyNumber"; return false; }
            if (DbScanBallRadius == null) { MissingSettingName = "DbScanBallRadius"; return false; }
            if (DbScanMinPointsInRegion == null) { MissingSettingName = "DbScanMinPointsInRegion"; return false; }
            if (InterNGramMergingThreshold == null) { MissingSettingName = "InterNGramMergingThreshold"; return false; }
            if (DocNGramTaggingThreshold == null) { MissingSettingName = "DocNGramTaggingThreshold"; return false; }
            if (RunStatistics == null) { MissingSettingName = "RunStatistics"; return false; }
            if (StatisticsSources == null) { MissingSettingName = "StatisticsSources"; return false; }
            if (StatisticsTarget == null) { MissingSettingName = "StatisticsTarget"; return false; }
            if (ReductionInNetworkEdgeValue == null) { MissingSettingName = "ReductionInNetworkEdgeValue"; return false; }
            if (RunNetworkStatistics == null) { MissingSettingName = "RunNetworkStatistics"; return false; }
            if (ReductionInWordFrequency == null) { MissingSettingName = "ReductionInWordFrequency"; return false; }
            if (RemoveLowWordFrequencyValue == null) { MissingSettingName = "RemoveLowWordFrequencyValue"; return false; }
            if (ReductionOfNetworkEdgesWeight == null) { MissingSettingName = "ReductionOfNetworkEdgesWeight"; return false; }
            if (TextPartioningAlgorithm == null) { MissingSettingName = "TextPartioningAlgorithm"; return false; }
            if (SplitTheSentences == null) { MissingSettingName = "SplitTheSentences"; return false; }
            if (HorizonSize == null) { MissingSettingName = "HorizonSize"; return false; }
            if (UseStemmingInTextStatistics == null) { MissingSettingName = "UseStemmingInTextStatistics"; return false; }
            if (CalculateConditionalProbability == null) { MissingSettingName = "CalculateConditionalProbability"; return false; }
            if (CalculatePointWiseMutualInformation == null) { MissingSettingName = "CalculatePointWiseMutualInformation"; return false; }
            if (CalculateCorrelationCoefficient == null) { MissingSettingName = "CalculateCorrelationCoefficient"; return false; }
            return true;
        }
    }
}