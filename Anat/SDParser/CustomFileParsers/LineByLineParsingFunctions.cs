﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SDParser.CustomFileParsers
{
    public static class LineByLineParsingFunctions
    {
        public static async Task<IEnumerable<string>> ReadLinesAsync(string fileName)
        {
            using (var reader = File.OpenText(fileName))
            {
                var fileText = await reader.ReadToEndAsync();
                return fileText.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }
        public static IEnumerable<string> ReadLines(string path)
        {
            using (var reader = File.OpenText(path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    yield return line.Trim();
                }
            }
        }
    }
}
