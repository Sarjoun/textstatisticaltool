﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SDParser.Extensions;
using SDParser.Primitives;

namespace SDParser.CustomFileParsers
{
    public static class CreateTaggedDocumentsFileFromOriginalAndUntagged
    {
        public static async Task<IEnumerable<string>> GetResultingTaggedDocumentsFileAsync(
            DocumentsLinksFileName originalDocumentsLinksFileName, 
            string remainingUntaggedDocuments)
        {
            var originalDocumentsLinks = await LineByLineParsingFunctions.ReadLinesAsync(originalDocumentsLinksFileName.Value);
            var originalDocumentsLinksList = originalDocumentsLinks.Select(x => x.RemoveInitialIndexTag()).ToList();

            var untaggedDocumentsLinks = await LineByLineParsingFunctions.ReadLinesAsync(remainingUntaggedDocuments);
            var untaggedDocumentsLinksList = untaggedDocumentsLinks.ToList();
            
            return originalDocumentsLinksList.Except(untaggedDocumentsLinksList);
        }
    }
}
