﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SDParser.Extensions;
using SDParser.Primitives;
using SDParser.Structures;

namespace SDParser.CustomFileParsers
{
    public static class DocWordFileParser
    {
        public static Dictionary<int, DocWord> DocWordDictionary { get; set; }

        public static async Task<Dictionary<int, DocWord>> GetDocWordsDictionaryAsync(
            DocWordFileName docWordFileName)
        {
            DocWordDictionary = new Dictionary<int, DocWord>();
            var streamReader = new StreamReader(docWordFileName, Encoding.UTF8);
            string line;

            for (var i = 1; i <= 3; i++)
                streamReader.ReadLine();

            while ((line = await streamReader.ReadLineAsync()) != null)
            {
                var docId = GetDocIdFromLine(line);

                if (DocWordDictionary.ContainsKey(docId))
                {
                    DocWordDictionary[GetDocIdFromLine(line)].
                        WordFrequencies.
                        Add(line.
                            DocWordFileLineEntryFromLine().
                            GetWordFrequency);
                }
                else
                {
                    DocWordDictionary.Add(
                        docId,
                        new DocWord(
                            docId,
                            line.DocWordFileLineEntryFromLine().GetWordFrequency
                        ));
                }
            }
            return DocWordDictionary;
        }

        public static int GetDocIdFromLine(string line)
        {
            return Convert.ToInt32(
                line.Split(
                    new[] { ' ', '\t' },
                    StringSplitOptions.RemoveEmptyEntries)
                    [0]);
        }
        
        public static bool StartingNewDocCheck(string line)
        {
            return DocWordDictionary.ContainsKey(
                Convert.ToInt32(
                    line.Split(
                        new[] { ' ', '\t' },
                        StringSplitOptions.RemoveEmptyEntries)
                        [0]));
        }

        //var streamReader = new StreamReader(docFilePath, Encoding.UTF8);
        //string line;
        //    while ((line = await streamReader.ReadLineAsync()) != null)
        //{
        //    fileContents += line + " ";
        //    fileContentsMinusTitle += line;
        //}


        public static async Task<Dictionary<string, StemWord>> GetDocWordsDictionaryAsyncOld(
                DocWordFileName docWordFileName)
        {
            var resultDictionary = new Dictionary<string, StemWord>();
            using (var reader = File.OpenText(docWordFileName.Value))
            {
                var fileText = await reader.ReadToEndAsync();
                var result = fileText.Split(
                    new[] { Environment.NewLine },
                    StringSplitOptions.None).ToArray();

                foreach (var entry in result)
                {
                    if (!string.IsNullOrEmpty(entry))
                        ((ICollection<KeyValuePair<string, StemWord>>)resultDictionary).
                            Add(
                                entry.
                                    Split(new[] { ' ', '\t' },
                                        StringSplitOptions.RemoveEmptyEntries).
                                    GetStemWordDictionaryKeyValuePairFromLine());
                }
            }
            return resultDictionary;
        }

        public static Dictionary<string, StemWord> GetStemWordsDictionaryFromFile(
            WordFrequencyFileName wordFrequencyFileName)
        {
            var resultDictionary = new Dictionary<string, StemWord>();

            var result = LineByLineParsingFunctions.
                ReadLinesAsync(wordFrequencyFileName.Value).
                Result.
                ToArray().
                Select(x => x.GetDictionaryKeyValuePairFromWordFrequencyFileLine());

            foreach (var entry in result)
            {
                ((ICollection<KeyValuePair<string, StemWord>>)resultDictionary).Add(entry);
            }
            return resultDictionary;
        }
    }
}
