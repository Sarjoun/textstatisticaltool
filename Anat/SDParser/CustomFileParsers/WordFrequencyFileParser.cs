﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SDParser.Extensions;
using SDParser.Primitives;
using SDParser.Structures;

namespace SDParser.CustomFileParsers
{
    public static class WordFrequencyFileParser
    {
        public static async Task<Dictionary<string, StemWord>> GetStemWordsDictionaryAsync(
            WordFrequencyFileName wordFrequencyFileName)
        {
            var resultDictionary = new Dictionary<string, StemWord>();
            using (var reader = File.OpenText(wordFrequencyFileName.Value))
            {
                var fileText = await reader.ReadToEndAsync();
                var result = fileText.Split(
                    new[] { Environment.NewLine },
                    StringSplitOptions.None).ToArray();

                foreach (var entry in result)
                {
                    if (!string.IsNullOrEmpty(entry))
                        ((ICollection<KeyValuePair<string, StemWord>>)resultDictionary).
                            Add(
                                entry.
                                    Split(new[] { ' ', '\t' },
                                        StringSplitOptions.RemoveEmptyEntries).
                                    GetStemWordDictionaryKeyValuePairFromLine());
                }
            }
            return resultDictionary;
        }

        public static Dictionary<string, StemWord> GetStemWordsDictionaryFromFile(
            WordFrequencyFileName wordFrequencyFileName)
        {
            var resultDictionary = new Dictionary<string, StemWord>();

            var result = LineByLineParsingFunctions.
                ReadLinesAsync(wordFrequencyFileName.Value).
                Result.
                ToArray().
                Select(x => x.GetDictionaryKeyValuePairFromWordFrequencyFileLine());

            foreach (var entry in result)
            {
                ((ICollection<KeyValuePair<string, StemWord>>)resultDictionary).Add(entry);
            }
            return resultDictionary;
        }
    }
}
