﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using SDParser.Primitives;
using SDParser.Extensions;
using SDParser.ComplexTypes;
using SDParser.Notifications;
using SDParser.FoldersManagement;

namespace SDParser
{
   public static class MainParserForSettingsFile
   {
      private const string LdaMarkerLimit = "## IONA simulation settings section:";
      public static IEnumerable<string> ReadLines(string path)
      {
         using (var reader = File.OpenText(path))
         {
            string line;
            while ((line = reader.ReadLine()) != null)
            {
               yield return line.Trim();
            }
         }
      }
      public static async Task<ParsedSimulationSettings> GetLDASettingsFromFile(CurrentSimulationSettingsFileName fileName)
      {
         return await Task.FromResult(ProcessAllSimulationSettingsValues(fileName));
      }
      public static string[] OnlyExtractLDASettingsValueFromFile(string[] lines, string lDAcutOff)
      {
         var keyIndex = Array.FindIndex(lines, w => w == lDAcutOff);
         return keyIndex == -1
             ? lines
             : lines.Take(keyIndex).ToArray();/*lines.Take(lines.Length - keyIndex).ToArray();*/
      }

      //Remember to add the new settings to both ldaSettingsValues and generalSettingsForIONA
      public static ParsedSimulationSettings ProcessAllSimulationSettingsValues(CurrentSimulationSettingsFileName fileName)
      {
         var data = OnlyExtractLDASettingsValueFromFile(File.ReadLines(fileName).ToArray(), LdaMarkerLimit);

         Array.Resize(ref data, data.Length + 1);
         data[data.Length - 1] = "SimulationSettingsFile=" + fileName;

         var ldaSettingsValues = new ParsedSimulationSettings
         {
            SimulationSettingsFile = SimulationSettingsFile.FromValue(fileName),
            FileContents = data
         };

         try
         {
            foreach (var line in data)
            {
               switch (line.ExtractLDAToken())
               {
                  case "Tier":
                     ldaSettingsValues.Tier = PrimitivesExtractor.GetTier(line.ExtractLDATokenValue());
                     break;
                  case "StartDate":
                     ldaSettingsValues.StartDate = PrimitivesExtractor.GetStartDate(line.ExtractLDATokenValue());
                     break;
                  case "FinishDate":
                     ldaSettingsValues.FinishDate = PrimitivesExtractor.GetFinishDate(line.ExtractLDATokenValue());
                     break;
                  case "MediaName":
                     ldaSettingsValues.MediaName = PrimitivesExtractor.GetClosestMediaNameFromSimulationMedia(line.ExtractLDATokenValue());
                     break;
                  case "RandomSeed":
                     ldaSettingsValues.RandomSeed = PrimitivesExtractor.GetRandomSeed(line.ExtractLDATokenValue());
                     break;
                  case "NumberOfGibbsIterations":
                     ldaSettingsValues.NumberOfGibbsIterations =
                         PrimitivesExtractor.GetNumberOfGibbsIterations(line.ExtractLDATokenValue());
                     break;
                  case "NumberOfInitialTopicsLDA":
                     ldaSettingsValues.NumberOfInitialTopicsLDA = PrimitivesExtractor.GetNumberOfInitialTopicsLDA(line.ExtractLDATokenValue());
                     break;
                  case "NumberOfTopWordsToUseFromBoWofTopic":
                     ldaSettingsValues.NumberOfTopWordsToUseFromBoWofTopic = PrimitivesExtractor.GetNumberOfTopWordsToUseFromBOWOfTopic(line.ExtractLDATokenValue());
                     break;
                  case "NumberOfWordsInStartNGRAM":
                     ldaSettingsValues.NumberOfWordsInStartNGRAM = PrimitivesExtractor.GetNumberOfWordsInStartNGRAM(line.ExtractLDATokenValue());
                     break;
                  case "NumberOfLDAIterations":
                     ldaSettingsValues.NumberOfLDAIterations = PrimitivesExtractor.GetNumberOfLDAIterations(line.ExtractLDATokenValue());
                     break;
                  case "NGramsThresholdValue":
                     ldaSettingsValues.NGramsThresholdValue = PrimitivesExtractor.GetNGramsThresholdValue(line.ExtractLDATokenValue());
                     break;
                  case "MinimumWordOccurenceForVocabularyEntry":
                     ldaSettingsValues.MinimumWordOccurenceForVocabularyEntry = PrimitivesExtractor.GetMinimumWordOccurenceForVocabularyEntry(line.ExtractLDATokenValue());
                     break;
                  case "IncludeTitleInSetup":
                     ldaSettingsValues.IncludeTitleInSetup = PrimitivesExtractor.GetIncludeTitleInSetup(line.ExtractLDATokenValue());
                     break;
                  case "OrderTopics":
                     ldaSettingsValues.OrderTopics = PrimitivesExtractor.GetOrderTopicsValue(line.ExtractLDATokenValue());
                     break;
                  case "WriteAllTopicsWordDistribution":
                     ldaSettingsValues.WriteAllTopicsWordDistribution = PrimitivesExtractor.GetWriteAllTopicsWordDistribution(line.ExtractLDATokenValue());
                     break;
                  case "IncludeTitleInTagging":
                     ldaSettingsValues.IncludeTitleInTagging = PrimitivesExtractor.GetIncludeTitleInTagging(line.ExtractLDATokenValue());
                     break;
                  case "RunLDA":
                     ldaSettingsValues.RunLDA = PrimitivesExtractor.GetRunLDA(line.ExtractLDATokenValue());
                     break;
                  case "RunNGramAnalysis":
                     ldaSettingsValues.RunNGramAnalysis = PrimitivesExtractor.GetRunNGramAnalysis(line.ExtractLDATokenValue());
                     break;
                  case "UseLocalMediaVocabulary":
                     ldaSettingsValues.UseLocalMediaVocabulary = PrimitivesExtractor.GetUseLocalMediaVocabulary(line.ExtractLDATokenValue());
                     break;
                  case "CreateCoOccurrenceMatrix":
                     ldaSettingsValues.CreateCoOccurrenceMatrix = PrimitivesExtractor.GetCreateCoOccurrenceMatrix(line.ExtractLDATokenValue());
                     break;
                  case "OutputFolder":
                     var resultOutputFolder = line.ExtractLDATokenValue();
                     if (string.IsNullOrEmpty(resultOutputFolder))
                        ldaSettingsValues.OutputFolder = OutputFolder.
                            FromValue(FolderManager.GetRootFolderPath().Value);
                     else if (resultOutputFolder.ToLower() == "desktop")
                        ldaSettingsValues.OutputFolder = OutputFolder.
                            FromValue(FolderManager.GetDesktopFolderPath().Value);
                     else
                        ldaSettingsValues.OutputFolder = PrimitivesExtractor.GetOutputFolder(line.ExtractLDATokenValue());
                     break;
                  case "RootFolderPath":
                     ldaSettingsValues.RootFolderPath = PrimitivesExtractor.GetRootFolder(line.ExtractLDATokenValue());
                     break;
                  case "DatabaseRootFolderPath":
                     ldaSettingsValues.DatabaseRootFolderPath = PrimitivesExtractor.GetDatabaseRootFolder(line.ExtractLDATokenValue());
                     break;
                  case "StudyNumber":
                     ldaSettingsValues.StudyNumber = PrimitivesExtractor.GetStudyNumber(line.ExtractLDATokenValue());
                     break;
                  case "UntaggedDocsFromPreviousTier":
                     ldaSettingsValues.UntaggedDocsFromPreviousTier = PrimitivesExtractor.GetUntaggedDocsFromPreviousTier(line.ExtractLDATokenValue());
                     break;
                  case "DbScanBallRadius":
                     ldaSettingsValues.DbScanBallRadius = PrimitivesExtractor.GetDbScanBallRadius(line.ExtractLDATokenValue());
                     break;
                  case "DbScanMinPointsInRegion":
                     ldaSettingsValues.DbScanMinPointsInRegion = PrimitivesExtractor.GetDbScanMinPointsInRegion(line.ExtractLDATokenValue());
                     break;
                  case "InterNGramMergingThreshold":
                     ldaSettingsValues.InterNGramMergingThreshold = PrimitivesExtractor.GetInterNGramMergingThreshold(line.ExtractLDATokenValue());
                     break;
                  case "DocNGramTaggingThreshold":
                     ldaSettingsValues.DocNGramTaggingThreshold = PrimitivesExtractor.GetDocNGramTaggingThreshold(line.ExtractLDATokenValue());
                     break;
                  case "RunStatistics": ldaSettingsValues.RunStatistics = PrimitivesExtractor.GetRunStatistics(line.ExtractLDATokenValue()); break;
                  case "StatisticsSources": ldaSettingsValues.StatisticsSources = PrimitivesExtractor.GetStatisticsSources(line.ExtractLDATokenValue()); break;
                  case "StatisticsTarget": ldaSettingsValues.StatisticsTarget = PrimitivesExtractor.GetStatisticsTarget(line.ExtractLDATokenValue()); break;
                  case "ReductionInNetworkEdgeValue": ldaSettingsValues.ReductionInNetworkEdgeValue = PrimitivesExtractor.GetReductionInNetworkEdgeValue(line.ExtractLDATokenValue()); break;
                  case "RunNetworkStatistics": ldaSettingsValues.RunNetworkStatistics = PrimitivesExtractor.GetRunNetworkStatistics(line.ExtractLDATokenValue()); break;
                  case "ReductionInWordFrequency": ldaSettingsValues.ReductionInWordFrequency = PrimitivesExtractor.GetReductionInWordFrequency(line.ExtractLDATokenValue()); break;
                  case "RemoveLowWordFrequencyValue": ldaSettingsValues.RemoveLowWordFrequencyValue = PrimitivesExtractor.GetRemoveLowWordFrequencyValue(line.ExtractLDATokenValue()); break;
                  case "ReductionOfNetworkEdgesWeight": ldaSettingsValues.ReductionOfNetworkEdgesWeight = PrimitivesExtractor.GetReductionOfNetworkEdgesWeight(line.ExtractLDATokenValue()); break;
                  case "TextPartioningAlgorithm": ldaSettingsValues.TextPartioningAlgorithm = PrimitivesExtractor.GetTextPartioningAlgorithm(line.ExtractLDATokenValue()); break;
                  case "SplitTheSentences": ldaSettingsValues.SplitTheSentences = PrimitivesExtractor.GetSplitTheSentences(line.ExtractLDATokenValue()); break;
                  case "HorizonSize": ldaSettingsValues.HorizonSize = PrimitivesExtractor.GetHorizonSize(line.ExtractLDATokenValue()); break;
                  case "UseStemmingInTextStatistics": ldaSettingsValues.UseStemmingInTextStatistics = PrimitivesExtractor.GetUseStemmingInTextStatistics(line.ExtractLDATokenValue()); break;
                  case "CalculateConditionalProbability": ldaSettingsValues.CalculateConditionalProbability = PrimitivesExtractor.GetCalculateConditionalProbability(line.ExtractLDATokenValue()); break;
                  case "CalculatePointWiseMutualInformation": ldaSettingsValues.CalculatePointWiseMutualInformation = PrimitivesExtractor.GetCalculatePointWiseMutualInformation(line.ExtractLDATokenValue()); break;
                  case "CalculateCorrelationCoefficient": ldaSettingsValues.CalculateCorrelationCoefficient = PrimitivesExtractor.GetCalculateCorrelationCoefficient(line.ExtractLDATokenValue()); break;
               }
            }
         }
         catch (FormatException e)
         {
            ShowMessage.ShowErrorMessageAndExit("The LDA Simulation file settings has some format error.", e);
            Console.WriteLine(e);
         }
         return ldaSettingsValues;
      }
      public static async Task<AllSettings> GetIONASettingsFromFileAsync(IONARunSettingsFileName ionaRunSettingsFileName)
      {
         return await Task.FromResult(ProcessIONASettingsValuesForTier(ionaRunSettingsFileName));
      }
      public static AllSettings ProcessIONASettingsValuesForTier(IONARunSettingsFileName ionaRunSettingsFileName)
      {
         var data = File.ReadLines(ionaRunSettingsFileName.Value).ToArray();
         var generalSettingsForIONA = new AllSettings
         {
            IONARunSettingsFileName = IONARunSettingsFileName.FromValue(ionaRunSettingsFileName)
         };

         try
         {
            foreach (var line in data)
            {
               switch (line.ExtractLDAToken())
               {
                  case "Tier":
                     generalSettingsForIONA.Tier = PrimitivesExtractor.GetTier(line.ExtractLDATokenValue());
                     break;
                  case "StartDate":
                     generalSettingsForIONA.StartDate = PrimitivesExtractor.GetStartDate(line.ExtractLDATokenValue());
                     break;
                  case "FinishDate":
                     generalSettingsForIONA.FinishDate = PrimitivesExtractor.GetFinishDate(line.ExtractLDATokenValue());
                     break;
                  case "MediaName":
                     generalSettingsForIONA.MediaName = PrimitivesExtractor.GetClosestMediaNameFromSimulationMedia(line.ExtractLDATokenValue());
                     break;
                  case "RandomSeed":
                     generalSettingsForIONA.RandomSeed = PrimitivesExtractor.GetRandomSeed(line.ExtractLDATokenValue());
                     break;
                  case "NumberOfGibbsIterations":
                     generalSettingsForIONA.NumberOfGibbsIterations = PrimitivesExtractor.GetNumberOfGibbsIterations(line.ExtractLDATokenValue());
                     break;
                  case "NumberOfInitialTopicsLDA":
                     generalSettingsForIONA.NumberOfInitialTopicsLDA = PrimitivesExtractor.GetNumberOfInitialTopicsLDA(line.ExtractLDATokenValue());
                     break;
                  case "NumberOfTopWordsToUseFromBoWofTopic":
                     generalSettingsForIONA.NumberOfTopWordsToUseFromBoWofTopic = PrimitivesExtractor.GetNumberOfTopWordsToUseFromBOWOfTopic(line.ExtractLDATokenValue());
                     break;
                  case "NumberOfWordsInStartNGRAM":
                     generalSettingsForIONA.NumberOfWordsInStartNGRAM = PrimitivesExtractor.GetNumberOfWordsInStartNGRAM(line.ExtractLDATokenValue());
                     break;
                  case "NumberOfLDAIterations":
                     generalSettingsForIONA.NumberOfLDAIterations = PrimitivesExtractor.GetNumberOfLDAIterations(line.ExtractLDATokenValue());
                     break;
                  case "NGramsThresholdValue":
                     generalSettingsForIONA.NGramsThresholdValue = PrimitivesExtractor.GetNGramsThresholdValue(line.ExtractLDATokenValue());
                     break;
                  case "MinimumWordOccurenceForVocabularyEntry":
                     generalSettingsForIONA.MinimumWordOccurenceForVocabularyEntry =
                         PrimitivesExtractor.GetMinimumWordOccurenceForVocabularyEntry(line.ExtractLDATokenValue());
                     break;
                  case "IncludeTitleInSetup":
                     generalSettingsForIONA.IncludeTitleInSetup = PrimitivesExtractor.GetIncludeTitleInSetup(line.ExtractLDATokenValue());
                     break;
                  case "OrderTopics":
                     generalSettingsForIONA.OrderTopics = PrimitivesExtractor.GetOrderTopicsValue(line.ExtractLDATokenValue());
                     break;
                  case "WriteAllTopicsWordDistribution":
                     generalSettingsForIONA.WriteAllTopicsWordDistribution = PrimitivesExtractor.GetWriteAllTopicsWordDistribution(line.ExtractLDATokenValue());
                     break;
                  case "IncludeTitleInTagging":
                     generalSettingsForIONA.IncludeTitleInTagging = PrimitivesExtractor.GetIncludeTitleInTagging(line.ExtractLDATokenValue());
                     break;
                  case "RunLDA":
                     generalSettingsForIONA.RunLDA = PrimitivesExtractor.GetRunLDA(line.ExtractLDATokenValue());
                     break;
                  case "RunNGramAnalysis":
                     generalSettingsForIONA.RunNGramAnalysis = PrimitivesExtractor.GetRunNGramAnalysis(line.ExtractLDATokenValue());
                     break;
                  case "UseLocalMediaVocabulary":
                     generalSettingsForIONA.UseLocalMediaVocabulary = PrimitivesExtractor.GetUseLocalMediaVocabulary(line.ExtractLDATokenValue());
                     break;
                  case "CreateCoOccurrenceMatrix":
                     generalSettingsForIONA.CreateCoOccurrenceMatrix = PrimitivesExtractor.GetCreateCoOccurrenceMatrix(line.ExtractLDATokenValue());
                     break;
                  case "OutputFolder":
                     var resultOutputFolder = line.ExtractLDATokenValue();
                     if (string.IsNullOrEmpty(resultOutputFolder))
                        generalSettingsForIONA.OutputFolder = OutputFolder.
                            FromValue(FolderManager.GetRootFolderPath().Value);
                     else if (resultOutputFolder.ToLower() == "desktop")
                        generalSettingsForIONA.OutputFolder = OutputFolder.
                            FromValue(FolderManager.GetDesktopFolderPath().Value);
                     else
                        generalSettingsForIONA.OutputFolder = PrimitivesExtractor.GetOutputFolder(line.ExtractLDATokenValue());
                     break;
                  case "RootFolderPath":
                     generalSettingsForIONA.RootFolderPath = PrimitivesExtractor.GetRootFolder(line.ExtractLDATokenValue());
                     break;
                  case "DatabaseRootFolderPath":
                     generalSettingsForIONA.DatabaseRootFolderPath = PrimitivesExtractor.GetDatabaseRootFolder(line.ExtractLDATokenValue());
                     break;
                  case "StudyNumber":
                     generalSettingsForIONA.StudyNumber = PrimitivesExtractor.GetStudyNumber(line.ExtractLDATokenValue());
                     break;
                  case "DbScanBallRadius":
                     generalSettingsForIONA.DbScanBallRadius = PrimitivesExtractor.GetDbScanBallRadius(line.ExtractLDATokenValue());
                     break;
                  case "DbScanMinPointsInRegion":
                     generalSettingsForIONA.DbScanMinPointsInRegion = PrimitivesExtractor.GetDbScanMinPointsInRegion(line.ExtractLDATokenValue());
                     break;
                  case "InterNGramMergingThreshold":
                     generalSettingsForIONA.InterNGramMergingThreshold = PrimitivesExtractor.GetInterNGramMergingThreshold(line.ExtractLDATokenValue());
                     break;
                  case "DocNGramTaggingThreshold":
                     generalSettingsForIONA.DocNGramTaggingThreshold = PrimitivesExtractor.GetDocNGramTaggingThreshold(line.ExtractLDATokenValue());
                     break;
                  ///////////////////////////////////////////////////////////////////////
                  case "AllNGramsFileName":
                     generalSettingsForIONA.AllNGramsFileName = PrimitivesExtractor.GetAllNGramsFileName(line.ExtractLDATokenValue());
                     break;
                  case "WordFrequencyFileName":
                     generalSettingsForIONA.WordFrequencyFileName = PrimitivesExtractor.GetWordFrequencyFileName(line.ExtractLDATokenValue());
                     break;
                   case "WordStreamFileName":
                       generalSettingsForIONA.WordStreamFileName = PrimitivesExtractor.GetWordStreamFileName(line.ExtractLDATokenValue());
                       break;
                  case "VocabularyFileName":
                     generalSettingsForIONA.VocabularyFileName = PrimitivesExtractor.GetVocabularyFileName(line.ExtractLDATokenValue());
                     break;
                  case "TopicsBagOfWordsFileName":
                     generalSettingsForIONA.TopicsBagOfWordsFileName = PrimitivesExtractor.GetTopicsBagOfWordsFileName(line.ExtractLDATokenValue());
                     break;
                  case "PopularNGramsFileName":
                     generalSettingsForIONA.PopularNGramsFileName = PrimitivesExtractor.GetPopularNGramsFileName(line.ExtractLDATokenValue());
                     break;
                  case "PopularNGramsSimilarityMatrixFileName":
                     generalSettingsForIONA.PopularNGramsSimilarityMatrixFileName = PrimitivesExtractor.GetPopularNGramsSimilarityMatrixFileName(line.ExtractLDATokenValue());
                     break;
                  case "InitialAgentsCosineSimilarityMatrixFileName":
                     generalSettingsForIONA.InitialAgentCosineSimilarityMatrixFileName = PrimitivesExtractor.GetInitialAgentCosineSimilarityMatrixFileName(line.ExtractLDATokenValue());
                     break;
                  case "ClusteredNGramMergeResultFileName":
                     generalSettingsForIONA.ClusteredNGramMergeResultFileName = PrimitivesExtractor.GetClusteredNGramMergeResultFileName(line.ExtractLDATokenValue());
                     break;
                  case "FirstMergeOfAgentsFileName":
                     generalSettingsForIONA.FirstMergeOfAgentsFileName = PrimitivesExtractor.GetFirstMergeOfAgentsFileName(line.ExtractLDATokenValue());
                     break;
                  case "FirstMergeTaggedDocIDsFileName":
                     generalSettingsForIONA.FirstMergeTaggedDocIDsFileName = PrimitivesExtractor.GetFirstMergeTaggedDocIDsFileName(line.ExtractLDATokenValue());
                     break;
                  case "FirstMergeOfAgentsGraphFileName":
                     generalSettingsForIONA.FirstMergeOfAgentsGraphFileName = PrimitivesExtractor.GetFirstMergeOfAgentsGraphFileName(line.ExtractLDATokenValue());
                     break;
                  case "SecondMergeOfAgentsFileName":
                     generalSettingsForIONA.SecondMergeOfAgentsFileName = PrimitivesExtractor.GetSecondMergeOfAgentsFileName(line.ExtractLDATokenValue());
                     break;
                  case "SecondMergeOfAgentsGraphFileName":
                     generalSettingsForIONA.SecondMergeOfAgentsGraphFileName = PrimitivesExtractor.GetSecondMergeOfAgentsGraphFileName(line.ExtractLDATokenValue());
                     break;
                  case "SecondMergeTaggedDocIdsFileName":
                     generalSettingsForIONA.SecondMergeTaggedDocIdsFileName = PrimitivesExtractor.GetSecondMergeTaggedDocIdsFileName(line.ExtractLDATokenValue());
                     break;
                  case "CountOfAllDocs":
                     generalSettingsForIONA.CountOfAllDocs = PrimitivesExtractor.GetCountOfAllDocs(line.ExtractLDATokenValue());
                     break;
                  case "CountOfAllWordsInDocs":
                     generalSettingsForIONA.CountOfAllWordsInDocs = PrimitivesExtractor.GetCountOfAllWordsInDocs(line.ExtractLDATokenValue());
                     break;
                  case "VocabularySize":
                     generalSettingsForIONA.VocabularySize = PrimitivesExtractor.GetVocabularySize(line.ExtractLDATokenValue());
                     break;
                  case "DocumentsLinksFileName":
                     generalSettingsForIONA.DocumentsLinksFileName = PrimitivesExtractor.GetDocumentsLinksFileName(line.ExtractLDATokenValue());
                     break;
                  case "DocWordFileName":
                     generalSettingsForIONA.DocWordFileName = PrimitivesExtractor.GetDocWordFileName(line.ExtractLDATokenValue());
                     break;
                  case "TaggedDocumentsSubCorporaFileName":
                     generalSettingsForIONA.TaggedDocumentsSubCorporaFileName = PrimitivesExtractor.GetTaggedDocumentsSubCorporaFileName(line.ExtractLDATokenValue());
                     break;
                  case "UnTaggedDocumentsSubCorporaFileName":
                     generalSettingsForIONA.UnTaggedDocumentsSubCorporaFileName = PrimitivesExtractor.GetUnTaggedDocumentsSubCorporaFileName(line.ExtractLDATokenValue());
                     break;
                  ////////////////////////////////////////////////////////
                  case "RunStatistics": generalSettingsForIONA.RunStatistics = PrimitivesExtractor.GetRunStatistics(line.ExtractLDATokenValue()); break;
                  case "StatisticsSources": generalSettingsForIONA.StatisticsSources = PrimitivesExtractor.GetStatisticsSources(line.ExtractLDATokenValue()); break;
                  case "StatisticsTarget": generalSettingsForIONA.StatisticsTarget = PrimitivesExtractor.GetStatisticsTarget(line.ExtractLDATokenValue()); break;
                  case "ReductionInNetworkEdgeValue": generalSettingsForIONA.ReductionInNetworkEdgeValue = PrimitivesExtractor.GetReductionInNetworkEdgeValue(line.ExtractLDATokenValue()); break;
                  case "RunNetworkStatistics": generalSettingsForIONA.RunNetworkStatistics = PrimitivesExtractor.GetRunNetworkStatistics(line.ExtractLDATokenValue()); break;
                  case "ReductionInWordFrequency": generalSettingsForIONA.ReductionInWordFrequency = PrimitivesExtractor.GetReductionInWordFrequency(line.ExtractLDATokenValue()); break;
                  case "RemoveLowWordFrequencyValue": generalSettingsForIONA.RemoveLowWordFrequencyValue = PrimitivesExtractor.GetRemoveLowWordFrequencyValue(line.ExtractLDATokenValue()); break;
                  case "ReductionOfNetworkEdgesWeight": generalSettingsForIONA.ReductionOfNetworkEdgesWeight = PrimitivesExtractor.GetReductionOfNetworkEdgesWeight(line.ExtractLDATokenValue()); break;
                  case "TextPartioningAlgorithm": generalSettingsForIONA.TextPartioningAlgorithm = PrimitivesExtractor.GetTextPartioningAlgorithm(line.ExtractLDATokenValue()); break;
                  case "SplitTheSentences": generalSettingsForIONA.SplitTheSentences = PrimitivesExtractor.GetSplitTheSentences(line.ExtractLDATokenValue()); break;
                  case "HorizonSize": generalSettingsForIONA.HorizonSize = PrimitivesExtractor.GetHorizonSize(line.ExtractLDATokenValue()); break;
                  case "UseStemmingInTextStatistics": generalSettingsForIONA.UseStemmingInTextStatistics = PrimitivesExtractor.GetUseStemmingInTextStatistics(line.ExtractLDATokenValue()); break;
                  case "CalculateConditionalProbability": generalSettingsForIONA.CalculateConditionalProbability = PrimitivesExtractor.GetCalculateConditionalProbability(line.ExtractLDATokenValue()); break;
                  case "CalculatePointWiseMutualInformation": generalSettingsForIONA.CalculatePointWiseMutualInformation = PrimitivesExtractor.GetCalculatePointWiseMutualInformation(line.ExtractLDATokenValue()); break;
                  case "CalculateCorrelationCoefficient": generalSettingsForIONA.CalculateCorrelationCoefficient = PrimitivesExtractor.GetCalculateCorrelationCoefficient(line.ExtractLDATokenValue()); break;
               }
            }
         }
         catch (FormatException e)
         {
            ShowMessage.ShowErrorMessageAndExit("The IONA Simulation file settings has some format error.", e);
            Console.WriteLine(e);
         }
         return generalSettingsForIONA;
      }
   }
}
