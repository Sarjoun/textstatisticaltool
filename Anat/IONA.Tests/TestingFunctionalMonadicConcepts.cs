﻿using LeagueOfMonads;
using LeagueOfMonads.NoLambda;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IONA.Tests
{
    [TestClass]
    public class TestingFunctionalMonadicConcepts
    {
        [TestMethod]
        public void TestMethod1()
        {
            //Fantasy Project -Stay(radio edit)

            //int z = 5;
            //var y = UnitTestHelperExtensionsFixture.Calculate(1)
            //   .PrintInt("filename1")
            //   .Calculate(2)
            //   .PrintInt("filename2")
            //   .Calculate(3)
            //   .PrintInt("filename3");

            Identity.Create(5)
               .Map(UnitTestHelperExtensionsFixture.Calculate, 1)
               .Tee(UnitTestHelperExtensionsFixture.PrintInt, "Filename");
        }

    }

    public static class UnitTestHelperExtensionsFixture
    {
        public static int Calculate(this int x, int threshold)
        {
            return x;
        }

        public static void PrintInt(this int x, string fileName)
        {
            // print
            //return x;
        }

    }

}
