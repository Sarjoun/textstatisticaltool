﻿using System.Collections.Generic;
using System.Linq;
using IONA.Algorithm.Algorithm.Clustering.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IONA.Tests
{
    /// <summary>
    /// Validate that the TaggedDocIds are correctly merged and clustered.
    /// </summary>
    [TestClass]
    public class TagDocumentVectorClusteringUnitTest
    {
        public List<List<float>> SetUp2()
        {
            var list1 =  new List<float> { 556, 630, 774, 930, 1292, 1384, 1453, 1510, 1562 };
            var list2 =  new List<float> { 518, 589, 662, 697, 737, 814, 890, 942, 965, 1039, 1115 };
            var list3 =  new List<float> { 527, 539, 611, 682, 748, 760, 825, 866, 941, 1016, 1051, 1127 };
            var list4 =  new List<float> { 211, 264, 316, 373, 389, 431, 452, 523, 595, 942, 987, 1093, 1169, 1273, 1343, 1396 };
            var list5 =  new List<float> { 211, 264, 316, 373, 389, 431, 452, 523, 595, 942, 987, 1018, 1093, 1169, 1273, 1343, 1396 };
            var list6 =  new List<float> { 288, 344, 346, 401, 407, 456, 466, 473, 526, 537, 539, 611, 612, 644, 682, 760, 866, 941, 1016 };
            var list7 =  new List<float> { 40, 82, 160, 214, 267, 592, 605, 678, 754, 817, 830, 893, 905, 938, 973, 981, 1045, 1083, 1351, 1428, 1489, 1544, 1594 };
            var list8 =  new List<float> { 288, 344, 346, 401, 407, 456, 466, 473, 526, 527, 537, 539, 611, 612, 644, 682, 748, 760, 825, 866, 941, 1016, 1051, 1127 };
            var list9 =  new List<float> { 211, 264, 316, 373, 389, 431, 452, 523, 595, 942, 987, 1018, 1057, 1093, 1137, 1169, 1214, 1273, 1312, 1343, 1363, 1396, 1435, 1492, 1547 };
            var list10 = new List<float> { 38, 80, 130, 168, 171, 182, 221, 224, 235, 274, 277, 294, 329, 334, 352, 387, 395, 411, 450, 517, 588, 660, 736, 813, 889, 963 };
            var list11 = new List<float> { 9, 25, 146, 199, 252, 996, 1071, 1106, 1147, 1182, 1211, 1231, 1258, 1285, 1307, 1358, 1385, 1454, 1483, 1484, 1511, 1539, 1540, 1563, 1590, 1591 };
            var list12 = new List<float> { 22, 47, 108, 158, 212, 265, 318, 375, 433, 679, 759, 870, 900, 944, 976, 1053, 1129, 1284, 1317, 1321, 1349, 1359, 1394, 1420, 1423, 1438, 1495 };
            var list13 = new List<float> { 22, 47, 108, 158, 212, 265, 318, 375, 433, 679, 759, 870, 900, 944, 976, 1053, 1129, 1284, 1317, 1321, 1349, 1359, 1394, 1420, 1423, 1438, 1495 };
            var list14 = new List<float> { 22, 47, 108, 158, 212, 265, 318, 375, 433, 679, 759, 870, 900, 944, 976, 1053, 1129, 1284, 1317, 1321, 1349, 1359, 1394, 1420, 1423, 1438, 1495 };
            var list15 = new List<float> { 22, 47, 108, 158, 212, 265, 318, 375, 433, 679, 759, 870, 900, 944, 976, 1053, 1129, 1284, 1317, 1321, 1349, 1359, 1394, 1420, 1423, 1438, 1495 };
            var list16 = new List<float> { 38, 44, 80, 130, 168, 171, 182, 221, 224, 235, 274, 277, 294, 329, 334, 352, 387, 395, 411, 450, 517, 588, 660, 710, 736, 790, 813, 867, 889, 963 };
            var list17 = new List<float> { 44, 111, 152, 162, 171, 205, 216, 224, 258, 269, 277, 311, 323, 334, 367, 381, 395, 422, 426, 439, 485, 490, 550, 622, 1187, 1263, 1338, 1384, 1415, 1451, 1453, 1478, 1508, 1510, 1560, 1562 };
            var list18 = new List<float> { 38, 44, 80, 130, 168, 171, 182, 221, 224, 235, 274, 277, 294, 329, 334, 352, 387, 390, 395, 411, 450, 453, 464, 490, 501, 517, 571, 588, 613, 660, 710, 736, 790, 813, 867, 889, 963, 987, 1093, 1169 };
            var list19 = new List<float> { 38, 44, 80, 130, 168, 171, 182, 221, 224, 235, 274, 277, 294, 329, 334, 352, 387, 390, 395, 411, 450, 453, 464, 490, 501, 517, 571, 588, 613, 660, 710, 736, 790, 813, 867, 889, 963, 987, 1093, 1169 };
            var list20 = new List<float> { 38, 80, 130, 168, 171, 182, 221, 224, 235, 274, 277, 294, 329, 334, 352, 387, 395, 411, 450, 467, 517, 565, 588, 590, 637, 660, 663, 715, 736, 741, 813, 820, 889, 963, 1287, 1365, 1460, 1517, 1570, 1597, 1602 };
            var list21 = new List<float> { 38, 80, 130, 168, 171, 182, 221, 224, 235, 274, 277, 294, 329, 334, 352, 387, 395, 411, 450, 467, 517, 565, 588, 590, 637, 660, 663, 715, 736, 741, 813, 820, 889, 963, 1287, 1365, 1460, 1517, 1570, 1597, 1602 };
            var list22 = new List<float> { 38, 80, 130, 168, 171, 182, 221, 224, 235, 274, 277, 294, 329, 334, 352, 387, 395, 411, 450, 467, 517, 565, 588, 590, 637, 660, 663, 715, 736, 741, 813, 820, 889, 963, 1287, 1365, 1460, 1517, 1570, 1597, 1602 };
            var list23 = new List<float> { 38, 80, 130, 168, 171, 182, 221, 224, 235, 274, 277, 294, 329, 334, 352, 387, 395, 411, 450, 467, 517, 565, 588, 590, 637, 660, 663, 715, 736, 741, 750, 813, 820, 827, 889, 963, 1287, 1365, 1460, 1517, 1570, 1597, 1602 };
            var list24 = new List<float> { 38, 80, 130, 168, 171, 182, 221, 224, 235, 274, 277, 294, 329, 334, 352, 387, 395, 411, 450, 467, 517, 565, 588, 590, 637, 660, 663, 715, 736, 741, 750, 813, 820, 827, 889, 963, 1287, 1365, 1460, 1517, 1570, 1597, 1602 };
            var list25 = new List<float> { 38, 80, 130, 168, 171, 182, 221, 224, 235, 274, 277, 294, 329, 334, 352, 387, 395, 411, 450, 467, 517, 565, 588, 590, 637, 660, 663, 715, 736, 741, 750, 813, 820, 827, 889, 963, 1287, 1365, 1460, 1517, 1570, 1597, 1602 };
            var list26 = new List<float> { 38, 44, 80, 130, 168, 171, 182, 221, 224, 235, 274, 277, 294, 329, 334, 352, 387, 395, 411, 450, 467, 517, 565, 588, 590, 600, 637, 660, 663, 675, 710, 715, 736, 741, 750, 790, 813, 820, 827, 867, 889, 963, 1287, 1365, 1460, 1517, 1570, 1597, 1602 };
            var list27 = new List<float> { 38, 44, 80, 130, 168, 171, 182, 221, 224, 235, 274, 277, 294, 329, 334, 352, 387, 395, 411, 450, 467, 517, 565, 588, 590, 600, 637, 660, 663, 675, 710, 715, 736, 741, 750, 790, 813, 820, 827, 867, 889, 963, 1287, 1365, 1460, 1517, 1570, 1597, 1602 };
            var list28 = new List<float> { 38, 44, 80, 130, 168, 171, 182, 221, 224, 235, 274, 277, 294, 329, 334, 352, 387, 395, 411, 450, 467, 517, 565, 588, 590, 600, 637, 660, 663, 675, 710, 715, 736, 741, 750, 790, 813, 820, 827, 867, 889, 963, 1287, 1365, 1460, 1517, 1570, 1597, 1602 };
            var list29 = new List<float> { 20, 38, 44, 45, 80, 88, 130, 138, 168, 171, 182, 191, 221, 224, 235, 244, 274, 277, 294, 297, 329, 334, 352, 378, 387, 395, 411, 436, 450, 467, 502, 517, 565, 588, 590, 637, 660, 663, 710, 715, 736, 750, 790, 813, 827, 867, 889, 963, 1287, 1365, 1460, 1517, 1570, 1597, 1602 };
            var list30 = new List<float> { 20, 38, 44, 45, 80, 88, 130, 138, 168, 171, 182, 191, 221, 224, 235, 244, 274, 277, 294, 297, 329, 334, 352, 378, 387, 395, 411, 436, 450, 467, 502, 517, 565, 588, 590, 637, 660, 663, 710, 715, 736, 750, 790, 813, 827, 867, 889, 963, 1287, 1365, 1460, 1517, 1570, 1597, 1602 };
            var list31 = new List<float> { 38, 44, 80, 130, 168, 171, 182, 221, 224, 235, 274, 277, 294, 329, 334, 352, 387, 390, 395, 411, 450, 453, 464, 467, 490, 501, 517, 565, 571, 588, 590, 600, 613, 637, 660, 663, 675, 710, 715, 736, 741, 750, 790, 813, 820, 827, 867, 889, 963, 987, 1093, 1169, 1287, 1365, 1460, 1517, 1570, 1597, 1602 };
            var list32 = new List<float> { 20, 38, 44, 45, 80, 88, 130, 138, 168, 171, 182, 191, 221, 224, 235, 244, 274, 277, 294, 297, 329, 334, 352, 378, 387, 395, 411, 436, 450, 467, 502, 517, 565, 588, 590, 600, 637, 660, 663, 675, 710, 715, 736, 741, 750, 790, 813, 820, 827, 867, 889, 963, 1287, 1365, 1460, 1517, 1570, 1597, 1602 };
            var list33 = new List<float> { 20, 38, 44, 45, 80, 88, 130, 138, 168, 171, 182, 191, 221, 224, 235, 244, 274, 277, 294, 297, 329, 334, 352, 378, 387, 390, 395, 411, 436, 450, 453, 464, 467, 490, 501, 502, 517, 565, 571, 588, 590, 613, 637, 660, 663, 710, 715, 736, 750, 790, 813, 827, 867, 889, 963, 987, 1093, 1169, 1287, 1365, 1460, 1517, 1570, 1597, 1602 };

            return new List<List<float>>
            {   list1, list2, list3, list4, list5, list6, list7, list8, list9, list10,
                list11, list12, list13, list14, list15, list16, list17, list18, list19, list20,
                list21, list22, list23, list24, list25, list26, list27, list28, list29, list30,
                list31, list32, list33};
        }

        public List<List<float>> SetUp1()
        {
            var list1 = new List<float> { 1, 2, 3, 4, 5, 9, 7 };
            var list2 = new List<float> { 5, 3, 4, 6, 2, 1, 7 };
            var list3 = new List<float> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var list4 = new List<float> { 10, 9, 7, 8, 6, 4, 2, 5, 3, 1 };
            var list5 = new List<float> { 1, 2, 3 };

            return new List<List<float>> { list1, list2, list3, list4, list5 };
        }


        [TestMethod]
        public void CheckThatAllIdenticalTaggedDocItemsAreMergedForSetup1()
        {
            var stemWordClusterPairs = SetUp1().
                CreateListOfPairsByGroupingAgentsUsingTheirTaggedDocsCount().
                ToList();

            var stemWordClusterSingles = SetUp1().
                CreateListOfSinglesBasedOnTheCountOfTheirTaggedDocsCount().
                ToList();

            var temp = SetUp1().ToList();
            var mergedStemWordNGrams = new List<List<float>>();

            foreach (var stemWordClusterPair in stemWordClusterPairs)
            {
                var result = SetUp1().Where(swn => swn.Count == stemWordClusterPair.Key).ToList();

                mergedStemWordNGrams.AddRange(
                    AgentsManager.MergeListOfListsOfFloatsHavingSameCountOfListValues(result));
            }

            foreach (var stemWordClusterSingle in stemWordClusterSingles)
            {
                mergedStemWordNGrams.
                    AddRange(
                    SetUp1().
                    Where(swn => swn.Count == stemWordClusterSingle.Key).ToList());
            }

            const int expectedResult = 4;
            Assert.IsTrue(SetUp1().Count >= mergedStemWordNGrams.Count);
            Assert.AreEqual(expectedResult, mergedStemWordNGrams.Count);
        }


        [TestMethod]
        public void CheckThatAllIdenticalTaggedDocItemsAreMergedForSetup2()
        {
            var stemWordClusterPairs = SetUp2().
                CreateListOfPairsByGroupingAgentsUsingTheirTaggedDocsCount().
                ToList();

            var stemWordClusterSingles = SetUp2().
                CreateListOfSinglesBasedOnTheCountOfTheirTaggedDocsCount().
                ToList();

            var temp = SetUp2().ToList();
            var mergedStemWordNGrams = new List<List<float>>();

            foreach (var stemWordClusterPair in stemWordClusterPairs)
            {
                var result = SetUp2().Where(swn => swn.Count == stemWordClusterPair.Key).ToList();

                mergedStemWordNGrams.AddRange(
                    AgentsManager.MergeListOfListsOfFloatsHavingSameCountOfListValues(result));
            }

            foreach (var stemWordClusterSingle in stemWordClusterSingles)
            {
                mergedStemWordNGrams.
                    AddRange(
                        SetUp2().
                            Where(swn => swn.Count == stemWordClusterSingle.Key).ToList());
            }

            const int expectedResult = 22;
            Assert.IsTrue(SetUp2().Count >= mergedStemWordNGrams.Count);
            Assert.AreEqual(expectedResult, mergedStemWordNGrams.Count);
        }

    }
}
