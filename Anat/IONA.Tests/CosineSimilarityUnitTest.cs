﻿using IONA.Algorithm.Algorithm.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IONA.Tests
{
    [TestClass]
    public class CosineSimilarityUnitTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            float[] vector1= { 1, 2, 3 };
            float[] vector2 = { 14, 305, 96 };

            var result = CosineSimilarity.ComputeCosineSimilarity(vector1, vector2);

            Assert.AreNotEqual(1, result);
            //Assert.IsTrue(result < 0.5);

        }
    }
}
