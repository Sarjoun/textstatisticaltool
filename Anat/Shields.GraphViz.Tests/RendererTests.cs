﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shields.GraphViz.Components;
using Shields.GraphViz.Services;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Reflection;
using Shields.GraphViz.Models;

namespace Shields.GraphViz.Tests
{
    [TestClass]
    public class RendererTests
    {
        private Lazy<IRenderer> _renderer;
        //private const string GraphVizBin = @"C:\Program Files (x86)\Graphviz2.38\bin";
        public const string MainFolderName = @"\anat\";
        public const string GraphVizFolderName = "Graphviz.2.38.0.2";


        private IRenderer Renderer => _renderer.Value;

        private string GetCurrentDirectoryRelativePath() => Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);


        [TestInitialize]
        public void Initialize()
        {
            _renderer = new Lazy<IRenderer>(() => new Renderer(GetGraphVizBinFolder()));
        }


        //public string GetGraphVizBin()
        //{
        //   string exeLocation2 = System.Reflection.Assembly.GetExecutingAssembly().Location;
        //   string token = @"\anat\";
        //   int index = exeLocation2.ToLower().IndexOf(token, StringComparison.Ordinal);
        //   return exeLocation2.Substring(0, index) + @"\anat\packages\Graphviz.2.38.0.2";
        //}
        public static string GetGraphVizBinFolder()
        {
            var mainFolderNameSize = MainFolderName.Length;
            var graphVizBinFolderPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var location = graphVizBinFolderPath.ToLower().IndexOf(MainFolderName, StringComparison.Ordinal) +
                           mainFolderNameSize;

            return graphVizBinFolderPath.Substring(0, location) +
                   @"\packages\" +
                   GraphVizFolderName;
        }


        //public void CheckCurrentFolder()
        // {
        //    string exeLocation = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
        //    string exeLocation2 = System.Reflection.Assembly.GetExecutingAssembly().Location;

        //    string exeDir = System.IO.Path.GetDirectoryName(exeLocation);
        //    string dataFile = Path.Combine(exeDir, "data//settings.xml");


        //    string token = "\\anat\\";
        //    int index = exeLocation2.IndexOf(token);

        //    string output = exeLocation2.Substring(0, index + 6);

        //    string output2 = exeLocation2.Substring(0, index) + @"\anat\packages\Graphviz.2.38.0.2\";
        //   Assert.IsFalse(string.IsNullOrEmpty(output2));

        //}

        [TestMethod]
        public async Task DotProducesCorrectPng()
        {
            var graph = Graph.Undirected
                .Add(EdgeStatement.For("a", "b"))
                .Add(EdgeStatement.For("a", "c"));
            using (var outputStream = new MemoryStream())
            {
                await Renderer.RunAsync(graph, outputStream, RendererLayouts.Dot, RendererFormats.Png, CancellationToken.None);
                var output = outputStream.ToArray();
                var expectedOutput = await ReadAllBytesAsync(Assembly.GetExecutingAssembly().GetManifestResourceStream("Shields.GraphViz.Tests.Resources.Graph1.png"));
                Assert.IsTrue(output.SequenceEqual(expectedOutput));
            }
        }

        [TestMethod]
        [ExpectedException(typeof(OperationCanceledException))]
        public async Task CancellationWorks()
        {
            var cts = new CancellationTokenSource();
            cts.Cancel();
            var graph = Graph.Undirected
                .Add(EdgeStatement.For("a", "b"))
                .Add(EdgeStatement.For("a", "c"));
            using (var outputStream = new MemoryStream())
            {
                await Renderer.RunAsync(graph, outputStream, RendererLayouts.Dot, RendererFormats.Png, cts.Token);
            }
        }

        private async Task<byte[]> ReadAllBytesAsync(Stream stream)
        {
            using (var memoryStream = new MemoryStream())
            {
                await stream.CopyToAsync(memoryStream);
                return memoryStream.ToArray();
            }
        }
    }
}
