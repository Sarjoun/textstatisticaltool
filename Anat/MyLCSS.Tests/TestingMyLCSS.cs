﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MyLCSS.Tests
{
    [TestClass]
    public class TestingMyLCSS
    {
        private const string Doc1 = "211,264,316,373,389,431,452,523,595,942,987,1018,1093,1169,1273,1343,1396,";
        private const string Doc2 = "211,264,316,373,389,431,452,523,595,942,987,1093,1169,1273,1343,1396,";
        private const string Doc3 = "211,264,316,373,389,431,452,523,595,942,987,1018,1093,1169,1273,1343,1396,";
        private const string Doc4 = "211,264,316,373,389,431,452,523,";
        private const string Doc5 = "211,264,316,373,389,431,452,523,";

        private const string NGram1 = "0,0,1,0,0,1,2,2,";
        private const string NGram2 = "0,1,1,0,0,1,1,2,";
        private const string NGram3 = "1,1,1,0,0,0,0,0,";


        [TestMethod]
        public void CheckThatOneDocumentSequenceIsNotInsideAnother()
        {
            var output1 = Doc1.ConvertoNumeral().ToList();
            var output2 = Doc2.ConvertoNumeral().ToList();

            Assert.IsFalse(
               Functions.AreTwoCollectionsIdentical(output1, output2));
        }

        [TestMethod]
        public void CheckThatOneDocumentSequenceIsInsideAnother()
        {
            var output1 = Doc1.ConvertoNumeral().ToList();
            var output3 = Doc3.ConvertoNumeral().ToList();

            Assert.IsTrue(Functions.AreTwoCollectionsIdentical(output1, output3));
        }

        [TestMethod]
        public void CheckThat2SequencesCanBeMergedIfTheyAreIdentical()
        {
            var output1 = Doc1.ConvertoNumeral().ToList();
            var output3 = Doc3.ConvertoNumeral().ToList();

            Assert.IsTrue(Functions.AreTwoCollectionsMergeable(output1, output3));

            var comparisonValue = Functions.CompareTwoCollectionsOfAnyLength(output1, output3);
            Assert.IsTrue(comparisonValue > Functions.Threshold);
        }
        
        [TestMethod]
        public void CheckThat2SequencesCanBeMergedIfTheyAreSimilarEnough()
        {
            var output1 = NGram1.ConvertoNumeral().ToList();
            var output2 = NGram2.ConvertoNumeral().ToList();
            var output3 = NGram3.ConvertoNumeral().ToList();

            var result1 = Functions.CompareTwoCollectionsOfAnyLength(output1, output2);
            var result2 = Functions.CompareTwoCollectionsOfAnyLength(output1, output3);
            var result3 = Functions.CompareTwoCollectionsOfAnyLength(output2, output3);

            Assert.IsTrue(Functions.CompareTwoCollectionsOfAnyLength(output1, output2) > Functions.CompareTwoCollectionsOfAnyLength(output1, output3));
            Assert.IsTrue(Functions.CompareTwoCollectionsOfAnyLength(output1, output2) > Functions.CompareTwoCollectionsOfAnyLength(output2, output3));

            var comparisonValue = Functions.CompareTwoCollectionsOfAnyLength(output1, output2);
            Assert.IsTrue(comparisonValue > Functions.Threshold);
        }

        [TestMethod]
        public void TestThat2SequencesAreMoreSimilarToEachOtherThanTheThird()
        {
            var output1 = Doc1.ConvertoNumeral().ToList();
            var output2 = Doc2.ConvertoNumeral().ToList();

            Assert.IsTrue(
                Functions.AreTwoCollectionsMergeable(output1, output2));

            var comparisonValue = Functions.CompareTwoCollectionsOfAnyLength(output1, output2);
            Assert.IsTrue(comparisonValue > Functions.Threshold);
        }

        [TestMethod]
        public void CheckDuplicates()
        {
            var output1 = Doc1.ConvertoNumeral().ToList();
            var output2 = Doc2.ConvertoNumeral().ToList();
            var output3 = Doc3.ConvertoNumeral().ToList();
            var output4 = Doc4.ConvertoNumeral().ToList();
            var output5 = Doc5.ConvertoNumeral().ToList();
            List<List<float>> test = new List<List<float>>();
            test.Add(output1); test.Add(output2); test.Add(output3); test.Add(output4); test.Add(output5);

            var query = test.GroupBy(x => x.Count)
                .Where(g => g.Count() > 1)
                .ToDictionary(x => x.Key, y => y.Count());

            Assert.AreEqual(2, query.Count);
        }

    }
}
