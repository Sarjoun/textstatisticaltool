﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ILDA.Algorithm.Algorithm.Helpers;
using ILDA.CalendarHelper;
using ILDA.Extensions;
using SDParser.Enums;
using SDParser.Primitives;

namespace ILDA.MediaHelper
{
    public static class MediaStories
    {
        public static List<string> GetMediaStoriesForDurationFromUntaggedDocuments(
            UntaggedDocsFromPreviousTier untaggedDocsFromPreviousTier)
        {
            var _files = FileReader.ReadFileAsync(untaggedDocsFromPreviousTier);

            var files = _files.Result.ToList();
            var counter = 1;
            return files.Select(x => counter++ + ")" + x).ToList();
        }

        public static List<string> GetMediaStoriesForDuration(MediaNameEnum mediaName, Duration duration,
            MainDatabaseFolderPath mainDatabaseFolderPath)
        {
            var files = new List<string>();
            foreach (var month in duration.Months)
            {
                files.AddRange(
                    GetStoriesLinksForMediaForMonth(
                        mediaName,
                        month,
                        duration.Start,
                        duration.Finish,
                        mainDatabaseFolderPath));
            }
            var counter = 1;
            return files.Select(x => counter++ + ")" + x).ToList();
        }

        public static List<string> GetStoriesLinksForMediaForMonth(MediaNameEnum mediaName, Month month,
            DateTime start, DateTime finish, MainDatabaseFolderPath mainDatabaseFolderPath)
        {
            var files = File.ReadAllLines(GetMediaIndexFile(mediaName, month));
            //return files.Where(file => file.CheckIfDateIsWithinRange(month.StartDate, month.FinishDate)).ToList();

            return (from file in files
                    where file.CheckIfDateIsWithinRange(start, finish)
                    select file
                    .AddMonthToStoryFileNameIndexValue(month)
                    .AddRootFolderToStoryFileName(mainDatabaseFolderPath)).ToList();
        }

        public static string GetMediaIndexFile(MediaNameEnum mediaName, Month month)
        {
            return month.IndexAddress + @"\" + MediaNameEnumUtilities.GetRssMediaName(mediaName) + ".txt";
        }
    }
}






//public static Task<string[]> ReadAllLinesAsync(string path)
//{
//    return ReadAllLinesAsync(path, Encoding.UTF8);
//}

//public static async Task<string[]> GetStoriesLinksForMediaForMonth(string path, Encoding encoding)
//{
//    var lines = new List<string>();

//    // Open the FileStream with the same FileMode, FileAccess
//    // and FileShare as a call to File.OpenText would've done.
//    using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
//    using (var reader = new StreamReader(stream, encoding))
//    {
//        string line;
//        while ((line = await reader.ReadLineAsync()) != null)
//        {
//            lines.Add(line);
//        }
//    }

//    return lines.ToArray();
//}




//public static async void GetStoriesLinksForMediaForMonth(MediaName mediaName, Month month)
//{
//    using (var reader = File.OpenText(GetMediaIndexFile(mediaName, month)))
//    return await reader.ReadToEndAsync();
//}
//public async string[] GetStoriesLinksForMediaForMonth(MediaName mediaName, Month month)
//{
//    using (var reader = File.OpenText("Words.txt"))
//    {
//        var fileText = await reader.ReadToEndAsync();
//        return fileText.Split(new[]
//        {
//            Environment.NewLine
//        }, StringSplitOptions.None);
//    }
//}


//static async void ReadMyFile()
//{

//    FileStream fs = new FileStream("thermopylae.txt",
//        FileMode.Open, FileAccess.Read);

//    using (StreamReader reader = new StreamReader(fs, Encoding.UTF8))
//    {
//        string text = await reader.ReadToEndAsync();
//     }
//}
