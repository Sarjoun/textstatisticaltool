﻿using System;
using System.Collections.Generic;
using ILDA.Enums;

using SDParser.Primitives;

namespace ILDA.CalendarHelper
{
    public class Month
    {
        public MonthName Name { get; set; }
        public int Index { get; set; }
        public int MaxDays { get; set; }
        public int Year { get; set; }
        public List<int> Days { get; set; }//The actual affected days
        public string IndexAddress { get; set; } //The file that contains the index for this month
        public MainDatabaseFolderPath MainDatabaseFolderPath { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }

        //the constructor with a year argument can populate the IndexAddress
        public Month(MonthName name, int days, int year, MainDatabaseFolderPath mainDatabaseFolderPath)
        {
            Name = name;
            MaxDays = days;
            Year = year;
            MainDatabaseFolderPath = mainDatabaseFolderPath;

            switch (name)
            {
                case MonthName.Jan: Index = 1; break;
                case MonthName.Feb: Index = 2; break;
                case MonthName.Mar: Index = 3; break;
                case MonthName.Apr: Index = 4; break;
                case MonthName.May: Index = 5; break;
                case MonthName.Jun: Index = 6; break;
                case MonthName.Jul: Index = 7; break;
                case MonthName.Aug: Index = 8; break;
                case MonthName.Sep: Index = 9; break;
                case MonthName.Oct: Index = 10; break;
                case MonthName.Nov: Index = 11; break;
                case MonthName.Dec: Index = 12; break;
                default: Index = 0; break;
            }

            StartDate = new DateTime(year, Index, 1);
            FinishDate = new DateTime(year, Index, MaxDays);

            IndexAddress = @mainDatabaseFolderPath.Value + @"indexes_" + name + "_" + year + @"\";
            //IndexAddress = @mainDatabaseFolderPath.Value + @"indexes_" + name + year + @"\";
        }

        public Month(int month, int year, MainDatabaseFolderPath mainDatabaseFolderPath)
        {
            Year = year;
            Index = month;
            MainDatabaseFolderPath = mainDatabaseFolderPath;
            switch (month)
            {
                case 1: Name = MonthName.Jan; MaxDays = 31; break;
                case 2: Name = MonthName.Feb; MaxDays = 28; break;
                case 3: Name = MonthName.Mar; MaxDays = 31; break;
                case 4: Name = MonthName.Apr; MaxDays = 30; break;
                case 5: Name = MonthName.May; MaxDays = 31; break;
                case 6: Name = MonthName.Jun; MaxDays = 31; break;
                case 7: Name = MonthName.Jul; MaxDays = 31; break;
                case 8: Name = MonthName.Aug; MaxDays = 31; break;
                case 9: Name = MonthName.Sep; MaxDays = 30; break;
                case 10: Name = MonthName.Oct; MaxDays = 31; break;
                case 11: Name = MonthName.Nov; MaxDays = 30; break;
                case 12: Name = MonthName.Dec; MaxDays = 31; break;
                default: Name = MonthName.Dec; MaxDays = 31; break;
            }

            StartDate = new DateTime(year, Index, 1);
            FinishDate = new DateTime(year, Index, MaxDays);

            IndexAddress = @mainDatabaseFolderPath.Value + @"indexes_" + Name + "_" + year + @"\";
            //IndexAddress = @mainDatabaseFolderPath.Value + @"indexes_" + Name + year + @"\";
        }
    }
}