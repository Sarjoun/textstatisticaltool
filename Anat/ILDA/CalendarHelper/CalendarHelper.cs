﻿using System;
using System.Collections.Generic;
using ILDA.Extensions;
using SDParser.Primitives;

namespace ILDA.CalendarHelper
{
   //No support for leap years.
   public class CalendarHelper
   {
      public List<string> GetMonthsIndexLists { get; set; }
      public List<Month> Months { get; set; }
      public DateTime StartPosition { get; set; }
      public DateTime FinishPosition { get; set; }
      public int TotalDays { get; set; }
      public MainDatabaseFolderPath MainDatabaseFolderPath { get; set; }

      public CalendarHelper()
      {

      }
      // assuming that the startPosition <= finishPosition
      public CalendarHelper(DateTime startPosition, DateTime finishPosition, int totalDays, MainDatabaseFolderPath mainDatabaseFolderPath)
      {
         StartPosition = startPosition;
         FinishPosition = finishPosition;
         TotalDays = totalDays + 1;
         MainDatabaseFolderPath = mainDatabaseFolderPath;
         Months = Initialize(startPosition, finishPosition, totalDays + 1);

         Duration dur = new Duration(startPosition, finishPosition);
         dur.Months = Months;
         dur.Days = totalDays + 1;
         Duration = dur;
      }

      public Duration Duration { get; set; }

      /// <summary>
      /// This function sets up the entire logical calendar by
      /// building a logical year made from logical months so 
      /// that I can subtract 2 dates and the get the months 
      /// involved.
      /// </summary>
      private List<Month> Initialize(DateTime startPosition, DateTime finishPosition, int totalDays)
      {
         List<Month> months = new List<Month>();

         //If in same Year
         if (startPosition.Year == finishPosition.Year)
         {
            months = GetMonthsForOneYear(startPosition, finishPosition, totalDays);
         }
         else //otherwise not in same Year
         {
            /*
            There are 3 values for which the months need to be calculated for:
            1. The current year. (1)
            2. The whole years in between the current year and final year. (?)    
            3. The final year. (1)
            */
            int totalYears = finishPosition.Year - startPosition.Year + 1;
            int startYear = startPosition.Year;
            List<int> durationYears = new List<int>();
            DateTime tempStartPosition = startPosition;
            DateTime tempFinishPosition = finishPosition;

            //For all years except last one
            for (int i = 0; i < totalYears - 1; i++)
            {
               tempFinishPosition = new DateTime(startYear + i, 12, 31);
               months.AddRange(GetMonthsForOneYear(tempStartPosition, tempFinishPosition, tempStartPosition.RemainingDays(tempFinishPosition)));
               tempStartPosition = new DateTime(startYear + i + 1, 1, 1);
            }
            //For last year
            tempStartPosition = new DateTime(finishPosition.Year, 1, 1);
            months.AddRange(GetMonthsForOneYear(tempStartPosition, finishPosition, tempStartPosition.RemainingDays(finishPosition)));
         }
         return months;
      }

      private List<Month> GetMonthsForOneYear(
          DateTime startPosition, DateTime finishPosition, int totalDays)
      {
         var years = new List<Year>();
         var months = new List<Month>();
         //var monthsID = new List<int>();
         //var monthsNames = new List<MonthName>();

         int remainderCounter = totalDays;

         //Double-check if in same Year
         if (startPosition.Year != finishPosition.Year) return months;

         years.Add(new Year(startPosition.Year, new List<int>(), MainDatabaseFolderPath));

         if (startPosition.Month == finishPosition.Month)
         {
            months.Add(new Month(startPosition.Month, startPosition.Year, MainDatabaseFolderPath));
            //If in the same Day
            if (startPosition.Day == finishPosition.Day)
            {
               months[0].Days = new List<int> { startPosition.Day };
            }
            else //otherwise
            {
               months[0].Days = new List<int>();
               for (var i = startPosition.Day; i <= finishPosition.Day; i++)
               {
                  months[0].Days.Add(i);
               }
            }
         }
         else //otherwise not in same Month, so that means it spans at least 2 months
              //where the first month is completely included starting from the start day.
         {
            //get the remainder days of current Month
            Month currentMonth = new Month(startPosition.Month, startPosition.Year, MainDatabaseFolderPath);
            List<int> currentDays = new List<int>();
            var currentCounter = 0;
            for (int i = startPosition.Day; i <= currentMonth.MaxDays; i++)
            {
               currentDays.Add(i);
               currentCounter++;
            }
            currentMonth.Days = currentDays;
            remainderCounter = remainderCounter - currentCounter;
            months.Add(currentMonth);

            //calculate the remainder Month(s)
            Month nextMonth = currentMonth;
            while (remainderCounter > 0)
            {
               //get the chronologically ordered next month and check if the maximum number
               //of days of that month are < or > than the remaining days. If less, then
               //take those days. If more, then repeat for more months.

               //get next by adding 1 to the previous month
               int nextMonthId = nextMonth.Index + 1;
               currentMonth = new Month(nextMonthId, currentMonth.Year, MainDatabaseFolderPath);
               currentDays = new List<int>();
               currentCounter = 0;
               int currentMaxDays = 0;

               //if this is the last month in the duration
               if (remainderCounter <= currentMonth.MaxDays)
               {
                  currentMaxDays = remainderCounter;
                  remainderCounter = 0;
               }
               else //there are more months
               {
                  currentMaxDays = currentMonth.MaxDays;
                  remainderCounter = remainderCounter - currentMaxDays;
               }

               for (int i = 1; i <= currentMaxDays; i++)
               {
                  currentDays.Add(i);
               }

               currentMonth.Days = currentDays;
               months.Add(currentMonth);
               nextMonth = currentMonth;
            }
         }
         return months;

      }

   }
}
