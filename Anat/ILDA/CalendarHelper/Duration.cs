using System;
using System.Collections.Generic;
using System.Linq;

namespace ILDA.CalendarHelper
{
    public class Duration
    {
        public List<Year> Years { get; set; }
        public List<Month> Months { get; set; }
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
        public int Days { get; set; }
        public List<DateTime> AllDays { get; private set; }

        public Duration(DateTime startPosition, DateTime finishPosition)
        {
            Start = startPosition;
            Finish = finishPosition;
            AllDays = CalculateAllDaysInRange(Start, Finish);
        }
        //    AllDays = new List<DateTime>();

        //    if((startPosition != null)&&(finishPosition != null))
        //        CalculateAllDays();
        //}

        //private void CalculateAllDays()
        //{
        //    DateTime currentPosition = Start;
        //    AllDays.Add(currentPosition);

        //    bool bug11 = true;

        //    if (currentPosition != null) //b1.1
        //    {
        //        while ((bug11) && currentPosition.LessThanOrEqual(Finish)) 
        //        {
        //            currentPosition = currentPosition.GetNextPosition();
        //            if (currentPosition == null)
        //                bug11 = false;
        //            else
        //                AllDays.Add(currentPosition);
        //        }
        //    }
        //}


        private List<DateTime> CalculateAllDaysInRange(DateTime startingDate, DateTime endingDate)
        {
            var allDates = new List<DateTime>();
            for (var date = startingDate; date <= endingDate; date = date.AddDays(1))
                allDates.Add(date);
            return allDates;
        }

        public IEnumerable<DateTime> DateRange(DateTime fromDate, DateTime toDate)
        {
            return Enumerable.Range(0, toDate.Subtract(fromDate).Days + 1)
                .Select(d => fromDate.AddDays(d));
        }
    }
}