﻿using System;

namespace ILDA.CalendarHelper
{
    public static class CalendarUtilities
    {
        public static bool CheckWithinFinishDuration(Duration duration, string date)
        {
            //the string format is [year-month-day]
            //[2011-04-01]
            //new format is year-month-day
            int year, month, day = 0;
            string[] dates = date.Split('-');
            year = Convert.ToInt32(dates[0]);
            month = Convert.ToInt32(dates[1]);
            day = Convert.ToInt32(dates[2]);

            if (year > duration.Finish.Year)
                return false;
            else
            {
                if (year == duration.Finish.Year)
                {
                    if (month > duration.Finish.Month)
                        return false;
                    else
                    {
                        if (month < duration.Finish.Month)
                        {
                            return true;//no need to check for days
                        }
                        else if (month == duration.Finish.Month)
                        {
                            //if (day <= duration.Finish.Day)
                            if (month == duration.Start.Month)
                            {
                                if ((day >= duration.Start.Day) && (day <= duration.Finish.Day))
                                    return true;
                                else
                                    return false;
                            }
                            else
                            {
                                if (day <= duration.Finish.Day)
                                    return true;
                                else
                                    return false;
                            }
                        }
                        else
                            return false;
                    }
                }
                else
                {
                    if (year < duration.Finish.Year)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
