using System.Collections.Generic;
using ILDA.Enums;
using SDParser.Primitives;



namespace ILDA.CalendarHelper
{
    public class Year
    {
        private readonly List<int> _availableMonths;
        public List<Month> Months { get; }//total months
        public int Value { get; set; }
        public MainDatabaseFolderPath MainDatabaseFolderPath { get; set; }

        private Month jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec;
        public Year(int value, List<int> availableMonths, MainDatabaseFolderPath mainDatabaseFolderPath)
        {
            _availableMonths = availableMonths;
            Value = value;
            MainDatabaseFolderPath = mainDatabaseFolderPath;

            Months = new List<Month>();

            jan = new Month(MonthName.Jan, 31,value, MainDatabaseFolderPath);
            feb = new Month(MonthName.Feb, 28,value, MainDatabaseFolderPath);
            mar = new Month(MonthName.Mar, 31,value, MainDatabaseFolderPath);
            apr = new Month(MonthName.Apr, 30,value, MainDatabaseFolderPath);
            may = new Month(MonthName.May, 31,value, MainDatabaseFolderPath);
            jun = new Month(MonthName.Jun, 30,value, MainDatabaseFolderPath);
            jul = new Month(MonthName.Jul, 31,value, MainDatabaseFolderPath);
            aug = new Month(MonthName.Aug, 31,value, MainDatabaseFolderPath);
            sep = new Month(MonthName.Sep, 30,value, MainDatabaseFolderPath);
            oct = new Month(MonthName.Oct, 31,value, MainDatabaseFolderPath);
            nov = new Month(MonthName.Nov, 30,value, MainDatabaseFolderPath);
            dec = new Month(MonthName.Dec, 31,value, MainDatabaseFolderPath);

            foreach (var m in availableMonths)
            {
                if (m == 1)
                    Months.Add(jan);
                if (m == 2)
                    Months.Add(feb);
                if (m == 3)
                    Months.Add(mar);
                if (m == 4)
                    Months.Add(apr);
                if (m == 5)
                    Months.Add(may);
                if (m == 6)
                    Months.Add(jun);
                if (m == 7)
                    Months.Add(jul);
                if (m == 8)
                    Months.Add(aug);
                if (m == 9)
                    Months.Add(sep);
                if (m == 10)
                    Months.Add(oct);
                if (m == 11)
                    Months.Add(nov);
                if (m == 12)
                    Months.Add(dec);            
            }
        }

        public Year(int value) 
        {
            Value = value;
            Months = new List<Month>();

            jan = new Month(MonthName.Jan, 31,value, MainDatabaseFolderPath);
            feb = new Month(MonthName.Feb, 28,value, MainDatabaseFolderPath);
            mar = new Month(MonthName.Mar, 31,value, MainDatabaseFolderPath);
            apr = new Month(MonthName.Apr, 30,value, MainDatabaseFolderPath);
            may = new Month(MonthName.May, 31,value, MainDatabaseFolderPath);
            jun = new Month(MonthName.Jun, 30,value, MainDatabaseFolderPath);
            jul = new Month(MonthName.Jul, 31,value, MainDatabaseFolderPath);
            aug = new Month(MonthName.Aug, 31,value, MainDatabaseFolderPath);
            sep = new Month(MonthName.Sep, 30,value, MainDatabaseFolderPath);
            oct = new Month(MonthName.Oct, 31,value, MainDatabaseFolderPath);
            nov = new Month(MonthName.Nov, 30,value, MainDatabaseFolderPath);
            dec = new Month(MonthName.Dec, 31,value, MainDatabaseFolderPath);

            Months = new List<Month>{jan, feb, mar, apr, may, jun,
                jul, aug, sep, oct, nov, dec};
        }

    }
}