﻿using System;
using SDParser.Primitives;



namespace ILDA.CalendarHelper
{
    public static class SimulationRange
    {
        public static CalendarHelper GetSimulationRange(
            MainDatabaseFolderPath mainDatabaseFolderPath,
            DateTime startDate, 
            DateTime endDate)
        {
            var timeSpan = endDate.Subtract(startDate);
            var calendarHelper = new CalendarHelper();

            //if (timeSpan.Days > 0)
            //{
            //    calendarHelper =
            //        new CalendarHelper(
            //            new Position(startDate.Day, startDate.Month, startDate.Year, 0),
            //            new Position(endDate.Day, endDate.Month, endDate.Year, 0),
            //            timeSpan.Days, mainDatabaseFolderPath);
            //}

            if (timeSpan.Days > 0)
            {
                calendarHelper =
                    new CalendarHelper(
                        startDate, 
                        endDate,
                        timeSpan.Days, mainDatabaseFolderPath);
            }

            return calendarHelper;
        }
    }
}
