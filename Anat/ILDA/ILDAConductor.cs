﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ILDA.Algorithm.Algorithm.Core;
using ILDA.Algorithm.Algorithm.Helpers;
using ILDA.Algorithm.Algorithm.Structs;
using ILDA.Extensions;
using SDParser;
using SDParser.Notifications;
using SDParser.Primitives;
using SDParser.StopWords;

namespace ILDA
{
    public static class ILDAConductor
    {
        public static Alpha Alpha;
        public static Beta Beta;
        public static Random Randomizer;
        public static WordDocFileStruct WordDocStruct;

        public static async Task<IONARunSettingsFileName> RunSingleLDAInstance(ParsedSimulationSettings ldaSettings)
        {
            try
            {
                try
                {
                    MediaContext.Setup(ldaSettings);
                }
                catch (Exception e)
                {
                    ShowMessage.ShowErrorMessageAndExit(@"Error in RunSingleLDAInstance at the MediaContext.Setup.", e);
                }

                WordStreamManager.
                        MakeWordStreamAndVariantWordsReturnDictionaryOrderedAlphabetically(
                            StopWordsHelper.GetStopWordsFromEmbeddedResource(),
                            MediaContext.MediaSettings.MediaLdaFileNames.DocumentsLinksFileName,
                            MediaContext.MediaSettings.MediaLdaFileNames.WordStreamFileName,
                            ldaSettings.IncludeTitleInSetup,
                            ldaSettings.CreateCoOccurrenceMatrix);

                var vocab = await VocabularyHelper.MakeVocabularyFile(
                    WordStreamManager.UniqueWordsAndTheirVariants,
                    MediaContext.MediaSettings.MediaLdaFileNames.VocabularyFileName,
                    ldaSettings.MinimumWordOccurenceForVocabularyEntry);

                WordStreamManager.UniqueWordsAndTheirVariants =
                    VocabularyHelper.
                    MinimumOccurrencePassingUniqueWordsAndTheirVariants.
                    UpdateStemWordDictionaryIdFromDictionaryId();

                VocabularyHelper.MakeFrequencyFile(
                    WordStreamManager.UniqueWordsAndTheirVariants,
                    MediaContext.MediaSettings.MediaLdaFileNames.WordFrequencyFileName,
                    MediaContext.MediaSettings.MediaLdaFileNames.WordFrequencyForDataFileName);

                DocWordMatrixManager.MakeDocWordFileReturnDocWordCounts(vocab,
                    VocabularyHelper.MinimumOccurrencePassingUniqueWordsAndTheirVariants,
                    MediaContext.MediaSettings.MediaLdaFileNames.DocWordFileName,
                    MediaContext.MediaSettings.MediaLdaFileNames.WordStreamFileName,
                    MediaContext.MediaSettings.MediaFolderPath,
                    MediaContext.MediaSettings.MediaName);

                WordDocStruct = WordDocStructManager.CreateWordDocStructFromDocWordFile(
                    MediaContext.MediaSettings.MediaLdaFileNames.DocWordFileName,
                    DocWordMatrixManager.CountOfAllWordsInDocs);

                Randomizer = new Random(ldaSettings.RandomSeed.Value);
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit(@"Error in ILDAConductor: the first part of RunSingleLDAInstance", e);
            }

            try
            {
                for (var i = 1; i <= ldaSettings.NumberOfLDAIterations.Value; i++)
                {
                    var gibbsDataStructure = TopicsInitializer.SetupInitialGibbsDataStructure(
                        WordDocStruct,
                        ldaSettings.NumberOfInitialTopicsLDA,
                        RandomSeed.FromValue(Randomizer.Next()),
                        VocabularyHelper.VocabularySize,
                        DocWordMatrixManager.CountOfAllWordsInDocs,
                        DocWordMatrixManager.CountOfAllDocs);

                    Alpha = LDAParametersCalculator.CalculateAlpha(
                        ldaSettings.NumberOfInitialTopicsLDA,
                        DocWordMatrixManager.CountOfAllWordsInDocs,
                        DocWordMatrixManager.CountOfAllDocs);

                    Beta = LDAParametersCalculator.CalculateBeta();

                    gibbsDataStructure.DocumentTopicMatrix.AddSmoothDirichletVariable(Alpha);
                    gibbsDataStructure.WordTopicMatrix.AddSmoothDirichletVariable(Beta);
                    gibbsDataStructure.TopicDistributionArray.AddSmoothDirichlet(VocabularyHelper.Vocabulary.Count * Beta);

                    for (var g = 1; g <= ldaSettings.NumberOfGibbsIterations.Value; g++)
                    {
                        gibbsDataStructure = MathHelper.RunGibbsSampleChain(
                            DocWordMatrixManager.CountOfAllWordsInDocs,
                            VocabularyHelper.Vocabulary.Count,
                            ldaSettings.NumberOfInitialTopicsLDA.Value,
                            gibbsDataStructure.WordArray,
                            gibbsDataStructure.DocumentArray,
                            gibbsDataStructure.TopicArray,
                            gibbsDataStructure.WordTopicMatrix,
                            gibbsDataStructure.DocumentTopicMatrix,
                            gibbsDataStructure.TopicDistributionArray,
                            gibbsDataStructure.OrderPattern,
                            Randomizer);
                    }

                    gibbsDataStructure.DocumentTopicMatrix.DeleteSmoothDirichletVariable(Alpha);
                    gibbsDataStructure.WordTopicMatrix.DeleteSmoothDirichletVariable(Beta);
                    gibbsDataStructure.TopicDistributionArray.DeleteSmoothDirichlet(VocabularyHelper.Vocabulary.Count * Beta);

                    await WriteWordTopicMatrixFile(ldaSettings, gibbsDataStructure);
                    await WriteDocumentTopicMatrixFile(ldaSettings, gibbsDataStructure);
                    await WriteTopicArrayFile(gibbsDataStructure);

                    //var vocab = VocabularyHelper.Vocabulary.Select(x => x.Key).ToArray();
                    TopicsWriter.PrintTopics(
                        MediaContext.MediaSettings.MediaLdaFileNames.TopicsBagOfWordsFileName,
                        MediaContext.MediaSettings.MediaLdaFileNames.TopicsBagOfWordsDataFileName,
                        MediaContext.MediaSettings.MediaLdaFileNames.WordTopicArrayFileName,
                        MediaContext.MediaSettings.MediaLdaFileNames.WordFrequencyFileName,
                        ldaSettings.NumberOfInitialTopicsLDA,
                        ldaSettings.NumberOfTopWordsToUseFromBoWofTopic,
                        VocabularyHelper.Vocabulary.Select(x => x.Key).ToArray());
                }

                await FileWriter.WriteIONARunSettingsFile(
                    Alpha,
                    Beta,
                    DocWordMatrixManager.DocWordCountStruct,
                    VocabularyHelper.VocabularySize,
                    ldaSettings.GetTextVersionForFilePrintOut(),
                    MediaContext.MediaSettings.MediaLdaFileNames,
                    MediaContext.MediaSettings.MediaAnalysisFileNames,
                    MediaContext.MediaSettings.MediaClusterTagsFileNames,
                    MediaContext.MediaSettings.MediaLdaFileNames.IONARunSettingsFileName);
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit(@"Error in ILDAConductor: in the second part of RunSingleLDAInstance (Gibbs\Iteration)", e);
            }

            return MediaContext.MediaSettings.MediaLdaFileNames.IONARunSettingsFileName;
        }

        private static async Task WriteWordTopicMatrixFile(
            ParsedSimulationSettings ldaSettings,
            GeneralGibbsParamStruct gibbsDataStructure)
        {
            await FileWriter.WriteSparseMatrixAsync(
                VocabularyHelper.Vocabulary.Count,
                ldaSettings.NumberOfInitialTopicsLDA.Value,
                gibbsDataStructure.WordTopicMatrix,
                MediaContext.MediaSettings.MediaLdaFileNames.WordTopicArrayFileName.Value);
        }
        private static async Task WriteDocumentTopicMatrixFile(
            ParsedSimulationSettings ldaSettings,
            GeneralGibbsParamStruct gibbsDataStructure)
        {
            await FileWriter.WriteSparseMatrixAsync(
                DocWordMatrixManager.CountOfAllDocs,
                ldaSettings.NumberOfInitialTopicsLDA.Value,
                gibbsDataStructure.DocumentTopicMatrix,
                MediaContext.MediaSettings.MediaLdaFileNames.TopicDocumentArrayFileName.Value);
        }
        private static async Task WriteTopicArrayFile(
            GeneralGibbsParamStruct gibbsDataStructure)
        {
            await FileWriter.WriteVectorAsync(
                DocWordMatrixManager.CountOfAllWordsInDocs,
                gibbsDataStructure.TopicArray,
                MediaContext.MediaSettings.MediaLdaFileNames.TopicsVectorFileName.Value);
        }
    }
}
