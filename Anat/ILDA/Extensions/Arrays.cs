﻿using System;
using System.Linq;

namespace ILDA.Extensions
{
    public static class Arrays
    {
        public static double[][] DeleteSmoothDirichletVariable(this double[][] matrix, double smooth)
        {
            var columns = matrix.GetColumnCount(0);
            var rows = matrix.GetRowCount(0);

            for (var i = 0; i < rows; i++)
                for (var t = 0; t < columns; t++)
                    matrix[i][t] -= smooth;

            return matrix;
        }

        public static double[][] AddSmoothDirichletVariable(this double[][] matrix, double smooth)
        {
            var rows = matrix.GetRowCount(0);
            var columns = matrix.GetColumnCount(0);

            for (var i = 0; i < rows; i++)
                for (var t = 0; t < columns; t++)
                    matrix[i][t] += smooth;

            return matrix;
        }

        public static int GetRowCount<T>(this T[][] array, int columnNumber) =>
            array
                .Count(subArray => subArray.Length > columnNumber);

        //public static int GetColumnSize<T>(this T[][] array) =>
        //    array.Length;

        public static int GetColumnCount<T>(this T[][] array, int rowNumber) =>
            array[rowNumber].Length;


        public static double[][] AddRowToMatrix(this double[][] matrix, int rowId, int numberOfColumns)
        {
            matrix[rowId] = new double[numberOfColumns];
            //Array.Clear(matrix[rowId], 0, matrix[rowId].Length);
            return matrix;
        }


        /// <summary>
        /// Locates and returns the index of the highest value, then assigns 0 to that index and returns it
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static int[] ReturnIndexValuesOfOrderedContent(this int[] array)
        {
            var maxSize = array.Length;
            var index = 0;
            var returnArray = new int[maxSize];
            Array.Clear(returnArray, 0, returnArray.Length);

            for (var k = 0; k < maxSize; k++)
            {
                var compareTo = 0;
                for (var i = 0; i < maxSize; i++)
                {
                    if (array[i] <= compareTo) continue;
                    compareTo = array[i];
                    index = i;
                }
                array[index] = 0;
                returnArray[k] = index;
            }
            return returnArray;
        }


        public static double[] AddSmoothDirichlet(this double[] array, double smooth)
        {
            return array.Select(i => i + smooth).ToArray();
        }

        public static double[] DeleteSmoothDirichlet(this double[] array, double smooth)
        {
            return array.Select(i => i - smooth).ToArray();
        }
    }
}


//For regular multidimensional array [,]
//int rowsOrHeight = x.GetLength(0);
//int colsOrWidth = x.GetLength(1);


//public static double[] AddSmoothDirichlet(this double[] array, double smooth)
//{
//    return array.Select(i => i + smooth).ToArray();
//}
