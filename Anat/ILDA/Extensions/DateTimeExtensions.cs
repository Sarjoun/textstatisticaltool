﻿using System;

namespace ILDA.Extensions
{
    public static class DateTimeExtensions
    {
        public static int RemainingDays(this DateTime currentDateTime, DateTime newDateTime)
        {
            var timeSpan = newDateTime.Subtract(currentDateTime);
                return Math.Abs(timeSpan.Days + 1);
        }
    }
}
