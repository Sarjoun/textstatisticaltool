﻿using System.Collections.Generic;
using System.Linq;
using SDParser.Structures;

namespace ILDA.Extensions
{
    public static class DictionaryExtensions
    {
        public static Dictionary<string, StemWord> UpdateStemWordDictionaryIdFromDictionaryId(
            this Dictionary<string, StemWord> currentDictionary)
        {
            var counter = 1;
            foreach (var entry in currentDictionary)
            {
                entry.Value.DictionaryId = counter;
                counter++;
            }
            return currentDictionary;
        }

        //public static Dictionary<string, StemWord> UpdateStemWordFrequencyIdFromDictionaryId(
        //    this Dictionary<string, StemWord> currentDictionary)
        //{
        //    var counter = 1;
        //    foreach (var entry in currentDictionary)
        //    {
        //        entry.Value.FrequencyId = counter;
        //        counter++;
        //    }
        //    return currentDictionary;
        //}

        public static Dictionary<string, StemWord> ReorderDictionaryAlphabetically(
            this Dictionary<string, StemWord> uniqueWordsAndTheirVariants)
        {
            return (
                from entry
                in uniqueWordsAndTheirVariants
                orderby entry.Value.DictionaryId 
                select entry).ToDictionary(pair => pair.Key, pair => pair.Value);
        }

    }
}
