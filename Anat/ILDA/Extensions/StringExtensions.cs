﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using ILDA.CalendarHelper;
using SDParser.Primitives;

namespace ILDA.Extensions
{
    public static class StringExtensions
    {
        public static string CheckIfFolderExistsAndCreateIfNotFound(this string folderPath)
        {
            try
            {
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Catastrophic error in creating output folder " + folderPath + ". " + e.Message);
                return folderPath;
            }
            return folderPath;
        }

        public static bool CheckIfDateIsWithinRange(this string fileName, DateTime start, DateTime finish)
        {
            var fileDate = fileName.ExtractDateTimeOfIndexFile();

            return start <= fileDate && finish >= fileDate;
        }

        public static DateTime ExtractDateTimeOfIndexFile(this string fileName)
        {
            return fileName.
                GetMatchesBetweenDelimiters("[", "]").
                ConvertStringToDateTimeForManyFormats();
        }

        public static DateTime ConvertStringToDateTimeForManyFormats(this string text)
        {
            var frmt = new System.Globalization.CultureInfo("en-US", true);
            var formats = new string[4];
            formats[0] = "yyyy-MM-dd";
            formats[1] = "yyyy-MM-d";
            formats[2] = "yyyy-M-d";
            formats[3] = "yyyy-M-dd";
            //formats[4] = "mm-dd-yyyy";
            //formats[5] = "mm-yyyy";
            //formats[6] = "yyyy-mm";
            //formats[7] = "yyyy-mm-dd";
            //formats[8] = "yyyy-dd-mm";
            return DateTime.ParseExact(text, formats, frmt, System.Globalization.DateTimeStyles.None);
        }

        public static string GetMatchesBetweenDelimiters(this string source, string beginDelim, string endDelim)
        {
            return Regex.Match(source, @"\" + beginDelim + @"(.*?)" + @"\" + endDelim).Groups[1].Value;
        }

        //required for the simulation to work
        public static string AddMonthToStoryFileNameIndexValue(this string fileName, Month month)
        {
            //return fileName.Replace("indexes","indexes_" + month.Name + month.Year);
            return fileName.Replace("indexes", "indexes_" + month.Name + "_" + month.Year);
        }

        public static string AddRootFolderToStoryFileName(this string fileName, MainDatabaseFolderPath mainDatabaseFolderPath)
        {
            return mainDatabaseFolderPath.Value + fileName;
        }

        ////https://stackoverflow.com/questions/421616/how-can-i-strip-punctuation-from-a-string
        ////public static string StripPunctuation(this string s)
        ////{
        ////    var sb = new StringBuilder();
        ////    foreach (char c in s)
        ////    {
        ////        if (!char.IsPunctuation(c))
        ////            sb.Append(c);
        ////        else
        ////            return sb.ToString();
        ////    }
        ////    return sb.ToString();
        ////}

        //public static string StripPossessiveApostropheAndS(this string word)
        //{
        //    return word.Contains("'") ? word.Substring(0, word.IndexOf("'", StringComparison.Ordinal)) : word;
        //}

        //public static string StripExHyphen(this string word)
        //{
        //    return word.Contains("ex-") ? word.Substring(0, word.IndexOf("'", StringComparison.Ordinal)) : word;
        //}

        //public static string RemoveExPrefixFollowedByHyphen(this string word)
        //{
        //    return word.Contains("ex-") ? word.Replace("-", "") : word;
        //}

        //public static string[] SplitOnDashes(this string word)
        //{
        //    return word.Split('-');
        //}

        //public static string StripPunctuation(this string s)
        //{
        //    return new string(s.Where(c => !char.IsPunctuation(c)).ToArray());
        //}

        //public static string StripPunctuationSpecial(this string word)
        //{
        //    var sb = new StringBuilder();
        //    var once = true;
        //    foreach (var c in word)
        //    {
        //        if (!char.IsPunctuation(c))
        //            sb.Append(c);
        //        else
        //        {
        //            var r = word.IndexOfAny(new char[] { '.' }) + 3;

        //            if (c == '.' && r <= word.Count())
        //            {
        //                if (once)
        //                {
        //                    sb.Append(c);
        //                    once = false;
        //                }
        //                else
        //                    return sb.ToString();
        //            }
        //            else
        //                return sb.ToString();
        //        }
        //    }
        //    return sb.ToString();
        //}

        //public static bool IsLower(this string word)
        //{
        //    return word.ToLower() == word;
        //}

        //public static bool IsNumeric(this string word)
        //{
        //    return Regex.IsMatch(word, @"([\.,])?\d([\.,][\d])?");
        //    //return Regex.IsMatch(word, @"([\.,-])?\d([\.,-][\d])?");
        //}

        //public static bool IsSingular(this string word)
        //{
        //    return Singularizer.Singularize(word) == word;
        //}

        //public static string ToSingular(this string word)
        //{
        //    return Singularizer.Singularize(word);
        //}

        //public static bool IsMinLength(this string word)
        //{
        //    return word.Length >= 3;
        //}

        //public static string RemoveDocIdFromStartOfPathIfItExists(this string word)
        //{
        //    return word.Substring(word.IndexOf(')') + 1);
        //}

        ////public static string CleanUpFilePathBackSlashes(this string fileName)
        ////{
        ////    return fileName.Replace(@"\\",@"\");
        ////}
    }
}
