using ILDA.Algorithm.Algorithm.Core;
using ILDA.CalendarHelper;
using SDParser;
using SDParser.ComplexTypes;
using SDParser.Primitives;

namespace ILDA.Algorithm.Algorithm.Helpers
{
    public static class MediaContext
    {
        public static ExperimentFolderPath ExperimentFolderPath;
        public static MediaSettings MediaSettings;

        public static void Setup(ParsedSimulationSettings ldaSettings)
        {
            ExperimentFolderPath = FolderManager.GetExperimentFolderPathFromOutputFolders(
                FolderManager.CreateBasicOutputFolders(
                    ldaSettings.RootFolderPath));

            var mainDatabaseFolderPath = FolderManager.GetMainDatabaseFolderPath(
                ldaSettings.DatabaseRootFolderPath);

            MediaSettings = FolderManager.CreateSimulationAndInitialMediaCommonFolders(
                ExperimentFolderPath,
                ldaSettings.Tier,
                ldaSettings.MediaName,
                ldaSettings.StudyNumber);

            var calendarHelper = SimulationRange.GetSimulationRange(
                mainDatabaseFolderPath,
                ldaSettings.StartDate.Value,
                ldaSettings.FinishDate.Value);

            if (string.IsNullOrEmpty(ldaSettings.UntaggedDocsFromPreviousTier.Value.Trim()))
                MediaSettings.AllTierStoryFiles = DurationMediaDocsManager
                .CreateAllDocumentsPathsFile(
                    ldaSettings.MediaName.Value,
                    calendarHelper.Duration,
                    MediaSettings.MediaLdaFileNames.DocumentsLinksFileName,
                    mainDatabaseFolderPath);
            else
                MediaSettings.AllTierStoryFiles = DurationMediaDocsManager
                    .CopyUntaggedDocumentsPathsFile(
                        MediaSettings.MediaLdaFileNames.DocumentsLinksFileName,
                        ldaSettings.UntaggedDocsFromPreviousTier);

            //MediaSettings.AllStoryFilesWithTagInformation = DurationMediaDocsManager
            //    .CreateAllStoryFilesWithTagInformation(MediaSettings.AllTierStoryFiles);
        }
    }
}
