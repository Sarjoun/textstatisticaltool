﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ILDA.Algorithm.Algorithm.Structs;
using ILDA.Extensions;
using SDParser.Primitives;
using SDParser.Structures;

namespace ILDA.Algorithm.Algorithm.Helpers
{
    public static class VocabularyHelper
    {
        public static VocabularySize VocabularySize => VocabularySize.FromValue(Vocabulary.Count);
        public static Dictionary<string, int> Vocabulary;//Only words that pass the minimum occurrence
        public static Dictionary<string, StemWord> MinimumOccurrencePassingUniqueWordsAndTheirVariants;

        //TODO: delete after refactoring
        //public static List<WordTopicStruct> WordTopicStructList;
        
        //public static Dictionary<string, StemWord> UpdateStemWordFrequencyId(
        //    Dictionary<string, StemWord> uniqueWordsAndTheirVariants)
        //{
        //    var allPassingUniqueWordsAndTheirVariantsSortedByFrequency = (
        //        from entry
        //        in uniqueWordsAndTheirVariants
        //        orderby entry.Value.Frequency descending
        //        select entry).ToDictionary(pair => pair.Key, pair => pair.Value);

        //    return allPassingUniqueWordsAndTheirVariantsSortedByFrequency.
        //        UpdateStemWordFrequencyIdFromDictionaryId();
        //}

        public static Dictionary<string, StemWord> UpdateStemWordAlphabetAndFrequencyIds(
            Dictionary<string, StemWord> uniqueWordsAndTheirVariants)
        {
            var allPassingUniqueWordsAndTheirVariantsSortedByAlphabet = (
                from entry
                in uniqueWordsAndTheirVariants
                orderby entry.Value.DictionaryId descending
                select entry).ToDictionary(pair => pair.Key, pair => pair.Value);

            return allPassingUniqueWordsAndTheirVariantsSortedByAlphabet.
                UpdateStemWordDictionaryIdFromDictionaryId();
        }

        //public static Dictionary<string, int> MakeVocabularyFile(
        //    Dictionary<string, StemWord> uniqueWordsAndTheirVariants,
        //    VocabularyFileName vocabularyFileName,
        //    WordFrequencyFileName wordFrequencyFileName,
        //    WordFrequencyForDataFileName wordFrequencyForDataFileName,
        //    MinimumWordOccurenceForVocabularyEntry minimumWordOccurenceForVocabularyEntry)
        //{
        //    //var allPassingUniqueWordsAndTheirVariantsSortedByFrequency = MakeWordFrequencyFileReturnDictionary(
        //    //    wordFrequencyFileName,
        //    //    wordFrequencyForDataFileName,
        //    //    uniqueWordsAndTheirVariants);

        //    //WordTopicStructList = CreateWordTopicStruct(
        //    //    allPassingUniqueWordsAndTheirVariantsSortedByFrequency);

        //    return Vocabulary = MakeVocabularyFile(
        //                            vocabularyFileName, 
        //                            uniqueWordsAndTheirVariants, 
        //                            minimumWordOccurenceForVocabularyEntry);
        //}

        public static async Task<Dictionary<string, int>> MakeVocabularyFile(
            Dictionary<string, StemWord> uniqueWordsAndTheirVariants,
            VocabularyFileName vocabularyFileName,
            MinimumWordOccurenceForVocabularyEntry minimumWordOccurenceForVocabularyEntry)
        {
            MinimumOccurrencePassingUniqueWordsAndTheirVariants =
                uniqueWordsAndTheirVariants
                .Where(p => p.Value.Frequency > minimumWordOccurenceForVocabularyEntry.Value)
                .ToDictionary(p => p.Key, p => p.Value);

            if (!File.Exists(vocabularyFileName.Value)) return null;
            var counter = 1;
            var streamWriter = new StreamWriter(vocabularyFileName.Value);
            try
            {
                foreach (var passingUnique in MinimumOccurrencePassingUniqueWordsAndTheirVariants)
                {
                    await streamWriter.WriteLineAsync(passingUnique.Key + "\t" +
                                                      counter++ + "\t" +
                                                      //passingUnique.Value.DictionaryId + "\t" +
                                                      passingUnique.Value.Frequency);
                }
            }
            finally
            {
                streamWriter.Close();
                streamWriter.Dispose();
            }

            //MinimumOccurrencePassingUniqueWordsAndTheirVariants.UpdateStemWordDictionaryIdFromDictionaryId();

            return Vocabulary = MinimumOccurrencePassingUniqueWordsAndTheirVariants
                .Select(x => x.Key).ToList()
                .Select((s, index) => new { s, index })
                .ToDictionary(x => x.s, x => x.index + 1);/* x.index + 1, +1 so that the first word in the vocab has value 1 (and not 0), 
                                                            since later the index of the word is used in a matrix and using zero (or -1 
                                                            as later on the calculation shows) will break the code. */
        }
        
        public static Dictionary<string, StemWord> MakeFrequencyFile(
            Dictionary<string, StemWord> uniqueWordsAndTheirVariants,
            WordFrequencyFileName wordFrequencyFileName,
            WordFrequencyForDataFileName wordFrequencyForDataFileName)
        {
            var sortedFreqDict = (
                from entry
                in uniqueWordsAndTheirVariants
                orderby entry.Value.Frequency descending
                select entry).ToDictionary(pair => pair.Key, pair => pair.Value);

            //Order on frequency
            if (File.Exists(wordFrequencyFileName.Value))
            {
                File.WriteAllLines(
                    wordFrequencyFileName.Value,
                    sortedFreqDict.Values.Select(w => w.GetRootAndVariantsAndAllFrequenciesStringRepresentation()),
                    Encoding.UTF8);
            }

            if (File.Exists(wordFrequencyForDataFileName.Value))
            {
                File.WriteAllLines(
                    wordFrequencyForDataFileName.Value,
                    sortedFreqDict.Values.Select(
                        w => w.PrintRootAndItsFrequencyForDataAnalysisCommaDelimited()),
                    Encoding.UTF8);
            }
            return sortedFreqDict;
        }


        //TODO: remove after refactoring
        //private static List<WordTopicStruct> CreateWordTopicStruct(Dictionary<string, StemWord> dictionary)
        //{
        //    return dictionary.Select(element => new WordTopicStruct(element.Key, element.Value.Frequency)).ToList();
        //}
    }
}
