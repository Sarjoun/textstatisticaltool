﻿using System;//  m±}╝8K↔r↨!ÖV±∞Ä☻☺♥♦♣→2♠◘◘○•♠◙♂♂♀♀♪♫☼↓→▲▬↨↑§  ك ظظδ⌡;  
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using ILDA.Algorithm.Algorithm.Core;
using SDParser.ComplexTypes;
using SDParser.Notifications;
using SDParser.Primitives;

namespace ILDA.Algorithm.Algorithm.Helpers
{
    public static class FileWriter
    {
        public static async Task WriteSparseMatrixAsync(
            int row,
            int column,
            double[][] matrixValue,
            string outputMatrixFileName)
        {
            var tw = new StreamWriter(outputMatrixFileName);
            try
            {
                await tw.WriteLineAsync(row.ToString());
                await tw.WriteLineAsync(column.ToString());
                tw.WriteLine(SumUpRowsAndColumns(row, column, matrixValue));
                for (var i = 0; i < row; i++)
                    for (var j = 0; j < column; j++)
                        if (Math.Abs(matrixValue[i][j]) > 1e-6)
                            await tw.WriteLineAsync($"{i + 1} {j + 1} {matrixValue[i][j]:f0}");
                tw.Close();
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }
        }

        //public static void WriteSparseMatrix(
        //    int row,
        //    int column,
        //    double[][] matrixValue,
        //    string outputMatrixFileName)
        //{
        //    var tw = new StreamWriter(outputMatrixFileName);
        //    try
        //    {
        //        tw.WriteLine(row);
        //        tw.WriteLine(column);
        //        tw.WriteLine(SumUpRowsAndColumns(row, column, matrixValue));
        //        for (var i = 0; i < row; i++)
        //            for (var j = 0; j < column; j++)
        //                if (Math.Abs(matrixValue[i][j]) > 1e-6)
        //                    tw.WriteLine("{0} {1} {2:f0}", i + 1, j + 1, matrixValue[i][j]);
        //        tw.Close();
        //    }
        //    finally
        //    {
        //        tw.Close();
        //        tw.Dispose();
        //    }
        //}

        public static int SumUpRowsAndColumns(int row, int column, double[][] element)
        {
            int i, j, topicCount = 0;
            for (i = 0; i < row; i++)
                for (j = 0; j < column; j++)
                    if (Math.Abs(element[i][j]) > 1e-6)
                        topicCount++;
            return topicCount;
        }

        public static async Task WriteVectorAsync(
            int vectorLength,
            int[] vectorValues,
            string outputVectorFileName)
        {
            TextWriter tw = new StreamWriter(outputVectorFileName);
            try
            {
                for (var i = 0; i < vectorLength; i++)
                    await tw.WriteLineAsync((vectorValues[i] + 1).ToString());

                tw.Close();
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }
        }
        public static void WriteVector(
            int vectorLength,
            int[] vectorValues,
            string outputVectorFileName)
        {
            TextWriter tw = new StreamWriter(outputVectorFileName);
            try
            {
                for (var i = 0; i < vectorLength; i++)
                    tw.WriteLine(vectorValues[i] + 1);
                tw.Close();
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }
        }

        //public static void WriteAllWordsTopicsDistribution(
        //    string allWordsTopicsFileName,
        //    int numberOfUniqueWords,
        //    int topicsNumber,
        //    double[][] wordTopicMatrix)
        //{
        //    TextWriter twDAll = new StreamWriter(allWordsTopicsFileName, true);

        //    try
        //    {
        //        var prob_w_given_t_true = new double[numberOfUniqueWords];
        //        var indx_true = new double[numberOfUniqueWords];

        //        for (var t = 0; t < topicsNumber; t++)
        //        {
        //            for (var w = 0; w < numberOfUniqueWords; w++)
        //                prob_w_given_t_true[w] = wordTopicMatrix[w][t] / numberOfUniqueWords;

        //            indx_true = prob_w_given_t_true.OrderByDescending(a => a).ToArray();

        //            //First top 10 word_array from each topic if there is more than 10, otherwise whatever is there
        //            /* It would be probably informative if I can also write another file 
        //               that shows the distribution value of each word in that topic? */

        //            for (var i = 0; i < numberOfUniqueWords; i++)
        //            {
        //                //vocab number is frequency
        //                if (i < numberOfUniqueWords - 1)
        //                {
        //                    twDAll.Write($"{indx_true[i]:0.000000000}" + " ");
        //                }
        //                else
        //                {
        //                    twDAll.WriteLine("{0:0.000000000}", indx_true[i]);
        //                }
        //            }
        //        }
        //    }
        //    finally
        //    {
        //        twDAll.Close();
        //    }
        //}

        private const string LDARunSettingsFileSection = "## LDA simulation settings section:";
        private const string IONARunSettingsFileComments = "## IONA simulation settings section:";
        private const string DividerLine = "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --";

        public static async Task WriteLDASettingsHeaderData(int tier, StreamWriter tw)
        {
            //Write the value of the general simulation settings once
            if (tier == 1)
            {
                await tw.WriteLineAsync("");
                await tw.WriteLineAsync(IONARunSettingsFileComments);
            }
        }
        
        public static async Task WriteIONARunSettingsFile(
            Alpha alpha,
            Beta beta,
            DocWordCountStruct docWordCountStruct,
            VocabularySize vocabularySize,
            IEnumerable<string> ldaRunSettingsContents,
            MediaLDAFileNames mediaLdaFileNames,
            MediaAnalysisFileNames mediaAnalysisFileNames,
            MediaClusterTagsFileNames mediaClusterTagsFileNames,
            IONARunSettingsFileName ionaRunSettingsFileName)
        {
            var tw = new StreamWriter(ionaRunSettingsFileName.Value);
            try
            {
                await tw.WriteLineAsync(LDARunSettingsFileSection);
                foreach (var setting in ldaRunSettingsContents)
                {
                    await tw.WriteLineAsync(setting);
                }

                await WriteLDASettingsHeaderData(mediaAnalysisFileNames.Tier.Value, tw);
                await tw.WriteLineAsync(DividerLine + " Tier : " + mediaAnalysisFileNames.Tier.Value + DividerLine);
                await tw.WriteLineAsync(mediaLdaFileNames.GetAllFileNamesAndTheirPathsToPrint());
                await tw.WriteLineAsync("--");
                await tw.WriteLineAsync(mediaAnalysisFileNames.GetAllFileNamesAndTheirPathsToPrint());
                await tw.WriteLineAsync("--");
                await tw.WriteLineAsync("Alpha=" + alpha.Value);
                await tw.WriteLineAsync("Beta=" + beta.Value);
                await tw.WriteLineAsync("CountOfAllWordsInDocs=" + docWordCountStruct.CountOfAllWordsInDocs.Value);
                await tw.WriteLineAsync("VocabularySize=" + vocabularySize.Value);
                await tw.WriteLineAsync("CountOfAllDocs=" + docWordCountStruct.CountOfAllDocs.Value);
                await tw.WriteLineAsync("CountOfAllTaggedDocs=");
                if (mediaClusterTagsFileNames != null)
                    await tw.WriteLineAsync(mediaClusterTagsFileNames
                        .GetAllFileNamesAndTheirPathsToPrint()); //not setup yet
                tw.Close();
            }
            catch (Exception e)
            {
                ShowMessage.ShowErrorMessageAndExit("Something went wrong while writing the output IONA file\r\n", e);
                Console.WriteLine(e);
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }
        }

        //template: DO NOT ERASE
        //static async Task WriteToFileAsync_TemplateToBeUsedForOtherFiles(string filePath, string text)
        //{
        //    if (string.IsNullOrEmpty(filePath))
        //        throw new ArgumentNullException(nameof(filePath));

        //    if (string.IsNullOrEmpty(text))
        //        throw new ArgumentNullException(nameof(text));

        //    byte[] buffer = Encoding.Unicode.GetBytes(text);
        //    Int32 offset = 0;
        //    Int32 sizeOfBuffer = 4096;
        //    FileStream fileStream = null;
        //    try
        //    {
        //        fileStream = new FileStream(filePath, FileMode.Append, FileAccess.Write,
        //            FileShare.None, bufferSize: sizeOfBuffer, useAsync: true);
        //        await fileStream.WriteAsync(buffer, offset, buffer.Length);
        //    }
        //    catch
        //    {
        //        //Write code here to handle exceptions.
        //    }

        //    finally
        //    {
        //        fileStream?.Dispose();
        //    }
        //}
    }
}
