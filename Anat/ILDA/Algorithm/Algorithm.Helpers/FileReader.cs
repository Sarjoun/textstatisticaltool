﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using SDParser.Primitives;
using SDParser.Structures;

namespace ILDA.Algorithm.Algorithm.Helpers
{
    public static class FileReader
    {
        public static async Task<IEnumerable<string>> ReadFileAsync(string fileName)
        {
            using (var reader = File.OpenText(fileName))
            {
                var fileText = await reader.ReadToEndAsync();
                return fileText.
                    Trim().
                    Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            }
        }
        public static List<WordFrequency> ReadWordFrequencyFile(WordFrequencyFileName wordFrequencyFileName)
        {
            StreamReader file = null;
            var freqWordList = new List<WordFrequency>();
            try
            {
                file = new StreamReader(wordFrequencyFileName.Value);
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    line = line.Trim();
                    var words = line.Split(new[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                    freqWordList.Add(new WordFrequency(words[1], Convert.ToInt32(words[0])));
                }
            }
            finally
            {
                file?.Close();
            }
            return freqWordList;
        }

    }
}
