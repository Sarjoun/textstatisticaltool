﻿using System;
using System.IO;
using System.Linq;
using ILDA.Algorithm.Algorithm.Structs;
using ILDA.Extensions;
using MessageBox = System.Windows.MessageBox;

namespace ILDA.Algorithm.Algorithm.Helpers
{
    public static class MathHelper
    {
        private const double Threshold = 1e-3;
        public static bool Approximately(double a, double b) { return Math.Abs(a - b) < Threshold; }
        public static bool Approximately(double a, double b, double threshold) { return Math.Abs(a - b) < threshold; }
        

        /* This function returns the right-sized matrices-arrays for the different matrices */
        public static double[][] ReturnZeroValueDoubleMatrix(int rows, int columns)
        {
            var matrixRow = Enumerable.
                Range(0, rows).
                Select(i => new double[columns])
                .ToArray();

            for (var i = 0; i < rows; i++)
            {
                matrixRow.AddRowToMatrix(i, columns);
            }

            return matrixRow;
        }

        public static int[][] ReturnZeroValueIntegerMatrix(int rows, int columns)
        {
            //this statement caused an out-of-memory error which according to the debugger is caused by either
            //an array that is not well defined or ran out of internal memory
#pragma warning disable
            var returnMatrix = Enumerable.Range(0, rows).Select(i => new int[columns]).ToArray();

            Array.Clear(returnMatrix, 0, returnMatrix.Length);

            for (var i = 0; i < rows; i++)
            {
                returnMatrix[i] = new int[columns];
                Array.Clear(returnMatrix[i], 0, returnMatrix.Length);
            }

            return returnMatrix;
        }

        public static string[] Return_ZeroVector_TypeString(int size)
        {
            var returnVector = new string[size];
            Array.Clear(returnVector, 0, returnVector.Length);
            return returnVector;
        }
        public static int[] ReturnArrayWithInitialRandomPermutations(
            Random random, int totalNumberOfUniqueWordsEntry)
        {
            int[] order = new int[totalNumberOfUniqueWordsEntry];//assign an array of length n
            Array.Clear(order, 0, order.Length);//initialize the values of the array to 0
            int k;

            /* Assign for the each of the values of order[] their respective index value */
            for (k = 0; k < totalNumberOfUniqueWordsEntry; k++)
                order[k] = k;

            var tempMaxNumber = totalNumberOfUniqueWordsEntry;

            /* This loop is randomly re-assigning the values order[] 
             * to other indices using drand48() */
            for (k = 0; k < totalNumberOfUniqueWordsEntry; k++)
            {
                // take a number between 0 and tempMaxNumber-1
                var takeSingleNumber = (int)(tempMaxNumber * random.NextDouble());
                var temp = order[tempMaxNumber - 1];
                order[tempMaxNumber - 1] = order[takeSingleNumber];
                order[takeSingleNumber] = temp;
                tempMaxNumber--;
            }
            return order;
        }
        

        /* Add the smoothing 1 Dirichlet for a single array */
        public static double[] Add_SmoothDirichlet_To_Array(int T, double[] x, double smooth)
        {
            int t;
            for (t = 0; t < T; t++)
                x[t] += smooth;
            return x;
        }

        /* Sample chain Dirichlet 
   That's the most important function in the program that captures the standard LDA model.        
    N = number of entries in Docfile...represents
    W = number of unique word_array
    T = number of topics
    w = word_array[] (array[] of word_array)
    d = document_array[] (array[] of docs)
    wordTopic_matrix = array[][] of WORDS-TOPICS which is w ~ Distrib(t) 
    documentTopic_matrix = array[][] of DOCUMENTS-TOPICS which is d ~ Distrib(t)
    topic_vector = topics[] array[] ~ Ditrib(t)
    *order = array[] (Gibbs parameter)
*/
        public static GeneralGibbsParamStruct RunGibbsSampleChain(
            int totalWordsCount, int uniqueWordsCount, int numberTopics, int[] wordsArray, int[] documentsArray, int[] z,
            double[][] wordTopicMatrix, double[][] documentTopicMatrix, double[] topicArray, int[] orderArray, Random random)
        {
            var TotalWords = totalWordsCount;
            var UniqueWordsCount = uniqueWordsCount;
            var NumberOfTopics = numberTopics;
            var UniqueWordsArray = wordsArray;
            var DocumentsArray = documentsArray;

            double[][] wordTopic_matrix = wordTopicMatrix;
            double[][] documentTopic_matrix = documentTopicMatrix;
            double[] topics_array = topicArray;
            int[] order = orderArray;

            int wCounter, i, topicID;
            double total_Probability, U, cumulative_Probability;
            double[] prob = new double[NumberOfTopics];
            Array.Clear(prob, 0, prob.Length); //assign to 0 just to be safe
            int wid, did;

            try
            {

                for (wCounter = 0; wCounter < TotalWords; wCounter++)
                {
                    i = order[wCounter];
                    wid = UniqueWordsArray[i];
                    did = DocumentsArray[i];
                    topicID = z[i];

                    //subtract 1
                    topics_array[z[i]]--;
                    wordTopic_matrix[UniqueWordsArray[i]][z[i]]--;
                    documentTopic_matrix[DocumentsArray[i]][z[i]]--;

                    total_Probability = 0;

                    for (topicID = 0; topicID < NumberOfTopics; topicID++)
                    {
                        prob[topicID] = documentTopic_matrix[did][topicID] * wordTopic_matrix[wid][topicID] / topics_array[topicID];
                        total_Probability += prob[topicID];
                    }

                    U = random.NextDouble() * total_Probability;
                    cumulative_Probability = prob[0];
                    topicID = 0;

                    //Add the probabilities to find the best approximation of the integral
                    while (U > cumulative_Probability)
                    {
                        topicID++;
                        cumulative_Probability += prob[topicID];
                    }
                    z[i] = topicID;

                    //add +1
                    wordTopic_matrix[wid][topicID]++;
                    documentTopic_matrix[did][topicID]++;
                    topics_array[topicID]++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($@"Error in Gibbs Sample Chain " + e);
                throw;
            }

            return new GeneralGibbsParamStruct(TotalWords, UniqueWordsCount, NumberOfTopics, UniqueWordsArray, DocumentsArray, z,
                wordTopic_matrix, documentTopic_matrix, topics_array, order);
        }
        public static int[][] ReadSparseMatrixFileAndReturnDoubleIntArray(string fname)
        {
            int[][] readMatrix;
            StreamReader file = null;
            try
            {
                file = new StreamReader(fname);

                //move the file reader 3 lines down
                // ReSharper disable once PossibleNullReferenceException
                var rows = Convert.ToInt32(file.ReadLine().Split(' ')[0]);
                // ReSharper disable once PossibleNullReferenceException
                var columns = Convert.ToInt32(file.ReadLine().Split(' ')[0]);
                file.ReadLine();

                //Check that there is data
                if (rows == 0 || columns == 0)
                {
                    MessageBox.Show(
                        @"Error, program will exit. 
                                      Reason: Assigning matrix with [rows/columns] = [" + rows + @"/" + columns + @"]"
                          + @"File " + fname + @" has wrong values for parameters.");
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }

                string line;
                readMatrix = GetIntegerMatrix(columns, rows);

                while ((line = file.ReadLine()) != null)
                {
                    var words = line.Split(' ');
                    var i = Convert.ToInt32(words[0]);
                    var j = Convert.ToInt32(words[1]);
                    var c = Convert.ToInt32(words[2]);
                    i--;
                    j--;
                    readMatrix[j][i] = c;
                }
            }
            finally
            {
                file?.Close();
            }
            return readMatrix;
        }

        public static int[][] GetIntegerMatrix(int rows, int columns)
        {
            int N = rows * columns;
            int[] tmp = new int[N];
            Array.Clear(tmp, 0, tmp.Length);

            int[][] x = ReturnZeroValueIntegerMatrix(rows, N);

            for (int r = 0; r < rows; r++)
                for (int f = 0; f < N; f++)
                    x[r][f] = 0;// tmp[f] + (topicNumber * r);
            return x;
        }
        public static Exception Exception { get; set; }
    }
}
