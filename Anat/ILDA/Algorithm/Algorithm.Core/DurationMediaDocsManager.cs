﻿using System.Collections.Generic;
using System.IO;
using ILDA.CalendarHelper;
using ILDA.Enums;
using ILDA.MediaHelper;
using SDParser.Enums;
using SDParser.Primitives;


//using SDParser.Primitives;



namespace ILDA.Algorithm.Algorithm.Core
{
    public static class DurationMediaDocsManager
    {
        public static Duration Duration { get; set; }
        public static MediaName MediaName { get; set; }
        public static DocumentsLinksFileName DocumentsLinksFileName { get; set; }
        public static TaggedDocumentsSubCorporaFileName TaggedDocumentsSubCorporaFileName { get; set; }
        public static UnTaggedDocumentsSubCorporaFileName UnTaggedDocumentsSubCorporaFileName { get; set; }


        //public static List<TaggedDoc> CreateAllStoryFilesWithTagInformation(List<string> allTierStoryFiles)
        //{
        //    var counter = 1;
        //    var listTaggedDoc = new List<TaggedDoc>();

        //    foreach (var file in allTierStoryFiles)
        //    {
        //        listTaggedDoc.Add(
        //            new TaggedDoc(
        //                file, 
        //                0, 
        //                counter,
        //                new TagVector(
        //                    -99,
        //                    new List<NGram>(), 
        //                    new List<int>())));
        //        counter++;
        //    }
        //    return listTaggedDoc;
        //}

        public static List<string> CreateAllDocumentsPathsFile(
            MediaNameEnum mediaName, 
            Duration duration,
            DocumentsLinksFileName documentsLinksFileName, 
            MainDatabaseFolderPath mainDatabaseFolderPath)
        {
            var result = MediaStories.GetMediaStoriesForDuration(
                mediaName, 
                duration, 
                mainDatabaseFolderPath);

            File.WriteAllLines(documentsLinksFileName.Value, result);
            return result;
        }


        public static List<string> CopyUntaggedDocumentsPathsFile(
            DocumentsLinksFileName documentsLinksFileName,
            UntaggedDocsFromPreviousTier untaggedDocsFromPreviousTier)
        {
            var result = MediaStories.GetMediaStoriesForDurationFromUntaggedDocuments(untaggedDocsFromPreviousTier);

            File.WriteAllLines(documentsLinksFileName.Value, result);
            return result;
        }
    }
}