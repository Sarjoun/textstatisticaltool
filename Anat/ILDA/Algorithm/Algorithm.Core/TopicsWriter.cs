﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using ILDA.Algorithm.Algorithm.Helpers;
using ILDA.Algorithm.Algorithm.Structs;
using ILDA.Extensions;
using SDParser.Primitives;
using SDParser.Structures;

namespace ILDA.Algorithm.Algorithm.Core
{
    public static class TopicsWriter
    {
        public static List<List<WordFrequency>> Topics { get; set; }
        public static List<WordFrequency> FreqWordList { get; set; }
        public static List<List<WordFrequency>> TopicWordProbList { get; set; }

        public static PrintTopicsStruct PrintTopics(
            TopicsBagOfWordsFileName outputTopicsFileName,
            TopicsBagOfWordsDataFileName outputTopicsDataFileName,
            WordTopicArrayFileName wordTopicsMatrixFileName,
            WordFrequencyFileName freqFile,
            NumberOfInitialTopicsLDA topicsNumber, 
            NumberOfTopWordsToUseFromBoWofTopic numOfWordsToShowForTopic,
            string[] vocabWords)
        {
            TextWriter tw = new StreamWriter(outputTopicsFileName, true);
            TextWriter twD = new StreamWriter(outputTopicsDataFileName, true);

            FreqWordList = FileReader.ReadWordFrequencyFile(freqFile);
            TopicWordProbList = new List<List<WordFrequency>>(); //for the corresp. analysis.

            var wordTopicMatrix = MathHelper.ReadSparseMatrixFileAndReturnDoubleIntArray(wordTopicsMatrixFileName);

            var returnTopics = "";
            var topics = new List<List<WordFrequency>>();
            var topicsWordDist = new List<List<WordFrequency>>();//for corresp an.

            var maxTopicWords = vocabWords.Length < numOfWordsToShowForTopic ? vocabWords.Length : numOfWordsToShowForTopic;

           try
            {
                var probabilityVocabWordsGivenSingleTopic = new int[vocabWords.Length];

                for (var t = 0; t < topicsNumber.Value; t++)
                {
                    var topicList = new List<WordFrequency>();
                    var topicListWordDist = new List<WordFrequency>();

                    for (var w = 0; w < vocabWords.Length; w++)
                        probabilityVocabWordsGivenSingleTopic[w] = wordTopicMatrix[t][w];

                    var orderedIndexTopicWords = probabilityVocabWordsGivenSingleTopic.ReturnIndexValuesOfOrderedContent();

                    var topicHead = "T-" + (t + 1) + ") ";
                    tw.Write(topicHead);
                    returnTopics += topicHead;

                    for (var i = 0; i < maxTopicWords; i++)
                    {
                        var topicWord = vocabWords[orderedIndexTopicWords[i]] + " ";
                        var topicWordProb = wordTopicMatrix[t][orderedIndexTopicWords[i]] + " ";

                        topicList.Add(
                            new WordFrequency(
                                topicWord,
                                FreqWordList.FirstOrDefault(w => w.Word == topicWord.Trim()).Frequency));
                        topicListWordDist.Add(new WordFrequency(topicWord, wordTopicMatrix[t][orderedIndexTopicWords[i]]));

                        if (i < maxTopicWords - 1)
                        {
                            tw.Write(topicWord);
                            twD.Write(topicWordProb);
                            returnTopics += topicWord;
                        }
                        else
                        {
                            tw.WriteLine(topicWord);
                            twD.WriteLine(topicWordProb);
                            returnTopics += topicWord + "\r\n";
                        }
                    }
                    topics.Add(topicList);
                    topicsWordDist.Add(topicListWordDist);
                }
                tw.Close(); twD.Close();
            }
            finally
            {
                tw.Close();
                twD.Close();
            }

            Topics = topics;
            TopicWordProbList = topicsWordDist;
            return new PrintTopicsStruct(returnTopics, topics);
        }
    }
}
