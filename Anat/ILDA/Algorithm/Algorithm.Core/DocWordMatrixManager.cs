﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SDParser.Primitives;
using SDParser.Structures;

namespace ILDA.Algorithm.Algorithm.Core
{
    public static class DocWordMatrixManager
    {
        private static Dictionary<string, int> _vocabulary;
        private static Dictionary<string, StemWord> _variantsVocabulary;
        public static CountOfAllWordsInDocs CountOfAllWordsInDocs;
        public static CountOfAllDocs CountOfAllDocs;
        public static DocWordCountStruct DocWordCountStruct;

        private static string GetRootWordFromVariantsVocabulary(string word)
        {
            foreach (var tuple in _variantsVocabulary)
            {
                if (tuple.Value.DoesWordExistInVariants(word))
                    return tuple.Key;
            }
            throw new Exception("The word " + word + " was not found in the Vocabulary! Something went terribly wrong!");
        }
        private static bool CheckIfWordIsInVariantsVocabulary(string word)
        {
            return _variantsVocabulary.Any(tuple => tuple.Value.DoesWordExistInVariants(word));
        }

        public static DocWordCountStruct MakeDocWordFileReturnDocWordCounts(
            Dictionary<string, int> vocabulary,
            Dictionary<string, StemWord> variantsVocabulary,
            DocWordFileName docWordFileName,
            WordStreamFileName wordStreamFileName,
            MediaFolderPath workingfolder,
            MediaName medianame)
        {
            _vocabulary = vocabulary;
            _variantsVocabulary = variantsVocabulary;
            var fileReader = new StreamReader(wordStreamFileName.Value);
            var fileWriter = new StreamWriter(docWordFileName.Value.Replace(".txt", "_temp.txt"));
            var documentCounterId = 0;
            string line;
            var totalNumberOfVocabularyWordsInAllDocs = 0;

            try
            {
                while ((line = fileReader.ReadLine()) != null)
                {
                    var lines = new List<string>();
                    if (!line.Contains("##SOD##")) continue;

                    line = fileReader.ReadLine();
                    while (line != "##EOD##")
                    {
                        lines.Add(line);
                        line = fileReader.ReadLine();
                    }
                    documentCounterId++;

                    totalNumberOfVocabularyWordsInAllDocs += ProcessNewsStory(
                        lines.ToArray(), documentCounterId, fileWriter);
                }
            }
            finally
            {
                fileReader.Close();
                fileWriter.Close();
            }

            //create the file using the temp file to insert additional information at the beginning of the file.
            try
            {
                fileReader = new StreamReader(docWordFileName.Value.Replace(".txt", "_temp.txt"));
                fileWriter = new StreamWriter(docWordFileName.Value);

                fileWriter.WriteLine(documentCounterId);
                fileWriter.WriteLine(vocabulary.Count);
                fileWriter.WriteLine(totalNumberOfVocabularyWordsInAllDocs); //fileWriter.WriteLine(totalNumberOfAllWordsInAllDocs);

                while ((line = fileReader.ReadLine()) != null)
                {
                    fileWriter.WriteLine(line);
                }
            }
            finally
            {
                fileReader.Close();
                fileWriter.Close();
                File.Delete(docWordFileName.Value.Replace(".txt", "_temp.txt")); //delete tempFile
            }
            CountOfAllWordsInDocs = CountOfAllWordsInDocs.FromValue(totalNumberOfVocabularyWordsInAllDocs);
            CountOfAllDocs = CountOfAllDocs.FromValue(documentCounterId);

            return DocWordCountStruct = new DocWordCountStruct(CountOfAllWordsInDocs, CountOfAllDocs);
        }


        private static int ProcessNewsStory(IEnumerable<string> lines,
            int docId, TextWriter stream)
        {
            var docwordsDictionary = new Dictionary<string, int>();
            var resList = lines.Select(x => x.Trim()).ToList();

            var wordsInDocumentCounter = 0;

            // TODO needs to redone in the view of using stem words in the WordStream file.
            foreach (var readword in resList)
            {
                if (!CheckIfWordIsInVariantsVocabulary(readword))
                {
                    continue;
                }
                var word = GetRootWordFromVariantsVocabulary(readword);

                if (docwordsDictionary.ContainsKey(word))
                    docwordsDictionary[word] = docwordsDictionary[word] + 1;
                else
                    docwordsDictionary.Add(word, 1);
                wordsInDocumentCounter++;
            }
            foreach (var pair in docwordsDictionary)
            {
                try
                {
                    stream.WriteLine(docId + " " + _vocabulary[pair.Key] + " " + pair.Value);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
            return wordsInDocumentCounter;
        }
    }
}