﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Iveonik.Stemmers;
using SDParser.Enums;
using SDParser.Extensions;
using SDParser.Primitives;
using SDParser.Structures;
using SDParser.TextProcessingCentral;

namespace ILDA.Algorithm.Algorithm.Core
{
    public static class WordStreamManager
    {
        public static Dictionary<string, StemWord> UniqueWordsAndTheirVariants { get; set; }
        public static List<List<string>> WordStreamList { get; set; }

        public static async Task<Dictionary<string, StemWord>> MakeWordStreamAndVariantWordsAsync( /* To be used later.. */
            List<string> stopWords,
            DocumentsLinksFileName documentsLinksFileName,
            WordStreamFileName wordStreamFileName,
            IncludeTitleInSetup includeTitleInSetup,
            CreateCoOccurrenceMatrix createCoOccurenceMatrix)
        {
            return await Task.FromResult(
                MakeWordStreamAndVariantWordsReturnDictionaryOrderedAlphabetically(
                    stopWords,
                    documentsLinksFileName,
                    wordStreamFileName,
                    //wordVariantsFileName,
                    includeTitleInSetup,
                    createCoOccurenceMatrix));
        }

        public static Dictionary<string, StemWord> MakeWordStreamAndVariantWordsReturnDictionaryOrderedAlphabetically(
            List<string> stopWords,
            DocumentsLinksFileName documentsLinksFileName,
            WordStreamFileName wordStreamFileName,
            IncludeTitleInSetup includeTitleInSetup,
            CreateCoOccurrenceMatrix createCoOccurenceMatrix/*not being used for now*/)
        {
            SentenceProcessor.Stemmer = new EnglishStemmer();
            SentenceProcessor.StopWords = stopWords;
            SentenceProcessor.NewWords = new List<string>();
            SentenceProcessor.UniqueWordsAndTheirVariants = new Dictionary<string, StemWord>();
            SentenceProcessor.WordStreamWordTypeEnum = WordStreamWordTypeEnum.StemmedWord;

            if (createCoOccurenceMatrix.Value)
                WordStreamList = new List<List<string>>();

            StreamReader file = null;
            TextWriter fileWriter = new StreamWriter(wordStreamFileName.Value);
            TextWriter fileWriter2 = new StreamWriter(wordStreamFileName.Value.Replace(".txt","_StemmedWords.txt"));
            try
            {
                file = new StreamReader(documentsLinksFileName);
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    var newWords = StoryProcessor.MakeWordStreamFromSingleStory(
                        line.RemoveDocIdFromStartOfPathIfItExists(),
                        includeTitleInSetup);

                    var stemmedNewWords = StoryProcessor.SpecialMakeWordStreamFromSingleStory(
                        line.RemoveDocIdFromStartOfPathIfItExists(),
                        includeTitleInSetup);
                    
                    if (createCoOccurenceMatrix.Value)
                        WordStreamList.Add(newWords);

                    if (newWords.Count <= 0) continue;

                    fileWriter.WriteLine("##SOD##" + line);
                    foreach (var word in newWords)
                        fileWriter.WriteLine(word);
                    fileWriter.WriteLine("##EOD##");
                    
                    fileWriter2.WriteLine(stemmedNewWords.Aggregate("", (current, word) => current + (word + "\t")).Trim());
                }
            }
            finally
            {
                file?.Close();
                fileWriter.Close();
                fileWriter2.Close();
            }
            return UniqueWordsAndTheirVariants = SentenceProcessor.UpdatedUniqueWordsAndTheirVariants();
        }
    }
}
