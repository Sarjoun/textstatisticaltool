using System;
using System.IO;
using ILDA.Algorithm.Algorithm.Structs;
using SDParser.Primitives;

namespace ILDA.Algorithm.Algorithm.Core
{
    public static class WordDocStructManager
    {
        /*
         * Reads the number of document_array and word_array found in the file (first 2 entries)
         * and then populates the vector arrays w(word) and d(document) with their
         * corresponding word's index and document's index. In other words,
         * it creates 2 pointer arrays for wordsArray and documentsArray such that 
         * all the elements of wordsArray in word[] in the same index order correspond 
         * to the document[] but with -1 difference. i.e. 
         * 
         *   w[] = [389, 520, ...]
         *     d[] = [  0,   0, ...]  while in the file it is 1-390, 1-520, etc...  
         */

        /// <summary>
        /// This function creates 2 arrays that are synchronous. 
        /// The element at w[i] represents the the word id 'i' that exists 
        /// in document d[i] at the same index 'i'
        /// </summary>
        /// <param name="docWordFileName"></param>
        /// <param name="arraySize"></param>
        /// <returns></returns>
        public static WordDocFileStruct CreateWordDocStructFromDocWordFile(
            DocWordFileName docWordFileName,
            CountOfAllWordsInDocs arraySize)
        {
            int documentCount, wordCount, totalEntriesInFile;
            var wordsArray = new int[arraySize];
            var documentsArray = new int[arraySize];
            var topicsArray = new int[arraySize];

            StreamReader file = null;
            try
            {
                file = new StreamReader(docWordFileName.Value);
                var count = 0;

                documentCount = Convert.ToInt32(file.ReadLine());
                wordCount = Convert.ToInt32(file.ReadLine());
                totalEntriesInFile = Convert.ToInt32(file.ReadLine());

                string line;
                while ((line = file.ReadLine()) != null)
                {
                    var words = line.Split(' ');
                    var documentValue = Convert.ToInt32(words[0]);
                    var wordValue = Convert.ToInt32(words[1]);
                    var countValue = Convert.ToInt32(words[2]);

                    int i;
                    for (i = count; i < count + countValue; i++)
                    {
                        wordsArray[i] = wordValue - 1;
                        documentsArray[i] = documentValue - 1;
                    }
                    count += countValue;
                }
                file.Close();
            }

            finally
            {
                file?.Close();
            }
            return new WordDocFileStruct(
                wordsArray,
                documentsArray,
                topicsArray,
                wordCount,
                documentCount,
                totalEntriesInFile);
        }

    }
}
