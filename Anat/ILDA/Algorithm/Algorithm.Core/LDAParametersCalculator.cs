﻿using SDParser.Primitives;

namespace ILDA.Algorithm.Algorithm.Core
{
    public static class LDAParametersCalculator
    {
        public static Alpha CalculateAlpha(
            NumberOfInitialTopicsLDA numberOfInitialTopics,
            int totalWordsCount, int documentsCount)
        {
            return Alpha.FromValue(0.05 * totalWordsCount /
                                   (documentsCount * numberOfInitialTopics.Value));
        }

        public static Beta CalculateBeta()
        {
            return Beta.FromValue(0.01);
        }

    }
}
