﻿using System;
using ILDA.Algorithm.Algorithm.Helpers;
using ILDA.Algorithm.Algorithm.Structs;
using SDParser.Primitives;

namespace ILDA.Algorithm.Algorithm.Core
{
    public static class TopicsInitializer
    {
        private static int _numberOfInitialTopicsLDA;

        public static Random Randomizer;

        public static int[] WordArray;
        public static int[] DocumentArray;
        public static int[] TopicArray;
        public static int[] OrderingArray;

        public static double[][] WordTopicMatrix;
        public static double[][] DocumentTopicMatrix;
        public static double[] TopicVector;


        public static GeneralGibbsParamStruct SetupInitialGibbsDataStructure(
            WordDocFileStruct wordDocStruct, 
            int numberOfInitialTopicsLDA,
            RandomSeed randomSeed,
            VocabularySize vocabularySize,
            CountOfAllWordsInDocs countOfAllWordsInDocs,
            CountOfAllDocs documentsCount)
        {
            _numberOfInitialTopicsLDA = numberOfInitialTopicsLDA;
            WordArray = wordDocStruct.AllWordsInstancesArray;
            DocumentArray = wordDocStruct.AllWordsInstancesDocumentsArray;
            TopicArray = wordDocStruct.AllWordsInstancesTopicsArray;
            

            WordTopicMatrix = MathHelper.ReturnZeroValueDoubleMatrix(
                vocabularySize.Value,
                numberOfInitialTopicsLDA);

            DocumentTopicMatrix = MathHelper.ReturnZeroValueDoubleMatrix(
                documentsCount.Value,
                numberOfInitialTopicsLDA);

            TopicVector = new double[numberOfInitialTopicsLDA];

            Randomizer = new Random(randomSeed.Value);

            for (var totalWordIndex = 0; totalWordIndex < countOfAllWordsInDocs.Value; totalWordIndex++)
            {
                var topicId = GetNextRandomTopicValue();
                try
                {
                    WordTopicMatrix[WordArray[totalWordIndex]][topicId]++;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

                try
                {
                    DocumentTopicMatrix[DocumentArray[totalWordIndex]][topicId]++;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

                try
                {
                    TopicArray[totalWordIndex] = topicId; //assigning the latent variable
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

                TopicVector[topicId]++;
            }

            OrderingArray = MathHelper.ReturnArrayWithInitialRandomPermutations(
                Randomizer,
                countOfAllWordsInDocs);

            return new GeneralGibbsParamStruct(
                countOfAllWordsInDocs,
                vocabularySize,
                numberOfInitialTopicsLDA,
                WordArray,
                DocumentArray,
                TopicArray,
                WordTopicMatrix,
                DocumentTopicMatrix,
                TopicVector,
                OrderingArray);
        }

        public static int GetNextRandomTopicValue()
        {
            return (int) (_numberOfInitialTopicsLDA * Randomizer.NextDouble());
        }
    }
}
