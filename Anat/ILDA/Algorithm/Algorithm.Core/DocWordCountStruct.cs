﻿using SDParser.Primitives;

namespace ILDA.Algorithm.Algorithm.Core
{
    public struct DocWordCountStruct
    {
        public CountOfAllWordsInDocs CountOfAllWordsInDocs;
        public CountOfAllDocs CountOfAllDocs;

        public DocWordCountStruct(CountOfAllWordsInDocs countOfAllWordsInDocs,
            CountOfAllDocs countOfAllDocs)
        {
            CountOfAllWordsInDocs = countOfAllWordsInDocs;
            CountOfAllDocs = countOfAllDocs;
        }
    }
}

