using SDParser.Primitives;

namespace ILDA.Algorithm.Algorithm.Structs
{
    public struct WordDocFileStruct
    {
        //TotalWordCountInAllDocs
        public int[] AllWordsInstancesArray { get; set; }
        public int[] AllWordsInstancesDocumentsArray { get; set; }
        public int[] AllWordsInstancesTopicsArray { get; set; } //a simple allocation of the array size
        public VocabularySize VocabularySize { get; set; }//instead of 'out' in function
        public CountOfAllDocs CountOfAllDocs { get; set; }//instead of 'out' in function
        public int TotalEntriesInFile { get; set; } //total number of the threesome entries in the file

        public WordDocFileStruct(int[] w, int[] d, int[] allWordsInstancesTopicsArray, 
            int vocabularySize, int countOfAllDocs, int totalEntriesInFile)
            : this()
        {
            AllWordsInstancesArray = w;
            AllWordsInstancesDocumentsArray = d;
            AllWordsInstancesTopicsArray = allWordsInstancesTopicsArray;
            VocabularySize = VocabularySize.FromValue(vocabularySize);
            CountOfAllDocs = CountOfAllDocs.FromValue(countOfAllDocs);
            TotalEntriesInFile = totalEntriesInFile;//should be equal to the total word counts
        }
    }
}
