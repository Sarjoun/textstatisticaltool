namespace ILDA.Algorithm.Algorithm.Structs
{
    public struct GeneralGibbsParamStruct
    {
        public int TotalWords { get; set; }  //TotalWords is N
        public int UniqueWordsCount { get; set; }
        public int TopicCount { get; set; }
        public int[] WordArray { get; set; }
        public int[] DocumentArray { get; set; }
        public int[] TopicArray { get; set; }
        public double[][] WordTopicMatrix { get; set; }
        public double[][] DocumentTopicMatrix { get; set; }
        public double[] TopicDistributionArray { get; set; }
        public int[] OrderPattern { get; set; }

        public GeneralGibbsParamStruct(
            int totalWords, 
            int uniqueWordsCount, 
            int topicCount,
            int[] wordArray, 
            int[] documentArray, 
            int[] topicArray,
            double[][] wordTopicMatrix, 
            double[][] documentTopicMatrix, 
            double[] topicDistributionArray, 
            int[] orderPattern) : this()
        {
            TotalWords = totalWords;
            UniqueWordsCount = uniqueWordsCount;
            TopicCount = topicCount;
            WordArray = wordArray;
            DocumentArray = documentArray;
            TopicArray = topicArray;
            WordTopicMatrix = wordTopicMatrix;
            DocumentTopicMatrix = documentTopicMatrix;
            TopicDistributionArray = topicDistributionArray;
            OrderPattern = orderPattern;
        }
    }
}
