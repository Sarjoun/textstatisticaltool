using System.Collections.Generic;
using SDParser.Structures;

namespace ILDA.Algorithm.Algorithm.Structs
{
    public struct PrintTopicsStruct
    {
        public string Text { get; set; }
        public List<List<WordFrequency>> Topics { get; set; }
        public double Perplexity { get; set; }//Debate wether this is for all the topics or 
                                              //we can get one for each of the topics.
        public PrintTopicsStruct(string text, List<List<WordFrequency>> list)
            : this()
        {
            Text = text;
            Topics = list;
        }
    }
}
