﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ILDA.Enums;
using ILDA.Extensions;
using ILDA.Properties;
using LeagueOfMonads;
using SDParser.ComplexTypes;
using SDParser.Enums;
using SDParser.Primitives;

namespace ILDA.Algorithm
{
    public static class FolderManager
    {
        public static StopWordsFilePath ConfirmThatStopWordsFileExistsInExperimentFolder(
            SimulationDateFolderPath simulationDateFolderPath)
        {
            var stopWordsFile = StopWordsFilePath.FromValue(simulationDateFolderPath.Value + @"\StopWords2.txt");
            try
            {
                File.WriteAllText(
                    stopWordsFile.Value,
                    SDParser.Properties.Resources.StopWords2);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return stopWordsFile;
        }

        public static bool CreateOrOverwriteLDASampleSettingsFileOnDesktop()
        {
            var ldaSettingsFile =
                StopWordsFilePath.FromValue(GetDesktopFolderPath().Value + @"\LDARunSettings.txt");

            try
            {
                File.WriteAllText(
                    ldaSettingsFile.Value,
                    SDParser.Properties.Resources.LDARunSettings);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
                //throw;
            }
            return true;
        }

        public static MediaSettings CreateSimulationAndInitialMediaCommonFolders(
            ExperimentFolderPath experimentFolderPath,
            Tier tier,
            MediaName mediaName,
                StudyNumber studyNumber)
        //Option<StudyNumber> existingStudyNumber)
        {
            //var studyNumber = $@"{DateTime.Now:HH.mm [dd-MM-yy]}";

            //if (!string.IsNullOrEmpty(existingStudyNumber?.Value))
            //    studyNumber = existingStudyNumber.Value;

            var simulationDateFolderPath = SimulationDateFolderPath.FromValue(
                experimentFolderPath.Value + @"\" + studyNumber + @"\");
            simulationDateFolderPath.Value.CheckIfFolderExistsAndCreateIfNotFound();

            var mediaFolderPath = MediaFolderPath.FromValue(
                simulationDateFolderPath.Value + @"\" + mediaName + @"\");
            mediaFolderPath.Value.CheckIfFolderExistsAndCreateIfNotFound();

            var currentCorpusTierFolderPath = CurrentCorpusTierFolderPath.FromValue(
               mediaFolderPath + @"\corpusTier" + tier.Value + @"\");
            currentCorpusTierFolderPath.Value.CheckIfFolderExistsAndCreateIfNotFound();

            return new MediaSettings(experimentFolderPath,
               simulationDateFolderPath, mediaName, mediaFolderPath,
                tier, currentCorpusTierFolderPath);
        }

        public static List<string> CreateBasicOutputFolders(RootFolderPath rootFolderPath)
        {
            var simulationBasicFolders = MediaNameEnumUtilities.GetNames<BasicOutputFolders>();

            return simulationBasicFolders.
               Select(basicFolderEnum
                           => GenerateFolderName(rootFolderPath, "TopicModels", basicFolderEnum)
                              .CheckIfFolderExistsAndCreateIfNotFound()
                     ).ToList();
        }
        
        public static string GenerateFolderName(
          RootFolderPath rootFolderPath,
          string extraName,
          string folderName)
        {
            if (string.IsNullOrEmpty(extraName))
                return rootFolderPath.Value + @"\" + folderName + @"\";
            return rootFolderPath.Value + @"\" + extraName + @"\" + folderName + @"\";
        }

        public const string MainFolderName = @"\anat\";

        public static ResourcesFolderPath GetSolutionResourcesFolder()
        {
            var mainFolderNameSize = MainFolderName.Length;
            var graphVizBinFolderPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var location = graphVizBinFolderPath.
                ToLower().
                IndexOf(MainFolderName, StringComparison.Ordinal) + mainFolderNameSize;

            return ResourcesFolderPath.
                FromValue(
                graphVizBinFolderPath.Substring(0, location) +
                @"\Anat\Resources\");
        }

        public static RootFolderPath GetRootFolderPath()
        {
            return RootFolderPath.FromValue(
                AppDomain.CurrentDomain.BaseDirectory.Substring(0, 3));
        }
        public static DesktopFolderPath GetDesktopFolderPath()
        {
            return DesktopFolderPath.FromValue(
                Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\");
        }
        public static MainDatabaseFolderPath GetMainDatabaseFolderPath(
            DatabaseRootFolderPath databaseRootFolderPath)
        {
            return MainDatabaseFolderPath.FromValue(
                databaseRootFolderPath + @"\");
        }

        public static ExperimentFolderPath GetExperimentFolderPathFromOutputFolders(List<string> basicOutputFolders)
        {
            /*The first entry basicOutputFolders is the main one, use that for now to create the type
              I can later make it so that a switch statement can make the determination*/
            return ExperimentFolderPath.FromValue(basicOutputFolders.FirstOrDefault());
        }

    }
}



//private void UpdateRootFolderDirectories()
//{
//Globals.MainFolder = Globals.RootFolder + @":\TopicModels\";
//Globals.ExperimentFolder = Globals.RootFolder + @":\TopicModels\Experiments\";
//Globals.StopWordsFile = Globals.RootFolder + @":\TopicModels\stopwords.txt";
//Globals.ExperimentCustomFolder = Globals.RootFolder + @":\TopicModels\ExperimentsCustom\";
//Globals.CustomDBFolderSources = Globals.RootFolder + @":\TopicModels\indexes_Custom\Sources\";
//}

